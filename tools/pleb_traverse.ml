open Plebeia
open Lwt.Syntax
open Utils

module IS = Set.Make(struct
    type t = Index.t
    let compare = compare
  end)

(*
   Suppose we have 2 root hashes, rh1, rh2, where rh2's parent is rh1.
   Let top1 and top2 are the top nodes of rh1 and rh2 respectively.

   A node n which is reachable from top2 is:

   * If the index of n is newer than the index of top1, n is unreachable
     from top1.

   * If the index of n is older than the index of top1,

       * If n is a small leaf which is cached, n may be unreachable from top1.
       * Otherwise, n is always reachable from top1.

   Using the above property, we can reduce the memory usage.
*)

let find_new_nodes vc past_nodes rh =
  let* entry, _ = Lwt.map Option.get @@ Vc.checkout vc rh in
  let parent_i =
    match Commit_db.parent (Vc.commit_db vc) entry with
    | Ok (Some p) -> p.index
    | Ok None | Error _ -> Index.zero
  in

  let* _, c = Lwt.map Option.get @@ Vc.checkout vc rh in

  Cursor.fold ~init:IS.empty c (fun new_nodes c ->
      let _, v = Cursor.view c in
      let i = Option.get @@ Node.index_of_view v in
      if i > parent_i then begin
        (* This is a new node, which cannot appear in past_nodes *)
        assert (not @@ IS.mem i past_nodes);
        if IS.mem i new_nodes then begin
          (* quite surprising to have shared node in one commit *)
          Format.eprintf "Shared node found in one commit! %a@." Index.pp i
        end;
        let new_nodes = IS.add i new_nodes in
        `Continue, new_nodes
      end else begin
        assert (IS.mem i past_nodes);
        `Continue, new_nodes
      end) |> Lwt.return


let get_non_small_nodes c =
  Cursor.fold ~init:IS.empty c (fun new_nodes c ->
      let _, v = Cursor.view c in
      let i = Option.get @@ Node.index_of_view v in
      let new_nodes = IS.add i new_nodes in
      `Continue, new_nodes)

let () = Lwt_main.run @@
  let path1 = Sys.argv.(1) in
  let* vc1 = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path1 in
  (* let path2 = Sys.argv.(2) in *)
  (* let _vc2 = Vc.create path2 in *)
  let commit_db = Vc.commit_db vc1 in

  let cells = Storage.get_current_length (Vc.context vc1).Context.storage in

  let t1 = Unix.gettimeofday () in

  (* past node table *)
  let past_nodes_tbl = Hashtbl.create 0 in

  let report i =
    let t2 = Unix.gettimeofday () in
    let ratio = Index.(Stdint.Uint32.to_float (to_uint32 i)
                       /. Stdint.Uint32.to_float (to_uint32 cells))
    in
    Format.eprintf "Report: index %a, %.02f, %.0f, %.0f@."
      Index.pp i
      (ratio *. 100.0)
      (t2 -. t1)
      ((t2 -. t1) /. ratio)
  in

  let f entry ~children () =
    let (past_nodes, n, threshold) =
      match Commit_db.parent commit_db entry with
      | Ok None | Error `Not_found -> (IS.empty, 0, 10000)
      | Ok (Some p) ->
          let parent_i = p.index in
          let (refc, past_nodes, n, threshold) = Hashtbl.find past_nodes_tbl parent_i in
          if refc <= 1 then begin
            Hashtbl.remove past_nodes_tbl parent_i;
            Format.eprintf "Forgot %a from past_node_tbl. %d entries in the table@."
              Index.pp parent_i
              (Hashtbl.length past_nodes_tbl);
          end else Hashtbl.replace past_nodes_tbl parent_i (refc-1, past_nodes, n, threshold);
          (past_nodes, n, threshold)
    in
    let* new_nodes = find_new_nodes vc1 past_nodes entry.hash in
    let new_n = IS.cardinal new_nodes in
    Format.eprintf "%s: %d new nodes@." (Commit_hash.to_hex_string entry.hash) new_n;
    let n = new_n + n in
    let* past_nodes, n, threshold =
      (* if nchildren is 0, no point to calculate *)
      if n > threshold then
        (* rescan the tree and refresh *)
        let* _, c = Lwt.map Option.get @@ Vc.checkout vc1 entry.hash in
        let past_nodes = get_non_small_nodes c in
        let n = IS.cardinal past_nodes in
        report entry.index;
        Lwt.return (past_nodes, n, n * 8)
      else
        Lwt.return (IS.union past_nodes new_nodes, n, threshold)
    in
    let nchildren = List.length children in
    if nchildren > 0 then begin
      Format.eprintf "Adding idx: %a  #n: %d  threshold #n: %d@." Index.pp entry.index n threshold;
      Hashtbl.replace past_nodes_tbl entry.index (nchildren, past_nodes, n, threshold)
    end;
    Lwt.return_unit
  in

  let* () = Commit_db.ordered_fold f commit_db () in
  let t2 = Unix.gettimeofday () in
  Format.eprintf "Traversed %a cells in %f secs@." Index.pp cells (t2 -. t1);
  Lwt.return_unit
