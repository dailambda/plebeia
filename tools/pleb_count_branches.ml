(*

   Scan the root hashes and prints out branches.

   $ dune exec ./pleb_count_branches.exe plebeia.context
*)
open Plebeia
open Utils
open Lwt.Syntax

let () = Lwt_main.run @@
  let path = Sys.argv.(1) in
  let* vc = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path in
  let* children = Commit_db.children (Vc.commit_db vc) in
  let* res = Commit_db.geneses (Vc.commit_db vc) in
  match res with
  | [] ->
      Format.eprintf "Roots has no genesis hashes@.";
      assert false
  | (_::_::_ as hs) ->
      Format.eprintf "Roots have %d genesis hashes@." (List.length hs);
      assert false
  | [genesis] ->
      let rec loop = function
        | [] -> ()
        | e::es ->
            let es' = children e.Commit.hash in
            begin match es' with
              | [] -> ()
              | [_] -> ()
              | children  ->
                  Format.eprintf "%d children: %a@."
                    (List.length children)
                    Commit.pp e
            end;
            loop (es @ es')
      in
      loop [genesis];
      Lwt.return_unit
