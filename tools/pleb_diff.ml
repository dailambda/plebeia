(* input 2 trees

   output diff of buds and leaves, checked their correctness
   by applying them to the original.

   Checked diffs and applied them succcessfully in 63.9 mins for 22G data
   in Jun's machine kurama.
*)

open Plebeia
open Utils
open Lwt.Syntax
open Node

let () = Lwt_main.run @@
  let path = Sys.argv.(1) in
  let* vc = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path in
  let context = Vc.context vc in
  let commit_db = Vc.commit_db vc in
  let* roots = Commit_db.to_list commit_db in
  let nroots = List.length roots in
  let processed = ref 0 in
  let t0 = Unix.gettimeofday () in

  Commit_db.ordered_fold (fun entry ~children:_ () ->
      incr processed;
      let n1 =
        match Commit_db.parent commit_db entry with
        | Ok None | Error `Not_found -> View (_Bud (None, None, None))
        | Ok (Some p) ->
            let v = Node_storage.read_node context p.index in
            View v
      in
      let* _, Cursor (_, n2, _) = Lwt.map Option.get @@ Vc.checkout vc entry.hash in

      let diffs = Diff.Internal.diff_with_check context n1 n2 in
      Format.eprintf "checking index %a@." Index.pp entry.index;
      Format.eprintf "%a: %d diffs@." Commit_hash.pp entry.hash (List.length diffs);

      let t = Unix.gettimeofday () in
      Format.eprintf "ETC: %.1f mins (Passed %.1f mins)@."
        ((t -. t0) /. float !processed *. float (nroots - !processed) /. 60.0)
        ((t -. t0) /. 60.0);
      Lwt.return_unit) commit_db ()
