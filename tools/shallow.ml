open Plebeia
open Lwt.Syntax
open Utils

type t = (Segment.segment * ptr) list
and ptr =
  | Plebeia_leaf of Index.t
  | Tribune_node of t * Index.t option
  (* | Disk of Index.t *)

let size_of_entry (seg, _n) =
  (Segment.length seg + 7) / 8 + 1 (* sides *)
  + 1 (* tag *) + 4 (* pointer *)
  + 1 (* murphy factor *)

let size_of_entry' (sides, _n) =
  (List.length sides + 7) / 8 + 1 (* sides *)
  + 1 (* tag *) + 4 (* pointer *)
  + 1 (* murphy factor *)

(* size of [t] on disk, excluding its sub-nodes *)
let size_in_bytes t =
  List.fold_left (fun sum ent -> sum + size_of_entry ent)
    1 (* version *)
    t

let maximum_size_in_bytes = 4096

let rec get_branch context rev_sides n =
  (* find the first branch or a leaf *)
  let v = Node.view context n in
  match v with
  | Node.Leaf (_, Some i, _)
  | Bud (_, Some i, _) -> (* and buds too *)
      [rev_sides, `Leaf i], []
  | Leaf (_, None, _) | Bud (_, None, _) ->
      (* [n] must be already saved *) assert false
  | Internal (nl, nr, _, _) ->
      [],
      [Segment.Left::rev_sides, `Node nl;
       Segment.Right::rev_sides, `Node nr]
  | Extender (seg, n, _, _) ->
      get_branch context (List.rev_append (Segment.to_sides seg) rev_sides) n

let rec convert depth context n =
  let rec loop current_size not_expandables expanded expandables =
    match expanded, expandables with
    | [], [] -> not_expandables
    | _, [] ->
        (* run again *)
        loop current_size not_expandables [] expanded
    | _, (_, `Leaf _ as e)::es ->
        loop current_size (e::not_expandables) expanded es
    | _, (rev_sides, `Node n as e)::es ->
        let new_not_expandables, new_expanded = get_branch context rev_sides n in
        let current_size' =
          current_size - size_of_entry' e
          + List.fold_left (fun sum e -> sum + size_of_entry' e)
            0 (new_not_expandables @ new_expanded)
        in
        if current_size' > maximum_size_in_bytes then
          (* this expansion is not possible *)
          loop current_size (e::not_expandables) expanded es
        else
          loop current_size' (new_not_expandables @ not_expandables) (new_expanded @ expanded) es
  in
  let xs =
    match Node.view context n with
    | Node.Leaf (_, Some i, _) | Bud (_, Some i, _) ->
        [[], `Leaf i]
    | Node.Leaf _ | Bud _ -> assert false
    | _ ->
        let e = ([], `Node n) in
        loop (size_of_entry' e + 1 (* verison *)) [] [] [e]
  in
  let xs =
    List.sort (fun (sides1, _x1) (sides2, _x2) -> compare sides1 sides2)
    @@ List.map (fun (rev_sides, x) -> List.rev rev_sides, x) xs
  in
  Format.eprintf "depth: %d xs: %d leaf: %d node: %d bs: %d@."
    depth
    (List.length xs)
    (List.length (List.filter (function (_,`Leaf _) -> true | _ -> false) xs))
    (List.length (List.filter (function (_,`Leaf _) -> false | _ -> true) xs))
    (List.fold_left (fun sum x -> sum + size_of_entry' x) 1 xs);
  List.map (fun (sides, x) ->
      let seg = Segment.of_sides sides in
      match x with
      | `Leaf i ->
          seg, Plebeia_leaf i
      | `Node n ->
          let n = convert (depth+1) context n in
          seg, Tribune_node (n, None)) xs

let rec traverse f acc t =
  let acc' = f acc t in
  List.fold_left (fun acc -> function
      | _, Plebeia_leaf _ -> acc
      | _, Tribune_node (t, _) -> traverse f acc t
      (* | _ -> acc *)) acc' t

let _is_balanced t = size_in_bytes t * 2 < maximum_size_in_bytes

let () = Lwt_main.run @@
  let path = Sys.argv.(1) in
  let* vc = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path in
  let commit_db = Vc.commit_db vc in

  let last_root =
    match Commit_db.read_the_latest commit_db with
    | Some root -> root
    | None -> assert false
  in

  Format.eprintf "last root index= %a@." Index.pp last_root.Commit.index;

  let* _, Cursor.Cursor (_, n, ctxt) = Lwt.map Option.get @@ Vc.checkout vc last_root.Commit.hash in
  prerr_endline "converting...";
  let n = match Node.view ctxt n with
    | Bud (Some n, _, _) -> n
    | Bud (None, _, _) -> assert false
    | _ -> assert false
  in
  let t = convert 1 ctxt n in
  prerr_endline "converted!";
  let f (ts, used) t =
    ts + 1,
    used + List.fold_left (fun sum e -> sum + size_of_entry e) 0 t
  in
  let ts, used = traverse f (0,0) t in
  Format.eprintf "ts %d used %d ratio %f@."
    ts used (float used /. float ts /. float maximum_size_in_bytes);
  Lwt.return_unit
