open Plebeia
open Utils
open Lwt.Syntax

let incr_count tbl value =
  match Hashtbl.find_opt tbl value with
  | None ->
      Hashtbl.add tbl value 1
  | Some count ->
      Hashtbl.replace tbl value (count + 1)

let count_new_nodes vc rh num_of_shared_values num_of_replesentative_values =
  let* entry, _ = Lwt.map Option.get @@ Vc.checkout vc rh in
  let parent_i =
    match Commit_db.parent (Vc.commit_db vc) entry with
    | Ok None | Error _ -> Index.zero
    | Ok (Some p) -> p.index
  in
  let* _, c = Lwt.map Option.get @@ Vc.checkout vc rh in

  let is_new_value index = index >= parent_i in

  Cursor.fold ~init:() c (fun () c ->
      let _, v = Cursor.view c in
      let i = Option.get @@ Node.index_of_view v in
      match v with
      | Leaf (value, _, _) when Value.length value <= 36 (* XXX hard coded *) ->
          begin if is_new_value i then
              (* not cached *)
              incr_count num_of_replesentative_values value
            else
              (* cached *)
              incr_count num_of_shared_values value
          end;
          `Continue, ()
      | _ ->
          if is_new_value i then
            `Continue, ()
          else
            `Up, ());
  Lwt.return_unit

let map_zip tbl1 tbl2 =
  let res = Hashtbl.create (Hashtbl.length tbl1) in
  Hashtbl.iter (fun k v ->
      let v1 = Some v in
      let v2 = Hashtbl.find_opt tbl2 k in
      Hashtbl.add res k (v1, v2)) tbl1;
  Hashtbl.iter (fun k v ->
      if Hashtbl.mem tbl1 k = false then
        let v1 = None in
        let v2 = Some v in
        Hashtbl.add res k (v1, v2)) tbl2;
  res

let dump_result cached reps =
  Format.eprintf "=== cached: (%d)\n" (Hashtbl.length cached);
  Format.eprintf "=== reps:   (%d)\n" (Hashtbl.length reps);
  Format.printf "Value, Value Size, Cached, Not cached, Caching performance[byte]\n";
  let rows =
    map_zip reps cached
    |> Hashtbl.to_seq
    |> Seq.filter (fun (_, (_, cached)) -> cached <> None)
    |> Seq.map (fun (v, (reps,cached)) -> (v, Option.get reps, Option.get cached))
    |> Seq.map (fun (value, reps, cached) ->
        let cells = if Value.length value <= 32 then 2 else 3 in
        if Value.length value > 36 then
          failwith (Printf.sprintf "BIG VALUE: %s: %d" (Value.to_hex_string value) (Value.length value));
        let performance = 32 * cached * (cells - 1) in
        (value, Value.length value, cached, reps, performance))
    |> List.of_seq
    |> List.sort (fun x y ->
        let size (_, len, cached, _, _) = len * cached in
        compare (size y) (size x))
  in
  rows
  |> List.iter (fun (value, len, cached, reps, performance) ->
      Format.printf "'%s', %d, %d, %d, %d\n" (Value.to_hex_string value)
        len cached reps performance
    );
  let (total_len, total_cached, total_reps, total_performance) =
    List.fold_left (fun
                     (len_sum, cached_sum, reps_sum, perf_sum)
                     (_value, len, cached, reps, performance) ->
                     (len+len_sum, cached+cached_sum, reps+reps_sum, performance+perf_sum)) (0, 0, 0, 0) rows
  in
  Format.printf "TOTAL, %d, %d, %d, %d\n" total_len total_cached total_reps total_performance

let () = Lwt_main.run @@
  let path1 = Sys.argv.(1) in
  let* vc1 = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path1 in

  let cells = Storage.get_current_length (Vc.context vc1).Context.storage in

  let t1 = Unix.gettimeofday () in

  let num_of_shared_values = Hashtbl.create 0 in
  let num_of_replesentative_values = Hashtbl.create 0 in

  let f ent =
    count_new_nodes vc1 ent.Commit.hash num_of_shared_values num_of_replesentative_values
  in

  let* () = Commit_db.ordered_fold (fun ent ~children:_ () -> f ent) (Vc.commit_db vc1) () in
  let t2 = Unix.gettimeofday () in
  dump_result num_of_shared_values num_of_replesentative_values;
  Format.eprintf "Counted %a cells in %f secs@." Index.pp cells (t2 -. t1);
  Lwt.return_unit
