open Plebeia
open Utils
open Lwt.Syntax

let () =
  Lwt_main.run @@
  let path = Sys.argv.(1) in
  let* vc = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path in
  let commit_db = Vc.commit_db vc in

  let last_root =
    match Commit_db.read_the_latest commit_db with
    | Some root -> root
    | None -> assert false
  in

  Format.eprintf "last root index= %a@." Index.pp last_root.Commit.index;

  let* _, Cursor.Cursor (_, n, ctxt) = Lwt.map Option.get @@ Vc.checkout vc last_root.Commit.hash in

  let rec traverse (i,deps,worst) curdep n =
    match Node.view ctxt n with
    | Node.Leaf _
    | Bud (None, _, _) -> (i+1, deps+curdep, Int.max worst curdep)
    | Internal (nl, nr, _, _) ->
        let (i,deps,worst) = traverse (i,deps, worst) (curdep+1) nl in
        traverse (i,deps,worst) (curdep+1) nr
    | Extender (_, n', _, _) ->
        traverse (i,deps,worst) (curdep+1) n'
    | Bud (Some n', _, _) ->
        traverse (i,deps,worst) (curdep+1) n'
  in
  let i, deps, worst = traverse (0,0,0) 1 n in
  Format.eprintf "leaves: %d  worst: %d avgdepth: %.2f@."
    i worst (float deps /. float i);
  Lwt.return_unit
