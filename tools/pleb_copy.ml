open Plebeia
open Lwt.Syntax
open Utils

let () = Lwt_main.run @@
  let path1 = Sys.argv.(1) in
  let* vc1 = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path1 in

  let path2 = Sys.argv.(2) in
  let* vc2 = Result_lwt.get_ok_lwt @@ Vc.create ~node_cache:Node_cache.(create ()) Context.default_config path2 in

  let* commits = Commit_db.to_list @@ Vc.commit_db vc1 in

  let* res = Copy.copy vc1 commits vc2 in
  match res with
  | Error e ->
      Log.fatal "Error: %a" Error.pp e;
      Error.raise e
  | Ok _ ->
      Log.notice "done";
      Lwt.return_unit
