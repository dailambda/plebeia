open Plebeia
open Utils
open Lwt.Syntax

let () = Lwt_main.run @@
  let path = Sys.argv.(1) in
  let* vc = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path in
  let commit_db = Vc.commit_db vc in

  let last_root =
    match Commit_db.read_the_latest commit_db with
    | Some root -> root
    | None -> assert false
  in

  Format.eprintf "last root index= %a@." Index.pp last_root.Commit.index;

  let segs = Hashtbl.create 0 in

  let* _, c = Lwt.map Option.get @@ Vc.checkout vc last_root.Commit.hash in
  let extenders =
    Cursor.fold ~init:0 c (fun extenders c ->
        match Cursor.view c with
        | _, Node.Extender (seg, _, _, _) ->
            begin match Hashtbl.find_opt segs seg with
              | None -> Hashtbl.add segs seg 1
              | Some n -> Hashtbl.replace segs seg (n+1)
            end;
            `Continue, extenders+1
        | _ -> `Continue, extenders)
  in
  Format.eprintf "extenders=%d@." extenders;
  let a = Array.init (Segment.max_length+1) (fun _ -> (0,0)) in
  Hashtbl.iter (fun k v ->
      let len = Segment.length k in
(*
      if len > 28*8-2 then begin Format.eprintf "seglen=%d@." len; assert false; end;
*)
      let (m,n) = Array.unsafe_get a len in
      Array.unsafe_set a len (m+1,n+v)) segs;
  Format.eprintf "--- extender statistics@.";
  Format.eprintf "length, distinct, total, dupe_ratio@.";
  for len = 0 to 28 * 8 - 2 do
    let (m,n) = Array.unsafe_get a len in
    Format.eprintf "%d, %d, %d, %.2f@." len m n (if m = 0 then 1.0 else float n /. float m)
  done;
  Lwt.return_unit
