open Plebeia
open Utils
open Lwt.Syntax

type t = (Segment.segment * ptr) list
and ptr =
  | Plebeia_leaf of Index.t
  | Tribune_node of t * Index.t option
  (* | Disk of Index.t *)

let size_of_entry (seg, _n) =
  (Segment.length seg + 7) / 8 + 1 (* sides *)
  + 1 (* tag *) + 4 (* pointer *)
  + 1 (* murphy factor *)

let size_of_entry' (sides, _n) =
  (List.length sides + 7) / 8 + 1 (* sides *)
  + 1 (* tag *) + 4 (* pointer *)
  + 1 (* murphy factor *)

(* size of [t] on disk, excluding its sub-nodes *)
let _size_in_bytes t =
  List.fold_left (fun sum ent -> sum + size_of_entry ent)
    1 (* version *)
    t

let maximum_size_in_bytes = 4096

let rec convert_ context n =
  match Node.view context n with
  | Node.Leaf (_, Some i, _) | Bud (_, Some i, _) ->
      [[], `Leaf i]
  | Node.Leaf (_, None, _) | Bud (_, None, _) -> assert false
  | Internal (nl, nr, _, _) ->
      let tl = convert_ context nl in
      let tr = convert_ context nr in
      let t =
        List.map (fun (rev_sides, t) -> Segment.Left :: rev_sides, t) tl
        @ List.map (fun (rev_sides, t) -> Segment.Right :: rev_sides, t) tr
      in
      let sz = List.fold_left (fun sum e -> sum + size_of_entry' e) 1 t in
      if sz < maximum_size_in_bytes then t
      else [ ([Segment.Left], `Node tl);  ([Segment.Right], `Node tr) ]
  | Extender (seg, n, _, _) ->
      let t = convert_ context n in
      let ss = Segment.to_sides seg in
      let t' = List.map (fun (rev_sides, t) ->
          (* XXX Prefix compression possible *)
          (List.rev_append ss rev_sides, t)) t
      in
      let sz = List.fold_left (fun sum e -> sum + size_of_entry' e) 1 t' in
      if sz < maximum_size_in_bytes then t'
      else [ (ss, `Node t) ]

let leaves = ref 0
let deps = ref 0
let worst = ref 0

let compress_sides_list sides_list =
  snd @@ List.fold_left (fun (prev,rev_acc) sides ->
      let common, _, postfix2 =
        Segment.common_prefix
          (Segment.of_sides prev)
          (Segment.of_sides sides)
      in
      sides, (Segment.length common, Segment.to_sides postfix2) :: rev_acc)
    ([], []) sides_list

let rec convert depth es =
  let ls = List.length (List.filter (function (_,`Leaf _) -> true | _ -> false) es) in
  let ns = List.length (List.filter (function (_,`Leaf _) -> false | _ -> true) es) in

  let xs =
    List.sort (fun (sides1, _) (sides2, _) -> compare sides1 sides2)
    @@ List.map (fun (rev_sides, x) -> List.rev rev_sides, x) es
  in
  let compressed = compress_sides_list @@ List.map fst xs in
  let compressed_size =
    List.fold_left (fun sum (_, sides) ->
        sum + (List.length sides + 7) / 8 + 1 + 1 + 4 + 1) 1 compressed
  in

  Format.eprintf "depth: %d xs: %d leaf: %d node: %d bs: %d comp: %d@."
    depth
    (List.length es)
    ls
    ns
    (List.fold_left (fun sum x -> sum + size_of_entry' x) 1 es)
    compressed_size;

  worst := Int.max !worst depth;
  leaves := !leaves + ls;
  deps := !deps + ls * depth;

  List.map (fun (sides, x) ->
      Segment.of_sides sides,
      match x with
      | `Leaf i -> Plebeia_leaf i
      | `Node es -> Tribune_node (convert (depth+1) es, None)) xs

let rec traverse f acc t =
  let acc' = f acc t in
  List.fold_left (fun acc -> function
      | _, Plebeia_leaf _ -> acc
      | _, Tribune_node (t, _) -> traverse f acc t
      (* | _ -> acc *)) acc' t

let () = Lwt_main.run @@
  let path = Sys.argv.(1) in
  let* vc = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path in
  let commit_db = Vc.commit_db vc in

  let last_root =
    match Commit_db.read_the_latest commit_db with
    | Some root -> root
    | None -> assert false
  in

  Format.eprintf "last root index= %a@." Index.pp last_root.Commit.index;

  let* _, Cursor.Cursor (_, n, ctxt) = Lwt.map Option.get @@ Vc.checkout vc last_root.Commit.hash in
  prerr_endline "converting...";
  let n = match Node.view ctxt n with
    | Bud (Some n, _, _) -> n
    | Bud (None, _, _) -> assert false
    | _ -> assert false
  in
  let t = convert 1 @@ convert_ ctxt n in
  prerr_endline "converted!";
  let f (ts, used) t =
    ts + 1,
    used + List.fold_left (fun sum e -> sum + size_of_entry e) 0 t
  in
  let ts, used = traverse f (0,0) t in
  Format.eprintf "ts %d used %d ratio %f@."
    ts used (float used /. float ts /. float maximum_size_in_bytes);
  Format.eprintf "worst %d leaves %d avg-depth %.2f@."
    !worst !leaves (float !deps /. float !leaves);
  Lwt.return_unit
