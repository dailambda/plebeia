(*

   OBSOLETE: this root hash traversal is random therefore super slow.
   Do not use this tool.  Use the technique like Pleb_copy.

   Counts the buds of ALL the roots.  It does not visit the buds already seen.
   Warning: it takes super long time for a big plebeia context.

   $ dune exec ./pleb_count_buds.exe plebeia.context

*)
open Plebeia
open Utils
open Lwt.Syntax

module IS = Set.Make(struct
    type t = Index.t
    let compare = compare
  end)

let () = Lwt_main.run @@
  let path = Sys.argv.(1) in
  let* ctxt = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config path in
  let commit_db = Vc.commit_db ctxt in

  (* Cursor.traversal can be too slow *)
  let t1 = Unix.gettimeofday () in
  let* roots = Commit_db.to_list commit_db in
  let nhashes = List.length roots in
  let _ = Lwt_list.fold_left_s
      (fun
        (seen, nseen, pointed, ncopied, nhashes_done)
        { Commit.index=_; hash; _} ->
        Format.eprintf "Checkout %s %d/%d@." (Commit_hash.to_hex_string hash) nhashes_done nhashes;
        let* _, c = Lwt.map Option.get @@ Vc.checkout ctxt hash in
        let (seen, nseen, pointed, ncopied) =
          Cursor.fold ~init:(seen, nseen, pointed, ncopied) c
            (fun (seen, nseen, pointed, ncopied) c ->
               let _, v = Cursor.view c in
               match v with
               | Bud (_, None, _) -> assert false
               | Bud (nopt, Some i, _) ->
                   if IS.mem i seen then
                     `Up, (seen, nseen, pointed, ncopied)
                   else begin
                     let seen = IS.add i seen in
                     let nseen = nseen + 1 in
                     if nseen mod 1000 = 0 then begin
                       Format.eprintf "%d bud seen@." nseen;
                     end;
                     begin match nopt with
                       | None ->
                           `Continue, (seen, nseen, pointed, ncopied)
                       | Some n ->
                           match Node.index n with
                           | None -> assert false
                           | Some j ->
                               if IS.mem j pointed then begin
                                 let ncopied = ncopied + 1 in
                                 if ncopied mod 100 = 0 then Format.eprintf "%d copies seen@." ncopied;
                                 `Up, (seen, nseen, pointed, ncopied)
                               end else
                                 `Continue, (seen, nseen, IS.add j pointed, ncopied)
                     end
                   end
               | _ ->
                   `Continue, (seen, nseen, pointed, ncopied))
        in

        let nhashes_done = nhashes_done + 1 in

        let t2 = Unix.gettimeofday () in
        Format.eprintf "%.2f sec / 10000 bud@." ((t2 -. t1) /. float (nseen / 10000));
        Format.eprintf "%d bud / commit@." (nseen / nhashes_done);

        Lwt.return (seen, nseen, pointed, ncopied, nhashes_done))
      (IS.empty, 0, IS.empty, 0, 0) roots
  in
  Lwt.return_unit
