open Plebeia
(*open Plebeia_msync.Msync *)
open Test_utils

let test_msync dir () =
  let fn = dir ^/ "test.data" in
  Format.eprintf "test file: %s@." fn;
  let oc = open_out fn in
  let buf = String.init 10_000 (fun _ -> '\000') in
  for _ = 1 to 10_000 do
    output_string oc buf;
  done;
  close_out oc;
  let fd = Unix.openfile fn [O_RDWR] 0o666 in
  let m = Mmap.make fd ~pos:0 ~shared:true 100_000_000 in
  Unix.close fd;
  Mmap.msync m;
  let b = Mmap.get_buffer ~off:0 ~len:100_000_000 m in
  let (), secs = with_time (fun () ->
      for i = 0 to 100_000_000 - 1 do
        Mmap.Buffer.set_char b i (Char.chr (i mod 256))
      done)
  in
  Format.eprintf "set in %a@." Mtime.Span.pp secs;
  let (), secs = with_time (fun () -> Mmap.msync m) in
  Format.eprintf "msync in %a@." Mtime.Span.pp secs;
  Unix.unlink fn

let test_msync_lwt dir () =
  Lwt_main.run @@
  let fn = dir ^/ "test.data" in
  Format.eprintf "test file: %s@." fn;
  let oc = open_out fn in
  let buf = String.init 10_000 (fun _ -> '\000') in
  for _ = 1 to 10_000 do
    output_string oc buf;
  done;
  close_out oc;
  let fd = Unix.openfile fn [O_RDWR] 0o666 in
  let m = Mmap.make fd ~pos:0 ~shared:true 100_000_000 in
  Unix.close fd;
  Mmap.msync m;
  let b = Mmap.get_buffer ~off:0 ~len:100_000_000 m in
  let (), secs = with_time (fun () ->
      for i = 0 to 100_000_000 - 1 do
        Mmap.Buffer.set_char b i (Char.chr (i mod 256))
      done)
  in
  Format.eprintf "set in %a@." Mtime.Span.pp secs;
  let open Lwt in
  let b = ref false in
  let t1 = Mtime_clock.now () in
  (* We can do other lwt-jobs while msyncing! *)
  Lwt.join [
    (Mmap.msync_lwt m >>= fun () -> b := true; Lwt.return ());
    (let rec f () =
       if !b then Lwt.return ()
       else
         Lwt_unix.sleep 0.001 >>= fun () ->
         Format.eprintf ".@."; f ()
     in
     f ())
  ] >>= fun () ->
  let t2 = Mtime_clock.now () in
  Format.eprintf "msync in %a@." Mtime.Span.pp (Mtime.span t1 t2);
  Unix.unlink fn;
  Lwt.return_unit

let () =
  let open Alcotest in
  with_tempdir @@ fun dir ->
  run "msync"
    [ "msync", ["test_msync", `Quick, test_msync dir];
      "msync_lwt", ["test_msync_lwt", `Quick, test_msync_lwt dir];
    ]
