open Test_utils
open Plebeia
open Cursor

let test () =
  with_random @@ fun rng ->
  for _i = 0 to 10 do
    run_ignore_lwt @@ with_cursor @@ fun c ->
    if is_with_count c then
      let rec loop () =
        let c0, _ops = Do_random.Deep.do_random ~commit:true ~bud_level_range:(1,3) ~segment_length:20 rng 10000 c in
        match Result.get_ok @@ go_below_bud c0 with
        | None -> loop ()
        | Some c ->
            let cnt1 = Cursor_stat.shallow_count c in
            let Cursor (_, n, ctxt) = c in
            let cnt2, _ = Option.get @@ Node_storage.count ctxt n in
            assert (cnt1 = cnt2);

            let c, _, _ = Result.get_ok @@ Cursor.Cursor_storage.write_top_cursor c0 in
            match Result.get_ok @@ go_below_bud c with
            | None -> assert false
            | Some c ->
                let Cursor (_, n, ctxt) = c in
                let cnt3, _ = Option.get @@ Node_storage.count ctxt n in
                assert (cnt1 = cnt3);
                Format.eprintf "cnt %d@." cnt1
      in
      loop ()
  done

let () =
  let open Alcotest in
  run "full_deep_random"
    ["full_deep_random", [ "test", `Quick, test ]]
