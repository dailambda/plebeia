open Plebeia
open Lwt.Syntax
open Test_utils

let doit rng ctxt fn ?min_nodes depth write_dot =
  (* make a random tree then test its snapshot *)
  let n = gen_node ctxt rng ?min_nodes depth in
  let n, nh = Node.compute_hash ctxt n in
  let n, _, _, _cnt = Result.get_ok @@ Node_storage.write_node ctxt n in
  (* forget some nodes *)
  let n = forget_random_nodes rng 0.2 n in
  if write_dot then begin
    let dotfn = "node.dot" in
    save_node_to_dot dotfn n;
  end;
  let* fd = Lwt_unix.openfile fn [O_CREAT; O_TRUNC; O_WRONLY] 0o644 in
  let* () = Snapshot.save fd ctxt n in
  let* () = Lwt_unix.close fd in
  let md5 = Digest.file fn in
  Format.eprintf "snapshot md5: %s@." Digest.(to_hex md5);
  let* fd = Lwt_unix.openfile fn [O_RDONLY] 0o644 in
  let reader = Data_encoding_tools.make_reader fd in
  let* n', with_hash = Snapshot.load ctxt.hasher reader in
  let* () = Lwt_unix.close fd in
  assert (not with_hash);
  assert (match n' with
      | Node.View v -> Node_type.hash_of_view v = Some nh
      | _ -> assert false);
  Lwt.return md5

let random_test () =
  run_ignore_lwt @@ with_random_lwt @@ fun rng ->
  with_context_lwt @@ fun ctxt ->
  let fn = Filename.temp_file "plebeia" ".dump" in
  Format.eprintf "writing to %s@." fn;
  for_lwt 0 100 (fun _i ->
    ignore_lwt @@ doit rng ctxt fn 20 true (* with dot *))

let regression_test () =
  run_lwt @@ Lwt.map (regression_by_hash __LOC__ 1018063598) @@
  let rng = RS.make [| 0 |] in
  with_context_lwt @@ fun ctxt ->
  let fn = Filename.temp_file "plebeia" ".dump" in
  Format.eprintf "writing to %s@." fn;
  doit rng ctxt fn 100 ~min_nodes:5_000 false (* no dot *)

let () =
  let open Alcotest in
  run "snapshot"
    ["snapshot", [ "random", `Quick, random_test
                 ; "regression", `Quick, regression_test
                 ]]
