open Plebeia
open Test_utils

module Fs = Fs_impl
open Fs

let enc = Fs_nameenc.bits8

open Fs.Op_lwt
open Fs.Op_lwt.Infix

let ( !/ ) s =
  match String.split_by_char (function '/' -> true | _ -> false) s with
  | "" :: ss -> List.filter (function "" -> false | _ -> true) ss
  | _ -> assert false

module Test = struct
  type Error.t += Must_fail_but_succeeded

  let () =
    Error.register_printer
    @@ function
    | Must_fail_but_succeeded -> Some "must fail but succeeded" | _ -> None

  let must_fail : 'a t -> unit t = fun at cur ->
    Lwt.map (function
        | Error e ->
            Format.eprintf "Ok error: %a@." Error.pp e;
            Ok (cur, ())
        | Ok _ ->
            Error Must_fail_but_succeeded)
      (at cur)

  (* write a file *)
  let write s0 : _ t =
    do_then (fun _ -> Format.eprintf "write %s@." s0) @@
    let s = !/s0 in
    let v = Value.of_string "init" in
    write s v
    >>= fun () ->
    read s
    >>= function
    | v' ->
        assert (v = v') ;
        (* overwrite test *)
        let v = Value.of_string s0 in
        write s v
        >>= fun () ->
        read s
        >>= function
        | v' ->
            assert (v = v') ;
            return ()

  let rm ?recursive ?ignore_error s =
    do_then (fun _ -> Format.eprintf "rm %s@." s) @@
    let s = !/s in
    rm ?recursive ?ignore_error s >>= function
    | false -> return ()
    | true -> must_fail (get s)

  let rmdir ?ignore_error s =
    do_then (fun _ -> Format.eprintf "rmdir %s@." s) @@
    let s = !/s in
    rmdir ?ignore_error s >>= function
    | false -> prerr_endline "1"; return ()
    | true -> prerr_endline "2"; must_fail (get s)

  let cat ?expect s0 =
    do_then (fun _ -> Format.eprintf "cat %s@." s0) @@
    let s = !/s0 in
    read s
    >>= function
    | v ->
        let expect = match expect with None -> s0 | Some s -> s in
        if expect <> Value.to_string v then
          failwithf
            "cat %s failed actual: %S expected: %S"
            s0
            (Value.to_string v)
            expect ;
        return ()

  let ls dir expectedf =
    do_then (fun _ -> Format.eprintf "ls %s@." dir) @@
    ls !/dir
    >>= fun xs ->
    let xs = List.sort compare @@ List.map (fun (x,_) -> x) xs in
    (* The result must be properly sorted *)
    let rec sorted = function
      | a::b::xs -> assert (a < b); sorted (b::xs)
      | _ -> ()
    in
    sorted xs;
    if not @@ expectedf xs then (
      Format.eprintf "Ouch: %s@." (String.concat "; " xs) ;
      assert false ) ;
    return ()

  let copy from to_ =
    do_then (fun _ -> Format.eprintf "copy %s %s@." from to_)
    @@
    let from = !/from in
    let to_ = !/to_ in
    copy from to_

  let f cursor =
    let _must_be at a =
      at
      >>= fun a' ->
      assert (a = a') ;
      return ()
    in
(*
    let stat s =
      Format.eprintf "stat %s@." s ;
      stat !/s
    in
*)
    run cursor
      (let root =
         must_fail @@ cat "/"
         >>= fun () ->
         ls "/" (function [] -> true | _ -> false)
       in
       root >>

       (* simple write *)
       write "/protocol" >>

       (* simple write w/o mkdir *)
       write "/data/genesis_key" >>

       must_fail @@ write "/protocol/foo" >>

       must_fail @@ write "/protocol/foo/bar" >>

       must_fail @@ cat "/" >>

       must_fail @@ cat "/data" >>

       must_fail @@ cat "/data/genesis_key/foo" >>

       must_fail @@ cat "/data/genesis_key/foo/bar" >>

       must_fail @@ cat "/non-existent" >>

       must_fail @@ rm "/non-existent" >>
       rm ~ignore_error:true "/non-existent" >>
       must_fail @@ rmdir "/non-existent" >>
       rmdir ~ignore_error:true "/non-existent" >>

       must_fail @@ rm "/data/genesis_key/foo" >>
       must_fail @@ rmdir "/data/genesis_key/foo" >>

       ls "/data" (function ["genesis_key"] -> true | _ -> false) >>

       (* removal of file *)
       do_then (fun _ -> prerr_endline "** 1 removal file") @@
       must_fail @@ rmdir "/data/genesis_key" >>

       ls "/data" (function ["genesis_key"] -> true | _ -> false) >>

       do_then (fun _ -> prerr_endline "** 2 removal file") @@
       rm "/data/genesis_key" >>

       (* it should remove /data recursively *)

(*
   XXX
   currently rm succeeds if the target is not found
   almost no point to have ~ignore_error

       do_then (fun _ -> prerr_endline "** 3 removal file") @@
       must_fail @@ rm ~ignore_error:false "/data/genesis_key" >>

       do_then (fun _ -> prerr_endline "** 4 removal file") @@
       rm ~ignore_error:true "/data/genesis_key" >>
*)

       (* /data must be gone *)
       ls "/" (function ["protocol"] -> true | _ -> false) >>

       write "/data/genesis_key" >>

       (* removal of directory *)
       must_fail @@ rm "/data" >>
       rm ~ignore_error:true "/data" >>
       rm ~recursive:true "/data" >>
       must_fail @@ rmdir  "/data" >>

       write "/data/a/b/c/d" >>
       write "/data/a/b/e/f" >>
       ls "/data/a" (function ["b"] -> true | _ -> false) >>

       rmdir "/data/a/b" >>
       must_fail @@ cat "/data/a/b/c/d" >>
       must_fail @@ cat "/data/a/b/e/f" >>

       write "/a/b" >>
       ls "/a" (function ["b"] -> true | _ -> false) >>
       copy "/a" "/a2" >>
       ls "/a" (function ["b"] -> true | _ -> false)  >>
       ls "/a2" (function ["b"] -> true | _ -> false) >>
       rmdir "/a"  >>
       rmdir "/a2" >>

       write "/data/contracts/0/a/b/c/d" >>
       write "/data/contracts/0/a/b/e/f" >>
       copy "/data/contracts/0" "/data/contracts/1" >>
       cat ~expect:"/data/contracts/0/a/b/c/d" "/data/contracts/1/a/b/c/d" >>
       cat ~expect:"/data/contracts/0/a/b/e/f" "/data/contracts/1/a/b/e/f" >>

       (* [copy] cannot copy non existent *)
       must_fail @@ copy "/non-existent" "/data/contracts/3" >>

       (* [copy] CAN copy a file *)
       copy "/data/contracts/0/a/b/c/d" "/data/contracts/2/a/b/c/d" >>

       (* [copy] CAN overwrite a directory *)
       copy "/data/contracts/0" "/data/contracts/3" >>

       (* [copy] does not create a loop *)
       copy "/data/contracts/0" "/data/contracts/0/x/y/z" >>

       ls "/" (function ["data"; "protocol"] -> true | _ -> false) >>
       ls "/data" (function ["contracts"] -> true | _ -> false) >>
       ls "/data/contracts" (function ["0"; "1"; "2"; "3"] -> true | _ -> false) >>

       (* cannot ls a file *)
       must_fail @@ ls "/protocol" (fun _ -> true) >>
       must_fail @@ ls "/protocol/foo/bar" (fun _ -> true) >>
       must_fail @@ ls "/non-existent" (fun _ -> true) >>
       must_fail @@ ls "/data/non-existent" (fun _ -> true) >>

       do_then (fun _ -> prerr_endline "fold'")
       @@ fold' [] !/"/"
         (fun acc path c ->
            let cur = get_raw_cursor c in
            Lwt.return @@ match snd @@ Cursor.view cur with
            | Bud _ -> Ok acc
            | Leaf _ -> Ok (path :: acc)
            | _ -> assert false)
       >>= fun _xs ->
       Format.eprintf "fold' done@.";
       (* XXX we have no check of the correctness of [xs] *)

(*
   XXX local access does not match well with the module

       do_then (fun _ -> Format.eprintf "local@.") @@

       lwt (get !/"/data/contracts/1")
       >>= (fun (c,_v) ->
           fun c0 ->
             Lwt.bind
               (run c (
                   do_then (fun _ -> Format.eprintf "cat a/b/e/f@.") @@
                   lwt (Fs.cat !/"a/b/e/f") >>= fun v ->
                   assert (Value.to_string v = "/data/contracts/0/a/b/e/f");
                   do_then (fun _ -> Format.eprintf "write a/b/e/f ...@.") @@
                   write "a/b/e/f" >>
                   do_then (fun _ -> Format.eprintf "cat a/b/e/f ...@.") @@
                   lwt (Fs.cat !/"a/b/e/f") >>= fun v ->
                   assert (Value.to_string v = "/a/b/e/f");
                   do_then (fun _ -> Format.eprintf "cat /a/b/e/f ok@.") @@
                   return ()))
               (function
                 | Error e ->
                     Format.eprintf "Error %a@." Error.pp e; assert false
                 | Ok (c_1,()) -> Lwt.return (Ok (c0, c_1))))
       >>= fun (c_1,_v) ->

       (* the local tree modification does not affect the original *)
       do_then (fun _ -> Format.eprintf "check the original@.") @@
       lwt (Fs.cat !/"/data/contracts/1/a/b/e/f")
       >>= fun v ->
       assert (Value.to_string v = "/data/contracts/0/a/b/e/f");

       do_then (fun _ -> Format.eprintf "setting@.") @@
       lwt (set !/"/data/contracts/1" c_1) >>

       do_then (fun _ -> Format.eprintf "checking...@.") @@
       lwt (Fs.cat !/"/data/contracts/1/a/b/e/f")
       >>= fun v ->
       assert (Value.to_string v = "/a/b/e/f");
       do_then (fun _ -> Format.eprintf "checked@.") @@
*)

       (* check the past bugs are fixed *)
       write "/f/g/h" >>
       must_fail @@ write "/f"
      )

  let test () =
    run_ignore_lwt @@ with_context_lwt @@ fun ctxt ->
    Lwt.bind (f (empty ctxt enc)) (function
        | Ok _ -> Lwt.return ()
        | Error e ->
            Format.eprintf "Error : %a@." Error.pp e;
            assert false)

  let do_random_test ?(verbose=false) rng n =
    with_context_lwt @@ fun ctxt ->

    let f c =
      let open Gen in
      let open Gen.Infix in
      let path =
        let name =
          int 23 >>= fun i ->
          let c = Char.chr (Char.code 'a' + i) in
          return @@ String.make 1 c
        in
        list (int_range (1, 8)) name >>| fun ns ->
        String.concat "/" ("" :: ns)
      in
      let value =
        int 100 >>| fun i ->
        Value.of_string @@ string_of_int i
      in
      let cat p =
        Lwt.return @@
        match Fs.Op.read !/p c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let get p =
        Lwt.return @@
        match Fs.Op.get !/p c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let write p v =
        Lwt.return @@
        match Fs.Op.write !/p v c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let rm p =
        Lwt.return @@
        match Fs.Op.rm ~recursive:true !/p c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let ls p =
        let open Result_lwt.Syntax in
        let*= res = Fs.Op_lwt.ls !/p c in
        match res with
        | Error _ -> Lwt.return c
        | Ok (c, _) when not ctxt.with_count -> Lwt.return c
        | Ok (c, xs) ->
            let xs = List.map fst xs in
            let len = List.length xs in
            let offset = if len = 0 then 0 else Random.State.int rng len in
            let length = if len = 0 then 0 else Random.State.int rng (len-offset) in
            let ys = List.take length @@ List.drop offset xs in
            let*= res = Fs.Op_lwt.ls2 ~offset ~length !/p c in
            match res with
            | Error _ -> assert false
            | Ok (c', zs) ->
                let zs = List.map fst zs in
                assert (ys = zs);
                Lwt.return c'
      in
      let copy p p' =
        Lwt.return @@
        match Fs.Op.get !/p c with
        | Error _ -> c
        | Ok (c, (cp,_v)) ->
            match Fs.Op.set !/p' cp c with
            | Error _ -> c
            | Ok (c, ()) -> c
      in
      let command =
        let op =
          int 6 >>= function
          | 0 ->
              path >>| fun p -> `cat p
          | 1 ->
              path >>= fun p ->
              value >>| fun v ->
              `write (p, v)
          | 2 ->
              path >>| fun p -> `rm p
          | 3 ->
              path >>| fun p -> `ls p
          | 4 ->
              path >>= fun p ->
              path >>| fun p' ->
              `copy (p, p')
          | 5 ->
              path >>| fun p -> `get p
          | _ -> assert false
        in
        op rng
      in
      match command with
      | `cat p ->
          if verbose then Format.eprintf "cat %s@." p;
          cat p
      | `get p ->
          if verbose then Format.eprintf "get %s@." p;
          get p
      | `write (p,v) ->
          if verbose then Format.eprintf "write %s %s@." p (Value.to_string v);
          write p v
      | `rm p ->
          if verbose then Format.eprintf "rm %s@." p;
          rm p
      | `ls p ->
          if verbose then Format.eprintf "ls %s@." p;
          ls p
      | `copy (p, p') ->
          if verbose then Format.eprintf "copy %s %s@." p p';
          copy p p'
    in
    let rec loop i c =
      if i = n then Lwt.return (snd @@ Result.get_ok @@ Fs.Op.compute_hash c)
      else
        Lwt.bind (f c) @@ fun c ->
            loop (i+1) c
    in
    loop 0 (empty ctxt enc)

  let random_test () =
    run_ignore_lwt @@ with_random_lwt @@ fun rng ->
    do_random_test rng 50_000

  let regression_test () =
    run_lwt @@ Lwt.map (regression_by_hash __LOC__ 541373882) @@
    do_random_test (RS.make [| 0 |]) 50_000
end

let () =
  let open Alcotest in
  run "fs"
    [ "unit", [ "test", `Quick, Test.test ]
    ; "random", [ "random_test", `Quick, Test.random_test
                ; "regression", `Quick, Test.regression_test
                ]
    ]
