(* node storage test *)
open Plebeia
open Lwt.Syntax
open Test_utils

open Storage

let doit bytes_per_cell =
  let config = Storage.{
      head_string = "test" ^ String.make 16 '\000';
      version = 0;
      bytes_per_cell = bytes_per_cell;
      max_index = Index.max_index;
    }
  in
  let f = temp_file "plebeia-" ".gstorage" in
  Format.eprintf "bytes_per_cell: %d file: %s@." bytes_per_cell f;
  let* key = Storage.make_new_writer_key [] in
  let* s = create ~config ~key f in
  let cell_contents i =
    String.init bytes_per_cell (fun j ->
        Char.chr @@ (Int64.to_int (Index.to_int64 i) mod 255 + j) mod 256)
  in
  let write () =
    let i = Result.get_ok @@ new_index s in
    let buf = get_cell s i in
    Mmap.Buffer.write_string (cell_contents i) buf 0;
  in
  let rec f n =
    if n > 10000 then flush s
    else begin
      write ();
      let* () = if n mod 100 = 0 then flush s else Lwt.return_unit in
      f (n + 1)
    end
  in
  let* () = f 0 in
  let last_i = Index.Unsafe.pred @@ get_current_length s in
  let rec f n =
    if n > last_i then ()
    else begin
      let buf = get_cell s n in
      assert (cell_contents n = Mmap.Buffer.to_string buf);
      f (Index.Unsafe.succ n)
    end
  in
  f (Storage.start_index s);
  Chunk.test_write_read (RS.make_self_init ()) s;
  close s

let test () = run_lwt begin
    let* () = doit 5 in
    let* () = doit 32 in
    let* () = doit 40 in
    let* () = doit 48 in
    let* () = doit 64 in
    doit 128
  end

let () =
  let open Alcotest in
  run "gstorage"
    ["gstorage", ["test", `Quick, test]]
