open Test_utils

let test () =
  run_lwt @@ with_random_lwt @@ fun rng ->
  for_lwt 1 10 (fun i ->
      ignore_lwt @@ with_cursor @@ fun c ->
      let (ops, _c) = Do_random.Flat.do_random rng 10000 c (Dumb.empty ()) in
      (* Format.eprintf "%d ops@." (List.length ops); *)
      Format.eprintf "%d done (%d ops)@." i
        (List.length ops);
      (* Debug.save_cursor_to_dot (Printf.sprintf "random%d.dot" i) c *))

let regression_test () =
  run_lwt @@ Lwt.map (regression_by_hash __LOC__ 781440102)
  @@ let rng = RS.make [| 0 |] in
  with_cursor @@ fun c ->
  let (_ops, c) = Do_random.Flat.do_random rng 10000 c (Dumb.empty ()) in
  let _, h = Plebeia.Cursor.compute_hash c in
  h

let () =
  let open Alcotest in
  run "full_random"
    ["full_random", [ "test", `Quick, test ];
     "regression", [ "regression", `Quick, regression_test ]
    ]
