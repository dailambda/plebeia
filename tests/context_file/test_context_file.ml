open Plebeia
open Lwt.Syntax
open Test_utils

let create_data_files () =
  let rng = RS.make [| 0 |] in
  run_ignore_lwt @@ with_vc ~prefix:"context" @@ Do_random.Vc.do_random rng 10000

let data_file_test () =
  run_ignore_lwt @@ with_context_conf_lwt @@ fun conf ->
  let prefix = Printf.sprintf "context-%s" (Context.config_name conf) in
  let prefix_tmp = Printf.sprintf "context-tmp-%s" (Context.config_name conf) in
  let* () = copy_file (prefix ^ ".commit_tree") (prefix_tmp ^ ".commit_tree") in
  let* () = copy_file (prefix ^ ".context") (prefix_tmp ^ ".context") in
  Format.eprintf "Testing %s@." prefix_tmp;
  let* vc = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read conf prefix_tmp in
  let commit_db = Vc.commit_db vc in
  let f commit ~parent:_ =
    let+ _, c = Lwt.map Option.get @@ Vc.checkout vc commit.Commit.hash in
    let _, h (* stored in disk *) = Cursor.compute_hash c in
    let c' = Cursor.Cursor_storage.read_fully ~reset_index:true c in
    let _, h' (* recalculated *) = Cursor.compute_hash c' in
    assert (h = h')
  in
  let* ncommits = Commit_db.fold
    (fun commit ~parent i ->
      Lwt.map (fun () -> i+1) @@ f commit ~parent) commit_db 0 in
  Format.eprintf "%d commits done@." ncommits;
  let* () = Lwt_unix.unlink (prefix_tmp ^ ".commit_tree") in
  Lwt_unix.unlink (prefix_tmp ^ ".context")

let () =
  (* let open Alcotest in *)
  let promote = ref false in
  Arg.parse ["--promote", Arg.Set promote, "data promotion"] (fun _ -> ()) "test_commit_tree_file";
  if !promote then create_data_files ();
  data_file_test ()
