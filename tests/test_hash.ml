open Plebeia
open Test_utils
open Node
open Tester.Hash

let dump_hash ctxt n =
  let _, nh = Node.compute_hash ctxt n in
  Format.eprintf "%a@." Node.pp n;
  Format.eprintf "hash: %s@.@." (Hash.to_hex_string nh)

let test () =
  run_ignore_lwt @@ with_context @@ fun ctxt ->
  let l = new_leaf @@ Value.of_string "hello world" in
  dump_hash ctxt l;
  let i = new_internal (new_bud None) (new_bud None) in
  dump_hash ctxt i;
  let b = new_bud (Some i) in
  dump_hash ctxt b;
  let e = new_extender Segment.(of_sides [Right]) (new_bud None) in
  dump_hash ctxt e

let () =
  let open Alcotest in
  run "hash"
    [ "hash", ["test", `Quick, test]]
