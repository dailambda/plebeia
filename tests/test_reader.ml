(* Reader root catchup test *)
open Plebeia
open Lwt.Syntax
open Test_utils

let test () =
  run_lwt @@
  with_tempdir @@ fun d ->
  Format.eprintf "Using directory %s@." d;
  let tempfile = d ^/ "plebeia" in
  let* vc_writer = Result_lwt.get_ok_lwt @@ Vc.create Context.default_config tempfile in
  let* vc_reader = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read Context.default_config tempfile in
  let key = Option.get @@ Storage.writer_key (Vc.context vc_writer).storage in
  let* res = Vc.enable_process_sync vc_reader key in
  let () = Result.get_ok res in
  let c_writer = Vc.empty vc_writer in
  let c_writer = Result.get_ok @@ Deep.insert c_writer [Segment.StringEnc.encode "file1"] (Value.of_string "file1") in
  let* c_writer, _hp1, commit = Result_lwt.get_ok_lwt @@ Vc.commit vc_writer ~parent: None c_writer ~hash_override:None in
  let rhash1 = commit.hash in
  prerr_endline "rhash1";

  let* cur = Vc.checkout vc_writer rhash1 in
  let _c_writer = Option.get cur in
  prerr_endline "The writer knows rhash1";

  let* cur = Vc.checkout vc_reader rhash1 in
  let _, c_reader = Option.get cur in
  assert (snd @@ Result.get_ok @@ Deep.get_value c_reader [Segment.StringEnc.encode "file1"] = Value.of_string "file1");

  let c_writer = Result.get_ok @@ Deep.insert c_writer [Segment.StringEnc.encode "file2"] (Value.of_string "file2") in
  let* _c_writer, _hp2, commit = Result_lwt.get_ok_lwt @@ Vc.commit vc_writer ~parent: None c_writer ~hash_override:None in
  let rhash2 = commit.hash in
  prerr_endline "rhash2";
  let* cur = Vc.checkout vc_reader rhash2 in
  let _, c_reader = Option.get cur in
  assert (snd @@ Result.get_ok @@ Deep.get_value c_reader [Segment.StringEnc.encode "file1"] = Value.of_string "file1");
  assert (snd @@ Result.get_ok @@ Deep.get_value c_reader [Segment.StringEnc.encode "file2"] = Value.of_string "file2");

  (* Reader only with RDONLY fails the following ! *)
  prerr_endline "rhash1";
  let* _, c_reader = Lwt.map Option.get @@ Vc.checkout vc_reader rhash1 in
  assert (snd @@ Result.get_ok @@ Deep.get_value c_reader [Segment.StringEnc.encode "file1"] = Value.of_string "file1");

  let rec f n parent c_writer =
    if n = 100 then Lwt.return (parent, c_writer)
    else
      let rec g c_writer = function
        | 0 -> c_writer
        | m ->
            let c_writer =
              Result.get_ok @@ Deep.insert c_writer
                [Segment.StringEnc.encode @@ string_of_int n ;
                 Segment.StringEnc.encode @@ string_of_int m]
                (Value.of_string (string_of_int m))
            in
            g c_writer (m-1)
      in
      let c_writer = g c_writer 10000 in
      let* c_writer, _hp, commit = Result_lwt.get_ok_lwt @@ Vc.commit vc_writer ~parent: (Some parent) c_writer ~hash_override:None in
      let rhash = commit.hash in
      f (n+1) rhash c_writer
  in
  let* rhash_final, _c_writer = f 0 rhash2 c_writer in

  Format.eprintf "rhash_final: %a@." Commit_hash.pp rhash_final;
  let* _, c_reader = Lwt.map Option.get @@ Vc.checkout vc_reader rhash_final in
  prerr_endline "checked out";
  assert (snd @@ Result.get_ok @@ Deep.get_value c_reader [Segment.StringEnc.encode "file1"] = Value.of_string "file1");
  Lwt.return_unit

let () =
  let open Alcotest in
  run "reader"
    ["reader", ["test", `Quick, test ]]
