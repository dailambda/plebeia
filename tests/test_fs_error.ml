open Plebeia

(* Errors of the 2 modules must be identical *)
module F = Fs
module FT = Fs_tree

let enc = Fs_nameenc.bits8

let () =
  match
    FT.Op.fail (Other ("a", "b"))
      (FT.empty (Context.memory_only Context.default_config) enc)
  with
  | Ok _ -> assert false
  | Error (F.FsError.FS_error _) -> ()
  | Error _ -> assert false
