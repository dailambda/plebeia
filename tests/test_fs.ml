open Plebeia
open Test_utils

module Fs = Fs_simulation.WithSimulation

let enc = Fs_nameenc.bits8

let ( !/ ) s =
  match String.split_by_char (function '/' -> true | _ -> false) s with
  | "" :: ss -> List.filter (function "" -> false | _ -> true) ss
  | _ -> assert false

open Fs
open Fs.Op
open Fs.Op_lwt

module Test = struct
  type Error.t += Must_fail_but_succeeded

  let () =
    Error.register_printer
    @@ function
    | Must_fail_but_succeeded -> Some "must fail but succeeded" | _ -> None

  let must_fail : 'a op_lwt -> unit op_lwt = fun at cur ->
    let open Lwt.Syntax in
    let* res = at cur in
    match res with
    | Error e ->
        Format.eprintf "Ok error: %a@." Error.pp e;
        Lwt.return_ok (cur, ())
    | Ok _ ->
        Lwt.return_error Must_fail_but_succeeded

  let log fmt =
    Format.kasprintf (fun s c -> prerr_endline s; Op_lwt.return () c) fmt

  (* write a file *)
  let write s0 =
    let open Op_lwt.Syntax in
    let* () = log "write %s" s0 in
    let s = !/s0 in
    let v = Value.of_string "init" in
    let* () = lift @@ write s v in
    let* v' = lift @@ read s in
    assert (v = v') ;
    (* overwrite test *)
    let v = Value.of_string s0 in
    let* () = lift @@ write s v in
    let* v' = lift @@ read s in
    assert (v = v') ;
    return ()

  let rm ?recursive ?ignore_error s =
    let open Op_lwt.Syntax in
    let* () = log "rm %s" s in
    let s = !/s in
    let* res = lift @@ rm ?recursive ?ignore_error s in
    match res with
    | false -> return ()
    | true -> must_fail (lift @@ get s)

  let rmdir ?ignore_error s =
    let open Op_lwt.Syntax in
    let* () = log "rmdir %s" s in
    let s = !/s in
    let* res = lift @@ rmdir ?ignore_error s in
    match res with
    | false -> return ()
    | true -> must_fail (lift @@ get s)

  let cat ?expect s0 =
    let open Op_lwt.Syntax in
    let* () = log "cat %s" s0 in
    let s = !/s0 in
    let* v = lift @@ read s in
    let expect = match expect with None -> s0 | Some s -> s in
    if expect <> Value.to_string v then
      failwithf
        "cat %s failed actual: %S expected: %S"
        s0
        (Value.to_string v)
        expect ;
    return ()

  let ls dir expectedf =
    let open Op_lwt.Syntax in
    let* () = log "ls %s" dir in
    let* xs = ls !/dir in
    let xs = List.sort compare @@ List.map (fun (x,_) -> x) xs in
    if not @@ expectedf xs then (
      Format.eprintf "Ouch: %s@." (String.concat "; " xs) ;
      assert false ) ;
    return ()

  let copy from to_ =
    let open Op_lwt.Syntax in
    let* () = log "copy %s %s" from to_ in
    let from = !/from in
    let to_ = !/to_ in
    lift @@ copy from to_

  let f cursor = run cursor begin
      let open Op_lwt.Syntax in
      let open Op_lwt.Infix in
      (* root directory is not a file *)
      must_fail @@ cat "/" >>

      (* empty tree has nothing to list *)
      ls "/" (function [] -> true | _ -> false) >>

      (* simple write *)
      write "/protocol" >>

      (* mkdir if necessary *)
      write "/data/genesis_key" >>

      (* a file is not a directory *)
      must_fail @@ write "/protocol/foo" >>
      must_fail @@ write "/protocol/foo/bar" >>

      (* dirs are not files *)
      must_fail @@ cat "/" >>
      must_fail @@ cat "/data" >>

      (* a file does not have sub-files *)
      must_fail @@ cat "/data/genesis_key/foo" >>
      must_fail @@ cat "/data/genesis_key/foo/bar" >>

      (* non existent file *)
      must_fail @@ cat "/non-existent" >>

      (* removal of non existent file/directory *)
      must_fail @@ rm "/non-existent" >>
      rm ~ignore_error:true "/non-existent" >>
      must_fail @@ rmdir "/non-existent" >>
      rmdir ~ignore_error:true "/non-existent" >>

      must_fail @@ rm "/data/genesis_key/foo" >>
      must_fail @@ rmdir "/data/genesis_key/foo" >>

      ls "/data" (function ["genesis_key"] -> true | _ -> false) >>

      (* file cannot be rmdir'ed *)
      must_fail @@ rmdir "/data/genesis_key" >>
      ls "/data" (function ["genesis_key"] -> true | _ -> false) >>

      rm "/data/genesis_key" >>

      (* empty dir (/data) is removed automatically unless it is the root.
         /data must be gone *)
      ls "/" (function ["protocol"] -> true | _ -> false) >>

      write "/data/genesis_key" >>

      (* removal of directory *)
      must_fail @@ rm "/data" >>
      rm ~ignore_error:true "/data" >>
      rm ~recursive:true "/data" >>
      must_fail @@ rmdir  "/data" >>

      write "/data/a/b/c/d" >>
      write "/data/a/b/e/f" >>
      ls "/data/a" (function ["b"] -> true | _ -> false) >>

      rmdir "/data/a/b" >>
      must_fail @@ cat "/data/a/b/c/d" >>
      must_fail @@ cat "/data/a/b/e/f" >>

      write "/a/b" >>
      ls "/a" (function ["b"] -> true | _ -> false) >>
      copy "/a" "/a2" >>
      ls "/a" (function ["b"] -> true | _ -> false)  >>
      ls "/a2" (function ["b"] -> true | _ -> false) >>
      rmdir "/a"  >>
      rmdir "/a2" >>

      write "/data/contracts/0/a/b/c/d" >>
      write "/data/contracts/0/a/b/e/f" >>
      copy "/data/contracts/0" "/data/contracts/1" >>
      cat ~expect:"/data/contracts/0/a/b/c/d" "/data/contracts/1/a/b/c/d" >>
      cat ~expect:"/data/contracts/0/a/b/e/f" "/data/contracts/1/a/b/e/f" >>

      (* [copy] cannot copy non existent *)
      must_fail @@ copy "/non-existent" "/data/contracts/3" >>

      (* [copy] CAN copy a file *)
      copy "/data/contracts/0/a/b/c/d" "/data/contracts/2/a/b/c/d" >>

      (* [copy] CAN overwrite a directory *)
      copy "/data/contracts/0" "/data/contracts/3" >>

      (* [copy] CANNOT create a loop *)
      must_fail @@ copy "/data/contracts/0" "/data/contracts/0/x/y/z" >>

      ls "/" (function ["data"; "protocol"] -> true | _ -> false) >>
      ls "/data" (function ["contracts"] -> true | _ -> false) >>
      ls "/data/contracts" (function ["0"; "1"; "2"; "3"] -> true | _ -> false) >>

      (* cannot ls a file *)
      must_fail @@ ls "/protocol" (fun _ -> true) >>
      must_fail @@ ls "/protocol/foo/bar" (fun _ -> true) >>
      must_fail @@ ls "/non-existent" (fun _ -> true) >>
      must_fail @@ ls "/data/non-existent" (fun _ -> true) >>

      log "fold'" >>
      let* _xs =
        fold' [] !/"/"
          ( (fun acc path c ->
                let cur = get_real_cursor c in
                match snd @@ Cursor.view cur with
                | Bud _ -> Lwt.return_ok acc
                | Leaf _ -> Lwt.return_ok (path :: acc)
                | _ -> assert false),
            (fun path c acc ->
               match get_model_tree c with
               | Dir _ -> Lwt.return_ok acc
               | File _ -> Lwt.return_ok (path :: acc)) )
      in
      log "fold' done" >>
      (* XXX we have no check of the correctness of [xs] *)

(*
   XXX local access does not match well with the module

       do_then (fun _ -> Format.eprintf "local@.") @@

       lwt (get !/"/data/contracts/1")
       >>= (fun (c,_v) ->
           fun c0 ->
             Lwt.bind
               (run c (
                   do_then (fun _ -> Format.eprintf "cat a/b/e/f@.") @@
                   lwt (Fs.cat !/"a/b/e/f") >>= fun v ->
                   assert (Value.to_string v = "/data/contracts/0/a/b/e/f");
                   do_then (fun _ -> Format.eprintf "write a/b/e/f ...@.") @@
                   write "a/b/e/f" >>
                   do_then (fun _ -> Format.eprintf "cat a/b/e/f ...@.") @@
                   lwt (Fs.cat !/"a/b/e/f") >>= fun v ->
                   assert (Value.to_string v = "/a/b/e/f");
                   do_then (fun _ -> Format.eprintf "cat /a/b/e/f ok@.") @@
                   return ()))
               (function
                 | Error e ->
                     Format.eprintf "Error %a@." Error.pp e; assert false
                 | Ok (c_1,()) -> Lwt.return (Ok (c0, c_1))))
       >>= fun (c_1,_v) ->

       (* the local tree modification does not affect the original *)
       do_then (fun _ -> Format.eprintf "check the original@.") @@
       lwt (Fs.cat !/"/data/contracts/1/a/b/e/f")
       >>= fun v ->
       assert (Value.to_string v = "/data/contracts/0/a/b/e/f");

       do_then (fun _ -> Format.eprintf "setting@.") @@
       lwt (set !/"/data/contracts/1" c_1) >>

       do_then (fun _ -> Format.eprintf "checking...@.") @@
       lwt (Fs.cat !/"/data/contracts/1/a/b/e/f")
       >>= fun v ->
       assert (Value.to_string v = "/a/b/e/f");
       do_then (fun _ -> Format.eprintf "checked@.") @@
*)

       (* check the past bugs are fixed *)
       write "/f/g/h" >>
       must_fail @@ write "/f"
    end

  let test () =
    let open Result_lwt.Syntax in
    run_ignore_lwt @@ with_context @@ fun ctxt ->
    let*= res = f (empty ctxt enc) in
    match res with
    | Ok _ -> Lwt.return_unit
    | Error e ->
        Format.eprintf "Error : %a@." Error.pp e;
        assert false

  let do_random_test ?(verbose=false) rng n =
    let open Lwt.Syntax in
    with_context_lwt @@ fun ctxt ->
    let f c =
      let open Gen in
      let open Gen.Infix in
      let path =
        let name =
          int 23 >>= fun i ->
          let c = Char.chr (Char.code 'a' + i) in
          return @@ String.make 1 c
        in
        list (int_range (1, 8)) name >>| fun ns ->
        String.concat "/" ("" :: ns)
      in
      let value =
        int 100 >>| fun i ->
        Value.of_string @@ string_of_int i
      in
      let read p =
        match Fs.Op.read !/p c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let get p =
        match Fs.Op.get !/p c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let write p v =
        match Fs.Op.write !/p v c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let rm p =
        match Fs.Op.rm ~recursive:true !/p c with
        | Error _ -> c
        | Ok (c, _) -> c
      in
      let ls p =
        let* res = Fs.Op_lwt.ls !/p c in
        match res with
        | Error _ -> Lwt.return c
        | Ok (c, _) -> Lwt.return c
      in
      let copy p p' =
        match Fs.Op.get !/p c with
        | Error _ -> c
        | Ok (c, (cp,_v)) ->
            match Fs.Op.set !/p' cp c with
            | Error _ -> c
            | Ok (c, ()) -> c
      in
      let command =
        let op =
          int 6 >>= function
          | 0 ->
              path >>| fun p -> `read p
          | 1 ->
              path >>= fun p ->
              value >>| fun v ->
              `write (p, v)
          | 2 ->
              path >>| fun p -> `rm p
          | 3 ->
              path >>| fun p -> `ls p
          | 4 ->
              path >>= fun p ->
              path >>| fun p' ->
              `copy (p, p')
          | 5 ->
              path >>| fun p -> `get p
          | _ -> assert false
        in
        op rng
      in
      match command with
      | `read p ->
          if verbose then Format.eprintf "read %s@." p;
          Lwt.return @@ read p
      | `get p ->
          if verbose then Format.eprintf "get %s@." p;
          Lwt.return @@ get p
      | `write (p,v) ->
          if verbose then Format.eprintf "write %s %s@." p (Value.to_string v);
          Lwt.return @@ write p v
      | `rm p ->
          if verbose then Format.eprintf "rm %s@." p;
          Lwt.return @@ rm p
      | `ls p ->
          if verbose then Format.eprintf "ls %s@." p;
          ls p
      | `copy (p, p') ->
          if verbose then Format.eprintf "copy %s %s@." p p';
          Lwt.return @@ copy p p'
    in
    let rec loop i c =
      if i mod 1000 = 0 then begin
        let _, h = Result.get_ok @@ compute_hash c in
        Format.eprintf "hash %d: %a@." i Hash.pp h;
      end;
      if i = n then Lwt.return @@ compute_hash c
      else
        let* c = f c in
        loop (i+1) c
    in
    let* ret = loop 0 (empty ctxt enc) in
    Lwt.return @@ snd @@ Result.get_ok ret

  let random_test () =
    run_ignore_lwt @@ with_random_lwt @@ fun rng ->
    do_random_test rng 50_000

  let regression_test () =
    run_ignore_lwt @@ Lwt.map (regression_by_hash __LOC__ 1047052) @@
    do_random_test (RS.make [| 0 |]) 50_000
end

let () =
  let open Alcotest in
  run "fs"
    [ "unit", [ "test", `Quick, Test.test ]
    ; "random", [ "random_test", `Quick, Test.random_test ]
    ; "regression", [ "regression", `Quick, Test.regression_test ]
    ]
