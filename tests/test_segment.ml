open Plebeia.Internal
open Test_utils
open Segment

let test_normalization () =
  let gen =
    let size = Gen.int Segment.max_length in
    Gen.segment size
  in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let seg = gen rng in
    let seg' = normalize seg in
    assert (to_sides seg = to_sides seg')
  done

(* XXX This does not check all the combinations... *)

(*
let test_compare () =
  let rec create len =
    if len = 0 then []
    else
      let side = if len mod 2 = 0 then Left else Right in
      side :: create (len - 1)
  in
  for i = 0 to 100 do
    let lst = create i in
    let seg0 = normalize @@ of_sides lst in
    let seg1 = normalize @@ of_sides (lst @ [Left]) in
    let seg2 = normalize @@ of_sides (lst @ [Right]) in
    let c10 = Segment.compare seg1 seg0 in
    assert (-c10 = Segment.compare seg0 seg1);
    let c02 = Segment.compare seg0 seg2 in
    assert (-c02 = Segment.compare seg2 seg0);
    let c12 = Segment.compare seg1 seg2 in
    assert (-c12 = Segment.compare seg2 seg1)
  done

let test_compare_random () =
  let gen =
    let size = Gen.int Segment.max_length in
    Gen.segment size
  in
  for _ = 0 to 1000 do
    with_random @@ fun st ->
      let seg1 = gen st in
      let seg2 = gen st in
      let seg1 = normalize seg1 in
      let seg2 = normalize seg2 in
      let expected = Stdlib.compare (to_sides seg1) (to_sides seg2) in
      let actual = compare seg1 seg2 in
      assert (expected = actual)
  done
*)

let test_slice () =
  let open Segment in
  let gen =
    let size = Gen.int Segment.max_length in
    Gen.segment size
  in
  with_random @@ fun rng ->
  for _ = 0 to 1000 do
      let seg = gen rng in
      let seg' = normalize seg in
      assert (equal seg seg');
      Format.eprintf "failed@.  %a@.  %a@." pp seg pp seg';
      match cut seg, cut seg' with
      | Some (side, sega), Some (side', sega') ->
          let fail () =
            Format.eprintf "failed@.  %a@.  %a@.  %a@.  %a@." pp seg pp sega pp seg' pp sega';
            assert false
          in
          if not (side = side') then fail ();
          if not (equal sega sega') then fail ();
          if not (to_sides sega = to_sides sega') then fail ()
      | None, None -> ()
      | _ -> assert false
  done

let () =
  let open Alcotest in
  run "segment"
    [ "segment normalization", ["normalization", `Quick, test_normalization]
(*
    ; "segment compare", ["test", `Quick, test_compare]
    ; "segment compare random", ["random_test", `Quick, test_compare_random]
*)
    ; "segment slice", ["test_slice", `Quick, test_slice]
    ]
