open Plebeia
open Commit_tree
open Test_utils

let gen_entry =
  let open Gen in
  let open Gen.Syntax in
  let open Gen.Infix in
  let* parent =
    let* b = bool in
    if b then Commit_hash.gen >|= fun x -> Some x
    else return None
  in
  let* index = int 1_000_000 >|= Index.Unsafe.of_int in
  return { parent; index }

let fn = "commit_tree.data"

let create_data_files () =
  let open Lwt.Syntax in
  run_lwt @@
  let rng = RS.make [| 0 |] in
  Format.eprintf "writing to %s@." fn;
  let* key = Storage.make_new_writer_key [] in
  let* t = create ~resize_step_bytes:1_000 ~key fn in
  let rec f i t = match i with
    | 0 -> t
    | i ->
        let entry = gen_entry rng in
        f (i-1) @@ add t (Commit_hash.gen rng) entry
  in
  let t = f 1000 t in
  let () = Result.get_ok @@ write t in
  Result_lwt.get_ok_lwt @@ close t

(* The regression hash is based on the old Index.t = uint32 *)
type old_entry = { parent : Commit_hash.t option; index : Stdint.Uint32.t }
[@@ocaml.warning "-69"]

let old_entry Commit_tree.{ parent; index } =
  { parent; index= Stdint.Uint32.of_int (Index.to_int index) }

let data_file_test () =
  let open Lwt.Syntax in
  run_lwt @@ Lwt.map (regression_by_hash __LOC__ 797260631) @@
  (* The data file is copied by dune without the write permission.
     We need to create another copy to open with O_RDWR *)
  let fn' = fn ^ ".tmp" in
  let* () = copy_file fn fn' in
  let* t = Lwt.map Result.get_ok @@ open_existing_for_read fn' in
  let* res = fold (fun h e acc -> Lwt.return @@ (h,e)::acc) t [] in
  let* () = Lwt_unix.unlink fn' in
  Lwt.return (List.map (fun (ch, ent) -> ch, old_entry ent) res)

let () =
  let open Alcotest in
  let promote = ref false in
  Arg.parse ["--promote", Arg.Set promote, "data promotion"] (fun _ -> ()) "test_commit_tree_file";
  if !promote then create_data_files ();
  run "commit_tree_file"
    ["commit_tree_file", [ "data_file_loading", `Quick, data_file_test ]]
