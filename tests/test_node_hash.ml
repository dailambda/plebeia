open Plebeia
open Test_utils

let test () =
  run_ignore_lwt @@ with_random_lwt @@ fun rng ->
  with_context @@ fun ctxt ->
  for _ = 0 to 100 do
    let n = Node_type.gen_bud 10 rng in
    let _, nh = Node.compute_hash ctxt n in
    let _, nh' =
      Node_hash.compute ctxt.hasher
        (* simplified function which visits all the subnodes even already hashed *)
        (function
          | Hash _ -> assert false
          | Disk index -> `Not_Hashed (Node.read_node ctxt index)
          | View v -> `Not_Hashed v) n
    in
    assert (nh = nh');
  done

let () =
  let open Alcotest in
  run "node_hash"
    ["node_hash", ["test", `Quick, test]]
