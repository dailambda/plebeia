open Plebeia
open Test_utils
open Info

let check info =
  match decode (encode info) with
  | None -> assert false
  | Some info' -> assert (Info.equal info info')

let test () =
  check { copies= [] };
  check { copies= [ Path.of_segments [ from_Some @@ Segment.of_string "L" ]] };
  check { copies= [ Path.of_segments [ from_Some @@ Segment.of_string "LRLRLR" ]] };
  check { copies= [ Path.of_segments [ from_Some @@ Segment.of_string "LRLRLR"
                    ; from_Some @@ Segment.of_string "RLRLRL" ]] };
  with_random @@ fun st ->
  let copies =
    let open Gen in
    (list (int_range (0,10))
     @@ map Path.of_segments
     @@ list (int_range (1, 10))
     @@ segment @@ int_range (1, Segment.max_length)) st
  in
  check { copies }

let () =
  let open Alcotest in
  run "info_encoding"
    ["info_enconding", ["test", `Quick, test]]
