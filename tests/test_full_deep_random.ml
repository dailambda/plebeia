open Test_utils
open Plebeia
open Cursor

(* check of fold *)
let check_first_buds_and_leaves (Cursor (trail, n, context) as c) =
  assert (trail = _Top);
  let set1 =
    match Node_storage.view context n with
    | Bud (None, _, _) -> []
    | Bud (Some n, _, _) ->
        let rec aux st = function
          | [] -> st
          | (seg,n)::ns ->
              match Node_storage.view context n with
              | Bud _ -> aux (`Bud (List.rev seg) :: st) ns
              | Leaf _ -> aux (`Leaf (List.rev seg) :: st) ns
              | Internal (n1, n2, _, _) ->
                  aux st ((Segment.Left::seg,n1)::(Segment.Right::seg,n2)::ns)
              | Extender (seg', n, _, _) ->
                  aux st ((List.rev_append (Segment.to_sides seg') seg,n)::ns)
        in
        List.sort compare @@ aux [] [([],n)]
    | _ -> assert false
  in
  let set2 =
    match Result.get_ok @@ Cursor.go_below_bud c  with
    | None -> []
    | Some c ->
        List.sort compare
        @@ Cursor.fold ~init:[] c (fun acc c ->
            let _, v = Cursor.view c in
            match v with
            | Bud _ -> `Up, (`Bud (Segment.to_sides @@ local_segment_of_cursor c)::acc)
            | Leaf _ -> `Up, (`Leaf (Segment.to_sides @@ local_segment_of_cursor c)::acc)
            | _ -> `Continue, acc)
  in
  let print = function
    | `Bud seg -> Format.eprintf "B %s@." (Segment.to_string @@ Segment.of_sides seg)
    | `Leaf seg -> Format.eprintf "L %s@." (Segment.to_string @@ Segment.of_sides seg)
  in
  if set1 <> set2 then begin
    Format.eprintf "Set1@.";
    List.iter print set1;
    Format.eprintf "Set2@.";
    List.iter print set2;
    Debug.save_cursor_to_dot "folder.dot" c;
    assert false
  end

(* check of fold *)
let check_nodes (Cursor (trail, n, context) as c) =
  assert (trail = _Top);
  let set1 =
    let rec aux st = function
      | [] -> st
      | (seg,n)::ns ->
          match Node_storage.view context n with
          | Bud (None, _, _) -> aux (`Bud (List.rev seg)::st) ns
          | Bud (Some n1, _, _) -> aux (`Bud (List.rev seg)::st) ((seg,n1)::ns)
          | Leaf _ -> aux (`Leaf (List.rev seg)::st) ns
          | Internal (n1, n2, _, _) ->
              aux (`Internal (List.rev seg)::st) ((Segment.Left::seg,n1)::(Segment.Right::seg,n2)::ns)
          | Extender (seg', n1, _, _) ->
              aux (`Extender (List.rev seg)::st) ((List.rev_append (Segment.to_sides seg') seg,n1)::ns)
    in
    List.sort compare @@ aux [] [([],n)]
  in
  let set2 =
    List.sort compare @@
    Cursor.fold ~init:[] c (fun acc c ->
        let _, v = Cursor.view c in
        let seg = List.concat @@ List.map Segment.to_sides @@ Path.to_segments @@ path_of_cursor c in
        match v with
        | Bud _ -> `Continue, (`Bud seg::acc)
        | Leaf _ -> `Continue, (`Leaf seg::acc)
        | Internal _ -> `Continue, (`Internal seg::acc)
        | Extender _ -> `Continue, (`Extender seg::acc))
  in
  let print = function
    | `Bud seg -> Format.eprintf "B %s@." (Segment.to_string @@ Segment.of_sides seg)
    | `Leaf seg -> Format.eprintf "L %s@." (Segment.to_string @@ Segment.of_sides seg)
    | `Internal seg -> Format.eprintf "I %s@." (Segment.to_string @@ Segment.of_sides seg)
    | `Extender seg -> Format.eprintf "E %s@." (Segment.to_string @@ Segment.of_sides seg)
  in
  if set1 <> set2 then begin
    Format.eprintf "ERROR!@.";
    Format.eprintf "Set1@.";
    List.iter print set1;
    Format.eprintf "Set2@.";
    List.iter print set2;
    Debug.save_cursor_to_dot "folder.dot" c;
    assert false
  end

let test () =
  with_random @@ fun rng ->
  for i = 0 to 100 do
    run_ignore_lwt @@ with_cursor @@ fun c ->
    let c, ops = Do_random.Deep.do_random ~commit:true rng 1000 c in
    (* check folder *)
    check_first_buds_and_leaves c;
    check_nodes c;
    if i mod 100 = 0 then begin
      Format.eprintf "%d done (%d ops)@." i
        (List.length ops);
      Debug.save_cursor_to_dot (Printf.sprintf "random%d.dot" i) c
    end
  done

let regression_test () =
  run_lwt @@ Lwt.map (regression_by_hash __LOC__ 715728153) begin
    let rng = RS.make [| 0 |] in
    with_cursor @@ fun c ->
    let c, _ops = Do_random.Deep.do_random ~commit:true rng 1000 c in
    (* check folder *)
    check_first_buds_and_leaves c;
    check_nodes c;
    snd @@ Cursor.compute_hash c
  end

let () =
  let open Alcotest in
  run "full_deep_random"
    ["full_deep_random", [ "test", `Quick, test
                         ; "regression", `Quick, regression_test ]]
