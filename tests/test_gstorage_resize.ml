(* resize never requires msync.  This test checks it. *)

open Plebeia
open Test_utils
open Lwt.Syntax
open Storage

let test () =
  run_lwt @@ with_random @@ fun rng ->
  let fn = temp_file "plebeia" "storage" in
  let cell_size = 10 in
  let* key = Storage.make_new_writer_key [] in
  let* t =
    create ~length:(Index.Unsafe.of_int 100) ~resize_step_bytes: 50
      ~config: { head_string= "PLEBEIA TEST xxxxxxx";
                 version= 0;
                 bytes_per_cell= cell_size;
                 max_index= Index.max_index }
      ~key
      fn
  in
  let rec loop is = function
    | 0 -> ()
    | n ->
        begin match is with
          | None -> ()
          | Some (i, s) ->
              let s' = Mmap.Buffer.to_string @@ get_cell t i in
              (* crashes if a new mmap has different contents
                 from the previous *)
              assert (s = s')
        end;
        let i = Result.get_ok @@ new_index t in (* this may resize the storage *)
        let b = get_cell t i in
        let s = Gen.(string (return cell_size) alpha_numeric) rng in
        Mmap.Buffer.blit_from_string s 0 b 0 cell_size;
        loop (Some (i,s)) (n-1)
  in
  loop None 300_000;
  Lwt.return_unit

let () =
  let open Alcotest in
  run "gstorage_resize"
    ["gstorage_resize", ["test", `Quick, test]]
