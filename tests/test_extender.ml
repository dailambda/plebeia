open Plebeia
open Test_utils
open Cursor
open Node

let commit_and_load c =
  let Cursor (tr, _n, context, info), i, _ = Result.get_ok @@ Cursor_storage.write_top_cursor c in
  let n = View (read_node context i Not_Extender) in
  _Cursor (tr, n, context, info)

let test () =
  run_ignore_lwt @@ with_cursor @@ fun c ->
  let c = ok_or_fail @@ upsert c (path "LLRR") (value "LLRR") in
  let c = ok_or_fail @@ upsert c (path "LLLLLL") (value "LLLLLL") in
  let c = ok_or_fail @@ upsert c (path "LLLLRR") (value "LLLLRR") in
  save_cursor_to_dot "ext1.dot" c;
  let c = commit_and_load c in
  save_cursor_to_dot "ext2.dot" c;
  let c = ok_or_fail @@ delete c (path "LLRR") in (* We got here Fatal error: exception (Failure "Extender: cannot have Disk with Maybe_Extender"), because of a bug of remove_up *)
  save_cursor_to_dot "ext3.dot" c;
  ignore c

let () =
  let open Alcotest in
  run "extender"
    ["extender", ["test", `Quick, test]]
