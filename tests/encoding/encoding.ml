open Plebeia

let dump pp enc x =
  Format.eprintf "%a : %s : %s@."
    pp x
    (let `Hex h = Hex.of_bytes @@ Data_encoding.Binary.to_bytes_exn enc x in h)
    Data_encoding.Json.(to_string @@ construct enc x)

let () =
  dump Segment.pp Segment.encoding Segment.empty;
  dump Segment.pp Segment.encoding Segment.(of_sides [Left; Right; Left; Right]);
  dump Segment.pp Segment.encoding Segment.(of_sides [Left; Right; Left; Right; Left; Right; Left; Right]);
  dump Segment.pp Segment.encoding Segment.(of_sides [Left; Right; Left; Right; Left; Right; Left; Right; Left]);
  dump Segment.pp Segment.encoding Segment.(of_sides (List.init 20 (fun _ -> Right)));
  prerr_endline "";

  Snapshot.Internal.Elem.(
    dump pp (encoding 28) End; (* 00 *)
    dump pp (encoding 28) BudNone; (* 01 *)
    dump pp (encoding 28) (Extender Segment.(of_sides [Left; Right; Left; Right; Left; Right; Left; Right])); (* 04, encoding of segment *)
    dump pp (encoding 28) (HashOnly (Hash.of_hex_string "0123456789abcdef0123456789abcdef0123456789abcdef01234567", "")); (* 07, hash, empty segment *)
    dump pp (encoding 28)
      (HashOnly (Hash.of_hex_string "0123456789abcdef0123456789abcdef0123456789abcdef01234567",
                 Segment.(Serialization.encode @@ of_sides [Left; Right; Left; Right; Left; Right; Left; Right]))));

  prerr_endline "";

  Value.(dump pp encoding @@ of_string "");
  Value.(dump pp encoding @@ of_string "abc");
  Value.(dump pp encoding @@ of_string "hi");
  Value.(dump pp encoding @@ of_string "\001");
(*
  Value.(dump pp encoding @@ of_string @@ String.make 1024 '\002');
*)
  (* It crashes.  Data_encoding's maximum string size is int30
     Value.(dump pp encoding @@ of_string @@ String.make 2147483648 '\002')
  *)
  prerr_endline "";

  Snapshot.Internal.Elem.(
    dump pp (encoding 28) (Cached 11l); (* tag 06 *)
  )
