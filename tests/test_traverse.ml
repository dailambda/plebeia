open Plebeia
open Test_utils

let test () =
  run_ignore_lwt @@ with_random_lwt @@ fun st ->
  with_context @@ fun ctxt ->
    for i = 0 to 500 do
      let n = Node_type.gen_bud 50 st in
      let sns_ = Node_tools.ls ctxt n in
      let segs_ =
        List.sort Stdlib.compare
        @@ List.map (fun (seg, _) ->
            Segment.to_sides seg) sns_
      in
      let sns = Node_tools.ls_rec ctxt n in
      let segs =
        List.sort Stdlib.compare
        @@ List.map (fun (segs, _) ->
            List.map Segment.to_sides segs) sns
      in
      let sns_', _ = Result.get_ok @@ Cursor_tools.ls (Cursor.(_Cursor (_Top, n, ctxt))) in
      let _, sns_'' = Result.get_ok @@ Cursor_tools.GenTraverse.ls (Cursor.(_Cursor (_Top, n, ctxt))) in
      (* Since the cursor is a bud, [ls] must return itself *)
      assert (List.length sns_' = 1);
      assert (List.length sns_'' = 1);

      let segs_' =
        List.sort Stdlib.compare
        @@ List.map (fun (seg, _, _) ->
            Segment.to_sides seg) sns_'
      in
      assert (segs_ = segs_');

      let sns', _ = Result.get_ok @@ Cursor_tools.ls_rec (Cursor.(_Cursor (_Top, n, ctxt))) in
      let _, sns'' = Result.get_ok @@ Cursor_tools.GenTraverse.ls_rec (Cursor.(_Cursor (_Top, n, ctxt))) in
      let segs' =
        List.sort Stdlib.compare
        @@ List.map (fun (segs, _, _) ->
            List.map Segment.to_sides segs) sns'
      in
      let segs'' =
        List.sort Stdlib.compare
        @@ List.map (fun (segs, _, _) ->
            List.map Segment.to_sides segs) sns''
      in
      assert (segs = segs');
      assert (segs = segs'');

      if i mod 100 = 0 then Format.eprintf "%d done@." i
    done

let () =
  let open Alcotest in
  run "traverse"
    ["traverse", ["test", `Quick, test]]
