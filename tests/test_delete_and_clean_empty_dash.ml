open Plebeia
open Test_utils
open Cursor
open Node

let insert c ss = Deep.insert c ss (Value.of_string (Segment.string_of_segments ss))
let delete = Deep.delete_and_clean_empty'

let test_simple () =
  let open Result.Infix in
  run_ignore_lwt @@ with_cursor @@ fun c ->
  ok_or_fail begin
    insert c [path "LL"; path "RR"; path "LLRR"; path "RRLL"]
    >>= fun c ->
    delete c [path "LL"; path "RR"; path "LLRR"; path "RRLL"]
    >>= fun c ->
    match Cursor.view c with
    | Cursor (Top, _, _), Bud (None, _, _ ) -> Ok ()
    | _ -> assert false
  end

let test2 () =
  let open Result.Infix in
  run_ignore_lwt @@ with_cursor @@ fun c ->
  ok_or_fail begin
    insert c [path "LL"; path "RR"; path "LLRR"; path "RRLL"]
    >>= fun c ->
    insert c [path "LL"; path "RR"; path "LRLR"; path "LLRR"]
    >>= fun c ->
    delete c [path "LL"; path "RR"; path "LLRR"; path "RRLL"]
    >>= fun c ->
    (* LL RR LLRR must not exist *)
    (match Deep.get c [path "LL"; path "RR"; path "LLRR"] with
     | Ok _ -> assert false
     | Error _ -> ());
    (* LL RR LRLR must exist *)
    (match Deep.get c [path "LL"; path "RR"; path "LRLR"] with
     | Ok (_, `Bud _) -> ()
     | Ok _ -> assert false
     | Error _ -> assert false);
    Ok ()
  end

let () =
  let open Alcotest in
  run "deep" [
    "delete_and_clean_empty_dash", [
      "simple", `Quick, test_simple;
      "2", `Quick, test2
    ]
  ]
