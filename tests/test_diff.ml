open Plebeia
open Test_utils

open Node_type

let between_random_trees () =
  run_ignore_lwt @@ with_random_lwt @@ fun st ->
  with_context @@ fun ctxt ->
  let n = 5000 in
  for _ = 1 to n do
    let n = gen_bud 10 st in
    let n' = gen_bud 10 st in
    let _v', nh' = Node.compute_hash ctxt n' in
    let diffs = Diff.diff ctxt n n' in

    let dump () =
      Debug.save_node_to_dot "src.dot" n;
      Debug.save_node_to_dot "dst.dot" n';
      Format.(eprintf "@[%a@]@." (list ";@ " Diff.pp) diffs);
    in

    let c = Cursor.(_Cursor (_Top, n, ctxt)) in
    match
      let open Result.Infix in
      Result.fold_leftM (fun (c,i) d ->
          Diff.apply c d >>= fun c ->
          Ok (c,i+1)) (c, 0) diffs
    with
    | exception e ->
        dump ();
        raise e
    | Error e ->
        Format.eprintf "Error: %a@." Error.pp e;
        dump ();
        assert false
    | Ok (c,_) ->
        let _c, nh = Cursor.compute_hash c in
        if nh <> nh' then begin
          dump ();
          assert false
        end
  done;
  Format.eprintf "%d between_random_trees done@." n

(* Tricky

     Extender (long_seg,
       Internal (left,
                 Extender (long_seg', n)))

   Removing the Internal, we will have an Extender (long_seg+R+long_seg', n)
   and the resulting segment may become longer than the limit
   [Segment.max_length]
                                                             .
*)
let between_resemble_trees () =
  run_ignore_lwt @@ with_random_lwt @@ fun st ->
  with_context @@ fun ctxt ->
  let n = 5000 in
  for _ = 1 to n do
    let n = gen_bud 20 st in

    let n' = make_resemble st ctxt n in

    let _v', nh' = try Node.compute_hash ctxt n' with
      | e ->
      Debug.save_node_to_dot "dst.dot" n';
      raise e
    in
    let diffs = Diff.diff ctxt n n' in

    (*
    Format.eprintf "diffs %d (add %d) (del %d)@." (List.length diffs)
      (List.length (List.filter (function Diff.Add _ -> true | _ -> false) diffs))
      (List.length (List.filter (function Diff.Del _ -> true | _ -> false) diffs));
    *)

    let dump () =
      Debug.save_node_to_dot "src.dot" n;
      Debug.save_node_to_dot "dst.dot" n';
      Format.(eprintf "@[%a@]@." (list ";@ " Diff.pp) diffs);
    in

    let c = Cursor.(_Cursor (_Top, n, ctxt)) in
    match
      let open Result.Infix in
      Result.fold_leftM (fun (c,i) d ->
          (*          Format.eprintf "%d %a@." i Diff.pp d; *)
          Diff.apply c d >>= fun c ->
          (* Debug.save_cursor_to_dot (Printf.sprintf "app%03d.dot" i) c; *)
          Ok (c,i+1)) (c, 0) diffs
    with
    | exception e ->
        dump ();
        raise e
    | Error e ->
        Format.eprintf "Error: %a@." Error.pp e;
        dump ();
        assert false
    | Ok (c,_) ->
        let _c, nh = Cursor.compute_hash c in
        if nh <> nh' then begin
          dump ();
          assert false
        end
  done;
  Format.eprintf "%d between_resemble_trees done@." n

let () =
  let open Alcotest in
  run "diff"
    ["diff",
     ["random", `Quick, between_random_trees;
      "resemble", `Quick, between_resemble_trees]]
