(* node storage test *)
open Plebeia
open Test_utils

let test () =
  run_ignore_lwt @@ with_random_lwt @@ fun st ->
  with_context @@ fun c ->
  for _ = 1 to 100 do
    Storage.Chunk.test_write_read st c.Context.storage;
  done

let () =
  let open Alcotest in
  run "chunk"
    ["chunk", ["test", `Quick, test]]
