open Plebeia.Internal
open Test_utils

module Ser = Segment.Serialization

let alcotest_seg = Alcotest.testable Segment.pp Segment.equal

let encoding_test seg =
  let h = Ser.encode seg in
  let seg' = Ser.decode_exn h in
(*
  Format.eprintf "Seg    : %a@." Segment.pp seg;
  Format.eprintf "Seg    : %a@." Segment.pp (Segment.normalize seg);
  Format.eprintf "Enc    : %d@." (String.length h);
  Format.eprintf "Enc    : %S@." h;
*)
  Alcotest.check alcotest_seg "encoding_test" seg seg'

let test_correctness st =
  (* try shorter first to get easier errors *)
  for _ = 1 to 5000 do
    let seg = Gen.(segment @@ int_range (1, 20)) st in
    encoding_test seg
  done;
  for _ = 1 to 10000 do
    let seg = Gen.(segment @@ int_range (1, Segment.max_length)) st in
    encoding_test seg
  done

let test () = with_random test_correctness

let test_list () =
  with_random @@ fun st ->
  let segments =
    let open Gen in
    list (int_range (0,255)) @@ segment @@ int_range (1, Segment.max_length)
  in
  for _ = 1 to 100 do
    let segs = segments st in
    let segs' = Option.get @@ Ser.decode_list @@ Ser.encode_list segs in
    assert (List.for_all2 (fun seg seg' -> Segment.equal seg seg') segs segs')
  done

let () =
  let open Alcotest in
  run "segment_encoding"
    ["segment_enconding", ["test", `Quick, test];
     "segment_list_enconding", ["test_list", `Quick, test_list]]
