open Plebeia
open Lwt.Syntax
open Test_utils

(* No branching *)
let build_file rng d conf i =
  let* vc = Result_lwt.get_ok_lwt @@ Vc.create conf (d ^/ "src") in
  let cur = Vc.empty vc in
  let rec loop parents (Cursor.Cursor (_, n, ctxt) as _cur) = function
    | 0 -> Lwt.return_unit
    | i ->
        let parent, parents =
          (* genesis at 1/100 *)
          if Gen.int 100 rng = 0 then None, parents
          else
            match Gen.shuffle parents rng with
            | p::ps ->
                begin match Gen.int 20 rng with
                  | 0 | 1 -> Some p, p::ps (* branch at 1/10 *)
                  | 2 ->
                      (* kill branch at 1/20 *)
                      begin match ps with
                        | _::ps ->
                            Some p, ps
                        | [] -> Some p, []
                      end
                  | _ -> Some p, ps
                end
            | [] -> None, []
        in
        let n' = make_resemble rng ctxt n in
        let cur' = Cursor.(_Cursor (_Top, n', ctxt)) in
        let* cur', _hp, commit =
          Result_lwt.get_ok_lwt @@
          Vc.commit
            vc
            ~parent
            ~hash_override: None
            cur'
        in
        let parents = commit.hash :: parents in
        loop parents cur' (i-1)
  in
  let* () = loop [] cur i in
  Result_lwt.get_ok_lwt @@ Vc.close vc

let copy_file rng d conf =
  let* vc_src = Result_lwt.get_ok_lwt @@ Vc.open_existing_for_read conf (d ^/ "src") in
  let* vc_dst = Result_lwt.get_ok_lwt @@ Vc.create conf (d ^/ "dst") in
  let* commits = Commit_db.to_list @@ Vc.commit_db vc_src in

  (* intentinally remove away some (5%) commits *)
  let commits =
    List.filter (fun _ -> Gen.int 20 rng <> 0) commits
  in

  let* res = Copy.copy vc_src commits vc_dst in
  match res with
  | Error e ->
      let* () = Lwt_fmt.eprintf "ERROR: %a@." Error.pp e in
      Error.raise e
  | Ok _ -> Lwt.return_unit

let test () =
  run_ignore_lwt @@ with_random_lwt @@ fun rng ->
  with_context_conf_lwt @@ fun conf ->
  with_tempdir @@ fun d ->
  let* () = build_file rng d conf 200 in
  prerr_endline "Now copying";
  copy_file rng d conf

let () =
  let open Alcotest in
  run "copy_file"
    ["copy_file",
     ["test", `Quick, test ]]
