(* node storage test *)
open Plebeia.Internal

module Blake2B_28 = Hashfunc.Blake2B(struct let bytes = 28 end)

let test () =
  assert (Hex.of_string @@ Blake2B_28.hash_string "Replacing SHA1 with the more secure function"
          = `Hex "6bceca710717901183f66a78a7a9f59441c56defcff32f33f1e1b578");
  assert (Hex.of_string @@ Blake2B_28.hash_strings ["Replacing SHA1 with the "; "more secure function" ]
          = `Hex "6bceca710717901183f66a78a7a9f59441c56defcff32f33f1e1b578")

let () =
  let open Alcotest in
  run "blake2b28"
       ["blake2b28", ["test", `Quick, test]]
