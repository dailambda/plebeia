open Plebeia
open Node
open Cursor

let test () =
  let open Path in

  (* "" *)
  let trail = _Top in
  assert (equal (path_of_trail trail) []) ;

  (* "/" *)
  let trail = _Budded (trail, Modified) in
  assert (equal (path_of_trail trail) [Segment.empty]) ;

  (* "/L" *)
  let trail = _Left (trail, new_bud None, Modified) in
  assert (equal (path_of_trail trail) [Segment.of_sides [Left]])

let test_local () =
  let open Segment in

  (* Here, we cannot distinguish Top and Budded *)

  (* "" *)
  let trail = _Top in
  assert (equal (local_segment_of_trail trail) Segment.empty) ;

  (* "" *)
  let trail = _Budded (trail, Modified) in
  assert (equal (local_segment_of_trail trail) Segment.empty) ;

  (* "L" *)
  let trail = _Left (trail, new_bud None, Modified) in
  assert (equal (local_segment_of_trail trail) (Segment.of_sides [Left]))

let () =
  let open Alcotest in
  run "trail" [
    "unit", [ "test", `Quick, test;
              "test_local", `Quick, test_local ]
  ]
