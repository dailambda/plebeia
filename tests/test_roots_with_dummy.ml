open Plebeia
open Lwt.Syntax
open Test_utils

let test () =
  run_lwt @@ with_random_lwt @@ fun st ->

  (* XXX prefix.context, prefix.lock, prefix.roots are not assured not existing *)
  let prefix = Filename.temp_file "plebeia" "" in

  let* vc = Result_lwt.get_ok_lwt @@ Vc.create Context.default_config prefix in

  let rec loop parent = function
    | 0 -> Lwt.return_unit
    | n ->
        let parent =
          if Gen.bool st then parent
          else
            (* Change the parent to make dummy parents *)
          if Gen.bool st then None (* force genesis *)
          else Some (Commit_hash.gen st)
        in
        let node =
          let rec f () =
            let n = Node_type.gen_bud 5 st in
            match n with
            | Disk _ -> assert false
            | View (Bud (None, _, _)) when RS.int st 10 <> 0 ->
                (* empty Bud has 0 hash is identical to "no parent" *)
                f ()
            | _ -> n
          in
          f ()
        in
        let c = Cursor.(_Cursor (_Top, node, Vc.context vc)) in
        let* _c, _hp, commit =
          Result_lwt.get_ok_lwt @@
          Vc.commit
            ~allow_missing_parent:true
            vc ~parent ~hash_override: None c
        in
        loop (Some commit.hash) (n-1)
  in
  let* () = loop None 100 in
  let* () = Result_lwt.get_ok_lwt @@ Vc.close vc in
  Lwt_fmt.eprintf "done : %s.*@." prefix

let () =
  let open Alcotest in
  run "roots_with_dummy"
    ["roots_with_dummy", ["test", `Quick, test]]
