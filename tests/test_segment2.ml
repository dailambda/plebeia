open Plebeia.Internal
open Test_utils

module Segment = Segment_int_array
open Segment

let gen_sides len = let open Gen in list len @@ elements [Left; Right]

let test_int63_and_sides () =
  let gen = gen_sides (Gen.int 256) in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let sides = gen rng in
    let i63, sides_left, nsides_used = Int63.of_sides sides in
    assert (nsides_used = Int.min 63 (List.length sides));
    assert (List.length sides_left = List.length sides - nsides_used);
    assert (Int63.to_sides i63 nsides_used @ sides_left = sides)
  done

module X = Random
let test_int63_common_prefix () =
  let gen =
    let open Gen in
    let open Gen.Syntax in
    let* i1 = int (1 lsl 21) in
    let* i2 = int (1 lsl 21) in
    let+ i3 = int (1 lsl 21) in
    i1 lsl 42 + i2 lsl 21 + i3
  in
  let check i1 i2 =
    let ncommonbits = Int63.common_prefix i1 i2 in
try
  assert (0 <= ncommonbits && ncommonbits <= 63);
    assert (i1 lsr (63 - ncommonbits) = i2 lsr (63 - ncommonbits));
    assert (ncommonbits = 63
            || (i1 land (1 lsl (62 - ncommonbits))
                <> i2 land (1 lsl (62 - ncommonbits))))
with
| e ->
    Format.eprintf "%d <?> %d@." i1 i2;
    raise e
  in
  check 0 0;
  check 0 (-1);
  for _ = 0 to 100000 do
    with_random @@ fun rng ->
    let i1 = gen rng in
    let i2 = gen rng in
    check i1 i2
  done

let test_vector_get () =
  let gen = gen_sides @@ Gen.int Segment.max_length in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let sides = gen rng in
    let bits = Bits.of_sides sides in
    let vec = bits.vec in
    List.iteri (fun i side ->
        assert (Vector.unsafe_get_side vec i = side)) sides
  done

let test_bits_and_sides () =
  let gen = gen_sides (Gen.int Segment.max_length) in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let sides = gen rng in
    let bits = Bits.of_sides sides in
    let sides' = Bits.to_sides bits in
    assert (sides = sides');
    let ndrops = Gen.int (List.length sides + 1) rng in
    let ntakes = Gen.int (List.length sides - ndrops + 1) rng in
    let subsides = List.take ntakes (List.drop ndrops sides) in
    let subbits = Bits.mk ~off:ndrops ~len:ntakes bits.vec in
    assert (Bits.to_sides subbits = subsides)
  done

let test_get_i63 () =
  let gen = gen_sides @@ Gen.int Segment.max_length in
  for _ = 0 to 1000 do
    with_random @@ fun rng ->
    let sides = gen rng in
    let nsides = List.length sides in
    (* Format.eprintf "nsides=%d@." nsides; *)
    let off = Gen.int (nsides + 1) rng in
    let len = Gen.int (nsides - off + 1) rng in
    (* Format.eprintf "off=%d len=%d@." off len; *)
    assert (off + len <= nsides);
    let bits = Bits.of_sides sides in
    let bits = Bits.mk ~off ~len bits.vec in
    let sides' = List.take len (List.drop off sides) in
    let sides'0 =
      List.init off (fun _ -> Left)
      @ sides'
      @ List.init (nsides - (off + len)) (fun _ -> Left)
    in
    assert (List.length sides = List.length sides'0);
    (* Format.eprintf "sides   = %s@." (string_of_sides sides);
       Format.eprintf "sides'0 = %s@." (string_of_sides sides'0);
       Format.eprintf "bits    = %s@." (string_of_sides (Bits.to_sides bits)); *)
    assert (Bits.to_sides bits = sides');
    let sides'' =
      List.init 128 (fun _ -> Left)
      @ sides'0
      @ List.init 128 (fun _ -> Left)
    in
    for off = -128 to nsides + 128 - 63 do
      let i63 = Bits.get_i63 bits off in
      let sub = List.take 63 (List.drop (off + 128) sides'') in
      let i63s = Int63.to_sides i63 63 in
      (* Format.eprintf "off: %d  i63: %d  sub: %d@." off i63 (List.length sub);
         Format.eprintf "sub  %s@." (string_of_sides sub);
         Format.eprintf "i63s %s@." (string_of_sides i63s); *)
      assert (sub = i63s)
    done
  done

let test_append () =
  let gen_bits =
    let open Gen in
    let open Gen.Syntax in
    let* nsides = int Segment.max_length in
    let* sides = gen_sides (return nsides) in
    let* off = int (nsides + 1) in
    let* len = int (nsides - off + 1) in
    assert (off + len <= nsides);
    let bits = Bits.of_sides sides in
    let bits = Bits.mk ~off ~len bits.vec in
    let sides' = List.take len (List.drop off sides) in
    assert (Bits.to_sides bits = sides');
    return bits
  in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let bits1 = gen_bits rng in
    let bits2 = gen_bits rng in
    let bits12 = Bits.append bits1 bits2 in
    if Bits.to_sides bits1 @ Bits.to_sides bits2 = Bits.to_sides bits12 then ()
    else begin
      Format.eprintf "bits1 : %s@." (string_of_sides (Bits.to_sides bits1));
      Format.eprintf "bits2 : %s@." (string_of_sides (Bits.to_sides bits2));
      Format.eprintf "bits12: %s@." (string_of_sides (Bits.to_sides bits12));
      assert false
    end
  done

let test_normalize () =
  let gen_bits =
    let open Gen in
    let open Gen.Syntax in
    let* nsides = int Segment.max_length in
    let* sides = gen_sides (return nsides) in
    let* off = int (nsides + 1) in
    let* len = int (nsides - off + 1) in
    assert (off + len <= nsides);
    let bits = Bits.of_sides sides in
    let bits = Bits.mk ~off ~len bits.vec in
    let sides' = List.take len (List.drop off sides) in
    assert (Bits.to_sides bits = sides');
    return bits
  in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let bits = gen_bits rng in
    let bits' = Bits.normalize bits in
    assert (Bits.to_sides bits = Bits.to_sides bits')
  done

let test_common_prefix_unit () =
  let seg1 = of_sides [Right] in
  let seg2 = of_sides [Right] in
  let shared, seg1', seg2' = common_prefix seg1 seg2 in
  assert (equal shared seg1 && equal shared seg2 && is_empty seg1' && is_empty seg2');
  let seg1 = Bits.mk ~off:4 ~len:1 (Bits.of_sides [Right; Left; Left; Right; Right; Left; Left]).vec in
  let seg2 = Bits.mk ~off:4 ~len:1 (Bits.of_sides [Right; Left; Left; Right; Right; Left; Left]).vec in
  let shared, seg1', seg2' = common_prefix seg1 seg2 in
  assert (equal shared seg1 && equal shared seg2 && is_empty seg1' && is_empty seg2')

let test_common_prefix () =
  let gen =
    let scale = 10 in
    let open Gen in
    let open Gen.Syntax in
    let* ncommon = int (2 * scale) in
    let* exact_same =
      let+ x = int 50 in
      x = 0
    in
    let* common_sides = gen_sides @@ return ncommon in
    let* sides1, sides2 =
      if exact_same then return (common_sides, common_sides)
      else
        let* npostfix1 = int (2 * scale) in
        let* npostfix2 = int (2 * scale) in
        let* postfix1 = gen_sides @@ return npostfix1 in
        let+ postfix2 = gen_sides @@ return npostfix2 in
        (common_sides @ Left :: postfix1), (common_sides @ Right :: postfix2)
    in
    let common = Bits.of_sides common_sides in
    let surround sides =
      let* nprefix = int 200 in
      let* npostfix = int 200 in
      let* prefix = gen_sides @@ return nprefix in
      let+ postfix = gen_sides @@ return npostfix in
      Bits.mk ~off:nprefix ~len:(List.length sides) (of_sides (prefix @ sides @ postfix)).vec
    in
    let* seg1 = surround sides1 in
    let+ seg2 = surround sides2 in
    common, seg1, seg2
  in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let common, seg1, seg2 = gen rng in
    let common', seg1', seg2' = common_prefix seg1 seg2 in
    assert (equal common common');
    assert (equal seg1 (append common' seg1'));
    assert (equal seg2 (append common' seg2'));
  done

let test_encoding_units () =
  let test sides =
    let t = Bits.of_sides sides in
    let t' = unsafe_of_bits t.len @@ snd @@ to_bits t in
    assert (equal t t')
  in
  test [ Left; Left; Right; Right; Left; Right ];
  test (List.init 33 (fun i -> List.nth [ Left; Right; Left; Left; Right; Right ] (i mod 6)));
  test (List.init 48 (fun i -> List.nth [ Left; Right; Left; Left; Right; Right ] (i mod 6)));
  test (List.init 63 (fun i -> List.nth [ Left; Right; Left; Left; Right; Right ] (i mod 6)));
  test (List.init 64 (fun i -> List.nth [ Left; Right; Left; Left; Right; Right ] (i mod 6)))

let test_encoding () =
  let open Gen in
  let open Gen.Syntax in
  let gen_sides len =
    list (return len) @@ elements [Left; Right] in
  let gen =
    let* nsides = int Segment.max_length in
    let* sides = gen_sides nsides in
    let* off = int (nsides + 1) in
    let+ len = int (nsides - off + 1) in
    assert (off + len <= nsides);
    let bits = Bits.of_sides sides in
    Bits.mk ~off ~len bits.vec
  in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let bits = gen rng in
    let _, s = to_bits bits in
    let bits' = unsafe_of_bits bits.len s in
    if not (equal bits bits') then begin
      Format.eprintf "%a@." pp bits;
      Format.eprintf "%a@." pp bits';
      Format.eprintf "%S@." s;
      assert false
    end
  done

let test_serialization () =
  let gen =
    let open Gen in
    let open Gen.Syntax in
    let* nsides = int Segment.max_length in
    let* sides = gen_sides @@ return nsides in
    let* off = int (nsides + 1) in
    let+ len = int (nsides - off + 1) in
    assert (off + len <= nsides);
    let bits = Bits.of_sides sides in
    Bits.mk ~off ~len bits.vec
  in
  for _ = 0 to 10000 do
    with_random @@ fun rng ->
    let bits = gen rng in
    let bits' = Serialization.decode_exn @@ Serialization.encode bits in
    if not (equal bits bits') then begin
      Format.eprintf "%a@." pp bits;
      Format.eprintf "%a@." pp bits';
      assert false
    end
  done

let test_cut () =
  let gen =
    let open Gen in
    let open Gen.Syntax in
    let* nsides = int Segment.max_length in
    let* sides = gen_sides @@ return nsides in
    let* off = int (nsides + 1) in
    let+ len = int (nsides - off + 1) in
    assert (off + len <= nsides);
    let bits = Bits.of_sides sides in
    Bits.mk ~off ~len bits.vec, List.take len @@ List.drop off sides
  in
  for _ = 0 to 2000 do
    with_random @@ fun rng ->
    let bits, sides = gen rng in
    let rec loop sides bits =
      match sides with
      | [] -> assert (cut bits = None)
      | s::sides ->
          match cut bits with
          | None -> assert false
          | Some (s',bits') ->
              assert (s = s');
              loop sides bits'
    in
    loop sides bits
  done

let () =
  let open Alcotest in
  run "segment2"
    [ "int63", ["int63_and_sides", `Quick, test_int63_and_sides;
                "int63_common_prefix", `Quick, test_int63_common_prefix];
      "vector", ["vector_get", `Quick, test_vector_get];
      "bits", ["bits_and_sides", `Quick, test_bits_and_sides;
               "append", `Quick, test_append;
               "normalize", `Quick, test_normalize;
               "get_i63", `Quick, test_get_i63;
               "common_prefix", `Quick, test_common_prefix;
               "common_prefix_unit", `Quick, test_common_prefix_unit;
               "cut", `Quick, test_cut;
              ];
      "encoding", [ "encoding_units", `Quick, test_encoding_units;
                    "encoding", `Quick, test_encoding;
                  ];
      "serialization", [ "serialization", `Quick, test_serialization; ];
    ]
