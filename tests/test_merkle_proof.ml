open Plebeia
open Test_utils

(* XXX The tests currently lack non existent segment tests *)

let test () =
  ignore @@ with_random @@ fun rng ->
  with_context @@ fun ctxt ->
  for _ = 0 to 10000 do
    let node = Node_type.gen_bud 5 rng in
    (* Format.eprintf "@.%a@." Node.pp node; *)
    let viewer = Node_storage.view ctxt in
    let _, h = Node.compute_hash ctxt node in
    let keys = Key.keys viewer node in
(*
    let gen_nkey () = Key.non_existing_key viewer node rng in
*)
    let keys = List.filter (fun _ -> Gen.int 3 rng = 0) keys in
(*
    let nks = List.init (List.length keys) (fun _ -> gen_nkey ()) in
*)
    let nks = [] in
    let proof, _res =
      Merkle_proof.make ctxt node
        (List.map (fun k ->
             match Key.to_path k with
             | Some p -> p
             | None -> assert false) (keys @ nks)) in
    (* Format.eprintf "proof:@.@[%a@]@." Merkle_proof.pp proof; *)
    match Merkle_proof.check ctxt.hasher proof with
    | h',_res' -> assert (h = h') (* XXX check res and res' *)
  done

let regression_test () =
  run_lwt @@ Lwt.map (regression_by_hash __LOC__ 209756974)
  @@ let rng = RS.make [| 0 |] in
  with_context @@ fun ctxt ->
  let node = Node_type.gen_bud 10 rng in
  let viewer = Node_storage.view ctxt in
  let _, h = Node.compute_hash ctxt node in
  let keys = Key.keys viewer node in
  let keys = List.filter (fun _ -> Gen.int 3 rng = 0) keys in
(*
  let random_keys =
  in
  let nks = List.init (List.length keys) (fun _ -> gen_nkey ()) in
*)
  let proof, _res =
    Merkle_proof.make ctxt node
      (List.map (fun k ->
           match Key.to_path k with
           | Some p -> p
           | None -> assert false) keys)
  in
  let h',_res' = Merkle_proof.check ctxt.hasher proof in
  assert (h = h'); (* XXX check res and res' *)
  Format.eprintf "hash: %a@." Hash.Long.pp h;
  Result.get_ok @@ Data_encoding.Binary.to_bytes (Merkle_proof.encoding ctxt) proof

let () =
  let open Alcotest in
  run "merkle_proof"
    ["merkle_proof", [ "test", `Quick, test ];
     "regression", [ "regression", `Quick, regression_test ]]
