open Lwt.Syntax

(* Test to see how Unix.map_file works *)
let make n =
  let open Bigarray in
  let* fd = Lwt_unix.openfile n [O_RDWR] 0o777 in
  Lwt.catch
    (fun () ->
       let fd' = Lwt_unix.unix_file_descr fd in
       let m = array1_of_genarray @@ Unix.map_file fd' ~pos:0L char c_layout false [| 1000000 |] in
       let write m pos c = Array1.set m pos c in
       let read m pos c =
         let c' = Array1.get m pos in
         if c <> c' then Lwt_fmt.eprintf "got unexpected char at %d@." pos else Lwt.return_unit
       in
       write m  900000 'A';
       let* () = read  m  900000 'A' in
       let m' = array1_of_genarray @@ Unix.map_file fd' ~pos:0L char c_layout false [| 1000000 |] in
       let* () = read  m' 900000 'A' in (* fails mmaps are independent even in one process *)
       let* () = Lwt_unix.close fd in
       let* s = Lwt_unix.stat n in
       (* unix.mli says:

          If all dimensions of the Bigarray are given, the file size is
          matched against the size of the Bigarray.  If the file is larger
          than the Bigarray, only the initial portion of the file is
          mapped to the Bigarray.  If the file is smaller than the big
          array, the file is automatically grown to the size of the Bigarray.
          This requires write permissions on [fd].

          No way to keep the size of the tempfile 0. :-(
       *)
       Lwt_fmt.eprintf "%s: ok: %d@." n s.Unix.st_size)
    (fun e ->
       let* () = Lwt_unix.close fd in
       Lwt_fmt.eprintf "%s: %s@." n (Printexc.to_string e))

let test () =
  (* Making a mmap over /dev/zero (or /dev/null).
    It did not work on Mac OS X, thought it should be supported in POSIX.
  *)
  let* () = make "/dev/null" in
  let* () = make "/dev/zero" in
  make (Filename.temp_file "plebeia" "test")

let () = Test_utils.exec_lwt test ()
