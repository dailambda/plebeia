open Plebeia.Internal
open Test_utils
open Hashcons

(* guess how many bytes required for 1 entry

   answer : 84.2 bytes/entry
*)

let test () =
  let cache =
    create { max_leaf_size= 36
           ; max_bytes_commit= max_int
           ; max_bytes_absolute= max_int
           ; shrink_ratio= 0.8
           }
  in
  let report i =
    let ebs = estimated_size_in_bytes cache in
    let ws = Obj.reachable_words (Obj.repr cache) in
    let bs = ws * Sys.word_size / 8 in
    Format.eprintf "nelems= %d words= %d bytes= %d bytes/nelems=%.2f - %d (value) estimated_bytes= %d ratio= %.2f@."
      i
      ws
      bs
      (float bs /. float i)
      ((10 / 8 + 2) * Sys.word_size / 8)
      ebs
      (float ebs /. float bs)
  in
  for i = 1 to 36 do
    (* words = len / 8 + 2 *)
    Format.eprintf "value word: len= %d words= %d@."
      i (Utils.reachable_words (String.make i 'a'))
  done;
  for i = 0 to 250000 do
    if i mod 10000 = 0 then report i;
    let s = Printf.sprintf "%010d" i in (* 10 / 8 + 2 = 3 words *)
    let v = Value.of_string s in
    ignore @@ add cache v Index.zero
  done

let () =
  let open Alcotest in
  run "hashcons_size"
    [ "hashcons_size", ["test", `Quick, test]
    ]
