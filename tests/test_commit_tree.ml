open Plebeia
open Lwt.Syntax
open Commit_tree
open Test_utils

let config =
  Storage.{
    head_string = "TEST" ^ String.make 16 '\000';
    bytes_per_cell = 40; (* must be 40! *)
    version = 0;
    max_index = Index.max_index
  }

let test_set () = run_lwt @@
  let open Internal in

  let n = Node.empty in
  Format.eprintf "@.empty: %a@.@." Node.pp n;

  let fn = Filename.temp_file "plebeia" ".commit" in
  let* () = Lwt_unix.unlink fn in
  let* key = Storage.make_new_writer_key [] in
  let* context = Storage.create ~config ~key fn in

  let (!~) s = Option.get @@ Segment.of_string s in
  let leaf n =
    Node.mk_leaf
      {parent = Some (Commit_hash.of_hex_string "0123456789012345678901234567890123456789012345678901234567890123");
       index = Index.Unsafe.of_int n}
  in

  let leaf0 = leaf 0 in
  let n = set context n !~"LRLR" leaf0 in
  Format.eprintf "1: %a@.@." Node.pp n;
  let l0 = Option.get @@ get context n !~"LRLR" in
  Format.eprintf "2: %a@.@." Node.pp l0;
  assert (Node.equal leaf0 l0);

  let leaf1 = leaf 1 in
  let n = set context n !~"LRRR" leaf1 in
  Format.eprintf "3: %a@.@." Node.pp n;
  let l0 = Option.get @@ get context n !~"LRLR" in
  Format.eprintf "4: %a@.@." Node.pp l0;
  assert (Node.equal leaf0 l0);
  let l1 = Option.get @@ get context n !~"LRRR" in
  Format.eprintf "5: %a@.@." Node.pp l1;
  assert (Node.equal leaf1 l1);

  let leaf2 = leaf 2 in
  let n = set context n !~"RLLL" leaf2 in
  Format.eprintf "6: %a@.@." Node.pp n;
  let l0 = Option.get @@ get context n !~"LRLR" in
  Format.eprintf "7: %a@.@." Node.pp l0;
  assert (Node.equal leaf0 l0);
  let l1 = Option.get @@ get context n !~"LRRR" in
  Format.eprintf "8: %a@.@." Node.pp l1;
  assert (Node.equal leaf1 l1);
  let l2 = Option.get @@ get context n !~"RLLL" in
  Format.eprintf "9: %a@.@." Node.pp l2;
  assert (Node.equal leaf2 l2);

  Lwt.return_unit

let test_random_set () = run_lwt @@
  let open Internal in
  with_random_lwt @@ fun rng ->

  let fn = Filename.temp_file "plebeia" ".commit" in
  let* () = Lwt_unix.unlink fn in
  let* key = Storage.make_new_writer_key [] in
  let* context = Storage.create ~config ~key fn in

  let tbl = Hashtbl.create 1000 in

  let f n =
    let seg = Gen.(segment (return 8)) rng in
    let h = Commit_hash.gen rng in
    let i = Gen.index rng in
    let leaf = Node.mk_leaf {parent=Some h; index=i} in
    Hashtbl.replace tbl seg leaf;
    let n = set context n seg leaf in
    Hashtbl.iter (fun seg leaf ->
        match get context n seg with
        | None -> assert false
        | Some n ->
            assert (Node.equal leaf n)) tbl;
    n
  in

  let rec g i n =
    if i = 0 then n
    else
      g (i-1) (f n)
  in
  let n = g 100 Node.empty in
  Format.eprintf "@.@.n : %a@." Node.pp n;
  Lwt.return_unit

let test_memory_size () = run_lwt @@
  with_random_lwt @@ fun rng ->

  let fn = Filename.temp_file "plebeia" ".commit" in
  let* () = Lwt_unix.unlink fn in
  let* key = Storage.make_new_writer_key [] in
  let* t = create ~key fn in
  let rec f t = function
    | 0 -> t
    | x ->
        let h1 = Commit_hash.gen rng in
        let h2 = Commit_hash.gen rng in
        let i = Gen.index rng in
        let t = add t h2 {parent=Some h1; index=i} in
        f t (x-1)
  in
  let t = f t 160000 in
  Format.eprintf "160000 commits: %d@." (reachable_words t);
  Lwt.return_unit

let test_random_set_with_save segsize num () = run_lwt @@
  let open Internal in
  with_random @@ fun rng ->

  let fn = Filename.temp_file "plebeia" ".commit" in
  let* () = Lwt_unix.unlink fn in
  let* key = Storage.make_new_writer_key [] in
  let* context = Storage.create ~config ~key fn in

  let tbl = Hashtbl.create 1000 in

  let f j n =
    let seg = Gen.(segment (return segsize)) rng in
    let h = Commit_hash.gen rng in
    let i = Gen.index rng in
    let leaf = Node.mk_leaf {parent=Some h; index=i} in
    Hashtbl.replace tbl seg leaf;
    let n = set context n seg leaf in
    let n =
      if j mod (num / 100) = (num / 100 / 2) then begin
        Format.eprintf "%d commit and forget@." j;
        Result.get_ok @@ Node.write context n;
        match Node.may_forget n with
        | Some n -> n
        | None -> assert false
      end else n
    in
    if j mod (num / 100) = 0 then begin
      Format.eprintf "%d checking@." j;
      let cntr = ref 0 in
      Hashtbl.iter (fun seg _leaf ->
          match Gen.int (Int.max 1 (j / 1000)) rng with
          | 0 ->
              incr cntr;
              begin match get context n seg with
                | None -> assert false
                | Some _n -> ()
              end
          | _ -> ()
        ) tbl;
      Format.eprintf "%d checked %d@." j !cntr;
    end;
    n
  in

  let rec g i n =
    if i = num then n
    else
      let i = i + 1 in
      g i (f i n)
  in
  ignore @@ g 0 Node.empty;
  Format.eprintf "test_random_set_with_save %d %d : %a@."
    segsize num
    Index.pp (Storage.get_current_length context);
  Lwt.return_unit

let test_api () = run_lwt @@
  let fn = Filename.temp_file "plebeia" ".commit" in
  let* () = Lwt_unix.unlink fn in
  let* key = Storage.make_new_writer_key [] in
  let* ctree = Commit_tree.create ~key fn in
  with_random @@ fun rng ->
  let tbl = Hashtbl.create 1000 in
  let m = 100000 in
  let rec f t n =
    if n = m + 1 then close t
    else
      let* t =
        if n mod 10000 = 5000 then begin
          Format.eprintf "saving %d/%d@." n m;
          let () = Result.get_ok @@ write t in
          let* () = flush t in
          let* t = update_reader t in (* XXX no point of this, since there is no reader *)
          Lwt.return @@ Option.get @@ may_forget t
        end else Lwt.return t
      in
      if n mod 10000 = 0 then begin
        Format.eprintf "checking %d/%d@." n m;
        Hashtbl.iter (fun hp (hp', i) ->
            (* check about 100 *)
            if Gen.int (Int.max 1 (n / 100)) rng = 0 then begin
              match find t hp with
              | None -> assert false
              | Some {parent=hp''; index= i'} ->
                  assert (hp' = hp'' && i = i')
            end) tbl;
      end;
      let h = Commit_hash.gen rng in
      let h' = Commit_hash.gen rng in
      let i = Gen.index rng in
      let t = add t h {parent=Some h'; index= i} in
      Hashtbl.replace tbl h (Some h', i);
      f t (n+1)
  in
  let* () = Result_lwt.get_ok_lwt @@ f ctree 0 in

  (* gc test *)
  Format.eprintf "first GC...@.";
  let* () = Result_lwt.get_ok_lwt @@ offline_gc fn in
  Format.eprintf "second GC...@.";
  let* () = Result_lwt.get_ok_lwt @@ offline_gc fn in (* to see fn.old is properly overwritten *)
  let* t = Lwt.map Result.get_ok @@ open_existing_for_read fn in
  Format.eprintf "checking GC'ed file@.";
  Hashtbl.iter (fun hp (hp', i) ->
      (* check about 100 *)
      if Gen.int (Int.max 1 (m / 100)) rng = 0 then begin
        match find t hp with
        | None -> assert false
        | Some {parent=hp''; index= i'} ->
            assert (hp' = hp'' && i = i')
      end) tbl;
  Result_lwt.get_ok_lwt @@ close t

let test_empty_load () = run_lwt @@
  let fn = Filename.temp_file "plebeia" ".commit" in
  let* () = Lwt_unix.unlink fn in
  let* key = Storage.make_new_writer_key [] in
  let* ctree = Commit_tree.create ~key fn in
  let* t = update_reader ctree in
  (with_random @@ fun rng ->
  let h = Commit_hash.gen rng in
  assert (find t h = None));
  Lwt.return_unit

let test_corner () = run_lwt @@
  let fn = Filename.temp_file "plebeia" ".commit" in
  let* () = Lwt_unix.unlink fn in
  let* key = Storage.make_new_writer_key [] in
  let* tree = Commit_tree.create ~key fn in
  (with_random @@ fun rng ->
  let h = Commit_hash.gen rng in
  assert (find tree h = None); (* Node.empty (= Disk 0) may be viewed *)
  let i = Gen.index rng in
  let h' = Commit_hash.gen rng in
  let tree = add tree h {parent=Some h'; index= i} in
  assert (find tree h = Some {parent=Some h'; index= i}));
  Lwt.return_unit


let () =
  let open Alcotest in
  run "commit_tree"
    [ "set", [ "set", `Quick, test_set ]
    ; "random_set", [ "random_set", `Quick, test_random_set]
    ; "memory_size", [ "memory_size", `Quick, test_memory_size]
    ; "random_set_with_save",
      [ "random_set_with_save", `Quick, test_random_set_with_save 16 1000
      ; "random_set_with_save_large", `Slow, test_random_set_with_save 256 10000
      ]
    ; "corner", ["corner", `Quick, test_corner]
    ; "corner2", ["corner2", `Quick, test_empty_load]
    ; "api", [ "api", `Quick, test_api ]
    ]
