open Plebeia

type name =
  [ `Digit of int (* ex. 123 *)
  | `Hex of string (* binary *)
  | `Name of string (* ex. "data" *)
  ]

(** [prepare ratio].  To get about 1 million samples, use [ratio = 0.62] *)
val prepare : float -> ((name * string * Segment.t) list * string) array
