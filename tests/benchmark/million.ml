open Plebeia
open Test_utils

let run shuffle =
  let a = Path_generate.prepare 0.62 in
  let (), t = with_time @@ fun () -> Gc.full_major () in
  Format.eprintf "full major gc in %a@." Mtime.Span.pp t;
  let rng = RS.make [||] in
  if shuffle then Gen.shuffle_inplace a rng;
  Add.run "2e2e766073752399e6e6a206eb05760db2ea656a917690b43e6fc9ef" a
  |> Lwt_main.run |> ignore
