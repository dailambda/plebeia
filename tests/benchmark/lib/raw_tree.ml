open Plebeia.Utils

type t =
  | Leaf of string
  | Dir of t String.Map.t

let hashcons hc s =
  match String.Set.find_opt s hc with
  | Some s -> hc, s
  | None ->
      String.Set.add s hc, s

let hashcons_empty = String.Set.empty

let add hc k bytes rt =
  let hc, s = hashcons hc @@ Bytes.to_string bytes in
  let hc, k =
    List.fold_right (fun n (hc,k) ->
        let hc, n = hashcons hc n in
        hc, n::k) k (hc,[])
  in
  let rec f k rt =
    match k, rt with
    | [], _ -> assert false
    | [n], Dir map -> Dir (String.Map.add n (Leaf s) map)
    | n::k, Dir map ->
        let rt =
          match String.Map.find_opt n map with
          | None -> Dir String.Map.empty
          | Some rt -> rt
        in
        Dir (String.Map.add n (f k rt) map)
    | _::_, Leaf _ -> assert false
  in
  hc, f k rt
