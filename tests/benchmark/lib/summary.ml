open Plebeia.Utils

type leaf =
  | Samples of String.Set.t
  | Statistics of { min: int; avg: float; max: int }
  | Array of string array

type t =
  | Digit of  { len: int; n: int; nsubs: int; sub: t }
  | Hex of { len: int; n: int; nsubs: int; sub: t }
  | Leaf of leaf
  | Name of { n: int; subs: (string * (int * t)) list }

let stat_huge_leaves t =
  let rec f = function
    | Digit d -> Digit { d with sub= f d.sub }
    | Hex d -> Hex { d with sub= f d.sub }
    | Name r -> Name { r with subs= List.map (fun (n, (nsubs, t)) -> (n, (nsubs, f t))) r.subs }
    | Leaf (Samples s) ->
        let sz = String.Set.cardinal s in
        if sz > 1000 then
          let min, max, sum = String.Set.fold (fun s (min, max, sum) ->
              let len = String.length s in
              Stdlib.min len min, Stdlib.max len max, sum + len)
              s (1000000,0,0)
          in
          let avg = float sum /. float sz in
          Leaf (Statistics { min; avg; max })
        else Leaf (Array (Array.of_list @@ String.Set.elements s))
    | Leaf stat -> Leaf stat
  in
  f t

let rec lines_of_d =
  let indent s ls =
    match ls with
    | [] -> assert false
    | l::ls ->
        let i = String.make (String.length s) ' ' in
        (s ^ l) :: List.map (fun l -> i ^ l) ls
  in
  function
  | Leaf (Array a) ->
      [Printf.sprintf "LEAF(sapmles=%d)" (Array.length a)]
  | Leaf (Samples s) ->
      let n = String.Set.cardinal s in
      let min_, max_ = String.Set.fold (fun s (min_, max_) ->
          let len = String.length s in (Stdlib.min min_ len, Stdlib.max max_ len)) s (1000000,0)
      in
      [Printf.sprintf "LEAF(samples=%d,min=%d,max=%d)" n min_ max_]
  | Leaf (Statistics {min; avg; max}) ->
      [Printf.sprintf "LEAF(min=%d,avg=%.2f,max=%d)" min avg max]
  | Name { n; subs= nds } ->
      List.concat @@ List.map (fun (s,(m, d)) -> indent (Printf.sprintf "%s(%.2f)" s (float m /. float n) ^ "/") @@ lines_of_d d) nds
  | Digit { len=_n; n= ndirs; nsubs= nsubdirs; sub= d } ->
      indent (Printf.sprintf "D(%.2f)/" (float nsubdirs /. float ndirs)) @@ lines_of_d d
  | Hex { len=n; n= ndirs; nsubs= nsubdirs; sub= d } ->
      indent (Printf.sprintf "H%d(%.2f)/" n (float nsubdirs /. float ndirs)) @@ lines_of_d d

let string_of_d d = String.concat "\n" (lines_of_d d)

let rec merge_ds ds =
  match ds with
  | [] -> assert false
  | d::ds ->
      List.fold_left (fun m d ->
          match m, d with
          | Leaf (Samples vs), Leaf (Samples vs') -> Leaf (Samples (String.Set.union vs vs'))
          | Leaf _, Leaf _ -> assert false
          | Name { n; subs= nds }, Name { n=n'; subs= nds' } ->
              let ns = List.sort_uniq compare @@ List.map fst (List.rev_append nds nds') in
              Name { n= n + n'
                   ; subs= List.map (fun n ->
                         n,
                         match List.assoc_opt n nds, List.assoc_opt n nds' with
                         | None, None -> assert false
                         | Some d, None -> d
                         | None, Some d -> d
                         | Some (n, d), Some (n', d') -> n + n', merge_ds [d; d'])
                         ns
                   }
          | Digit { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Digit { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } ->
              let n = if n = n' then n else 0 in
              Digit { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | Hex { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Hex { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } when n = n' ->
              Hex { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | Digit { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Hex { len= n'; n= ndirs'; nsubs=  nsubdirs'; sub=  d' }
          | Hex { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Digit { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } when n = n' ->
              Hex { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | Hex { len= n; n= ndirs; nsubs= nsubdirs; sub= d }, Hex { len= n'; n= ndirs'; nsubs= nsubdirs'; sub= d' } when n = n' ->
              Hex { len= n; n= ndirs + ndirs'; nsubs= nsubdirs + nsubdirs'; sub= merge_ds [d; d'] }
          | x1, x2 ->
              Format.eprintf "%s@. and@.%s@." (string_of_d x1) (string_of_d x2);
              assert false
        ) d ds

let merge_ds ds =
  try merge_ds ds with
  | e ->
      List.iter (fun d -> Format.eprintf "%s@.@." (string_of_d d)) ds;
      raise e
