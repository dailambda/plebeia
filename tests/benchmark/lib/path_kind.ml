open Plebeia.Utils

let string_of_pat = function
  | `Name n -> Printf.sprintf "%s" n
  | `H n -> Printf.sprintf "<H%d>" n
  | `D -> Printf.sprintf "<D>"

let kind rev_whole n =
  let len = String.length n in
  let cs = List.of_seq @@ String.to_seq n in
  let is_digit = function
    | '0' .. '9' -> true
    | _ -> false
  in
  let is_hex = function
    | '0' .. '9' | 'a' .. 'f' -> true
    | _ -> false
  in
  if List.for_all is_digit cs then
    `Digit len
  else
    match cs with
    | '-'::cs when
        Format.eprintf "warning: strange int index %s@." (Key.to_string (List.rev rev_whole));
        List.for_all is_digit cs -> `Digit 0 (* cannot mix with hex *)
    | _ ->
        if len mod 2 = 0 && List.for_all is_hex cs then `Hex len
        else `Name

let merge_kind = function
  | `Name, `Name -> `Name
  | `Digit len, `Digit len' -> `Digit (if len = len' then len else 0)
  | `Hex len, `Hex len' when len = len' -> `Hex len
  | `Digit len, `Hex len'
  | `Hex len, `Digit len' when len = len' -> `Hex len
  | _, _ -> `Name
