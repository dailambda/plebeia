open Plebeia.Utils

type t = string list

let pp ppf k =
  Format.string ppf (String.escaped ("/" ^ String.concat "/" k))

let parse s =
  match String.split_by_char (function '/' -> true | _ -> false) s with
  | "" :: xs ->
      List.filter (function "" -> false | _ -> true) xs
  | _ ->
      assert false

let to_string = String.concat "/"
