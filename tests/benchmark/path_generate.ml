open Plebeia
open Path_summarize_lib
open Test_utils

module Path_encoding : sig
  val digit : int -> Segment.t
  val hex : string -> Segment.t
  val name : string -> Segment.t
end = struct

  open Segment

  let of_bits s = unsafe_of_bits (String.length s * 8) s

  (* 4 byte length checksum *)
  module Hashfunc_4 = (val Hashfunc.make { algorithm= `Blake2B; length= 4 })

  let digit n =
    let z = Z.of_int n in
    let s = Data_encoding.Binary.to_string_exn Data_encoding.z z in
    of_bits s

  let hex = of_bits

  let name =
    let cache = Hashtbl.create 1000 in
    fun s ->
      match Hashtbl.find_opt cache s with
      | Some seg -> seg
      | None ->
          let seg = of_bits Hashfunc_4.(to_raw_string @@ hash_string s) in
          Hashtbl.replace cache s seg;
          seg
end

let digits avgn rng =
  let nis = Int.max 1 @@ RS.int rng (Int.max 1 (avgn * 2)) in (* 1 .. nsubs*2-1 *)
  let rec gen j is last =
    if j = nis then is
    else
      let i = RS.int rng (nis / 5 + 1) + last + 1 in
      gen (j+1) (i::is) i
  in
  List.rev (gen 0 [] 0)

let generate sm ratio rng =
  let leaves = ref 0 in
  let rec f sm = match sm with
    | Summary.Leaf (Array a) ->
        let i = RS.int rng (Array.length a) in
        incr leaves;
        if !leaves mod 100000 = 0 then Format.eprintf "nleaves %d@." !leaves;
        `Leaf (Array.unsafe_get a i)
    | Leaf (Statistics { min; avg=_; max }) ->
        (* uniform distribution ignoring avg *)
        let len = RS.int rng (max - min + 1) in
        incr leaves;
        if !leaves mod 100000 = 0 then Format.eprintf "nleaves %d@." !leaves;
        `Leaf (String.init len (fun _ -> Char.chr @@ RS.int rng 256))
    | Leaf (Samples _) -> assert false
    | Digit { n; nsubs; sub; _ } ->
        let is = digits (int_of_float (float nsubs /. float n *. ratio)) rng in
        `Digit (List.rev_map (fun i -> (i, Path_encoding.digit i, f sub)) is)
    | Hex { len; n; nsubs; sub; _ } when len = 2 ->
        let prob = float (nsubs-1) /. float n /. 256.0 *. ratio in
        let rec g i st =
          if i = 256 then List.rev st
          else if RS.float rng 1.0 <= prob then g (i+1) (i::st)
          else g (i+1) st
        in
        let is =
          match g 0 [] with
          | [] -> [RS.int rng 256] (* at least 1 *)
          | is -> is
        in
        `Hex (List.map (fun i -> (let s = String.make 1 (Char.chr i)in s, Path_encoding.hex s, f sub)) is)
    | Hex { len; n; nsubs; sub; _ } ->
        let len = len / 2 in
        let n = RS.int rng (Int.max 1 @@ int_of_float (float nsubs /. float n *. ratio) * 2 - 1)  + 1 in
        if n > 10 then Format.eprintf "ouch %d@." n;
        let rand rng =
          String.concat ""
          @@ Stdlib.List.init len (fun _ ->
              String.make 1 (Char.chr (RS.int rng 256)))
        in
        let rec g i st =
          if i = n then st
          else
            let x = rand rng in
            if List.mem x st then g i st
            else g (i+1) (x::st)
        in
        let hs = g 0 [] in
        `Hex (List.map (fun h -> h, Path_encoding.hex h, f sub) hs)
    | Name { n; subs } ->
        let subs0 = subs in
        let subs = List.filter (fun (_, (m, _)) -> RS.int rng n <= int_of_float (float m *. ratio)) subs in
        let subs =
          if subs = [] then
            [Stdlib.List.nth subs0 (RS.int rng (List.length subs0))]
          else subs
        in
        `Name (List.map (fun (s, (_, sub)) -> s, Path_encoding.name s, f sub) subs)
  in
  let res = f sm in
  Format.eprintf "generated %d leaves@." !leaves;
  res

let generate ratio rng file =
  let ic = open_in file in
  let sm : Summary.t = input_value ic in
  close_in ic;
  let tree, t = with_time @@ fun () -> generate sm ratio rng in
  Format.eprintf "tree done in %a@." Mtime.Span.pp t;
  tree

let _save file tree =
  let oc = open_out_bin file in
  let poses = ref [] in
  let pos = ref 0 in
  let rec traverse rev_segs =
    let open Utils in
    function
    | `Digit xs ->
        List.iter (fun (_, seg, a) -> traverse (seg::rev_segs) a) xs
    | `Hex xs | `Name xs ->
        List.iter (fun (_, seg, a) -> traverse (seg::rev_segs) a) xs
    | `Leaf s ->
        let segs = List.rev rev_segs in
        let s = Marshal.to_string (segs, s) [] in
        poses := !pos :: !poses;
        pos := !pos + String.length s;
        output_string oc s
  in
  traverse [] tree;
  close_out oc;
  !poses

type name = [`Digit of int | `Hex of string | `Name of string]

let to_list tree =
  let rec traverse rev_segs acc =
    function
    | `Digit xs ->
        List.concat_map (fun (i, seg, a) ->
            traverse ((`Digit i,string_of_int i,seg)::rev_segs) acc a) xs
    | `Hex xs ->
        List.concat_map (fun (h, seg, a) ->
            let s =
              String.init (String.length h * 2) (fun i ->
                  let c = Char.code @@ String.unsafe_get h (i / 2) in
                  let j = if i mod 2 = 0 then c / 16 else c mod 16 in
                  if j < 10 then Char.chr (Char.code '0' + j)
                  else Char.chr (Char.code 'a' + j - 10))
            in
            traverse ((`Hex h,s,seg)::rev_segs) acc a) xs
    | `Name xs ->
        List.concat_map (fun (n, seg, a) ->
            traverse ((`Name n,n,seg)::rev_segs) acc a) xs
    | `Leaf s ->
        let segs = List.rev rev_segs in
        (segs,s)::acc
  in
  traverse [] [] tree

(*
let prepare () =
  let rng = RS.make [||] in (* 13361532 leaves.  too many *)
  let tree = generate 0.62 rng "path_summarize.dat" in
  let poses, t = with_time @@ fun () -> save "path_flat.dat" tree in
  Format.eprintf "written in %.2f secs@." t;
  let oc = open_out_bin "path_flat.poses" in
  output_value oc poses;
  close_out oc;
  poses

let load_poses () =
  let ic = open_in_bin "path_flat.poses" in
  let poses = (input_value ic : int list) in
  close_in ic;
  poses

let () =
  let poses = prepare () in
  let (), t = with_time @@ fun () -> Gc.full_major () in
  Format.eprintf "full major gc in %.2f secs@." t;
  let poses = Array.of_list poses in
  let rng = RS.make [||] in
  Gen.shuffle_inplace poses rng;
  let fd = Unix.(openfile "path_flat.dat" [O_RDONLY] 0o777) in
  let load_at pos =
    ignore @@ Unix.(lseek fd pos SEEK_SET);
    let ic = Unix.in_channel_of_descr fd in
    (input_value ic : Segment.t list * string)
  in
  let report i = Format.eprintf "done %d@." i in
  let rec f i c =
    if i = Array.length poses then report i
    else
      let pos = Array.unsafe_get poses i in
      let segs, v = load_at pos in
      let c =
        match Deep.insert c segs (Value.of_string v) with
        | Ok c -> c
        | Error e -> Format.eprintf "%a@>" Error.pp e; assert false
        in
        let i = i + 1 in
        if i mod 10000 = 0 then report i;
(*
        if i mod 100000 = 0 then begin
          prerr_endline "committing";
          let c, _idx, _hp = Cursor_storage.save_top_cursor c in
          prerr_endline "committed";
          let c = from_Some @@ Cursor.may_forget c in
           f i c
        end else
*)
          f i c
  in
  with_cursor @@ fun c ->
  f 0 c
*)

let prepare ratio =
  let rng = RS.make [||] in (* 13361532 leaves.  too many *)
  let tree = generate ratio rng "path_summarize.dat" in
  let a, t = with_time @@ fun () ->
    let list = to_list tree in
    let a = Array.of_list list in
    a
  in
  Format.eprintf "to array in %a@." Mtime.Span.pp t;
  a
