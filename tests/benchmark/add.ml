open Plebeia
open Lwt.Syntax
open Test_utils

let run expected_hash_hex_string a =
  let (), t = with_time @@ fun () -> Gc.full_major () in
  Format.eprintf "full major gc in %a@." Mtime.Span.pp t;
  let maxrss0 = Rusage.get_maxrss () in
  let t0 = Unix.gettimeofday () in
  let report i =
    let maxrss = Rusage.get_maxrss () in
    let t = Unix.gettimeofday () in
    Format.eprintf "done: %d time %.2f: maxrss delta: %Ld@." i (t -. t0) (Int64.sub maxrss maxrss0) in
  let rec f i c =
    let commit c =
      let c, _idx, _hp = Result.get_ok @@ Cursor.Cursor_storage.write_top_cursor c in
      Option.get @@ Cursor.may_forget c
    in
    if i = Array.length a then begin
      report i;
      commit c
    end else
      let path, v = Array.unsafe_get a i in
      let segs = List.map (fun (_,_,seg) -> seg) path in
      let c =
        match Deep.insert c segs (Value.of_string v) with
        | Ok c -> c
        | Error e -> Format.eprintf "%a@." Error.pp e; assert false
      in
      let i = i + 1 in
      if i mod 10000 = 0 then report i;
      if i mod 10000 = 0 then
        let c = commit c in
         f i c
      else f i c
  in
  let tempfile = temp_file "plebeia" ".context" in
  let* key = Storage.make_new_writer_key [] in
  let* ctxt = Context.create ~key Context.default_config tempfile in (* Not use the test conf *)
  let c = Cursor.empty ctxt in
  let c = f 0 c in
  let _, h = Cursor.compute_hash c in
  Format.eprintf "Final size: %Ld@." (Storage.size (Context.get_storage ctxt));
  Format.eprintf "Final hash: %a@." Hash.Long.pp h;
  let h0 = Hash.Long.of_prefix @@ Hash.of_hex_string expected_hash_hex_string in
  assert (h = h0);
  prerr_endline "hashes agree";
  Lwt.return_unit
