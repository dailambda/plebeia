open Plebeia
open Test_utils

let run shuffle =
  let a = Path_generate.prepare 0.62 in
  let (), t = with_time @@ fun () -> Gc.full_major () in
  Format.eprintf "full major gc in %a@." Mtime.Span.pp t;
  let rng = RS.make [||] in
  if shuffle then Gen.shuffle_inplace a rng;
  Fs_add.run "1d659ffbc4ee226a237438253967b68b1b4e84e5c89e6338027de47f" a
  |> Lwt_main.run |> ignore
