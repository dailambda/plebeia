# Fs

MacBook Pro (13-inch, 2017, Two Thunderbolt 3 ports)
With power adapter
macOS BigSur 11.6

commit 7611e612346b793b7ca325d0bff06c2e90664a41

```
$ dune exec ./benchmark.exe -- fs_million
done: 1036146 time 244.69: maxrss delta: 2111229952
done: 1036146 time 226.90: maxrss delta: 2109255680
done: 1036146 time 227.15: maxrss delta: 2112745472
done: 1036146 time 229.37: maxrss delta: 2112733184
```

```
$ dune exec ./benchmark.exe -- fs_million no_shuffle
done: 1036146 time 63.91: maxrss delta: 689979392
done: 1036146 time 69.09: maxrss delta: 689979392
done: 1036146 time 70.10: maxrss delta: 689967104
done: 1036146 time 65.41: maxrss delta: 689979392
```

|    | Shuffle | No shuffle |
|----|--------:|-----------:|
| Fs |  226.90 |      63.91 |

## With `result` for Index overflow

```
done: 1036146 time 221.95: maxrss delta: 2158280704
done: 1036146 time 224.84: maxrss delta: 1965080576
done: 1036146 time 238.05: maxrss delta: 2146983936
```




