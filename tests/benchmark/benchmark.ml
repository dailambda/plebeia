let () =
  match Array.to_list @@ Sys.argv with
  | _ :: "million" :: [] -> Million.run true
  | _ :: "million" :: "no_shuffle" :: [] -> Million.run false
  | _ :: "order" :: [] -> Order.run ()
  | _ :: "fs_million" :: [] -> Fs_million.run true
  | _ :: "fs_million" :: "no_shuffle" :: [] -> Fs_million.run false
  | _ -> assert false
