open Plebeia
open Lwt.Syntax
open Test_utils

module F = Fs_impl

let conv = Fs_nameenc.bits8

let run expected_hash_hex_string a =
  let (), t = with_time @@ fun () -> Gc.full_major () in
  Format.eprintf "full major gc in %a@." Mtime.Span.pp t;
  let maxrss0 = Rusage.get_maxrss () in
  let t0 = Unix.gettimeofday () in
  let report i =
    let maxrss = Rusage.get_maxrss () in
    let t = Unix.gettimeofday () in
    Format.eprintf "done: %d time %.2f: maxrss delta: %Ld@." i (t -. t0) (Int64.sub maxrss maxrss0)
  in
  let commit c =
    prerr_endline "committing";
    let c, span = with_time @@ fun () -> fst @@ Result.get_ok (F.write_top_cursor c) in
    Format.eprintf "committed in %a@." Mtime.Span.pp span;
    c
  in
  let rec f i c =
    if i = Array.length a then begin
      report i;
      commit c
    end else
      let segs, v = Array.unsafe_get a i in
      let fn = List.map (fun (_,n,_) -> n) segs in
      let c =
        match F.Op.write ("/" :: fn) (Value.of_string v) c with
        | Ok (c,()) -> c
        | Error e -> Format.eprintf "%a@." Error.pp e; assert false
      in
      let i = i + 1 in
      if i mod 10000 = 0 then report i;
      if i mod 10000 = 0 then
        let c = commit c in
        f i c
      else f i c
  in
  let tempfile = temp_file "plebeia" ".context" in
  let* key = Storage.make_new_writer_key [] in
  let* ctxt = Context.create ~key Context.default_config tempfile in (* Not use the test conf *)
  let cmt = F.empty ctxt conv in
  let cmt = f 0 cmt in
  let _cmt, h = Cursor.compute_hash (F.get_raw_cursor cmt) in
  Format.eprintf "Final size: %Ld@." (Storage.size (Context.get_storage ctxt));
  Format.eprintf "Final hash: %a@." Hash.Long.pp h;
  let h0 = Hash.Long.of_prefix @@ Hash.of_hex_string @@ expected_hash_hex_string in
  assert (h = h0);
  prerr_endline "hashes agree";
  Lwt.return_unit
