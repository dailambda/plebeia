open Plebeia
open Test_utils

let run () =
  let a = Path_generate.prepare 0.2 in
  (* change the writing order for each time *)
  let rng = RS.make_self_init () in
  Gen.shuffle_inplace a rng;
  Add.run "fc00b4acc198254b7e52a52539060cf22a5f92d66de56baaf2e45bf3" a
  |> Lwt_main.run |> ignore
