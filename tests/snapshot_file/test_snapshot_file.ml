open Plebeia
open Lwt.Syntax
open Test_utils

let count_nodes ctxt n =
  let cntr = ref 0 in
  Traverse.Iter.f (fun t ->
      incr cntr;
      `Continue (Node_storage.view ctxt t)) n;
  !cntr

(* build a node with enough depth and size *)
let build_node ctxt rng ?min_nodes depth =
  match min_nodes with
  | None ->
      Node_type.gen_bud depth rng
  | Some min_nodes ->
      let rec aux (max_ns, m) = function
        | 0 ->
            Format.eprintf "Warning: could not build a node with size >= %d@." min_nodes;
            m
        | i ->
            let n = Node_type.gen_bud depth rng in
            let ns = count_nodes ctxt n in
            if min_nodes <= ns then begin
              Format.eprintf "Build a node with size %d@." ns;
              n
            end else
              let max_ns, m =
                if max_ns < ns then ns, n
                else max_ns, m
              in
              aux (max_ns, m) (i-1)
      in
      aux (1, Node_type.gen_leaf rng) 100

let doit rng ctxt fn ?min_nodes depth write_dot =
  (* make a random tree then test its snapshot *)
  let n = build_node ctxt rng ?min_nodes depth in
  let n, nh = Node.compute_hash ctxt n in
  let n, _, _, _cnt = Result.get_ok @@ Node_storage.write_node ctxt n in
  (* forget some nodes *)
  let n = forget_random_nodes rng 0.2 n in
  if write_dot then begin
    let dotfn = "node.dot" in
    save_node_to_dot dotfn n;
  end;
  let* fd = Lwt_unix.openfile fn [O_CREAT; O_TRUNC; O_WRONLY] 0o644 in
  let* () = Snapshot.save fd ctxt n in
  let* () = Lwt_unix.close fd in
  let md5 = Digest.file fn in
  Format.eprintf "snapshot md5: %s@." Digest.(to_hex md5);
  let* fd = Lwt_unix.openfile fn [O_RDONLY] 0o644 in
  let reader = Data_encoding_tools.make_reader fd in
  let* n', with_hash = Snapshot.load ctxt.hasher reader in
  let* () = Lwt_unix.close fd in
  assert (not with_hash);
  assert (match n' with
      | Node.View v -> Node_type.hash_of_view v = Some nh
      | _ -> assert false);
  Lwt.return md5

let random_test () =
  run_ignore_lwt @@ with_random @@ fun rng ->
  with_context_lwt @@ fun ctxt ->
  let fn = Filename.temp_file "plebeia" ".dump" in
  Format.eprintf "writing to %s@." fn;
  for_lwt 0 100 (fun _i ->
      ignore_lwt @@ doit rng ctxt fn 20 true (* with dot *))

let create_data_files () =
  let rng = RS.make [| 0 |] in
  run_ignore_lwt @@ with_context_lwt @@ fun ctxt ->
  let fn = Printf.sprintf "snapshot-%s" (Context.get_config_name ctxt) in
  Format.eprintf "writing to %s@." fn;
  doit rng ctxt fn 100 ~min_nodes:5_000 false (* no dot *)

let data_file_test () =
  run_lwt @@ Lwt.map (regression_by_hash __LOC__ 213187418)
  @@ with_context_lwt @@ fun ctxt ->
  let fn = Printf.sprintf "snapshot-%s" (Context.get_config_name ctxt) in
  let* fd = Lwt_unix.openfile fn [O_RDONLY] 0o644 in
  let reader = Data_encoding_tools.make_reader fd in
  let* n, with_hash = Snapshot.load ctxt.hasher reader in
  let* () = Lwt_unix.close fd in
  assert (not with_hash);
  match n with
  | Node.View v -> Lwt.return @@ Node_type.hash_of_view v
  | _ -> assert false

let () =
  let open Alcotest in
  let promote = ref false in
  Arg.parse ["--promote", Arg.Set promote, "data promotion"] (fun _ -> ()) "test_snapshot_file";
  if !promote then create_data_files ();
  run "snapshot_file"
    ["snapshot_file", [ "data_file_loading", `Quick, data_file_test
                      ; "random_test", `Quick, random_test
                      ]]
