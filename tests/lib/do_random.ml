open Plebeia
open Helper
open Cursor
open Node

module Flat = struct
  let gen_segment = Gen.(segment @@ int_range (3,5))

  let do_random ?(no_commit=false) st sz c dumb =
    let segs = ref SegmentSet.empty in
    let add_seg seg = segs := SegmentSet.add seg !segs in
    let del_seg seg = segs := SegmentSet.remove seg !segs in
    let pick_seg st =
      let segs = SegmentSet.elements !segs in
      let len = List.length segs in
      if len < 20 then gen_segment st
      else
        let pos = RS.int st (List.length segs) in
        List.nth segs pos
    in
    let rev_ops = ref [] in
    let add_op o = rev_ops := o :: !rev_ops in
    let rec f c dumb i =
      if i = sz then (c, dumb)
      else
        let (c, dumb) =
          let op = match RS.int st (if no_commit then 7 else 8) with
            | 0 -> `Insert (gen_segment st, Gen.value st)
            | 1 -> `Upsert (gen_segment st, Gen.value st)
            | 2 -> `Subtree (gen_segment st)
            | 7 -> `Commit
            | _ -> `Delete (pick_seg st)
          in
          begin match op with
            | `Insert (sg, v) ->
                Format.eprintf "insert %a %a@." Segment.pp sg Value.pp v
            | `Upsert (sg, v) ->
                Format.eprintf "upsert %a %a@." Segment.pp sg Value.pp v
            | `Subtree sg ->
                Format.eprintf "subtree %a@." Segment.pp sg
            | `Commit -> Format.eprintf "commit@."
            | `Delete sg ->
                Format.eprintf "delete %a@." Segment.pp sg
          end;
          match op with
          | `Insert (seg, v) ->
              begin match
                insert c seg v,
                Dumb.insert dumb seg v
              with
              | Ok c, Ok dumb ->
                  compare_trees dumb c;
                  let Cursor (_, n, context) = c in
                  (* check the invariants of the node *)
                  validate_node context n;
                  add_op op;
                  add_seg seg;
                  (c, dumb)
              | Error _, Error _ ->
                  (c, dumb)
              | _ -> assert false
            end

          | `Upsert (seg, v) ->
              begin match
                upsert c seg v,
                Dumb.upsert dumb seg v
              with
              | Ok c, Ok dumb ->
                  compare_trees dumb c;
                  let Cursor (_, n, context) = c in
                  (* check the invariants of the node *)
                  validate_node context n;
                  add_op op;
                  add_seg seg;
                  (c, dumb)
              | Error _, Error _ ->
                  (c, dumb)
              | _ -> assert false
            end

          | `Subtree seg ->
              begin match
                  create_subtree c seg,
                  Dumb.create_subtree dumb seg
              with
              | Ok c, Ok dumb ->
                  compare_trees dumb c;
                  let Cursor (_, n, context) = c in
                  (* check the invariants of the node *)
                  validate_node context n;
                  add_op op;
                  add_seg seg;
                  (c, dumb)
              | Error _, Error _ ->
                  (c, dumb)
              | _ -> assert false
            end

          | `Delete seg ->
              begin match
                  delete c seg,
                  Dumb.delete dumb seg
              with
              | Ok c, Ok dumb ->
                  compare_trees dumb c;
                  let Cursor (_, n, context) = c in
                  (* check the invariants of the node *)
                  validate_node context n;
                  add_op op;
                  del_seg seg;
                  (c, dumb)
              | Error _, Error _ ->
                  (c, dumb)
              | _ -> assert false
            end

          | `Commit ->
              let Cursor(_, _, context), i, _ = Result.get_ok @@ Cursor_storage.write_top_cursor c in
              let v = Node_storage.read_node context i in
              add_op op;
              (_Cursor (_Top, View v, context), dumb)
        in
        f c dumb (i+1)
    in
    let (c,_) = f c dumb 0 in
    (List.rev !rev_ops, Cursor_storage.read_fully ~reset_index:false c)
end

module Deep = struct
  (* We cannot compare with Dumb *)

  let do_random ~commit ?(bud_level_range=(1,5)) ?(segment_length=3) st sz c =
    let gen_segments = gen_segments bud_level_range segment_length in
    let rev_ops = ref [] in
    let add_op o = rev_ops := o :: !rev_ops in
    let rec f c i =
      if i = sz then c
      else
        let c =
          let rec get_op () = match RS.int st 8 with
            | 0 -> `Insert (gen_segments st, Gen.value st)
            | 1 -> `Upsert (gen_segments st, Gen.value st)
            | 2 -> `Subtree (gen_segments st)
            | 3 -> if commit then `Commit else get_op ()
            | 4 ->
                begin match random_segs_to_bud_or_leaf st c with
                  | None -> get_op ()
                  | Some segs ->
                      let segs' = gen_segments st in
                      `Copy (segs, segs')
                end
            | _ ->
                match random_segs_to_bud_or_leaf st c with
                | None -> get_op ()
                | Some segs -> `Delete segs
          in
          let op = get_op () in
          match op with
          | `Insert (segs, v) ->
              (* Format.eprintf "Insert at %s@." @@ string_of_segs segs; *)
              begin match Deep.insert c segs v with
                | Ok c ->
                    add_op op;
                    check_cursor_is_top c;
                    c
                | Error _ -> c
              end
          | `Upsert (segs, v) ->
              (* Format.eprintf "Upsert at %s@." @@ string_of_segs segs; *)
              begin match Deep.upsert c segs v with
                | Ok c ->
                    add_op op;
                    check_cursor_is_top c;
                    c
                | Error _ -> c
              end
          | `Subtree segs ->
              (* Format.eprintf "Create_subtree at %s@." @@ string_of_segs segs; *)
              begin match Deep.create_subtree ~create_subtrees:true c segs with
                | Ok c ->
                    add_op op;
                    check_cursor_is_top c;
                    c
                | Error _ -> c
              end
          | `Delete segs ->
              (* Format.eprintf "Delete at %s@." @@ string_of_segs segs; *)
              begin match
                  Deep.delete c segs
                with
                | Ok c ->
                    add_op op;
                    check_cursor_is_top c;
                    c
                | Error _ -> c
              end
          | `Copy (segs, segs') ->
              begin match Deep.copy ~create_subtrees:true c segs segs' with
                | Ok c ->
                    add_op op;
                    check_cursor_is_top c;
                    c
                | Error _ -> c
              end
          | `Commit ->
              let Cursor(_, _, context), i, _ = Result.get_ok @@ Cursor_storage.write_top_cursor c in
              let v = Node_storage.read_node context i in
              add_op op;
              _Cursor (_Top, View v, context)
              in
              f c (i+1)
          in
          let c = f c 0 in
          (c, List.rev !rev_ops)
end

module Vc = struct
  open Lwt.Syntax

  module Deep = Plebeia.Deep

  let debug = false

  let gen_segments = gen_segments (1,5) 3

  let do_random rng sz vc =
    let commits = Queue.create () in
    let rec f parent c i =
      if i = sz then Lwt.return c
      else
        let op =
          let rec get_op () = match RS.int rng 8 with
            | 0 -> `Insert (gen_segments rng, Gen.value rng)
            | 1 -> `Upsert (gen_segments rng, Gen.value rng)
            | 2 -> `Subtree (gen_segments rng)
            | 3 ->
                begin match random_segs_to_bud_or_leaf rng c with
                  | None -> get_op ()
                  | Some segs ->
                      let segs' = gen_segments rng in
                      `Copy (segs, segs')
                end
            | 4 | 5 ->
                begin match random_segs_to_bud_or_leaf rng c with
                | None -> get_op ()
                | Some segs -> `Delete segs
                end
            | 6 -> `Commit
            | 7 ->
                if Queue.length commits > 0 then
                  `Checkout (Queue.take commits)
                else get_op ()
            | _ -> assert false
          in
          get_op ()
        in
        let* c, parent =
          match op with
          | `Insert (segs, v) ->
              if debug then Format.eprintf "Insert at %a@." Segment.pp_segments segs;
              Lwt.return begin match Deep.insert c segs v with
                | Ok c ->
                    check_cursor_is_top c;
                    c, parent
                | Error _ -> c, parent
              end
          | `Upsert (segs, v) ->
              if debug then Format.eprintf "Upsert at %a@." Segment.pp_segments segs;
              Lwt.return begin match Deep.upsert c segs v with
                | Ok c ->
                    check_cursor_is_top c;
                    c, parent
                | Error _ -> c, parent
              end
          | `Subtree segs ->
              if debug then Format.eprintf "Create_subtree at %a@." Segment.pp_segments segs;
              Lwt.return begin match Deep.create_subtree ~create_subtrees:true c segs with
                | Ok c ->
                    check_cursor_is_top c;
                    c, parent
                | Error _ -> c, parent
              end
          | `Delete segs ->
              if debug then Format.eprintf "Delete at %a@." Segment.pp_segments segs;
              Lwt.return begin match
                  Deep.delete c segs
                with
                | Ok c ->
                    check_cursor_is_top c;
                    c, parent
                | Error _ -> c, parent
              end
          | `Copy (segs, segs') ->
              if debug then Format.eprintf "Copy %a %a@." Segment.pp_segments segs Segment.pp_segments segs';
              Lwt.return begin match Deep.copy ~create_subtrees:true c segs segs' with
                | Ok c ->
                    check_cursor_is_top c;
                    c, parent
                | Error _ -> c, parent
              end
          | `Commit ->
              if debug then Format.eprintf "Commit@.";
              let* c, _, commit = Result_lwt.get_ok_lwt @@ Vc.commit ~parent ~hash_override:None vc c in
              Queue.add commit.hash commits;
              Lwt.return (c, parent)
          | `Checkout commit_hash ->
              if debug then Format.eprintf "Checkout %a@." Commit_hash.pp commit_hash;
              let* res = Vc.checkout vc commit_hash in
              match res with
              | None -> assert false
              | Some (_,c) -> Lwt.return (c, Some commit_hash)
        in
        f parent c (i+1)
    in
    f None (Vc.empty vc) 0
end
