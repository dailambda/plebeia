open Test_utils
open Plebeia
open Lwt.Syntax

let test () = run_lwt @@
  let config =
    Storage.{ head_string = "PLEBEIA TESTxxxxxxxx"
             ; version = 99
             ; bytes_per_cell = 32
             ; max_index = Index.Unsafe.of_int 1000 }
  in
  let fn = temp_file "plebeia" ".dat" in
  let* key = Storage.make_new_writer_key [] in
  let* s =
    Storage.create
      ~resize_step_bytes:20_000 (* one resize will overflow *)
      ~config
      ~key
      fn
  in
  Format.eprintf "current_index %a@." Index.pp (Storage.get_current_length s);
  let i = Result.get_ok @@ Storage.new_indices s (20_000 / 32 - 256 / 32) in
  Format.eprintf "index %a@." Index.pp i;
  (* The next index triggers a resize, which must fail *)
  match Storage.new_index s with
  | Ok _ ->
      let* () = Storage.close s in
      assert false
  | Error (Storage.Index_overflow _) ->
      prerr_endline "closing";
      Storage.close s
  | Error _ -> assert false

let () =
  let open Alcotest in
  run "index_overflow"
    ["index_overflow", [ "test", `Quick, test
                       ] ]
