open Plebeia
open Lwt.Syntax
open Test_utils
open Storage
open Storage.Internal

let config =
  { head_string = "TEST" ^ String.make 16 '\000';
    version = 0;
    bytes_per_cell = 32;
    max_index = Index.max_index;
  }

(* Special header value to stop the test iteration *)
let end_header =
  { Header.last_next_index = Index.Unsafe.of_int 1;
    last_root_index = Some (Index.Unsafe.of_int 1)
  }

let header i =
  let h =
    { Header.last_next_index = Index.Unsafe.of_int (i + 2);
      last_root_index = Some (Index.Unsafe.of_int (i + 2))
    }
  in
  assert (h <> end_header);
  h

let set_header_values s h =
  Storage.Internal.set_current_length s h.Header.last_next_index;
  Storage.set_last_root_index s h.last_root_index

let check_header h =
  if not (Some h.Header.last_next_index = h.last_root_index) then begin
    let* () = Lwt_fmt.eprintf "%a@." Header.pp h in
    assert false
  end else Lwt.return_unit

let single fn sync n =
  let* key = Storage.make_new_writer_key [] in
  let* s = open_for_write ~config ~key fn in
  Format.eprintf "write+read %d headers, single process, sync=%b@." n sync;
  let* (), t =
    with_time_lwt (fun () ->
        for_lwt 1 n (fun i ->
          let h = header i in
          set_header_values s h;
          let* () = if sync then Storage.flush s else Storage.commit s in
          let* res =
            if sync then Header.read_disk_sync s
            else
              let+ res = Header.read_process_sync s in Option.map snd res
          in
          match res with
          | Some h' when h <> h' ->
              Format.eprintf "h: %a  h': %a@." Header.pp h Header.pp h';
              assert false
          | None -> assert false
          | _ -> Lwt.return_unit))
  in
  let* () = close s in
  let* () =
    Lwt_fmt.eprintf "%a for %d header write+read, single process, sync=%b %.2f/sec@."
      Mtime.Span.pp t n sync (float n /. (Utils.MtimeSpan.to_float_s t))
  in
  Lwt.return_unit

let multi_race fn sync n =
  let* () = Lwt_fmt.eprintf "One reader process and one writer process sync=%b, racing for %d headers@." sync n in

  let read s =
    if sync then Header.read_disk_sync s
    else
      let+ res = Header.read_process_sync s in Option.map snd res
  in

  let reader () =
    let* _config, s = Lwt.map Result.get_ok @@ open_existing_for_read fn in
    let rec loop prev reads diffs =
      let* res = read s in
      match res with
      | Some h when h = end_header ->
          Lwt.return (reads+1, diffs+1)
      | Some h ->
          let* () = check_header h in
          let reads = reads + 1 in
          if prev <> Some h then begin
            let diffs = diffs + 1 in
            let* () =
              if diffs mod (n/10) = 0 then Lwt_fmt.eprintf "reader: %d reads %d diffs@." reads diffs
              else Lwt.return_unit
            in
            loop (Some h) reads diffs
          end else begin
            let* () = Lwt_unix.sleep 0.000002 in
            loop prev reads diffs;
          end
      | None ->
          prerr_endline "reader crash";
          exit 1 (* broken! *)
    in
    let* reads, diffs = loop None 0 0 in
    let* () = close s in
    let* () =
      Lwt_fmt.eprintf "reader finished: %d reads (%.2f%%) %d diff items (%.2f%%) msync=%b@."
        reads (float  reads /. float n *. 100.0)
        diffs (float diffs /. float n *. 100.0)
        sync
    in
    exit 0
  in
  let* key = Storage.make_new_writer_key [] in
  let* s = open_for_write ~config ~key fn in

  (* We must initialize the header before the reader starts working. *)
  set_header_values s (header 0);
  let* () = Storage.flush s in

  let write s = if sync then Storage.flush s else Storage.commit s in

  let writer () =
    let* () =
      for_lwt 0 n (fun i ->
          let* () =
            if i mod (n/10) = 0 then Lwt_fmt.eprintf "writer: %d/%d@." i n
            else Lwt.return_unit
          in
          let h = header i in
          set_header_values s h;
          let* () = write s in
          Lwt_unix.sleep 0.000001)
    in
    set_header_values s end_header;
    let* () = write s in
    prerr_endline "writer finished";
    Lwt.return_unit
  in

  match Lwt_unix.fork () with
  | 0 -> reader ()
  | _reader_pid ->
      let* () = Lwt_unix.sleep 0.1 in
      let* (), t = with_time_lwt writer in
      let* () = Lwt_fmt.eprintf "took %a@." Mtime.Span.pp t in
      let* res = Lwt_unix.wait () in
      match res with
      | _reader_pid, WEXITED 0 ->
          (* [close] overwrites the header, therefore must be called
             after the reader exists. *)
          close s
      | _reader_pid, _ -> assert false

(* No writer with a broken data.  The reader must find it broken. *)
let broken fn =
  let* _config, s = Lwt.map Result.get_ok @@ open_existing_for_read fn in

  (* destroy the header *)
  let* fd = Lwt_unix.openfile fn [O_RDWR] 0o777 in
  let* _ = Lwt_unix.lseek fd 32 SEEK_SET in
  let* _ = Lwt_unix.write fd (Bytes.make 64 '\000') 0 64 in (* invalid 2 header values *)
  let* () = Lwt_unix.close fd in
  let* res = Header.read_disk_sync s in
  match res with
  | None -> Lwt.return_unit
  | Some _ -> assert false

let test () =
  run_lwt @@ with_tempdir @@ fun d ->
  let fn = Filename.concat d "data" in
  let* () = single fn false 10000 in
  let* () = single fn true 10000 in
  let* () = multi_race fn false 10000 in
  let* () = multi_race fn true 10000 in
  broken fn

let () =
  let open Alcotest in
  run "header" [
    "header", [ "test", `Quick, test ] ]
