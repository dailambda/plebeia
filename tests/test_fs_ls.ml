open Plebeia
open Test_utils

module Fs = Fs

let ( !/ ) s =
  match String.split_by_char (function '/' -> true | _ -> false) s with
  | "" :: ss -> List.filter (function "" -> false | _ -> true) ss
  | _ -> assert false

let enc = Fs_nameenc.bits8

module Test = struct
  let test () =
    run_ignore_lwt @@
    with_random @@ fun rng ->
    with_context_lwt @@ fun ctxt ->

    if not ctxt.with_count then Lwt.return_unit else

    let open Fs.Op_lwt.Syntax in

    let add n : unit Fs.Op_lwt.t =
      let s = Printf.sprintf "/dir/dir/%d" n in
      Fs.Op_lwt.write !/s (Value.of_string s)
    in

    let module IntSet = Set.Make(struct type t = int let compare (x : int) y = compare x y end) in

    let init () : IntSet.t Fs.Op_lwt.t =
      let rec loop set = function
        | 10000 -> Fs.Op_lwt.return set
        | i ->
            let n = Random.State.int rng 10000 in
            let* () = add n in
            loop (IntSet.add n set) (i+1)
      in
      loop IntSet.empty 0
    in

    let doit () =
      let* is = init () in
      let ss = List.sort compare @@ List.map string_of_int (IntSet.elements is) in
      Format.eprintf "%d files built@." (List.length ss);
      let* xs = Fs.Op_lwt.ls !/"/dir/dir" in
      let xs = List.map fst xs in
      assert (ss = xs);
      let len = List.length ss in
      let rec loop = function
        | 0 -> Fs.Op_lwt.return ()
        | i ->
            let offset = Random.State.int rng len in
            let length =
              (* intentionally overflow *)
              Random.State.int rng ((len - offset) * 2)
            in
            Format.eprintf "offset %d length %d@." offset length;
            let* ys = Fs.Op_lwt.ls2 ~offset ~length !/"/dir/dir" in
            let ys = List.map fst ys in
            let zs = List.take length @@ List.drop offset ss in
            assert (ys = zs);
            loop (i-1)
      in
      loop 100
    in

    let cursor = Fs.empty ctxt enc in

    let open Result_lwt.Syntax in
    let+= res = Fs.Op_lwt.run cursor (doit ()) in
    let _c, _set = Result.get_ok res in
    ()
end

let () =
  let open Alcotest in
  run "fs_ls"
    [ "unit", [ "test", `Quick, Test.test ]
    ]
