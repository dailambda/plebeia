open Test_utils

let test () =
  run_lwt @@ with_random_lwt @@ fun st ->
  for_lwt 0 100 (fun i ->
      ignore_lwt @@ with_memory_only_cursor @@ fun c ->
      let (ops, _c) = Do_random.Flat.do_random ~no_commit:true st 1000 c (Dumb.empty ()) in
      (* Format.eprintf "%d ops@." (List.length ops); *)
      if i mod 100 = 0 then begin
        Format.eprintf "%d done (%d ops)@." i
          (List.length ops);
        (* Debug.save_cursor_to_dot (Printf.sprintf "random%d.dot" i) c *)
      end)

let () =
  let open Alcotest in
  run "full_random_memory_only"
    ["full_random_memory_only", ["test", `Quick, test ]]
