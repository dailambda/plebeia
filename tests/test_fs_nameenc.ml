open Plebeia.Internal
open Test_utils

module P = Plebeia

let test () =
  let path = temp_file "plebeia_names" "txt" in
  let module Compressed = Fs_nameenc_impl.Compressed(struct let path = path end)() in
  List.iter Compressed.Test.test ["hello"; "world"; "data"; "contracts"; "big_map"; "-2"];
  List.iter Compressed.Test.test (List.init 100 string_of_int);
  with_random @@ fun rng ->
  for _ = 0 to 100 do
    let h =
      let open Gen in
      let open Gen.Infix in
      string (int 10 >|= fun i -> i * 2 + 16)
        (elements ['0'; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'; '9';
                   'a'; 'b'; 'c'; 'd'; 'e'; 'f']) rng
    in
    Compressed.Test.test h;
    Compressed.Test.test ("-" ^ h);
  done

let test_init () =
  let path = temp_file "plebeia_names" "txt" in
  let module Compressed = Fs_nameenc_impl.Compressed(struct let path = path end)() in
  with_random @@ fun rng ->
  for i = 0 to 100 do
    Format.eprintf "Loop %d@." i;
    let hl, segl =
      let rec loop () =
        let f _ =
          let open Gen in
          let open Gen.Infix in
          string (int 10 >|= fun i -> i * 2 + 16) alpha_numeric rng
        in
        let hl = List.init 10 f in
        (* Hash collision may occur *)
        List.iteri (fun i h -> Format.eprintf "h%d: %s@." i h) hl;
        match List.map Compressed.Test.to_segment_no_register hl with
        | exception _ -> loop () (* low probability of hash collision *)
        | segl -> hl, segl
      in
      loop ()
    in
    Compressed.init [];
    let exception Assert_is_not_raised in
    begin try
        let so = Compressed.of_segment (List.hd segl) in
        Format.eprintf "??? %a@." (Option.pp Format.pp_print_string) so;
        raise Assert_is_not_raised
      with
      | Assert_is_not_raised as e -> raise e
      | _e -> ()
    end;
    Compressed.init hl;
    let hl' = List.map (fun seg -> Option.get @@ Compressed.of_segment seg) segl in
    assert (hl = hl')
  done


let () =
  let open Alcotest in
  run "fs_name"
    ["unit", [ "test", `Quick, test
             ; "test2", `Quick, test_init]
    ]
