open Plebeia
open Test_utils
open Cursor

let test () =
  let open Result.Infix in
  run_ignore_lwt @@ with_cursor @@ fun c ->
  let c = ok_or_fail @@ create_subtree c (path "LLL") in
  let c, () =
    ok_or_fail @@ Deep.deep ~dont_move:true ~create_subtrees:true c
      [path "LLL"; path "RRR"]
      (fun cur seg -> upsert cur seg (value "LLL/RRR") >>| fun c -> (c, ()))
  in
  let Cursor (trail, _, _) = c in
  if trail <> _Top then begin
    Format.eprintf "deep strange return cursor: %a@." Path.pp (path_of_trail trail);
    assert false
  end;
  let c = ok_or_fail @@ Deep.copy ~create_subtrees:true c [path "LLL"] [path "RRR"] in
  save_cursor_to_dot "copy.dot" c;
  let v = snd @@ ok_or_fail @@ Deep.get_value c [path "RRR"; path "RRR"] in
  assert (v = (value "LLL/RRR"));

  ignore c

let () =
  let open Alcotest in
  run "copy"
    ["copy", ["test", `Quick, test ]]
