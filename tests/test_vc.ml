open Plebeia
open Lwt.Syntax
open Test_utils

let test_reader_open_non_exist () =
  run_lwt @@
  let fn = temp_file "plebeia-nonexist" "context" in
  let* _vc = Lwt.catch
      (fun () -> Vc.open_existing_for_read Context.default_config fn)
      (fun e -> Format.eprintf "Error: %s@." (Printexc.to_string e); exit 0)
  in
  Lwt.return_unit

let () =
  let open Alcotest in
  run "vc"
    ["reader_open_non_exist", [ "test", `Quick, test_reader_open_non_exist ]]
