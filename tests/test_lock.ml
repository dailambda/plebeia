open Plebeia
open Lwt.Syntax

let test () =
  Test_utils.run_lwt @@
  let tempfile = Test_utils.temp_file "plebeia" ".context" in

  let* _lock1 = Lock.lock tempfile in
  prerr_endline "locked";

  match Lwt_unix.fork () with
  | 0 ->
      Lwt.catch
        (fun () ->
           let* _lock2 = Lock.lock tempfile in
           prerr_endline "locked again";
           exit 2)
        (function
          | Unix.Unix_error (Unix.EAGAIN, "lockf", _) ->
              prerr_endline "properly locked";
              exit 0
          | e -> Lwt.fail e)
  | _ ->
      let* res = Lwt_unix.wait () in
      match res with
      | _, WEXITED 0 -> Lwt.return_unit
      | _, WEXITED _ -> assert false
      | _ -> assert false

let () =
  let open Alcotest in
  run "lock"
    ["lock", ["test", `Quick, test ]]
