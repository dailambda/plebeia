open Plebeia
open Test_utils
open Cursor

let test () =
  run_ignore_lwt @@ with_cursor @@ fun c ->
  let c = ok_or_fail @@ create_subtree c (path "LLL") in
  let c = ok_or_fail @@ create_subtree c (path "LLRR") in
  let c, _, _ = Result.get_ok @@ Cursor_storage.write_top_cursor c in
  let c =
    match access_gen c (path "LLR") with
    | Ok (Reached (c, _v)) ->
       from_Some @@ may_forget c
    | _ -> assert false
  in
  let c = go_top c in
  to_file ~file:"remove_up_disk.dot" @@ Debug.dot_of_cursor c;
  match delete c (path "LLL") with
  | Ok c ->
     prerr_endline "ok";
     to_file ~file:"remove_up_disk2.dot" @@ Debug.dot_of_cursor c
  | Error _ -> prerr_endline "error"


let () =
  let open Alcotest in
  run "remove_up_disk"
    ["remove_up_disk", ["test", `Quick, test]]
