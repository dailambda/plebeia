open Plebeia
open Lwt.Syntax
open Test_utils

let name = Gen.(string (return 2) alpha_numeric)
let path = Gen.(list (return 3) name)

module F = Fs

let enc = Fs_nameenc.bits8

let test () =
  ignore @@ with_random @@ fun rng ->
  run_lwt @@ with_vc @@ fun vc ->

  let vc = F.Vc.{ vc; enc } in

  let c = F.Vc.empty vc in

  (* Build a random files and directories *)
  let tbl = Hashtbl.create 101 in
  let rec build i =
    let open F.Op.Syntax in
    if i = 10000 then F.Op.return (List.of_seq (Hashtbl.to_seq tbl))
    else
      let p = path rng in
      let v = name rng in
      Hashtbl.replace tbl p v;
      let* () = F.Op.write p (Value.of_string v) in
      build (i+1)
  in

  let c, pvs = Result.get_ok @@ F.Op.run c @@ build 0 in

  (* Check c is properly populated *)
  let check pvs =
    let open F.Op in
    let open F.Op.Syntax in
    mapM_ (fun (p, v) ->
        let* (_, view) = F.Op.get p in
        match view with
        | Node_type.Leaf (v', _, _) ->
            assert (Value.of_string v = v');
            F.Op.return ()
        | _ -> assert false) pvs
  in

  let c, () = Result.get_ok @@ F.Op.run c @@ check pvs in

  let* _c, (_hash, commit) =
    Lwt.map Result.get_ok @@ F.Vc.commit vc ~parent:None ~hash_override:None c
  in

  let* _, c = Lwt.map Option.get @@ F.Vc.checkout vc commit.hash in

  (* Recheck c is properly poulated again *)
  let _c, () = Result.get_ok @@ F.Op.run c @@ check pvs in

  let* _, c = Lwt.map Option.get @@ F.Vc.checkout vc commit.hash in

  let check_proof c p v =
    Format.eprintf "Proving for %a@." F.Path.pp p;
    let _c, hp = Result.get_ok @@ F.Op.compute_hash c in
    let _c, (proof, details) = Result.get_ok @@ F.Merkle_proof.make [p] c in
    let check_details details =
      match details with
      | [p', _, Some (Node_type.View (Leaf (v', _, _)))] ->
          assert (p = p' && v = Value.to_string v')
      | [p', _, Some n] ->
          Format.eprintf "%a : %a@." F.Path.pp p' Node.pp n;
          assert false
      | _ -> assert false
    in
    check_details details;
    Format.eprintf "proof: %a@." F.Merkle_proof.pp proof;
    let hp', details' = Result.get_ok @@ F.Merkle_proof.check vc proof in
    check_details details';
    assert (hp = hp')
  in

  let proof_from_root c =
    (* Pick a random file.  ex.  /ab/cd/ef *)
    let p, v = Gen.elements pvs rng in
    check_proof c p v
  in

  proof_from_root c;

  let proof_from_middle c =
    let p, v = Gen.elements pvs rng in
    let dir = [List.hd p] in
    let path = List.tl p in
    (* Build a Merkle proof from the middle of the tree. *)
    Format.eprintf "Proving for %a from %a@." F.Path.pp dir F.Path.pp p;
    (* c2: the tree at dir *)
    let c2, () = Result.get_ok @@ F.Op.chdir dir c in
    check_proof c2 path v
  in

  proof_from_middle c;

  Lwt.return_unit

let () =
  let open Alcotest in
  run "fs_merkle_proof"
    ["fs_merkle_proof", [ "test", `Quick, test ]]
