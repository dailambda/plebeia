open Plebeia
open Test_utils

let test rng nfiles sizes =
  let sizes = List.filter (fun x -> x * 10 < nfiles) sizes in
  with_context @@ fun ctxt ->
  let path_bits = 32 (* contracts index is encoded to 44 hex chars = 22 bytes = 176 bits *) in
  let c = Cursor.empty ctxt in
  let c, segs = prepare_tree rng c ~path_bits ~nfiles ~vbytes:64 in
  Format.eprintf "Tree created@.";
  let c, hash = Cursor.compute_hash c in
  let Cursor.Cursor (_, n, _) = c in
  let keys = List.map (fun seg -> Key.of_segments [seg]) segs in
  let test nkeys =
    let keys = Gen.nelements nkeys keys rng in
    let proof, _info = Merkle_proof.make ctxt n
        (List.map (fun k ->
             match Key.to_path k with
             | Some p -> p
             | _ -> assert false) keys)
    in
    let hash', _info = Merkle_proof.check ctxt.Context.hasher proof in
    assert (hash = hash');
    let bytes =
      match Data_encoding.Binary.to_bytes (Merkle_proof.encoding ctxt) proof with
      | Ok bytes -> bytes
      | Error e ->
          Format.eprintf "%a@." Data_encoding.Binary.pp_write_error e;
          assert false
    in
    let size = Bytes.length bytes in
    let proof' =
      match Data_encoding.Binary.of_bytes (Merkle_proof.encoding ctxt) bytes with
      | Ok proof' -> proof'
      | Error e ->
          Format.eprintf "%a@." Data_encoding.Binary.pp_read_error e;
          assert false
    in
    let hash', _info = Merkle_proof.check ctxt.Context.hasher proof' in
    assert (hash = hash');
    let stat = Snapshot.Stat.get @@ Merkle_proof.Tree.snapshot ctxt proof.tree in
    Format.eprintf "stat: nkeys %d : size: %d, %a@." nkeys size Snapshot.Stat.pp stat;
    bytes
  in
  List.map test sizes

let random_test nfiles =
  run_ignore_lwt @@ with_random_lwt @@ fun rng ->
  let nfiles = match nfiles with None -> 10_000 | Some n -> n in
  test rng nfiles [1; 10; 100; 1_000; 10_000]

let regression_test nfiles =
  match nfiles with
  | Some _ -> () (* if specified no regression test *)
  | None ->
      run_lwt @@ Lwt.map (regression_by_hash __LOC__ 367146464) @@
      test (RS.make [| 0 |]) 10_000 [10]

let sizes =
  let doc = "test sizs" in
  let open Cmdliner.Arg in
  value
  & opt (some int) None
  & info ["n"; "nfiles"] ~doc ~docv:"NUM"

let () =
  Alcotest.run_with_args "api" sizes
    [
      "merkle_proof_bench", [ "random", `Quick, random_test
                            ; "regression", `Quick, regression_test
                            ]
    ]
