Require Import Program.
Require Import Base.
Require  Value Segment.

Module Prefix.
  Parameter t : Set.
  Parameter zero : t.
End Prefix.

Definition t := (Prefix.t * ocaml_string)%type.

Parameter make_h : option ocaml_int -> ocaml_int -> list ocaml_string -> Prefix.t.
Parameter to_strings : t -> list ocaml_string.

Definition of_bud node_opt :=
  match node_opt with
  | None => (Prefix.zero, string_empty)
  | Some hash => (make_h (Some 3) 2 @@ to_strings hash, string_empty)
  end.

Definition of_leaf v := (make_h None 0 [Value.to_string v], string_empty).

Parameter merge : t -> t -> list ocaml_string.
Infix "^^" := merge (at level 50).

Definition of_internal l r :=
  (make_h (Some 0) 1 (l ^^ r), string_empty).

Definition of_extender seg (h : t) :=
  (fst h,  Segment.encode seg).
