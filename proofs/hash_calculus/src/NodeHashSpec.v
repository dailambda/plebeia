Require Import Program Omega.
Require Import Base Hash HashSpec Node NodeHash.

Inductive Is_Extender : node -> Prop :=
| IE : forall seg n, Is_Extender (Extender seg n).
Hint Constructors Is_Extender : core.

Inductive Invariant_node : node -> Prop :=
| InvBudNone : Invariant_node (Bud None)
| InvBudSome : forall n, Invariant_node n -> Invariant_node (Bud (Some n))
| InvLeaf : forall v, Invariant_node (Leaf v)
| InvInt : forall n1 n2, Invariant_node n1 -> Invariant_node n2 ->
                         Invariant_node (Internal n1 n2)
| InvExt : forall seg n, Invariant_node n -> ~Segment.IsEmpty seg -> ~ Is_Extender n ->
    Invariant_node (Extender seg n).

Lemma variable_length_part_is_empty_except_extender : forall node,
    ~Is_Extender node -> snd (compute node) = string_empty.
Proof.
  destruct node as [[|]| | | ]; simpl; try reflexivity.
  now destruct 1.
Qed.

Theorem hash_CR : forall n1 n2,
    Invariant_node n1 -> Invariant_node n2 ->
    compute n1 = compute n2 -> n1 = n2.
Proof.
  induction n1 using node_ind'; intros n2 Inv1 Inv2.
  - (* n1 = Bud None *)
    destruct n2 as [[n' |] | | |]; unfold compute; fold compute; intro hash_eq.
    + edestruct not_eq_of_bud_Some_None. now rewrite <- hash_eq.
    + reflexivity.
    + edestruct not_eq_of_bud_of_leaf. exact hash_eq.
    + edestruct not_eq_of_bud_of_internal. exact hash_eq.
    + edestruct not_eq_of_bud_of_extender. exact hash_eq.
  - (* n1 = Bud (Some n) *)
    destruct n2 as [[n' |] | | |]; unfold compute; fold compute; intro hash_eq.
    + do 2 f_equal. apply IHn1; [now inversion Inv1 | now inversion Inv2 |].
      now injection (of_bud_injective _ _ hash_eq).
    + edestruct not_eq_of_bud_Some_None. now rewrite <- hash_eq.
    + edestruct not_eq_of_bud_of_leaf. exact hash_eq.
    + edestruct not_eq_of_bud_of_internal. exact hash_eq.
    + edestruct not_eq_of_bud_of_extender. exact hash_eq.
  - (* n1 = Leaf v *)
    destruct n2 as [[n' |] | | |]; unfold compute; fold compute; intro hash_eq.
    + edestruct not_eq_of_bud_of_leaf. now rewrite <- hash_eq.
    + edestruct not_eq_of_bud_of_leaf. now rewrite <- hash_eq.
    + apply HashSpec.of_leaf_injective in hash_eq. now rewrite hash_eq.
    + edestruct not_eq_of_leaf_of_internal. now rewrite hash_eq.
    + edestruct not_eq_of_leaf_of_extender. now rewrite hash_eq.
  - (* n1 = Internal n1_1 n2_2 *)
    destruct n2 as [[n' |] | | |]; unfold compute; fold compute; intro hash_eq.
    + edestruct not_eq_of_bud_of_internal. now rewrite <- hash_eq.
    + edestruct not_eq_of_bud_of_internal. now rewrite <- hash_eq.
    + edestruct not_eq_of_leaf_of_internal. now rewrite <- hash_eq.
    + (* n2 = Internal n2_1 n2_2 *)
      injection (HashSpec.of_internal_injective _ _ _ _ hash_eq). intros eq1 eq2.
      rewrite (IHn1_1 n2_1); [| now inversion Inv1| now inversion Inv2| assumption].
      rewrite (IHn1_2 n2_2); [| now inversion Inv1| now inversion Inv2| assumption].
      reflexivity.
    + (* n2 = Extender seg n2 *)
      edestruct not_eq_of_internal_of_extender. now rewrite hash_eq.
  - (* n1 = Extender seg n *)
    destruct n2 as [[n' |] | | |]; unfold compute; fold compute; intro hash_eq.
    + edestruct not_eq_of_bud_of_extender. now rewrite hash_eq.
    + edestruct not_eq_of_bud_of_extender. now rewrite hash_eq.
    + edestruct not_eq_of_leaf_of_extender. now rewrite hash_eq.
    + (* n2 = Internal n2_1 n2_2 *)
      edestruct not_eq_of_internal_of_extender. now rewrite hash_eq.
    + (* n2 = Extender seg' n2 *)
      inversion Inv1. inversion Inv2. subst.
      assert (snd (compute n1) = snd (compute n2)).
      * now do 2 (rewrite variable_length_part_is_empty_except_extender; [|assumption]).
      * injection (of_extender_injective _ _ _ _ H hash_eq). intros eq1 eq2. rewrite <- eq2.
        now rewrite (IHn1 n2).
Qed.
