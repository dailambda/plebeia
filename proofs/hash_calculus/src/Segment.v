Require Import Base.

Parameter t : Set.
Parameter encode : t -> ocaml_string.

Parameter IsEmpty : t -> Prop.

Axiom encode_non_empty : forall seg, encode seg <> string_empty.
Axiom encode_injective : forall x y, encode x = encode y -> x = y.
