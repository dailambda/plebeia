Require Import String.
Require Import Base Value Segment.

Inductive node : Set :=
| Bud : option node -> node
| Leaf : Value.t -> node
| Internal : node -> node -> node
| Extender : Segment.t -> node -> node.

Fixpoint node_ind' (P : node -> Prop)
         (Case1 : P (Bud None))
         (Case2 : forall n : node, P n -> P (Bud (Some n)))
         (Case3 : forall v : Value.t, P (Leaf v))
         (Case4 : forall n1, P n1 -> forall n2, P n2 -> P (Internal n1 n2))
         (Case5 : forall (seg : Segment.t) (n : node), P n -> P (Extender seg n))
         (n : node) : P n :=
  match n with
  | Bud None => Case1
  | Bud (Some n) => Case2 n (node_ind' P Case1 Case2 Case3 Case4 Case5 n)
  | Leaf v => Case3 v
  | Internal n1 n2 => Case4 n1 (node_ind' P Case1 Case2 Case3 Case4 Case5 n1)
                            n2 (node_ind' P Case1 Case2 Case3 Case4 Case5 n2)
  | Extender seg n => Case5 seg n (node_ind' P Case1 Case2 Case3 Case4 Case5 n)
  end.
