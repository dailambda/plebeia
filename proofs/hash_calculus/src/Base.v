Require Ascii.
Parameter ocaml_int : Set.

Parameters int_zero int_one int_two int_three : ocaml_int.
Notation "0" := int_zero.
Notation "1" := int_one.
Notation "2" := int_two.
Notation "3" := int_three.

Axiom one_two : 1 <> 2.
Hint Immediate one_two : core.

Parameter ocaml_string : Set.
Parameter string_empty : ocaml_string.
Parameter string_append : ocaml_string -> ocaml_string -> ocaml_string.
Infix "^" := string_append.
Parameter string_length : ocaml_string -> nat.

Axiom string_append_nil_l : forall xs, string_empty ^ xs = xs.
Axiom string_append_nil_r : forall xs, xs ^ string_empty = xs.
Axiom string_append_assoc : forall xs ys zs,
    (xs ^ ys) ^ zs = xs ^ (ys ^ zs).
Axiom string_append_injective_with_length : forall xs1 xs2 ys1 ys2,
    (xs1 ^ ys1) = (xs2 ^ ys2) -> string_length ys1 = string_length ys2 -> (xs1, ys1) = (xs2, ys2).
Axiom string_length_append : forall xs ys,
    string_length (xs ^ ys) = string_length xs + string_length ys.

Parameter char_of_nat : nat -> Ascii.ascii.
Parameter string_make1 : Ascii.ascii -> ocaml_string.
Axiom string_length_make1 : forall c, string_length (string_make1 c) = 1%nat.
Axiom char_of_nat_injective : forall n1 n2, char_of_nat n1 = char_of_nat n2 -> n1 = n2.
Axiom string_make1_injective : forall c1 c2, string_make1 c1 = string_make1 c2 -> c1 = c2.

Infix "@@" := (fun f x => f x) (at level 60).
