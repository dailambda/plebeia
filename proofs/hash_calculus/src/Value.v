Require Import Base.

Parameter t : Set.
Parameter to_string : t -> ocaml_string.

Axiom to_string_inj : forall x y, to_string x = to_string y -> x = y.
