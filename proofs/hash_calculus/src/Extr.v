Require Import ExtrOcamlBasic.
Require Import ExtrOcamlNatInt.
Require Hash.

Extraction Language OCaml.

Extract Inlined Constant fst => "fst".
Extract Inlined Constant snd => "snd".

Extract Inlined Constant Base.ocaml_int => "int".
Extract Inlined Constant Base.int_zero  => "0".
Extract Inlined Constant Base.int_one   => "1".
Extract Inlined Constant Base.int_two   => "2".
Extract Inlined Constant Base.int_three => "3".
Extract Inlined Constant Base.ocaml_string => "string".
Extract Inlined Constant Base.string_empty => """""".

Extract Inlined Constant Value.t => "Value.t".
Extract Inlined Constant Value.to_string => "Value.to_string".

Extract Inlined Constant Segment.t => "Segment.t".
Extract Inlined Constant Segment.encode => "Segment.encode".

Extract Inlined Constant Hash.Prefix.t => "Hash.Prefix.t".
Extract Inlined Constant Hash.Prefix.zero => "Hash.Prefix.zero".
Extract Inlined Constant Hash.make_h => "(fun set_last_2bits n ss -> Hash.make_h ~set_last_2bits n ss)".
Extract Inlined Constant Hash.to_strings => "Hash.to_strings".
Extract Inlined Constant Hash.merge => "Hash.(^^)".

Extraction "coq_hash.ml" Hash.of_bud Hash.of_internal Hash.of_leaf Hash.of_extender.
