Require Import Base Value Hash Node.

Fixpoint compute node :=
  match node with
  | Bud None => Hash.of_bud None
  | Bud (Some n) =>
    Hash.of_bud (Some (compute n))
  | Leaf v => Hash.of_leaf v
  | Internal n1 n2 => Hash.of_internal (compute n1) (compute n2)
  | Extender seg n => Hash.of_extender seg (compute n)
  end.
