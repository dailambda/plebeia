(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** { 1 Node hash } *)

val of_bud : Hash.Hasher.t -> Hash.Long.t option -> Hash.t
val of_internal : Hash.Hasher.t -> Hash.Long.t -> Hash.Long.t -> Hash.t
val of_leaf : Hash.Hasher.t -> Value.t -> Hash.t
val of_extender : Segment.segment -> Hash.t -> Hash.Long.t

val compute :
  Hash.Hasher.t
  -> (Node_type.t -> [< `Hashed of Hash.Long.t * Node_type.t | `Not_Hashed of Node_type.view ])
  -> Node_type.t
  -> Node_type.t * Hash.Long.t
(** Compute the node hash of the given node.

    The function argument for short cutting.  It returns the hash of the node if it is
    available without traversing the node's children.

    Tail recursive.
*)

val compute_without_context : Hash.Hasher.t -> Node_type.t -> (Node_type.t * Hash.Long.t) option
(** Compute the node hash, but without the loading of Disk nodes.
    If the node contains [Disk _], it returns [None] *)
