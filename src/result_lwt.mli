(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
(** { 1 Result monad } *)

type ('a, 'z) t = ('a, 'z) Result.t Lwt.t

val return : 'a -> ('a, 'z) t
val bind : ('a, 'z) t -> ('a -> ('b, 'z) t) -> ('b, 'z) t
val map : ('a -> 'b) -> ('a, 'z) t -> ('b, 'z) t
val mapM : ('a -> ('b, 'z) t) -> 'a list -> ('b list, 'z) t
val mapM_ : ('a -> (unit, 'z) t) -> 'a list -> (unit, 'z) t
val fold_leftM : ('a -> 'b -> ('a, 'z) t) -> 'a -> 'b list -> ('a, 'z) t

val map_error : ('err1 -> 'err2) -> ('a, 'err1) t -> ('a, 'err2) t

val get_ok_lwt : ('a, _) t -> 'a Lwt.t

val get_error_lwt : (_, 'e) t -> 'e Lwt.t

val value : ('a, 'e) t -> default:('e -> 'a) -> 'a Lwt.t
(** Error recovery *)

val errorf : ('a, unit, string, ('b, string) t) format4 -> 'a
(** Printf interface to produce a string error *)

module Infix : sig
  (* =? : Result+Lwt monad *)
  val ( >>=? ) : ('a, 'b) t -> ('a -> ('c, 'b) t) -> ('c, 'b) t
  val ( >|=? ) : ('a, 'b) t -> ('a -> 'c) -> ('c, 'b) t

  (* ? : Result monad *)
  val ( >>? ) : ('a, 'b) result -> ('a -> ('c, 'b) result) -> ('c, 'b) result
  val ( >|? ) : ('a, 'b) result -> ('a -> 'c) -> ('c, 'b) result

  (* = : Lwt monad *)
  val ( >>= ) : 'a Lwt.t -> ('a -> 'b Lwt.t) -> 'b Lwt.t
  val ( >|= ) : 'a Lwt.t -> ('a -> 'b) -> 'b Lwt.t
end

module Syntax : sig
  (* =? : Result+Lwt monad *)
  val ( let* ) : ('a, 'b) t -> ('a -> ('c, 'b) t) -> ('c, 'b) t
  val ( let+ ) : ('a, 'b) t -> ('a -> 'c) -> ('c, 'b) t

  (* ? : Result monad *)
  val ( let*? ) : ('a, 'b) result -> ('a -> ('c, 'b) result) -> ('c, 'b) result
  val ( let+? ) : ('a, 'b) result -> ('a -> 'c) -> ('c, 'b) result

  (* = : Lwt monad *)
  val ( let*= ) : 'a Lwt.t -> ('a -> 'b Lwt.t) -> 'b Lwt.t
  val ( let+= ) : 'a Lwt.t -> ('a -> 'b) -> 'b Lwt.t
end
