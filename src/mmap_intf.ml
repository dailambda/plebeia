module type S = sig
  (** Memory buffer *)
  module Buffer : sig
    type t

    (** length in bytes *)
    val len : t -> int

    val to_string : t -> string

    val get_char : t -> int -> char
    val set_char : t -> int -> char -> unit

    val get_uint8 : t -> int -> int
    val set_uint8 : t -> int -> int -> unit

    val get_uint16 : t -> int -> int
    val set_uint16 : t -> int -> int -> unit

    val get_uint32 : t -> int -> int
    val set_uint32 : t -> int -> int -> unit

    val get_uint64 : t -> int -> int64
    val set_uint64 : t -> int -> int64 -> unit

    val get_index : t -> int -> Index.t
    val set_index : t -> int -> Index.t -> unit

    (** [copy t off len] copies the contents of the given region
        to a string *)
    val copy : t -> int -> int -> string

    val blit_from_string : string -> int -> t -> int -> int -> unit

    (** [write_string s t off] is equivalent
        with [blit_from_string s 0 t off (String.length s)] *)
    val write_string : string -> t -> int -> unit

    (** Create a buffer on memory of the specified bytes *)
    val create : int -> t

    (** Copy the given buffer to a new buffer on memory *)
    val duplicate : t -> t
  end

  (** Type of mmap'ed file *)
  type t

  (** [make fd ~pos ~shared len] maps the region of the file
      specified by [fd], [pos], and [len] to an mmap.
      If [shared=true], the modifications to [t] is shared with
      other processes and written back to the file.
  *)
  val make :
    Unix.file_descr
    -> pos:int
    -> shared:bool
    -> int
    -> t

  (** Build a dummy empty mmap.  [get_buffer] always fails. *)
  val null : t

  val is_null : t -> bool

  (** Call [msync(2)] to flush the changes to the mapped file *)
  val msync : t -> unit
  val msync2 : t -> off:int -> len:int -> unit

  val msync_lwt : t -> unit Lwt.t
  val msync2_lwt : t -> off:int -> len:int -> unit Lwt.t

  val madvise_random : t -> unit

  (** Get a buffer to read/write the specified region of the mmap *)
  val get_buffer : off:int -> len:int -> t -> Buffer.t

  (** The length of the mmap in bytes *)
  val len : t -> int

  val get_char : t -> int -> char

  (** On memory map *)
  val init : int -> (int -> char) -> t
end
