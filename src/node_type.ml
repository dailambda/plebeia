(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Arthur Breitman                                        *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Utils

type node =
  | Disk of Index.t
  | View of view
  | Hash of Hash.Long.t

and view =
  | Internal of node * node
               * Index.t option
               * Hash.t option
  (* An internal node, left and right children *)

  | Bud of node option
          * Index.t option
          * Hash.t option
  (* Buds represent the end of a segment and the beginning of a new tree. They
     are used whenever there is a natural hierarchical separation in the key
     or, in general, when one wants to be able to grab sub-trees. For instance
     the big_map storage of a contract in Tezos would start from a bud. *)

  | Leaf of Value.t
          * Index.t option
          * Hash.t option
  (* Leaf of a tree, the end of a path, contains or points to a value.

     XXX Value can be huge.  It would be nice if we can load the hash without
     loading the value *)

  | Extender of Segment.t
                * node
                * Index.t option
                * Hash.t option
  (* Extender node, contains a path to the next node. Represents implicitely
     a collection of internal nodes where one child is Null. *)
  (* XXX Q: [Extender] should only take encoded segments?
         A: Sounds reasonable once we stop using temporary _Extender creations
            in diff.ml *)

(* A trail represents the content of the memory stack when recursively exploring a tree.
   Constructing these trails from closure would be easier, but it would make it harder
   to port the code to C. The type parameters of the trail keep track of the type of each
   element on the "stack" using a product type. *)

type t = node

let indexed = function
  | Disk _ -> true
  | View ( Bud (_, Some _, _)
         | Leaf (_, Some _, _)
         | Internal (_, _, Some _, _)
         | Extender (_, _, Some _, _) ) -> true
  | View _ -> false
  | Hash _ -> false

let index_of_view = function
  | Bud (_, Some i, _) -> Some i
  | Leaf (_, Some i, _) -> Some i
  | Internal (_, _, Some i, _) -> Some i
  | Extender (_, _, Some i, _) -> Some i
  | Bud _ | Leaf _ | Internal _ | Extender _ -> None

let index = function
  | Disk i -> Some i
  | View v -> index_of_view v
  | Hash _ -> None

let hashed = function
  | Disk _ -> true
  | View (Bud (_, _, Some _)) -> true
  | View (Bud (_, _, None)) -> false
  | View (Leaf (_, _, Some _)) -> true
  | View (Leaf (_, _, None)) -> false
  | View (Internal (_, _, _, Some _)) -> true
  | View (Internal (_, _, _, None)) -> false
  | View (Extender (_, _, _, Some _)) -> true
  | View (Extender (_, _, _, None)) -> false
  | Hash _ -> true

let hash_available = function
  | Disk _ -> true (* can be loaded from the disk *)
  | View (Bud (_, _, Some _)) -> true
  | View (Bud (_, Some _, None)) -> true
  | View (Bud (_, _, None)) -> false
  | View (Leaf (_, _, Some _)) -> true
  | View (Leaf (_, Some _, None)) -> true
  | View (Leaf (_, _, None)) -> false
  | View (Internal (_, _, _, Some _)) -> true
  | View (Internal (_, _, Some _, None)) -> true
  | View (Internal (_, _, _, None)) -> false
  | View (Extender (_, _, _, Some _)) -> true
  | View (Extender (_, _, Some _, None)) -> true
  | View (Extender (_, _, _, None)) -> false
  | Hash _ -> true

let hash_prefix_of_view v =
  match v with
  | Bud (_, _, Some h)
  | Leaf (_, _, Some h)
  | Internal (_, _, _, Some h)
  | Extender (_, _, _, Some h) -> Some h
  | Bud (_, _, None)
  | Leaf (_, _, None)
  | Internal (_, _, _, None)
  | Extender (_, _, _, None) -> None

(* XXX compile time switch the invariant checks *)
module Invariant_view : sig
  val check : view -> view
end = struct
  (* XXX Do we use error_monad ? *)
  type Error.t += Node_invariant of string

  let () = Error.register_printer (function
      | Node_invariant s -> Some ("Node invariant: " ^ s)
      | _ -> None)

  let error_node_invariant s = Error (Node_invariant s)

  let view_shape_invariant : view -> (unit, Error.t) Result.t = function
    | Bud (None, _, _) -> Ok ()
    | Bud (Some (Disk _), _, _) ->
        (* We do not check whether Disk points non Bud node here *)
        Ok ()
    | Bud (Some (View (Bud _)), _, _) -> error_node_invariant "Bud: cannot have Bud"
    | Bud (Some (View (Leaf _)), _, _) -> error_node_invariant "Bud: cannot have Leaf"
    | Bud (Some (View (Internal _)), _, _) -> Ok ()
    | Bud (Some (View (Extender _)), _, _) -> Ok ()
    | Bud (Some (Hash _), _, _) -> Ok ()
    | Extender (_, Disk _, _, _) ->
        (* We should check Disk must not point to an Extender, but it is impossible *)
        Ok ()
    | Extender (_, View (Extender _), _, _) -> error_node_invariant "Extender: cannot have Extender"
    | Extender (_, View _, _, _) -> Ok ()
    | Extender (_, Hash (_,""), _, _) -> Ok ()
    | Extender (_, Hash (_,_), _, _) ->
        error_node_invariant "Extender: cannot have Hash with postfix"
    | Leaf _ -> Ok ()
    | Internal _ -> Ok ()

  let view_indexed_invariant : view -> (unit, Error.t) Result.t = function
    | Bud (None, Some _, _) -> Ok ()
    | Bud (Some n, Some _, _) when indexed n -> Ok ()
    | Bud (_, None, _) -> Ok ()
    | Leaf (_, Some _, _) -> Ok ()
    | Leaf (_, None, _) -> Ok ()
    | Internal (l, r, Some _i, _) ->
        begin match index l, index r with
          | None, _ -> error_node_invariant "Internal: invalid index"
          | _, None -> error_node_invariant "Internal: invalid index"
  (* We abandoned this strict invariant on internals.
          | Some li, Some ri ->
              if Index.(i - li = one || i - ri = one) then Ok ()
              else error_node_invariant "Internal: invalid indices"
  *)
          | _ -> Ok () (* we now use fat internals *)
        end
    | Internal (_l, _r, None, _) -> Ok ()
    | Extender (_, n, Some _, _) when indexed n -> Ok ()
    | Extender (_, _, None, _) -> Ok ()
    | Bud (_, Some _, _)
    | Extender (_, _, Some _, _)  -> error_node_invariant "Invalid Indexed"

  let view_hashed_invariant : view -> (unit, Error.t) Result.t = function
    | Leaf _ -> Ok ()
    | Bud (None, _, _) -> Ok ()
    | Bud (_, _, None) -> Ok ()
    | Bud (Some n, _, Some _) when hash_available n -> Ok ()
    | Internal (l, r, _, Some _) when hash_available l && hash_available r -> Ok ()
    | Internal (_, _, _, None) -> Ok ()
    | Extender (_, n, _, Some _) when hash_available n -> Ok ()
    | Extender (_, _, _, None) -> Ok ()
    | _ -> error_node_invariant "Invalid Hashed"

  let extender_segment_length_invariant = function
    | Extender (seg, _, _, _) ->
        let len = Segment.length seg in
        if 0 < len && len <= Segment.max_length then Ok ()
        else error_node_invariant (Printf.sprintf "invalid segment length %d" len)
    | _ -> Ok ()

  let view_invariant : view -> (unit, Error.t) Result.t = fun v ->
    let open Result_lwt.Infix in
    view_shape_invariant v >>? fun () ->
    view_indexed_invariant v >>? fun () ->
    view_hashed_invariant v >>? fun () ->
    extender_segment_length_invariant v

  let _check v =
    match view_invariant v with
    | Ok _ -> v
    | Error e -> failwith (Error.show e)

  (* Currently the check is off *)
  let check x = x
end

let [@inline] _Internal (n1, n2, ir, hit) =
  Invariant_view.check @@ Internal (n1, n2, ir, hit)

let [@inline] _Bud (nopt, ir, hit) =
  Invariant_view.check @@ Bud (nopt, ir, hit)

let [@inline] _Leaf (v, ir, hit) =
  Invariant_view.check @@ Leaf (v, ir, hit)

let [@inline] _Extender (seg, n, ir, hit) =
  Invariant_view.check @@ Extender (seg, n, ir, hit)

let [@inline] new_leaf v = View (_Leaf (v, None, None))

(* XXX Why only new_extender is so "kind"?
   Sato: it was used in Cursor.remove_up
*)
let [@inline] new_extender : Segment.segment -> node -> node = fun segment node ->
  if Segment.is_empty segment then node (* XXX We should simply reject it *)
  else
    (* This connects segments *)
    match node with
    | View (Extender (seg, n, _, _)) ->
        View (_Extender (Segment.append segment seg, n, None, None))
    | _ ->
        (* XXXX If the subnode is Disk, we have to make sure merging *)
        View (_Extender (segment, node, None, None))

let [@inline] new_bud no = View (_Bud (no, None, None))

let [@inline] new_internal n1 n2 = View (_Internal (n1, n2, None, None))

let may_forget = function
  | Disk _ as n -> Some n
  | View (Internal (_, _, Some i, _)) -> Some (Disk i)
  | View (Bud (_, Some i, _)) -> Some (Disk i)
  | View (Leaf (_, Some i, _)) -> Some (Disk i)
  | View (Extender (_, _, Some i, _)) -> Some (Disk i)
  | _ -> None

let rec pp ppf = function
  | Hash h ->
      Format.fprintf ppf "Hash %a" Hash.Long.pp h
  | Disk i ->
      Format.fprintf ppf "Disk %a" Index.pp i
  | View v ->
      let f = function
        | Internal (n1, n2, _i, _h) ->
            Format.fprintf ppf "@[<v2>Internal@ L %a@ R %a@]"
              pp n1
              pp n2
        | Bud (None, _, _) ->
            Format.fprintf ppf "Bud None"
        | Bud (Some n, _, _) ->
            Format.fprintf ppf "@[<v2>Bud@ %a@]"
              pp n
        | Leaf (v, _, _) ->
            Format.fprintf ppf "Leaf %s"
              (let s = Value.to_hex_string v in
               if String.length s > 16 then String.sub s 0 16 ^ "..." else s)
        | Extender (seg, n, _, _) ->
            Format.fprintf ppf "@[<v2>Extender %s@ %a@]"
              (Segment.to_string seg)
              pp n
      in
      f v

module Mapper = struct
  type job =
    | BudSome of Index.t option * Hash.t option
    | Do of t
    | Extender of Segment.segment * Index.t option * Hash.t option
    | Internal of Index.t option * Hash.t option

  type 'a state = job list * (t * 'a) list

  type 'a mkview =
    { bud : (node * 'a) option -> Index.t option -> Hash.t option -> node * 'a
    ; extender : Segment.t -> node * 'a -> Index.t option -> Hash.t option -> node * 'a
    ; internal : node * 'a -> node * 'a -> Index.t option -> Hash.t option -> node * 'a
    ; leaf : Value.t -> Index.t option -> Hash.t option -> node * 'a
    }

  let default_mkview =
    { bud= (function
          | None -> fun i h -> View (_Bud (None, i, h)), ()
          | Some (n,()) -> fun i h -> View (_Bud (Some n, i, h)), ())
    ; extender= (fun seg (n,()) i h -> View (_Extender (seg, n, i, h)), ())
    ; internal= (fun (nl,()) (nr,()) i h -> View (_Internal (nl, nr, i, h)), ())
    ; leaf= (fun v i h -> View (_Leaf (v, i, h)), ())
    }

  let step ~node ~view:mkview (js, ss) = match js, ss with
    | [], [s] -> `Right s
    | [], _ -> assert false
    | BudSome (i,h)::js, s::ss ->
        let s = mkview.bud (Some s) i h in
        `Left (js, (s::ss))
    | Extender (seg,i,h)::js, s::ss ->
        let s = mkview.extender seg s i h  in
        `Left (js, (s::ss))
    | Internal (i,h)::js, sr::sl::ss ->
        let s = mkview.internal sl sr i h in
        `Left (js, (s::ss))
    | (BudSome _ | Extender _ | Internal _)::_, _ -> assert false
    | Do n::js, ss ->
        match node n with
        | `Return s -> `Left (js, (s::ss))
        | `Continue v ->
            match v with
            | Leaf (v, i, h) ->
                let s = mkview.leaf v i h in
                `Left (js, s::ss)
            | Bud (None, i, h) ->
                let s = mkview.bud None i h in
                `Left (js, s::ss)
            | Bud (Some n, i, h) ->
                `Left ((Do n :: BudSome (i,h) :: js), ss)
            | Extender (seg, n, i, h) ->
                `Left ((Do n :: Extender (seg, i,h) :: js), ss)
            | Internal (nl, nr, i, h) ->
                `Left ((Do nl :: Do nr :: Internal (i,h) :: js), ss)

  let interleaved ~node ~view n =
    let js_ss = [Do n], [] in
    let stepper x = step ~node ~view x in
    stepper, js_ss

  let map ~node ~view n =
    let rec loop js_ss = match step ~node ~view js_ss with
      | `Right res -> res
      | `Left js_ss -> loop js_ss
    in
    loop ([Do n], [])
end

module Fold = struct
  type job =
    | BudSome of Index.t option * Hash.t option
    | Do of t
    | Extender of Segment.segment * Index.t option * Hash.t option
    | Internal of Index.t option * Hash.t option

  type 'a state = job list * 'a list

  type 'a leave =
    { bud : 'a option -> Index.t option -> Hash.t option -> 'a
    ; extender : Segment.t -> 'a -> Index.t option -> Hash.t option -> 'a
    ; internal : 'a -> 'a -> Index.t option -> Hash.t option -> 'a
    ; leaf : Value.t -> Index.t option -> Hash.t option -> 'a
    }

  let default_leave_rebuild =
    { bud= (function
          | None -> fun i h -> View (_Bud (None, i, h))
          | Some n -> fun i h -> View (_Bud (Some n, i, h)))
    ; extender= (fun seg n i h -> View (_Extender (seg, n, i, h)))
    ; internal= (fun nl nr i h -> View (_Internal (nl, nr, i, h)))
    ; leaf= (fun v i h -> View (_Leaf (v, i, h)))
    }

  let step ~enter ~leave (js, ss) = match js, ss with
    | [], [s] -> `Right s
    | [], _ -> assert false
    | BudSome (i,h)::js, s::ss ->
        let s = leave.bud (Some s) i h in
        `Left (js, (s::ss))
    | Extender (seg,i,h)::js, s::ss ->
        let s = leave.extender seg s i h  in
        `Left (js, (s::ss))
    | Internal (i,h)::js, sr::sl::ss ->
        let s = leave.internal sl sr i h in
        `Left (js, (s::ss))
    | (BudSome _ | Extender _ | Internal _)::_, _ -> assert false
    | Do n::js, ss ->
        match enter n with
        | `Return s -> `Left (js, (s::ss))
        | `Continue v ->
            match v with
            | Leaf (v, i, h) ->
                let s = leave.leaf v i h in
                `Left (js, s::ss)
            | Bud (None, i, h) ->
                let s = leave.bud None i h in
                `Left (js, s::ss)
            | Bud (Some n, i, h) ->
                `Left ((Do n :: BudSome (i,h) :: js), ss)
            | Extender (seg, n, i, h) ->
                `Left ((Do n :: Extender (seg, i,h) :: js), ss)
            | Internal (nl, nr, i, h) ->
                `Left ((Do nl :: Do nr :: Internal (i,h) :: js), ss)

  let interleaved ~enter ~leave n =
    let js_ss = [Do n], [] in
    let stepper x = step ~enter ~leave x in
    stepper, js_ss

  let fold ~enter ~leave n =
    let rec loop js_ss = match step ~enter ~leave js_ss with
      | `Right res -> res
      | `Left js_ss -> loop js_ss
    in
    loop ([Do n], [])
end

let hash_of_view =
  function
  | ( Bud (_, _, Some hp)
    | Leaf (_, _, Some hp))
    | (Internal (_, _, _, Some hp)) -> Some (hp,"")
  | Extender (seg, _, _, Some hp) -> Some (hp, Segment.Serialization.encode seg)
  | _ -> None

let hash_of_node node =
  match node with
  | Disk _ -> (None, None)
  | Hash h -> (None, Some h)
  | View v -> (Some v, hash_of_view v)

open Gen
open Gen.Infix

(* Tricky.  In one Bud level, we cannot have more than [Segment.max_length]
     level of segments, otherwise removing nodes from it might create an Extender
     with very long segment over this limit.
  *)
let rec gen_leaf = value >>| new_leaf

and gen_bud_none = return (new_bud None)

and gen_bud depth =
  let depth0 = depth in
  if depth <= 1 then return (new_bud None)
  else
    int depth >>= fun n ->
    if n = 0 then return (new_bud None)
    else begin
      let depth = depth - 1 in
      one_of [gen_extender depth; gen_internal depth] >>= fun n ->
      begin match n with
        | View (Leaf _) -> gen_bud depth0
        | View (Bud _) -> gen_bud depth0
        | _ -> return (new_bud (Some n))
      end
    end

and gen_internal depth =
  let f =
    if depth <= 1 then
      (* we cannot create an internal*)
      one_of [gen_bud_none; gen_leaf]
    else
      let depth = depth - 1 in
      int depth >>= fun n ->
      if n = 0 then one_of [gen_bud_none; gen_leaf]
      else one_of [gen_internal depth;
                   gen_extender depth;
                   gen_bud depth]
  in
  f >>= fun n1 ->
  f >>| fun n2 ->
  let n = new_internal n1 n2 in
  match n with
  | View (Internal _) -> n
  | _ -> assert false

and gen_extender depth =
  if depth <= 1 then
    (* we cannot create an extender *)
    one_of [gen_bud_none; gen_leaf]
  else
    int depth >>= fun n ->
    if n = 0 then one_of [gen_bud_none; gen_leaf]
    else
      let limit = Int.max 1 (Int.min Segment.max_length depth) in
      segment (int_range (1, limit)) >>= fun seg ->

      let depth = depth - Segment.length seg in
      one_of [gen_internal depth; gen_bud depth] >>| fun n ->
      new_extender seg n
