(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** { 1 Commit entry }

    This module defines the data type for the commit entry.

    How it is loaded/saved to the disk is defined in Commit_tree
    and Commit_db.Store.
*)

type t =
  { parent : Commit_hash.t option

  ; index  : Index.t
    (** Index of the Plebeia tree root node in storage_context.

        If the commit is a dummy, field [index] is set to [Index.zero].
    *)

  ; hash   : Commit_hash.t
  }

(** Returns [true] if the entry is a genesis: no parent *)
val is_genesis : t -> bool

val pp : Format.formatter -> t -> unit

(** Hash computation for a new commit entry *)
val compute_hash :
  (module Hashfunc.S)
  -> parent:Commit_hash.t option
  -> Hash.t
  -> Commit_hash.t

(** Make a new commit entry.

    If [hash_override] is set, [hash] of the new entry is overridden by it,
    instead of computing from [parent] and the given [Hash.t].
*)
val make :
  (module Hashfunc.S)
  -> parent:Commit_hash.t option
  -> index:Index.t
  -> ?hash_override: Commit_hash.t
  -> Hash.t
  -> t
