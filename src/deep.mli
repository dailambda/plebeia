(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cursor

(** Functions in this module assumes the given cursor points to a bud. *)

val deep :
  dont_move: bool (* recover the original cursor position or not *)
  -> create_subtrees: bool (* create_subtree if necessary *)
  -> t
  -> Segment.t list (* cannot be empty *)
  -> (t -> Segment.t -> (t * 'a, Error.t) Result.t)
  -> (t * 'a, Error.t) Result.t
(** Multi Bud level interface. [deep ~stay ~create_subtrees t segs f] performs [f]
    against the node pointed by the multi segments [segs].

    - [segs] cannot be empty.

    - If [stay] is [true], the result cursor keeps the original position.
    - otherwise, the result cursor points to the node pointed by [segs].

    - If [create_subtrees] is [true], subtrees are created on demand.
*)

val get : t -> Segment.t list -> (t * [`Bud of Node_type.view | `Leaf of Node_type.view], Error.t) Result.t

val get_value : t -> Segment.t list -> (t * Value.t, Error.t) Result.t

val insert : t -> Segment.t list -> Value.t -> (t, Error.t) Result.t

val upsert : t -> Segment.t list -> Value.t -> (t, Error.t) Result.t

val update : t -> Segment.t list -> Value.t -> (t, Error.t) Result.t

val delete : t -> Segment.t list -> (t, Error.t) Result.t
(** If the target does not exists, do nothing *)

val delete2 : t -> Segment.t list -> (t, Error.t) Result.t
(** Delete also internal and extender *)

val delete_and_clean_empty_buds : t -> Segment.t list -> (t, Error.t) Result.t
(** If the target does not exists, do nothing.  If the result of the removal
    generates an empty bud, [delete_and_clean_empty_buds] also cleans it. This
    empty bud cleaning is recursive.

    The result cursor points to the original position.  If the Bud
    at the original position becomes empty, the funciton fails.
*)

val create_subtree : create_subtrees: bool -> t -> Segment.t list -> (t, Error.t) Result.t

val subtree : t -> Segment.t list -> (t, Error.t) Result.t

val subtree_or_create : create_subtrees: bool -> t -> Segment.t list -> (t, Error.t) Result.t

val copy : ?allow_overwrite: bool -> ?only_bud:bool -> create_subtrees: bool -> t -> Segment.t list -> Segment.t list -> (t, Error.t) Result.t
(** Subtree copy by making two nodes point to the same subtree.

    - [allow_overwrite] (default: [false]):
      - if [true], if a node exists at the destination, it is overwritten
      - if [false], if a node exists at the destination, the funciton fails
    - [only_bud] (default: [true])
      - if [true], copying non bud fails

    This never creates a loop, just like "cp -r a/b a/b/c/d" never creates
    a loop.
*)

val link : Node_type.node -> t -> Segment.t list -> (t, Error.t) Result.t
(** [link n c segs] makes a link to [n] at [c/segs].

    [link] is like [copy], but [n] needs not to be a part of the tree of [c].

    [n] and [c] must be of the same context.
*)

val alter : Cursor.t -> Segment.t list -> (Node_type.view option -> (Node_type.t, Error.t) result) -> (Cursor.t, Error.t) result
(** Deep version of [Cursor.alter] *)
