(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Node_type

module Iter : sig
  (** Iteration.  Parent nodes are iterated before their subnodes.
      Special case of [Fold]
  *)

  val f :
    (t -> [`Continue of view | `Return of unit ]) ->
    t -> unit

  type state

  val interleaved :
    (t -> [`Continue of view | `Return of unit ]) ->
    t ->
    (state -> [`Left of state | `Right of unit ]) * state
end

module Fold : sig
  val f :
    ('acc -> t -> [`Continue of 'acc * view | `Return of 'acc ]) -> 'acc -> t -> 'acc

  type 'acc state

  val interleaved :
    ('acc -> t -> [`Continue of 'acc * view | `Return of 'acc ]) ->
    'acc -> t ->
    ('acc state -> [`Left of 'acc state | `Right of 'acc ]) * 'acc state
end

module Map : sig
  val f :
    enter:(t -> [`Continue of view | `Return of t ]) ->
    leave:(org:view -> view -> t) ->
    t -> t

  type state

  val interleaved :
    enter:(t -> [`Continue of view | `Return of t ]) ->
    leave:(org:view -> view -> t) ->
    t ->
    (state -> [`Left of state | `Right of t ]) * state
end
