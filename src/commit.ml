(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Utils

type t =
  { parent : Commit_hash.t option
  ; index  : Index.t
  ; hash   : Commit_hash.t
  }

let is_genesis ent = ent.parent = None

let pp ppf { index ; parent ; hash } =
  let f fmt = Format.fprintf ppf fmt in
  f "%a at %a parent=%a"
    Commit_hash.pp hash
    Index.pp index
    (Format.option Commit_hash.pp) parent

let compute_hash (module H : Hashfunc.S) ~parent hash_prefix =
  let parent = Option.value ~default:Commit_hash.zero parent in
  Commit_hash.of_raw_string @@ H.to_raw_string @@
  H.hash_strings [ Hash.to_raw_string hash_prefix;
                   Commit_hash.to_raw_string parent ]

let make h ~parent ~index ?hash_override hash_prefix =
  let hash =
    match hash_override with
    | None -> compute_hash h ~parent hash_prefix
    | Some h -> h
  in
  { parent; index; hash }
