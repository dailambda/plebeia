(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type t

val encoding : t Data_encoding.t

type gside = Left | Right | Bud

val empty : t

(** Assumes the segments start from a Bud *)
val of_segments : Segment.t list -> t

val to_segments : t -> [> `From_Bud of Segment.t list | `From_non_Bud of Segment.t list]

val of_path : Path.t -> t

val to_path : t -> Path.t option

val is_empty : t -> bool

val cut : t -> (gside * t) option

val cons : gside -> t -> t

(** [has_prefix seg k] returns [Some postfix] when [seg] is a prefix of [k]
    and the postfix is [postfix]
*)
val has_prefix : Segment.t -> t -> t option

(** [is_prefix k seg] returns [Some postfix] when [k] is a prefix of [seg]
    and the postfix is [postfix]
*)
val is_prefix : t -> Segment.t -> Segment.t option

val to_string : t -> string

val pp : Format.formatter -> t -> unit

(** List all the keys of the node *)
val keys : (Node_type.t -> Node_type.view) -> Node_type.t -> t list
