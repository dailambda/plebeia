(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Segment

open Node_type

let ls context n =
  let rec f acc frontiers = match frontiers with
    | [] -> acc
    | (n, rev_seg)::frontiers ->
        let v = Node_storage.view context n in
        let n = View v in
        match v with
        | Leaf _ | Bud _ ->
            f ((Segment.of_sides @@ List.rev rev_seg, n) :: acc) frontiers
        | Internal (n1, n2, _, _) ->
            f acc ((n2,Right::rev_seg)::(n1,Left::rev_seg)::frontiers)
        | Extender (seg, n, _, _) ->
            f acc ((n,  List.rev_append (to_sides seg) rev_seg) :: frontiers)
  in
  f [] [n, []]

let ls_rec context n = match Node_storage.view context n with
  | Internal _ | Extender _ -> assert false
  | Leaf _ -> [([], n)]
  | Bud (None, _, _) -> []
  | Bud (Some n, _, _) ->
      let rec f res = function
        | [] -> res
        | (segs, n) :: segsns ->
            match Node_storage.view context n with
            | Internal (n1, n2, _, _) ->
                f res ((Segs.add_side segs Right, n2)
                       :: (Segs.add_side segs Left, n1)
                       :: segsns)
            | Extender (seg, n, _, _) ->
                f res ((Segs.append_segment segs seg, n) :: segsns)
            | Leaf _ ->
                f ((Segs.to_segments segs, n) :: res) segsns
            | Bud (Some n, _, _) ->
                f res ((Segs.push_bud segs, n) :: segsns)
            | Bud (None, _, _) ->
                f res segsns
      in
      f [] [(Segs.empty', n)]
