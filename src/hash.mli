(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* This module does not define how to compute hashes.
   It just defines the data type.
*)

(** Type of the prefix part of hash.
    The length is not fixed in this implementation.

    Please note that there is no typing guarantee to prevent merging
    different size of hashes.
*)
type t

type hash = t

(** Get the [bytes] bytes binary string representation of [t]. *)
val to_raw_string : t -> string

(** [bytes] bytes binary string representation to [t].
    Note: no check of length. *)
val of_raw_string : string -> t

val length : t -> int

val overwrite_last_2bits : t -> int -> t

(** [zero n] returns [n] bytes of zeros *)
val zero : int -> t

(** Convert to [bytes * 2] chars of hexdigits *)
val to_hex_string : t -> string

(** Reverse of [to_hex_string].  Fails if the string is inappropriate. *)
val of_hex_string : string -> t

val show_ref : (t -> string) ref

val show : t -> string

val pp : Format.formatter -> t -> unit

val encoding : int (* bytes *) -> t Data_encoding.t
(** Encoding: fixed length bytes.  The size is given as the argument. *)

val gen : int -> t Gen.t

val to_32byte_string : t -> string


module Long : sig
  (** Long hash, hash with a postfix string for Extenders *)
  type t = hash * (string (* <= Limit.max_hash_postfix_bytes *))

  val encoding : int (* bytes of hash *) -> t Data_encoding.t
  (** Encoding.  The length of the prefix is given by the argument:

       <- fixed length -> <1B> <-- len bytes -->
      +------------------+----+-----------------+
      |   hash prefix    |len | postfix string  |
      +------------------+----+-----------------+
  *)

  val to_hex_string : t -> string

  val pp : Format.formatter -> t -> unit

  val of_prefix : hash -> t

  val prefix : t -> hash

  (** [true] with non empty postfix *)
  val is_long : t -> bool

  val to_strings : t -> string list
end

module Hasher : sig
  type config =
    { hashfunc : Hashfunc.config
    ; flags_combined : bool
    }

  (** Hashing size and functions *)
  type t = private
    { config : config
    ; compute : flags:int -> string list -> hash
    ; zero : hash (* bytes of 0s *)
    ; hash_of_empty_leaf : hash
    }

  val is_compatible : t -> t -> bool

  val flags_combined : bytes_per_hash:int -> bytes_per_cell:int -> with_count:bool -> bool

  val make_config : Hashfunc.config -> bytes_per_cell:int -> with_count:bool -> config

  val make : config -> t
end
