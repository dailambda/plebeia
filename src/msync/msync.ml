external msync : ('a, 'b, 'c) Bigarray.Genarray.t -> unit = "caml_ba_msync"
external msync2 : ('a, 'b, 'c) Bigarray.Genarray.t -> off:int -> len:int -> unit = "caml_ba_msync2"
external madvise_random : ('a, 'b, 'c) Bigarray.Genarray.t -> unit = "caml_madvise_random"

module Lwt = struct
  open Lwt_unix

  external msync_job : ('a, 'b, 'c) Bigarray.Genarray.t -> unit job = "caml_ba_msync_job"
  external msync2_job : ('a, 'b, 'c) Bigarray.Genarray.t -> off:int -> len:int -> unit job = "caml_ba_msync2_job"

  let msync a = Lwt_unix.run_job (msync_job a)
  let msync2 a ~off ~len = Lwt_unix.run_job (msync2_job a ~off ~len)
end
