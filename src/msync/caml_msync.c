/*****************************************************************************/
/*                                                                           */
/* Open Source License                                                       */
/* Copyright (c) 2020 DaiLambda, Inc. <contact@dailambda.jp>                 */
/*                                                                           */
/* Permission is hereby granted, free of charge, to any person obtaining a   */
/* copy of this software and associated documentation files (the "Software"),*/
/* to deal in the Software without restriction, including without limitation */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,  */
/* and/or sell copies of the Software, and to permit persons to whom the     */
/* Software is furnished to do so, subject to the following conditions:      */
/*                                                                           */
/* The above copyright notice and this permission notice shall be included   */
/* in all copies or substantial portions of the Software.                    */
/*                                                                           */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*/
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*/
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       */
/* DEALINGS IN THE SOFTWARE.                                                 */
/*                                                                           */
/*****************************************************************************/


/* Needed (under Linux at least) to get pwrite's prototype in unistd.h.
   Must be defined before the first system .h is included. */
#define _XOPEN_SOURCE 600

#include <stddef.h>
#include "caml/bigarray.h"
#include "caml/fail.h"
#include "caml/io.h"
#include "caml/mlvalues.h"
#include "caml/signals.h"
#include "caml/sys.h"
#include "caml/unixsupport.h"
#include "caml/alloc.h"
#include "caml/threads.h"

#include <errno.h>
#ifdef HAS_UNISTD
#include <unistd.h>
#endif
#ifdef HAS_MMAP
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#endif

int caml_msync(intnat flags, void * addr, uintnat len)
{
    if ( (flags & CAML_BA_MANAGED_MASK) == CAML_BA_MAPPED_FILE ) {
        uintnat page = sysconf(_SC_PAGESIZE);
        uintnat delta = (uintnat) addr % page;
        if (len == 0) return 0;
        addr = (void *)((uintnat)addr - delta);
        len  = len + delta;
        return msync(addr, len, MS_SYNC);
    } else return 0;
}

// int madvise(void *addr, size_t length, int advice);
void caml_madvise_random(value v)
{
  struct caml_ba_array * b = Caml_ba_array_val(v);

  caml_release_runtime_system();
  // Other OCaml native threads can run while msync
  posix_madvise(b->data, caml_ba_byte_size(b), POSIX_MADV_RANDOM);
  caml_acquire_runtime_system();
}

void caml_ba_msync(value v)
{
  struct caml_ba_array * b = Caml_ba_array_val(v);

  caml_release_runtime_system();
  // Other OCaml native threads can run while msync
  int res = caml_msync(b->flags, b->data, caml_ba_byte_size(b));
  caml_acquire_runtime_system();

  if ( res != 0  )
  {
      /* error */
      int error = errno;
      value string_argument = caml_copy_string("");
      unix_error(error, "msync", string_argument);
  }
}

void caml_ba_msync2(value v, value off, value len)
{
  struct caml_ba_array * b = Caml_ba_array_val(v);

  caml_release_runtime_system();
  // Other OCaml native threads can run while msync
  int res = caml_msync(b->flags, b->data + Long_val(off), Long_val(len));
  caml_acquire_runtime_system();

  if ( res != 0  )
  {
      /* error */
      int error = errno;
      value string_argument = caml_copy_string("");
      unix_error(error, "msync", string_argument);
  }
}

/////////// Lwt version which allows other lwt threads run while msync

#include <lwt_unix.h>

struct job_msync {
    /* Informations used by lwt.
       It must be the first field of the structure. */
    struct lwt_unix_job job;
    /* This field store the result of the call. */
    int result;
    /* This field store the value of [errno] after the call. */
    int errno_copy;
    void *addr;
    size_t len;
    intnat flags;
};

/* The function calling [mkdir]. */
static void worker_msync(struct job_msync* job)
{
    job->result = caml_msync(job->flags, job->addr, job->len);
    /* Save the value of errno. */
    job->errno_copy = errno;
}

/* The function building the caml result. */
static value result_msync(struct job_msync* job)
{
  /* Check for errors. */
  if (job->result < 0) {
    /* Save the value of errno so we can use it
       once the job has been freed. */
    int error = job->errno_copy;
    /* Copy the contents of job->path into a caml string. */
    value string_argument = caml_copy_string("");
    /* Free the job structure. */
    lwt_unix_free_job(&job->job);
    /* Raise the error. */
    unix_error(error, "msync", string_argument);
  }
  /* Free the job structure. */
  lwt_unix_free_job(&job->job);
  /* Return the result. */
  return Val_unit;
}

/* The stub creating the job structure. */
CAMLprim value caml_ba_msync_job(value v)
{
    struct caml_ba_array * b = Caml_ba_array_val(v);
    /* Allocate a new job. */
    struct job_msync* job =
        (struct job_msync*)lwt_unix_new_plus(struct job_msync, 0);
    job->addr = b->data;
    job->len = caml_ba_byte_size(b);
    job->flags = b->flags;
    /* Initialize function fields. */
    job->job.worker = (lwt_unix_job_worker)worker_msync;
    job->job.result = (lwt_unix_job_result)result_msync;
    /* Wrap the structure into a caml value. */
    return lwt_unix_alloc_job(&job->job);
}

/* The stub creating the job structure. */
CAMLprim value caml_ba_msync2_job(value v, value off, value len)
{
    struct caml_ba_array * b = Caml_ba_array_val(v);
    /* Allocate a new job. */
    struct job_msync* job =
        (struct job_msync*)lwt_unix_new_plus(struct job_msync, 0);

    long offc = Long_val(off);

    job->addr = b->data + offc;
    job->len = Long_val(len);

    if ( job->len < 0 || offc < 0
         || job->len + offc > caml_ba_byte_size(b) ) {
        caml_invalid_argument("msync2");
    }
        
    job->flags = b->flags;
    /* Initialize function fields. */
    job->job.worker = (lwt_unix_job_worker)worker_msync;
    job->job.result = (lwt_unix_job_result)result_msync;
    /* Wrap the structure into a caml value. */
    return lwt_unix_alloc_job(&job->job);
}
