(** If the array is not a mapped file, do nothing. *)
val msync : ('a, 'b, 'c) Bigarray.Genarray.t -> unit
val msync2 : ('a, 'b, 'c) Bigarray.Genarray.t -> off:int -> len:int -> unit

val madvise_random : ('a, 'b, 'c) Bigarray.Genarray.t -> unit

module Lwt : sig
  (** Other lwt threads can run while msyncing. *)
  val msync : ('a, 'b, 'c) Bigarray.Genarray.t -> unit Lwt.t
  val msync2 : ('a, 'b, 'c) Bigarray.Genarray.t -> off:int -> len:int -> unit Lwt.t
end
