(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Arthur Breitman <arthur.breitman+nospam@tezos.com>     *)
(* Copyright (c) 2019 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Node_type

(** { 1 Zipper } *)

(** { 2 Types: Trail and cursor } *)

type modified =
  | Modified
  | Unmodified of Index.t option * Hash.t option

(** A trail represents the content of the memory stack when recursively
    exploring a tree.  Constructing these trails from closure would be easier,
    but it would make it harder to port the code to C. The type parameters of
    the trail keep track of the type of each element on the "stack" using
    a product type.

    The constructors are private.  Use '_' prefixed functions with runtime
    invariant checks.
*)
type trail = private
  | Top
  | Left of (* we took the left branch of an internal node *)
      trail
      * node (* the right node *)
      * modified

  | Right of (* we took the right branch of an internal node *)
      node (* the left node *)
      * trail
      * modified

  | Budded of
      trail
      * modified

  | Extended of
      trail
      * Segment.t
      * modified

type cursor = private
    Cursor of trail
              * node
              * Context.t
(** The cursor, also known as a zipper combines the information contained in a
   trail and a subtree to represent an edit point within a tree. This is a
   functional data structure that represents the program point in a function
   that modifies a tree. We use an existential type that keeps the .mli sane
   and enforces the most important: that the hole tags match between the trail
   and the Node *)

type t = cursor

(** { 2 Constructor with invariant checks } *)

val _Top : trail
val _Left : trail * node * modified -> trail
val _Right : node * trail * modified -> trail
val _Budded : trail * modified -> trail
val _Extended : trail * Segment.t * modified -> trail
val _Cursor : (trail * node * Context.t) -> cursor

(** { 2 Creation } *)

val empty : Context.t -> t
(** Creates a cursor to a new, empty tree. *)

(** { 2 Accessors } *)

val context : t -> Context.t

val get_storage : t -> Storage.t

val index : t -> Index.t option
(** Get the index of the node pointed by the cursor, if indexed. *)

(** { 2 Segments } *)

val path_of_trail : trail -> Path.t
(** Segment side list of the given trail, splitted by buds *)

val path_of_cursor : t -> Path.t
(** Segment side list of the given cursor, splitted by buds *)

val local_segment_of_trail : trail -> Segment.t
(** Segment side list of the given trail, splitted by buds *)

val local_segment_of_cursor : t -> Segment.t
(** Segment side list of the given cursor, splitted by buds *)

(** { 2 View } *)

val view : t -> t * view
(** Get the view of the cursor.  Returns also the updated cursor with
    the view. *)

val may_forget : t -> t option
(** If the node pointed by the cursor is indexed, forget the details *)

(** { 2 Zipper functions } *)

type Error.t +=
  | Cursor_invariant of string
  | Write of string
  | Move of string

(** { 3 Simple 1 step cursor movement } *)

val go_below_bud : t -> (t option, Error.t) Result.t
(** This function expects a cursor positionned on a bud
    and moves it one step below. *)

val go_down_extender : t -> (t, Error.t) Result.t
(** Go down an Extender node.  The cursor must point to an Extender. *)

val go_side : Segment.side -> t -> (t, Error.t) Result.t
(** Go down an Internal node.  The cursor must point to an Internal. *)

val go_up : t -> (t, Error.t) Result.t
(** Go up one level. If the cursor points to the root node, it returns itself. *)

(** { 3 Complex multi step cursor movement }

    Many of these functions fail when the given cursor does not point to a bud.
*)

(** Result of access_gen *)
type access_result =
  | Empty_bud
      (* The bud is empty *)
  | Collide of cursor * view
      (* The segment was blocked by an existing leaf or bud *)
  | Middle_of_extender of cursor * Segment.t * Segment.t * Segment.t
      (* The segment ends or diverges at the middle of an Extender with the common prefix,
         the remaining extender, and the rest of segment *)
  | Reached of cursor * view
      (* just reached to a node *)
  | HashOnly of cursor * Hash.Long.t * Segment.t
      (* Collided with Hash only node *)

type Error.t +=
  | Access of access_result

val error_access : access_result -> ('a, Error.t) Result.t
(** Make an access result into an error *)

val access_from_bud : t -> Segment.t -> (access_result, Error.t) Result.t
(** Follow a segment from a bud.  [t] must point to a bud.
    The function first go below the bud, then follow the segment.

   If the segment is empty, the function returns a node attached to the bud.
*)

val go_top : t -> t
(** Move up to the top *)

val go_up_to_bud : t -> (t, Error.t) Result.t
(** Moves the cursor back to the bud above.
    Note that this is not like "cd ../".

    If the cursor is already at a bud, the cursor will move to its parent bud.
*)

val parent : t -> (t, Error.t) Result.t
(** Moves the cursor back to the bud above.  Like "cd ../".
    The cursor must point to a bud otherwise [parent] fails.
*)

val subtree : t -> Segment.t -> (t, Error.t) Result.t
(** Moves the cursor down a segment, to the root of a sub-tree. Think
    "cd segment/" *)

val create_subtree: t -> Segment.t -> (t, Error.t) Result.t
(** Create a subtree (bud). Think "mkdir segment".
    The cursor does NOT move from the original position. *)

val subtree_or_create : t -> Segment.t -> (t, Error.t) Result.t
(** Same as subtree but create a subtree if not exists *)

val get : t -> Segment.t -> (t * [`Leaf of view | `Bud of view], Error.t) Result.t
(** Gets a value if present in the current tree at the given
    segment. *)

val get_value : t -> Segment.t -> (t * Value.t, Error.t) Result.t
(** Gets a value or a bud at the given segment. *)

val insert: t -> Segment.t -> Value.t -> (t, Error.t) Result.t
(** Inserts a value at the given segment in the current tree.
    It fails if a value already exists at the segment.
    The cursor does NOT move from the original position. *)

val upsert: t -> Segment.t -> Value.t -> (t, Error.t) Result.t
(** Upserts. If a value alrady exists at the segment, it is overwritten.
    This can still fail if the segment points to a subtree.
    The cursor does NOT move from the original position. *)

val update: t -> Segment.t -> Value.t -> (t, Error.t) Result.t
(** Update. A value must be bound at the segment. *)

val delete: t -> Segment.t -> (t, Error.t) Result.t
(** Delete a leaf or subtree.
    The cursor does NOT move from the original position. *)

val delete': t -> Segment.t -> (t, Error.t) Result.t
(** Delete a node.
    The cursor does NOT move from the original position. *)

val alter :
  t ->
  Segment.segment ->
  (view option -> (node, Error.t) Result.t) -> (t, Error.t) Result.t
(** [alter] can easily break the invariants. *)

val fold :
  init:'acc
  -> t
  -> ('acc -> t -> [< `Continue | `Exit | `Up ] * 'acc)
  -> 'acc
(** Folding over the node tree.  The function can choose the node traversal
    from the given cursor: either continuing into its sub-nodes,
    not traversing its sub-nodes, or quit the entire folding.

    If a node is shared at multiple places it is visited MORE THAN ONCE.
    If you want to avoid visiting a shared node at most once, carry
    a set of visited nodes by indices and check a node is visited or not.
*)

val traverse :
  'a
  -> t list
  -> ('a -> t -> [< `Exit | `Up | `Continue ] * 'a)
  -> 'a * t list
(** More generic tree traversal than [fold], which has a step-by-step
    visitor interface: it does not recurse the structure by itself.

    If a node is shared at multiple places it is visited MORE THAN ONCE.
*)

(** { 2 Cursor hash compuation } *)

val compute_hash : t -> t * Hash.Long.t
(** Returns the hash of the node pointed by the cursor.
    It traverses nodes and compute hashes if necessary.

    It also returns the updated cursor with the hash information.
*)

val compute_hash' :
  (Node_type.t -> [< `Hashed of Hash.Long.t * Node_type.t | `Not_Hashed of Node_type.view ])
  -> t
  -> t * Hash.Long.t
(** Compute the node hash of the node pointed by the given cursor.  Tail recursive.

    The function argument for short cutting, especially for [Disk _].  It can do either

      * Obtain the node hash of the node somehow from an external source
      * Retrieve the view of the node to let [compute'] calculate the node hash of it
*)

val is_with_count : t -> bool

(** count the leaves and buds, loading nodes on demand.

    [count c], if [c] points to a [Bud], it returns the number of the leaves and buds
    under [c].

    Returns [None] when the pagination count is not in the context.
*)
val count : t -> (int * t) option

(** { 2 Statistics } *)

val stat : t -> Stat.t

(** { 2 Debug } *)

val dot_of_cursor_ref : (t -> string) ref
(** Placeholder of Graphviz rendering of cursors *)

module Monad : sig
  (** Same as cursor, but in Monadic interface.  Experimental. *)
  include Monad.S1 with type 'a t = cursor -> cursor * ('a, Error.t) Result.t
  (**
     The position of the cursor never changes if the result is an [Error].
  *)

  val path : Path.t t
  (** Returns the absolute position of the cursor. *)

  val local_segment : Segment.t t
  (** Returns the relative position of the cursor from the nearest Bud above. *)

  val view : Node_type.view t
  (** Returns the view of the node pointed by the cursor. *)

  val index : Index.t option t
  (** Return the index of the node pointed by the cursor, if available. *)

  val go_below_bud : unit t
  (** Move the cursor below the Bud pointed by it.

      The function fails and the cursor does not move if:
        * The cursor does not point to a Bud or if the pointed Bud is empty.
  *)

  val go_side : Segment.side -> unit t
  (** Move the cursor Left or Right below the Internal pointed by it.

      The function fails and the cursor does not move if:
        * The cursor does not point to an Internal.
  *)

  val go_down_extender : unit t
  (** Move the cursor below the Extender pointed by it.

      The function fails and the cursor does not move if:
        * The cursor does not point to an Extender.
  *)

  val go_up : unit t
  (** Move up the cursor.

      The function fails and the cursor does not move if:
        * The cursor points to the top Bud.
  *)

  val go_top : unit t
  (** Move the cursor to the top Bud. *)

  val go_up_to_bud : unit t
  (** Move the cursor up to the nearest Bud above.

      The function fails and the cursor does not move if:
        * The cursor points to the top Bud.
  *)

  val subtree : Segment.t -> unit t
  (** Move the cursor pointing at a Bud to a sub-Bud specified by the segment.

      The function fails and the cursor does not move if:
        * The cursor does not point to a Bud.
        * The sub-Bud does not exist.
  *)

  val get : Segment.t -> [`Leaf of Node_type.view | `Bud of Node_type.view] t
  (** Get the Leaf or Bud of the node pointed by the cursor.

      The function fails if:
        * The cursor does not point to a Bud.
        * The speficied node does not exist nor is not a Bud nor a Leaf.
  *)

  val get_value : Segment.t -> Value.t t
  (** Get the value of the Leaf pointed by the cursor.

      The function fails if:
        * The cursor does not point to a Bud.
        * The speficied node does not exist nor is not a Leaf.
  *)

  val delete : Segment.t -> unit t
  (** Delete the node at the segment from the cursor.

      The function fails if:
        * The cursor does not point to a Bud.
        * The speficied node does not exist.
  *)

  val alter : Segment.t -> (Node_type.view option -> (Node_type.node, Error.t) Result.t) -> unit t
  (** Alter the node at the segment from the Bud pointed by the cursor.

      The alteration function takes the view of the node or [None] if it does not
      exist.  The function returns a new node to replace the original (or be inserted
      if the target does not exist) or an error.

      The function fails if:
        * The cursor does not point to a Bud.
        * The segment is blocked by a Bud or an Extender.
        * The alteration function returns an Error.
  *)

  val insert : Segment.t -> Value.t -> unit t
  (** Insert a Leaf of the specified value at the segment from the Bud pointed
      by the cursor.

      The function fails if:
        * The cursor does not point to a Bud.
        * The segment is blocked by a Bud or an Extender.
        * The specified node already exists.
  *)

  val update : Segment.t -> Value.t -> unit t
  (** Update the value of the Leaf specified by the segment from the Bud pointed
      by the cursor.  The Leaf must exist.

      The function fails if:
        * The cursor does not point to a Bud.
        * The specified node is not a Leaf, or does not exist.
  *)

  val upsert : Segment.t -> Value.t -> unit t
  (** Upsert the value of the Leaf specified by the segment from the Bud pointed
      by the cursor.  The Leaf may or may not exist.

      The function fails if:
        * The cursor does not point to a Bud.
        * The segment is blocked by a Bud or an Extender.
        * The specified node already exists and it is not a Leaf.
  *)

  val create_subtree : Segment.t -> unit t
  (** Create a sub Bud at the segment from the Bud pointed by the cursor.

      The function fails if:
        * The cursor does not point to a Bud.
        * The segment is blocked by a Bud or an Extender.
        * The specified node already exists.
  *)

  val subtree_or_create : Segment.t -> unit t
  (** Move the cursor down to a sub Bud specified by the segment.
      If the sub Bud does not exist, it creates before moving down the cursor.

      The function fails if:
        * The cursor does not point to a Bud.
        * The segment is blocked by a Bud or an Extender.
        * The specified node already exists and it is not a Bud.
  *)

  val stat : Stat.t t
  (** Returns the statistics of the context *)

  val may_forget : unit t
  (** Forget the contents of the node (and its sub-nodes) pointed by the cursor,
      if they are already stored in the storage.

      If the contents are not yet stored in the storage, it does nothing.
  *)

end

module Cursor_storage : sig
  val write_top_cursor : cursor -> (cursor * Index.t * Hash.t, Error.t) result
  (** Write the node pointed by the cursor to the storage, and returns
      the updated cursor with the index and the hash of the stored node.

      Note that this function does not sync the storage nor write [Commit.t].
      Use one of [*.commit] functions to make it persistent.
  *)

  val read_fully : reset_index: bool -> cursor -> cursor
  (** Recursively load the node pointed by the cursor.

      if [reset_index], all the indices are reset to [Not_Indexed].
  *)

  module Internal : sig
    val read_fully_for_test : cursor -> cursor
    (** Recursively load the node pointed by the cursor.

        Not tail recursive.  Test purpose only
    *)
  end
end

val deep_stat : int32 -> t -> unit Lwt.t
(** debug *)
