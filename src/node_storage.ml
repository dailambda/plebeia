(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module GlobalStat = Stat
open Node_type
open Storage

module B = Mmap.Buffer

(** node storage.
    See Layout.md for the format *)

exception ReadFailure of Error.t

let max_size_small_leaf = 128

let make_tag n = (1 lsl 32) - n

let tag_empty_bud = make_tag 256
let tag_large_leaf = make_tag 253
let tag_extra_large_leaf = make_tag 255
let tag_link = make_tag 254
let tag_max_size_small_leaf = make_tag max_size_small_leaf

let () = assert (make_tag Context.hard_limit_max_tag <= tag_empty_bud)

(* tag_int [max_size_small_leaf..253] are reserved for future extension *)
let () = assert (tag_link < make_tag max_size_small_leaf)

let zero_sized_leaf_index = Index.zero

let rec read_hash_prefix (context : Context.t) i =
  if i = zero_sized_leaf_index then
    let v = Value.of_string "" in
    (* TODO: avoid recomputing hash of empty string *)
    Node_hash.of_leaf context.hasher v
  else
    let offsets = context.Context.offsets in
    let storage = Context.get_storage context in
    let buf = get_cell storage i in
    let get_hash_prefix () =
      Hash.of_raw_string @@ B.copy buf offsets.hash_prefix context.hasher.config.hashfunc.length
    in
    let tag = Index.to_int @@ B.get_index buf offsets.index_part in
    if tag_max_size_small_leaf <= tag then
      (* small or medium leaf *)
      get_hash_prefix ()

    else if tag_empty_bud <= tag then
      (* another tag *)

      if tag = tag_large_leaf || tag = tag_extra_large_leaf then
        get_hash_prefix ()

      else if tag = tag_link then (* linked *)
        let i' = B.get_index buf offsets.link in
        read_hash_prefix context i'

      else if tag = tag_empty_bud then (* empty bud *)
        context.hasher.Hash.Hasher.zero

      else assert false

    else
      (* It is not a tag but an index *)
      let flags = B.get_uint8 buf offsets.flags in
      match flags land 0b11 with
      | 0b01 ->
          (* extender  |<- segment ->0..0|6bits|01| |<- the index of the child ->| *)
          let i' = B.get_index buf offsets.index_part in
          (* require to read the subnode to get the hash *)
          read_hash_prefix context i'
      | 0b11 -> (* non empty bud *)
          get_hash_prefix ()
      | 0b00 | 0b10 -> (* internal *)
          if not context.hasher.config.flags_combined then
            get_hash_prefix ()
          else
            (* the last 2 bits of the hash prefix must be reset *)
            let bytes_per_hash = context.hasher.config.hashfunc.length in
            let s = B.copy buf offsets.hash_prefix bytes_per_hash in
            let b = Bytes.unsafe_of_string s in
            Bytes.unsafe_set b (bytes_per_hash - 1) @@ Char.chr @@ flags land 0xfc;
            Hash.of_raw_string @@ Bytes.unsafe_to_string b
      | _ -> assert false

(* If [keep_hash=false], hashes are not read into the memory except trivial ones,
   but it remains in the disk cache *)
let rec parse_cell ~keep_hash (context : Context.t) i =
  if i = zero_sized_leaf_index then (* zero size leaf *)
    let v = Value.of_string "" in
    let h = Some context.hasher.hash_of_empty_leaf in
    _Leaf (v, Some i, h)
  else
    let offsets = context.offsets in
    let bytes_per_cell = context.bytes_per_cell in
    let storage = Context.get_storage context in
    let buf = get_cell storage i in
    let get_hash_prefix () =
      Hash.of_raw_string @@ B.copy buf offsets.hash_prefix context.hasher.config.hashfunc.length
    in
    let [@inline] ncells_for_bytes nbytes =
      (nbytes + bytes_per_cell - 1) / bytes_per_cell
    in
    let parse_leaf_value ~extra_cells l =
      let buf = get_bytes storage Index.Unsafe.(i - of_int extra_cells) l in
      let v = Value.of_string @@ B.copy buf 0 l in
      let h = if keep_hash then Some (get_hash_prefix ()) else None in
      _Leaf (v, Some i, h)
    in
    let tag = Index.to_int @@ B.get_index buf offsets.index_part in
    if tag_max_size_small_leaf <= tag then
      (* small or medium leaf *)
      let l = 1 lsl 32 - tag in
      assert (l > 0);
      assert (l <= max_size_small_leaf);
      if context.with_count then begin
        let extra_cells = ncells_for_bytes @@ l - 4 in
        parse_leaf_value ~extra_cells l
      end else begin (* context.with_count = false *)
        let extra_cells = ncells_for_bytes l in
        parse_leaf_value ~extra_cells l
      end

    else if tag_empty_bud <= tag then
      (* another tag *)

      if tag = tag_large_leaf then
        if context.with_count then
          let l = B.get_uint32 buf offsets.count in
          let extra_cells = ncells_for_bytes l in
          parse_leaf_value ~extra_cells l
        else
          let buf = get_cell storage Index.Unsafe.(pred i) in
          let l = B.get_uint32 buf (bytes_per_cell - 4) in
          let extra_cells = ncells_for_bytes @@ l + 4 in
          parse_leaf_value ~extra_cells l

      else if tag = tag_extra_large_leaf then (* large value in chunk *)
        let v = Value.of_string @@ Chunk.read storage @@ Index.Unsafe.pred i in
        let h = if keep_hash then Some (get_hash_prefix ()) else None in
        _Leaf (v, Some i, h)

      else if tag = tag_link then (* linked *)
        let i' = B.get_index buf offsets.link in
        parse_cell ~keep_hash context i'

      else if tag = tag_empty_bud then (* empty bud *)
        let h = Some context.hasher.Hash.Hasher.zero in
        _Bud (None, Some i, h)

      else assert false

    else
      (* It is not a tag but an index *)
      let flags = B.get_uint8 buf offsets.flags in
      match flags land 0b11 with
      | 0b01 ->
          (* extender  |<- segment ->0..0|6bits|01| |<- the index of the child ->| *)
          let i' = B.get_index buf offsets.index_part in
          let cells_extra = flags lsr 2 in
          assert (cells_extra <= 63);
          let nbytes = cells_extra * bytes_per_cell + offsets.segment_area_length in
          let buf = get_bytes storage Index.(Unsafe.(i - of_int cells_extra)) nbytes in
          let s = B.copy buf 0 nbytes in
          let seg = Segment.Serialization.decode_exn s in
          if keep_hash then
            let v = parse_cell ~keep_hash context i' in
            let h =
              match v with
              | Leaf (_, _, h)
              | Internal (_, _, _, h)
              | Bud (_ ,_, h) -> h
              | Extender _ -> assert false
            in
            _Extender (seg, View v, Some i, h)
          else
            _Extender (seg, Disk i', Some i, None)
      | 0b11->
          let i' = B.get_index buf offsets.index_part in
          let h = if keep_hash then Some (get_hash_prefix ()) else None in
          _Bud (Some (Disk i'), Some i, h)
      | (0b00 | 0b10 as bb) -> (* internal *)
          if not context.hasher.config.flags_combined then
            let refer_to_right = bb = 2 in
            let i' = B.get_index buf offsets.index_part in
            let j = Index.Unsafe.pred i in
            let h = if keep_hash then Some (get_hash_prefix ()) else None in
            if refer_to_right then
              _Internal (Disk j, Disk i', Some i, h)
            else
              _Internal (Disk i', Disk j, Some i, h)
          else
            let bytes_per_hash = context.hasher.config.hashfunc.length in
            (* the last 2 bits of the hash prefix must be reset *)
            let refer_to_right = bb = 2 in
            let i' = B.get_index buf offsets.index_part in
            (* Because of the link, Index.pred i may not be the index of the child! *)
            let j = Index.Unsafe.pred i in
            let h =
              if keep_hash then
                (* the last 2 bits of the hash prefix must be reset *)
                let h =
                  let s = B.copy buf offsets.hash_prefix bytes_per_hash in
                  let b = Bytes.unsafe_of_string s in
                  Bytes.unsafe_set b (bytes_per_hash - 1) @@ Char.chr @@ flags land 0xfc;
                  Hash.of_raw_string @@ Bytes.unsafe_to_string b
                in
                Some h
              else None
            in
            if refer_to_right then
              _Internal (Disk j, Disk i', Some i, h)
            else
              _Internal (Disk i', Disk j, Some i, h)
      | _ -> assert false

let parse_cell ~keep_hash context index =
  let v = parse_cell ~keep_hash context index in
  if keep_hash then begin
    match hash_of_view v with
    | Some nh -> Context.add_to_cache context nh index
    | _ -> ()
  end;
  v

(* Hash may not be not loaded *)
let read_node context index =
  let keep_hash = context.Context.keep_hash in
  let v = parse_cell ~keep_hash context index in
  GlobalStat.incr_loaded_nodes context.Context.stat;
  v

(* Hash are assured to be loaded *)
let read_node_with_hash context index =
  let v = parse_cell ~keep_hash:true context index in
  GlobalStat.incr_loaded_nodes context.Context.stat;
  v

(* If the hash of the node is not loaded yet, load it if possible *)
let rec read_hash context n = match n with
  | Disk i ->
      let v = read_node_with_hash context i in
      begin match v with
        | Leaf (_, _, Some hp) -> `Hashed ((hp, ""), View v)
        | Bud (_, _, Some hp) -> `Hashed ((hp, ""), View v)
        | Internal (_, _, _, Some hp) -> `Hashed ((hp, ""), View v)
        | Extender (seg, _, _, Some hp) -> `Hashed (Node_hash.of_extender seg hp, View v)
        | _ -> assert false
      end

  | Hash nh -> `Hashed (nh, n)

  | View (Leaf (_, _, Some hp)) -> `Hashed ((hp, ""), n)
  | View (Bud (_, _, Some hp)) -> `Hashed ((hp, ""), n)
  | View (Internal (_, _, _, Some hp)) -> `Hashed ((hp, ""), n)
  | View (Extender (seg, _, _, Some hp)) -> `Hashed (Node_hash.of_extender seg hp, n)

  | View (Leaf (a, Some i, None)) ->
      let hp = read_hash_prefix context i in
      let nh = hp, "" in
      `Hashed (nh, View (_Leaf (a, Some i, Some hp)))
  | View (Leaf (a, None, None)) ->
      let hp = Node_hash.of_leaf context.hasher a in
      let nh = hp, "" in
      `Hashed (nh, View (_Leaf (a, None, Some hp)))
  | View (Bud (None, i, None)) ->
      let hp = context.hasher.Hash.Hasher.zero in
      let nh = hp, "" in
      `Hashed (nh, View (_Bud (None, i, Some hp)))
  | View (Bud (a, Some i, None)) ->
      let hp = read_hash_prefix context i in
      let nh = hp, "" in
      `Hashed (nh, View (_Bud (a, Some i, Some hp)))
  | View (Bud (Some a, None, None)) ->
      begin match read_hash context a with
      | `Hashed (nh, a) ->
          let hp = Node_hash.of_bud context.hasher (Some nh) in
          `Hashed ((hp,""), View (_Bud (Some a, None, Some hp)))
      | `Not_Hashed v ->
          `Not_Hashed (_Bud (Some (View v), None, None))
      end
  | View (Internal (a, b, Some i, None)) ->
      let hp = read_hash_prefix context i in
      let nh = hp, "" in
      `Hashed (nh, View (_Internal (a, b, Some i, Some hp)))
  | View (Internal (a, b, None, None)) ->
      begin match read_hash context a, read_hash context b with
        | `Hashed (nha, a), `Hashed (nhb, b) ->
            let hp = Node_hash.of_internal context.hasher nha nhb in
            `Hashed ((hp,""), View (_Internal (a, b, None, Some hp)))
        | `Hashed (_, a), `Not_Hashed vb ->
            `Not_Hashed (_Internal (a, View vb, None, None))
        | `Not_Hashed va, `Hashed (_, b) ->
            `Not_Hashed (_Internal (View va, b, None, None))
        | `Not_Hashed va, `Not_Hashed vb ->
            `Not_Hashed (_Internal (View va, View vb, None, None))
      end
  | View (Extender (seg, a, i, None)) ->
      begin match read_hash context a with
      | `Hashed ((hp, ""), a) ->
          let nh = Node_hash.of_extender seg hp in
          `Hashed (nh, View (_Extender (seg, a, i, Some hp)))
      | `Not_Hashed v ->
          `Not_Hashed (_Extender (seg, View v, i, None))
      | `Hashed _ -> assert false
      end

exception HashOnly of Hash.Long.t

let view c = function
  | Hash h -> raise (HashOnly h)
  | View v -> v
  | Disk i -> read_node c i

let read_node_rec ~reset_index context n =
  Traverse.Map.f
    ~enter:(fun n -> `Continue (view context n))
    ~leave:(fun ~org v ->
        let index = if reset_index then (fun _ -> None) else (fun i -> i) in
        View
          (match org, v with
           | Leaf (v, i, h), Leaf _ -> _Leaf (v, index i, h)
           | Internal (_, _, i, h), Internal(nl, nr, _, _) -> _Internal (nl, nr, index i, h)
           | Bud (_, i, h), Bud (no, _, _) -> _Bud (no, index i, h)
           | Extender (_, _, i, h), Extender (seg, n, _, _) -> _Extender (seg, n, index i, h)
           | _ -> assert false))
    n

let leaf _context (v, ir, hit) = View (_Leaf (v, ir, hit))

let internal _context (nl, nr, ir, hit) = View (_Internal (nl, nr, ir, hit))

let bud context (nopt, ir, hit) =
  View (match nopt with
      | None -> _Bud (None, ir, hit)
      | Some n ->
          (* Bud cannot have Disk to avoid having potential Bud-Bud/Bud-Leaf *)
          _Bud (Some (View (view context n)), ir, hit))

let extender context (seg, n, ir, hit) =
  (* Extender cannot have Disk to avoid having potential Extender-Extender *)
  View (_Extender (seg, View (view context n), ir, hit))

let change_context ~src:context ~dst:context' n =
  if not @@ Hash.Hasher.is_compatible context.Context.hasher context'.Context.hasher then
    invalid_arg "Node_storage.change_context";
  let enter n =
    let v = view context n in
    (* This is not very helpful since hashes are no longer loaded by default *)
    match Node_type.hash_of_view v with
    | Some nh ->
        begin match Context.find_in_cache context' nh with
        | None -> `Continue v
        | Some i -> `Return (Disk i)
        end
    | _ -> `Continue v
  in
  (* We must reset the index of [context], since it is invalid for [context'] *)
  let leave =
    Fold.
      { leaf= (fun v _i h -> leaf context' (v, None, h))
      ; bud= (fun xop _i h -> bud context' (xop, None, h))
      ; extender= (fun seg n _i h -> extender context' (seg, n , None, h))
      ; internal= (fun nl nr _i h -> internal context' (nl, nr, None, h))
      }
  in
  Fold.fold ~enter ~leave n

(* index 32 bits (4294967296)
   block 32 bytes
   max size of the storage 137_438_953_472 =~ 128GB when byte_per_cell = 32
*)
let index n = match index n with
  | Some i -> i
  | None -> assert false

let zero_link (t : Context.t) = String.make (t.bytes_per_cell - 8) '\000'

(* The code assumes the only one writer. *)

(* for small values *)
let write_small_value storage i v =
  let l = Value.length v in
  let buf = get_bytes storage i l in
  B.write_string (Value.to_string v) buf 0

(* Unlike the above, this function allocates cells by itself *)
let write_extra_large_value storage v =
  let open Result.Infix in
  Storage.Chunk.write storage (Value.to_string v) >|= fun _ -> ()

let write_leaf_cell context i ~tag hp =
  (* It must not touch the count field *)
  let storage = Context.get_storage context in
  let offsets = context.offsets in
  let buf = get_cell storage i in
  let hps = Hash.to_raw_string hp in
  B.write_string hps buf offsets.hash_prefix;
  B.set_index buf offsets.index_part tag;
  GlobalStat.incr_written_leaves context.Context.stat;
  i

let write_leaf (context : Context.t) value hp =
  let open Result.Syntax in
  let len = Value.length value in
  let bytes_per_cell = context.bytes_per_cell in
  let storage = context.storage in
  assert (len > 0);
  let+ i =
    let [@inline] ncells_for_bytes nbytes =
      (nbytes + bytes_per_cell - 1) / bytes_per_cell
    in
    if context.with_count then
      (* We can make use of the count area of the leaf cell *)
      if len <= max_size_small_leaf then
        let ncells = ncells_for_bytes (len - 4) + 1 in
        let+ i = Storage.new_indices storage ncells in
        write_small_value storage i value;
        let i = Index.Unsafe.(+) i (Index.of_int (ncells - 1)) in
        write_leaf_cell context i ~tag:(Index.Unsafe.of_int (-len)) hp
      else
        let* () = write_extra_large_value storage value in
        let+ i = Storage.new_index storage in
        write_leaf_cell context i ~tag:(Index.of_int tag_extra_large_leaf) hp
    else
      if len <= max_size_small_leaf then
        let ncells = ncells_for_bytes len + 1 in
        let+ i = Storage.new_indices storage ncells in
        write_small_value storage i value;
        let i = Index.Unsafe.(+) i (Index.of_int (ncells - 1)) in
        write_leaf_cell context i ~tag:(Index.Unsafe.of_int (-len)) hp
      else if len < 1 lsl 32 then
        let offsets = context.offsets in
        if context.with_count then begin
          let ncells = ncells_for_bytes len + 1 in
          let+ i = Storage.new_indices storage ncells in
          write_small_value storage i value;
          let i = Index.Unsafe.(+) i (Index.of_int (ncells - 1)) in
          let buf = get_cell storage i in
          B.set_uint32 buf offsets.count len;
          write_leaf_cell context i ~tag:(Index.Unsafe.of_int tag_large_leaf) hp
        end else begin
          let ncells = ncells_for_bytes (len + 4) + 1 in
          let+ i = Storage.new_indices storage ncells in
          write_small_value storage i value;
          let i = Index.Unsafe.(+) i (Index.of_int (ncells - 2)) in
          let buf = get_cell storage i in
          B.set_uint32 buf (bytes_per_cell - 4) len;
          let i = Index.Unsafe.succ i in
          write_leaf_cell context i ~tag:(Index.Unsafe.of_int tag_large_leaf) hp
        end
      else
        let* () = write_extra_large_value storage value in
        let+ i = Storage.new_index storage in
        write_leaf_cell context i ~tag:(Index.of_int tag_extra_large_leaf) hp
  in
  GlobalStat.incr_written_leaf_sizes context.stat len;
  i

let with_node_cache ctxt nh f =
  let open Result.Syntax in
  match Context.find_in_cache ctxt nh with
  | Some i -> Ok i
  | None ->
      let+ i = f () in
      Context.add_to_cache ctxt nh i;
      i

let write_leaf context value hp =
  with_node_cache context (hp,"") @@ fun () -> write_leaf context value hp

let write_link context storage i index =
  (* |<- 0's ->|<-   child index  ->| |<- 2^32 - 254 ->| *)
  let zero_link = zero_link context in
  let offsets = context.offsets in
  let buf = get_cell storage i in
  B.write_string zero_link buf 0;
  B.set_index buf offsets.link index;
  B.set_index buf offsets.index_part (Index.of_int tag_link);
  GlobalStat.incr_written_links context.Context.stat

let write_internal context nl nr nh cnt =
  let open Result.Syntax in
  (* internal  |<-- cnt -->|<- hash -------->|D|0| |<- the index of one of the child ----->|
       if combine_flags

     internal  |<-- cnt -->|<- hash -------->|..0..|D|0| |<- the index of one of the child ----->|
       if not combine_flags
  *)
  let storage = Context.get_storage context in
  let offsets = context.offsets in
  let hp = Hash.Long.prefix nh in
  let+ i = with_node_cache context nh @@ fun () ->
    let hpstr = Hash.to_raw_string hp in
    let il = index nl in
    let ir = index nr in

    let* i = Storage.new_index storage in
    let i' = Index.Unsafe.pred i in

    let+ must_refer_to, i =
      if i' = il then Ok (`Right, i)
      else if i' = ir then Ok (`Left, i)
      else begin
        (* Fat internal *)
        (* Write the link to the right at i *)
        write_link context storage i ir;
        let+ i = Storage.new_index storage in
        (`Left, i)
      end
    in

    let buf = get_cell storage i in
    let bytes_per_hash = context.hasher.config.hashfunc.length in

    (match context.with_count, cnt with
     | true, Some cnt -> B.set_uint32 buf offsets.count cnt
     | true, None -> assert false
     | false, _ -> ());

    if context.hasher.config.flags_combined then begin
      (* 0 to 215 bits *)
      B.blit_from_string hpstr 0 buf offsets.hash_prefix (bytes_per_hash - 1);

      (* fix for the 223rd and 224th bits (pos 222, 223) *)
      B.set_uint8 buf offsets.flags
        (let c = Char.code @@ String.unsafe_get hpstr (bytes_per_hash - 1) in
         let c = c land 0xfc in
         if must_refer_to = `Left then c else c lor 2);

      (* next 32bits *)
      B.set_index buf offsets.index_part (if must_refer_to = `Left then il else ir);
    end else begin
      B.blit_from_string hpstr 0 buf offsets.hash_prefix bytes_per_hash;

      B.set_uint8 buf offsets.flags (if must_refer_to = `Left then 0 else 2);

      (* next 32bits *)
      B.set_index buf offsets.index_part (if must_refer_to = `Left then il else ir);
    end;
    GlobalStat.incr_written_internals context.Context.stat;
    i
  in
  (_Internal (nl, nr, Some i, Some hp), i, nh, cnt)

let write_empty_bud context =
  let open Result.Syntax in
  let storage = Context.get_storage context in
  let offsets = context.offsets in
  let bud_hash_part = String.make context.hasher.config.hashfunc.length '\255' in
  (* XXX No point to store the empty bud more than once... *)
  (* empty bud |<- 0 ->| |<- 1111111111111111111111111111 ->| |<- 2^32 - 256 ->| *)
  let+ i = Storage.new_index storage in
  let buf = get_cell storage i in
  (match context.with_count with
   | true -> B.set_uint32 buf offsets.count 0
   | false -> ());
  B.write_string bud_hash_part buf offsets.hash_prefix;
  B.set_index buf offsets.index_part (Index.of_int tag_empty_bud);
  GlobalStat.incr_written_buds context.Context.stat;
  GlobalStat.incr_written_empty_buds context.Context.stat;
  let hp = context.hasher.zero in
  (_Bud (None, Some i, Some hp), i, (hp, ""), Some 1)

let write_bud context n nh cnt =
  let open Result.Syntax in
  (* bud       |<- count ->| |<- hash -------->|1|1| |<- the index of the child ->| *)
  let hasher = context.Context.hasher in
  let storage = Context.get_storage context in
  let offsets = context.offsets in
  let hp = Hash.Long.prefix nh in
  let+ i = with_node_cache context nh @@ fun () ->
    let+ i = Storage.new_index storage in
    let buf = get_cell storage i in
    (match context.with_count, cnt with
     | true, Some cnt -> B.set_uint32 buf offsets.count cnt
     | true, None -> assert false
     | false, _ -> ());
    if hasher.config.flags_combined then begin
      (* flags are already in [hp] *)
      let s = Hash.to_raw_string hp in
      B.write_string s buf offsets.hash_prefix;
      B.set_index buf offsets.index_part @@ index n;
    end else begin
      let s = Hash.to_raw_string hp in
      B.write_string s buf offsets.hash_prefix;
      B.set_uint8 buf offsets.flags 0b11;
      B.set_index buf offsets.index_part @@ index n;
    end;
    GlobalStat.incr_written_buds context.Context.stat;
    i
  in
  (_Bud (Some n, Some i, Some hp), i, nh, Some 1)

let write_extender context seg n nh =
  let open Result.Syntax in
  (* extender  |<- segment --|..|---------->|6bits|01| |<- the index of the child ->| *)
  let storage = Context.get_storage context in
  let offsets = context.offsets in
  let bytes_per_cell = context.bytes_per_cell in
  let hp = Hash.Long.prefix nh in
  let [@inline] ncells_for_bytes nbytes =
    (nbytes + bytes_per_cell - 1) / bytes_per_cell
  in
  let+ i' = with_node_cache context nh @@ fun () ->
    let sseg = Segment.Serialization.encode seg in
    let extra_cells = ncells_for_bytes (String.length sseg - offsets.segment_area_length) in
    if extra_cells > 63 then assert false;
    let+ i = Storage.new_indices storage (extra_cells + 1) in
    let i' = Index.Unsafe.(+) i (Index.Unsafe.of_int extra_cells) in
    let buf = get_cell storage i' in
    B.set_index buf offsets.index_part @@ index n;
    B.set_uint8 buf offsets.flags (extra_cells lsl 2 + 0b01);

    let buf = get_bytes storage i ((extra_cells + 1) * bytes_per_cell) in
    B.write_string sseg buf 0;
    let zeros = extra_cells * bytes_per_cell + offsets.segment_area_length - String.length sseg in
    B.write_string (String.make zeros '\x00') buf (String.length sseg);
    GlobalStat.incr_written_extenders context.Context.stat;
    i'
  in
  (_Extender (seg, n, Some i', Some hp), i', nh)

(*
          |<------- 32 bits ------>| |< ----   224 bits -------->| |<------- 32 bits ------>|
internal  |<------- count -------->| |<- first 222 of hash ->|D|0| |<- index of A child --->| (also refers to the previous cell)
*)
(* No check the cell really has a count field or not *)
let read_count ctxt i =
  let buf = get_cell ctxt.Context.storage i in
  B.get_uint32 buf ctxt.offsets.count

let rec count_itself ctxt n =
  let v = view ctxt n in
  let cnt, v = count_itself_view ctxt v in
  cnt, View v

and count_itself_view ctxt v =
  match v with
  | Leaf _ | Bud _ -> 1, v
  | Extender (seg, n, a, b) ->
      let j, n = count_itself ctxt n in
      j, _Extender (seg, n, a, b)
  | Internal (_, _, Some i, _) as v ->
      read_count ctxt i, v
  | Internal (l, r, None, b) ->
      let cntl, l = count_itself ctxt l in
      let cntr, r = count_itself ctxt r in
      cntl + cntr, _Internal (l, r, None, b)

type Error.t += Cannot_write_hash_only_node

let () = Error.register_printer (function
    | Cannot_write_hash_only_node -> Some "cannot write hash only node"
    | _ -> None)

let write_node ?(clear=true) context node =
  let open Result.Syntax in
  if Context.is_memory_only context then
    failwith "MemoryOnly context cannot write a node"
  else
    let read_count =
      if not context.Context.with_count then
        fun _ _ -> None
      else
        fun context i -> Some (read_count context i)
    in
    let count_itself_view =
      if not context.Context.with_count then
        fun _ v -> None, v
      else
        fun context v ->
          let cnt, v = count_itself_view context v in
          Some cnt, v
    in
    let count_itself =
      if not context.Context.with_count then
        fun _ n -> None, n
      else
        fun context n ->
          let cnt, n = count_itself context n in
          Some cnt, n
    in
    let hasher = context.Context.hasher in
    if not (Context.mode context = Writer) then
      failwith "Non Writer context cannot write a node";
    (* It is not tail recursive.  It will crash if the depth of the tree exceeds
       the stack limit.  But Plebeia trees never get so deep.
    *)
    let rec write_aux : node -> (node * Index.t * Hash.Long.t * int option, _) result = function
      | Hash _ -> Error Cannot_write_hash_only_node
      | Disk index ->
          (* Need to get the hash from the disk *)
          let v = read_node_with_hash context index in
          let cnt, v = count_itself_view context v in
          let nh = Option.get @@ Node_type.hash_of_view v in
          Ok ((if clear then Disk index else View v), index, nh, cnt)
      | View v ->
          let+ v', i, nh, cnt = write_aux' v in
          ((if clear then Disk i else View v'), i, nh, cnt)

    and write_aux' : view -> (view * Index.t * Hash.Long.t * int option, _) result = fun v ->
      match v with
      | Leaf (_, Some i, h)
      | Bud (_, Some i, h) ->
          let hp = Option.value_f h ~default:(fun () -> read_hash_prefix context i) in
          let nh = Hash.Long.of_prefix hp in
          Ok (v, i, nh, Some 1)
      | Internal (_, _, Some i, h) ->
          let hp = Option.value_f h ~default:(fun () -> read_hash_prefix context i) in
          let nh = Hash.Long.of_prefix hp in
          let cnt = read_count context i in
          Ok (v, i, nh, cnt)
      | Extender (seg, n, Some i, h) ->
          let cnt, n = count_itself context n in
          let v = _Extender (seg, n, Some i, h) in
          let hp = Option.value_f h ~default:(fun () -> read_hash_prefix context i) in
          let nh = Node_hash.of_extender seg hp in
          Ok (v, i, nh, cnt)

      (* indexing is necessary below.  If required, the hash is also computed *)
      | Leaf (value, None, h) ->
          (* if the size of the value is 1 <= size <= bytes_per_cell, the contents are
              written to the previous index of the leaf

              XXX we may write the value at the node bytes_per_cell > 32 and
              the value is very small
          *)
          let hp = Option.value_f h ~default:(fun () -> Node_hash.of_leaf hasher value) in
          let nh = Hash.Long.of_prefix hp in
          let len = Value.length value in
          GlobalStat.incr_committed_leaf_sizes context.stat len;
          if len = 0 then
            (* We don't store 0 size leaves *)
            let index = zero_sized_leaf_index in
            Ok (_Leaf (value, Some index, Some hp), index, nh, Some 1)
          else
            let+ i = write_leaf context value hp in
            (_Leaf (value, Some i, Some hp), i, nh, Some 1)

      | Bud (None, None, _) -> write_empty_bud context

      | Bud (Some underneath, None, h) ->
          let* (node, _, nh', cnt) = write_aux underneath in
          let hp = match h with
          | Some hp -> hp
          | None -> Node_hash.of_bud hasher (Some nh') in
          let nh = Hash.Long.of_prefix hp in
          write_bud context node nh cnt

      | Internal (left, right, None, h) ->
          let* (left, _il, nhl, cntl) = write_aux left in
          let* (right, _ir, nhr, cntr) = write_aux right in
          let hp = Option.value_f h ~default:(fun () -> Node_hash.of_internal hasher nhl nhr) in
          let nh = Hash.Long.of_prefix hp in
          let cntlr =
            match cntl, cntr with
            | Some cntl, Some cntr -> Some (cntl + cntr)
            | _ -> None
          in
          write_internal context left right nh cntlr

      | Extender (segment, underneath, None, _) ->
          let* (underneath, _i, nh', cnt) = write_aux underneath in
          let hp' = Hash.Long.prefix nh' in
          let nh = Node_hash.of_extender segment hp' in
          let+ n, i, nh = write_extender context segment underneath nh in
          (n, i, nh, cnt)
    in
    let+ (node, i, nh, cnt) = write_aux node in
    (node, i, Hash.Long.prefix nh, cnt)

let count (ctxt : Context.t) n =
  if not ctxt.with_count then None
  else
    Some (match view ctxt n with
        | Bud (None, _, _) as v -> 0, View v
        | Bud (Some _, Some i, _) as v ->
            let cnt = read_count ctxt i in
            cnt, View v
        | Bud (Some n, None, a) ->
            let cnt, n = count_itself ctxt n in
            cnt, View (_Bud (Some n, None, a))
        | v ->
            let cnt, v = count_itself_view ctxt v in
            cnt, View v)

let [@inline] read_leaf_value _ctxt = function
  | Leaf (v, _, _) -> v
  | _ -> invalid_arg "read_leaf_value"

module Internal = struct
  let parse_cell = parse_cell

  let read_node_rec_for_test context n =
    let rec aux n =
      let v = match n with
        | Hash _ -> assert false
        | Disk i -> read_node context i
        | View v -> v
      in
      match v with
      | Leaf _ -> View v
      | Bud (None, _, _) -> View v
      | Bud (Some n, i, h) ->
          let n = aux n in
          View (_Bud (Some n, i, h))
      | Internal (n1, n2, i, h) ->
          let n1 = aux n1 in
          let n2 = aux n2 in
          View (_Internal (n1, n2, i, h))
      | Extender (seg, n, i, h) ->
          let n = aux n in
          View (_Extender (seg, n, i, h))
    in
    aux n

  let equal_for_test context n1 n2 =
    let rec aux = function
      | [] -> Ok ()
      | (n1,n2)::rest ->
          match n1, n2 with
          | Disk i1, Disk i2 when i1 = i2 -> aux rest
          | Disk _, Disk _ -> Error (n1,n2)
          | Disk i, n2 ->
              let n1 = View (read_node context i) in
              aux @@ (n1,n2)::rest
          | n1, Disk i ->
              let n2 = View (read_node context i) in
              aux @@ (n1,n2)::rest
          | View v1, View v2 ->
              begin match v1, v2 with
              | Internal (n11, n12, _, _), Internal (n21, n22, _, _) ->
                  aux @@ (n11,n21)::(n12,n22)::rest
              | Bud (None, _, _), Bud (None, _, _) -> aux rest
              | Bud (Some n1, _, _), Bud (Some n2, _, _) -> aux @@ (n1,n2) :: rest
              | Leaf (v1, _, _), Leaf (v2, _, _) when v1 = v2 -> aux rest
              | Extender (seg1, n1, _, _), Extender (seg2, n2, _, _) when Segment.equal seg1 seg2 ->
                  aux @@ (n1,n2)::rest
              | _ -> Error (n1,n2)
              end
          | Hash h1, Hash h2 when h1 = h2 -> Ok ()
          | Hash _, _ | _, Hash _ -> Error (n1, n2)
    in
    aux [(n1, n2)]
end
