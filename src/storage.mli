(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Generic cell based storage *)

(** Mode to access the storage.

    - Reader: Read only mode.  Multiple readers can exist.
    - Writer: Read write mode.  Only 1 writer can exist for a storage.
*)
type mode = Reader | Writer

type config =
  { head_string : string (** exactly 20 bytes *)
  ; version : int
  ; bytes_per_cell : int
  ; max_index : Index.t (** Maximal possible index. Requesting more indicies
                            than [max_index] fails. *)
  }

val pp_config : Format.formatter -> config -> unit

type storage
type t = storage
(** The type *)

type Error.t +=
  | No_such_file of string
  | Not_reader
  | Not_writer
  | Index_overflow of t (** Error when Index.t exceeds the [max_index] or overflows *)
  | Config_mismatch of { actual: config; expected: config }

module Writer_key : sig
  (** Key required to enable fast process communcation between the writer process
      and readers *)
  type t = private string

  val encoding : t Data_encoding.t
end

(** Make a writer key which is new to all the given [t]s *)
val make_new_writer_key : t list -> Writer_key.t Lwt.t

(** { 2 Open, close, and commit } *)

val create :
  ?length: Index.t
  -> ?resize_step_bytes: int
  -> config:config
  -> key:Writer_key.t
  -> string
  -> t Lwt.t
(** Create a new storage

    * length: The initial size of the storage in cells
    * resize_step_bytes: How many cells allocated for each resize
    * config : Configuration
    * key : The writer key
    * string : The path name
*)

val open_existing_for_read :
  ?resize_step_bytes: int
  -> ?key:Writer_key.t
  -> string
  -> (config * t, Error.t) result Lwt.t
(** Open an existing storage *)

(** Open a file for writing.  If the file does not exist, create it. *)
val open_for_write :
  ?resize_step_bytes:int
  -> config: config
  -> key:Writer_key.t
  -> string
  -> t Lwt.t

(** Dummy storage *)
val null : t

(** [is_null t] returns [true] if [t] is [null] *)
val is_null : t -> bool

val truncate : ?length: int -> t -> unit Lwt.t
(** Truncate the data file and reinitialize it.  All the contents are lost.
    [length] is the initial reserved size of the reinitialized file.
*)

val update_reader : t -> unit Lwt.t
(** For reader, catch up the storage states updated by the writer process.

    For Writer and Private, it does nothing. *)

val close : t -> unit Lwt.t
(** Close the storage.  For the Writer, it calls [commit] before closing. *)

val commit : t -> unit Lwt.t
(** Write the current state of the storage.  Once called, the state is shared
    with the reader processes with a writer key.

    Note that [commit] does NOT assure the persistence of the data after
    a system crash.  Use [flush] to make it persistent even after a crash.
*)

val flush : t -> unit Lwt.t
(** Flushes all the added data to the disk.  Once called, the state is shared
    with all the readers, even without the writer key.

    The data added to the storage before the call of [flush] are persisted
    even after a system crash. Data added after the last call of [flush]
    may be lost after a system crash.

    [flush] may trigger huge disk writes.  Too frequent calls of [flush]
    may degrates the performance.
*)

(** A reader process obtains the latest state of the storage calling
    [update_reader] function. By default, it can detect the updates only
    after the writer calls [flush]. [flush] synchronizes the disk and
    therefore is a heavy operation.

    [enable_process_sync t key] optimizes this state update for the reader
    process: the reader process can retrieve the latest storage state
    even without the writer's call of [flush]. The reader must call this
    function with the same writer [key] made by the writer.

    The writer and a reader process must run on the same machine to use
    [enable_process_sync] correcly.  (In any way, it is not recommended
    to use Plebeia via network file systems.)

    Typical usage:

    1. The writer process creates a new writer key.
    2. The writer opens storages (context and commit_tree) with the writer key.
       (Vc.create and Vc.open_for_write do this internally.)
    3. The writer sends the writer key to reader processes.
    4. The reader processes call [enable_process_sync] to enable the faster
       process synchronization.

    Call of this function with an invalid writer key returns an [Error].
    Call of this function in the writer process returns an [Error].
*)
val enable_process_sync : t -> Writer_key.t -> (unit, Error.t) result Lwt.t

(** { 2 Accessor } *)

val filename : t -> string
(** Return the file name *)

val mode : t -> mode
(** Return the opening mode *)

val get_last_root_index  : t -> Index.t option

val get_current_length   : t -> Index.t
(** Get the status of the storage

    For Reader, it only returns the lastest information it knows in memory,
    which can be smaller than the information on the disk updated by the writer.
*)

val size : t -> int64
(** Used cells in bytes *)

val version : t -> int
(** Storage version*)

val start_index : t -> Index.t

val writer_key : t -> Writer_key.t option

val override_version : t -> int -> unit
(** Override the header version of the file *)

val set_last_root_index  : t -> Index.t option -> unit
(** Set the last index of root hash.
    Note that the data are only saved to the file when [Header.commit]
    is called.
*)

(** { 2 Read and write } *)

val get_cell : t -> Index.t -> Mmap.Buffer.t
(** Get the content of the cell specified by the index *)

val get_cell2 : t -> Index.t -> Mmap.Buffer.t
(** make a 2-cell-wide writable buffer from the beginning of
    the cell of the given index
*)

val get_bytes : t -> Index.t -> int -> Mmap.Buffer.t
(** Get the contiguous bytes from the head of the index *)

val new_index : t -> (Index.t, Error.t) result
(** Allocate a cell and returns its index *)

val new_indices : t -> int -> (Index.t, Error.t) result
(** Allocate cells and return the first index *)

module Chunk : sig
  (** Bigger data than a cell *)

  val read : t -> Index.t -> string
  (** Read a big data which is stored from the specified index *)

  val write : t -> string -> (Index.t, Error.t) result
  (** Write a big data and returns the index *)

  val test_write_read : Random.State.t -> t -> unit
  (** A test function *)
end

(** { 2 Statistics } *)

module Stat : sig
  type t =
    { mutable flushes : int
    ; mutable flush_time : Mtime.Span.t
    }

  val zero : t

  val pp : Format.formatter -> t -> unit
end

val stat : t -> Stat.t

(** { 2 Internal use } *)

module Internal : sig
  (** Call [msync] to synchronize the mmap and the disk *)
  val msync : t -> unit Lwt.t

  val set_current_length : t -> Index.t -> unit

  module Header : sig

    (** Header has the fixed size 256 bytes. *)

    type t = { last_next_index : Index.t; last_root_index : Index.t option; }

    val pp : t Utils.Format.t

    val read_disk_sync : storage -> t option Lwt.t
    val write_disk_sync : storage -> t -> unit Lwt.t

    val read_process_sync : storage -> ([> `BadKey | `GoodKey | `NoKey] * t) option Lwt.t
    val write_process_sync : storage -> t -> unit Lwt.t
  end
end
