(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Cursor

type Error.t += Segments_cannot_be_empty

let () = Error.register_printer (function
    | Segments_cannot_be_empty ->
        Some "Segments cannot be empty"
    | _ -> None)

(** Multi Bud level interface *)
let deep ~dont_move ~create_subtrees cur segs f =
  let open Result.Syntax in
  let rec ups cur = function
    | [] -> Ok cur
    | _seg::segs ->
        let* cur = parent cur in
        ups cur segs
  in
  let rec aux hist cur = function
    | [] -> assert false
    | [seg] ->
        let* cur, res = f cur seg in
        if dont_move then
          let* cur = ups cur hist in
          Ok (cur, res)
        else
          Ok (cur, res)
    | seg::segs ->
        let* cur =
          (if create_subtrees then subtree_or_create else subtree) cur seg
        in
        aux (seg::hist) cur segs
  in
  (* XXX We may have an Error *)
  if segs = [] then Error Segments_cannot_be_empty
  else aux [] cur segs

let get cur segs =
  deep ~dont_move:true ~create_subtrees:false cur segs get

let get_value cur segs =
  deep ~dont_move:true ~create_subtrees:false cur segs get_value

let upsert cur segs v =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees:true cur segs (fun cur seg ->
      upsert cur seg v >|? fun cur -> cur, ()) >|? fst

let insert cur segs v =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees:true cur segs (fun cur seg ->
      insert cur seg v >|? fun cur -> cur, ()) >|? fst

let update cur segs v =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees:false cur segs (fun cur seg ->
      update cur seg v >|? fun cur -> cur, ()) >|? fst

let delete cur segs =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees:false cur segs (fun cur seg ->
      delete cur seg >|? fun cur -> cur, ()) >|? fst

let delete2 cur segs =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees:false cur segs (fun cur seg ->
      delete' cur seg >|? fun cur -> cur, ()) >|? fst

let delete_and_clean_empty_buds cur segs =
  let open Result_lwt.Infix in
  let rec loop c = function
    | [] -> assert false
    | [seg] -> Cursor.delete c seg
    | seg::segs ->
        subtree c seg
        >>? fun c -> loop c segs
        >>? fun c ->
        let empty = match snd @@ Cursor.view c with
          | Bud (None, _, _) -> true
          | Bud _ -> false
          | _ -> assert false
        in
        Cursor.go_up_to_bud c
        >>? fun c ->
        if empty then Cursor.delete c seg
        else Ok c
  in
  loop cur segs >>? fun c ->
  (* If the Bud at the original point becomes empty,
     the function fails.  Exception: root *)
  match Cursor.view c with
  | Cursor (Top, _, _), _ -> Ok c
  | _, Bud (None, _, _) -> Error (Cursor.Write "delete_and_clean_empty': cannot delete the starting point")
  | _, Bud _ -> Ok c
  | _ -> assert false

let create_subtree ~create_subtrees cur segs =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees cur segs (fun cur seg ->
      create_subtree cur seg >|? fun cur -> (cur, ())) >|? fst

let subtree cur segs =
  let open Result_lwt.Infix in
  deep ~dont_move:false ~create_subtrees:false cur segs (fun cur seg ->
      subtree cur seg >|? fun cur -> (cur, ())) >|? fst

let subtree_or_create ~create_subtrees cur segs =
  let open Result_lwt.Infix in
  deep ~dont_move:false ~create_subtrees cur segs (fun cur seg ->
      subtree_or_create cur seg >|? fun cur -> (cur, ())) >|? fst

(* bud copy by link *)
let copy ?(allow_overwrite=false) ?(only_bud=true) ~create_subtrees cur segs1 segs2 =
  (* the absolute position *)
  (* find the source *)
  let open Result.Syntax in
  let* _, v =
    deep ~dont_move:false ~create_subtrees:false cur segs1 @@ fun cur seg ->
    let* res = access_from_bud cur seg in
    match res with
    | Reached (cur, (Bud _ as v)) ->
        Ok (cur, v) (* Bud is always copiable *)
    | Reached (cur, v) when not only_bud ->
        Ok (cur, v) (* Not only Bud but also internals when only_bud= false *)
    | res -> error_access res
  in

  (* make a link. [cur] is still at the original position *)
  (* this never creates a loop, since [v] has no loop and [v] is
     not modified *)
  let+ cur, () =
    deep ~dont_move:true ~create_subtrees cur segs2 @@ fun cur seg ->
    let+ cur =
      alter cur seg (function
          | Some _ when not allow_overwrite ->
              Error (Cursor.Write "a node already presents at this segment")
          | _ -> Ok (View v)
        )
    in
    cur, ()
  in
  cur

let link n cur segs =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees:true
    cur
    segs
    (fun c seg ->
       (* XXX this alter has once inserted Disk (Maybe_Extender)
          under an extender and crashed program.

          We here workaround this problem to force to view [n]
       *)
       let Cursor (_, _, context) = c in
       (* [n] must be under [context] *)
       let v = Node_storage.view context n in
       let n = Node_type.View v in
       alter c seg (function
           | Some _ -> assert false
           | None -> Ok n) >|? fun c -> (c, ()))
  >|? fst

let alter cur segs f =
  let open Result_lwt.Infix in
  deep ~dont_move:true ~create_subtrees:true
    cur
    segs
    (fun c seg -> alter c seg f >|? fun c -> (c, ()))
  >|? fst
