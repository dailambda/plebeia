(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Lwt.Syntax

let hard_limit_max_tag = 256

module Int64 = Stdint.Int64

module Head_string = struct
  let make ~hash_func ~bytes_per_hash ~with_count =
    assert (0 <= bytes_per_hash && bytes_per_hash < 256);
    Printf.sprintf "PLEBEIA%s%c%c%c"
      (String.make 10 '\000')
      (Char.chr (if with_count then 1 else 0))
      (Char.chr (match hash_func with `Blake2B -> 0 | `Blake3 -> 1))
      (Char.chr bytes_per_hash)

  let _parse head_string =
    if String.length head_string <> 20 then None
    else if String.sub head_string 0 7 <> "PLEBEIA" then None
    else
      let open Option.Syntax in
      let* bytes_per_hash =
        match Char.code head_string.[19] with
        | 0 -> None
        | n -> Some n
      in
      let* hash_func =
        match Char.code head_string.[18] with
        | 0 -> Some `Blake2B
        | 1 -> Some `Blake3
        | _ -> None
      in
      let+ with_count =
        match Char.code head_string.[17] with
        | 0 -> Some false
        | 1 -> Some true
        | _ -> None
      in
      (hash_func, bytes_per_hash, with_count)
end

type config =
  { hasher : Hash.Hasher.config
  ; bytes_per_cell : int
  ; with_count : bool
  }

let make_config ~bytes_per_cell ~with_count ~hash_func ~bytes_per_hash =
  let hasher =
    Hash.Hasher.make_config
      ~bytes_per_cell ~with_count {algorithm= hash_func; length= bytes_per_hash}
  in
  { hasher; bytes_per_cell; with_count }

let default_config =
  make_config ~bytes_per_cell:36 ~with_count:true ~hash_func:`Blake2B ~bytes_per_hash:28

let config_name config =
  Printf.sprintf "%s-%d-%d%s"
    (match config.hasher.hashfunc.algorithm with `Blake2B -> "2" | `Blake3 -> "3")
    config.hasher.hashfunc.length
    config.bytes_per_cell
    (if config.with_count then "" else "-no-count")

let pp_config ppf config =
  Format.fprintf ppf "@[{ bytes_per_cell= %d; hash_func= %s; bytes_per_hash= %d; with_count= %b }@]"
    config.bytes_per_cell
    (match config.hasher.hashfunc.algorithm with `Blake2B -> "`Blake2B" | `Blake3 -> "`Blake3")
    config.hasher.hashfunc.length
    config.with_count

(* This is included in the regression test hashes! *)
type old_config =
  { bytes_per_cell : int
  ; hash_func : [`Blake2B | `Blake3 ]
  ; bytes_per_hash : int
  ; with_count : bool
  }

let old_config (config : config) =
  { bytes_per_cell = config.bytes_per_cell
  ; with_count = config.with_count
  ; hash_func = config.hasher.hashfunc.algorithm
  ; bytes_per_hash = config.hasher.hashfunc.length
  }

type t =
  { config : config
  ; storage : Storage.t
  ; node_cache : Index.t Node_cache.t option
  ; stat : Stat.t (* Statistics *)
  ; hasher : Hash.Hasher.t
  ; keep_hash : bool (* XXX keep_hash is not of context but of node_storage *)
  ; bytes_per_cell : int
  ; with_count : bool
  ; offsets : Node_offsets.t
  }

let get_config_name ctxt = config_name ctxt.config

let cell_bytes_override = Envconf.int "PLEBEIA_CELL_BYTES_OVERRIDE"
let hash_bytes_override = Envconf.int "PLEBEIA_HASH_BYTES_OVERRIDE"
let hash_function_override = Envconf.one_of "PLEBEIA_HASH_FUNCTION_OVERRIDE" ["blake2b", `Blake2B; "blake3", `Blake3]
let keep_hash_override = Envconf.bool "PLEBEIA_KEEP_HASH_OVERRIDE"
let with_count_override = Envconf.bool "PLEBEIA_WITH_COUNT"

let config_override (config : config) keep_hash =
  let bytes_per_cell = Envconf.get_with_default config.bytes_per_cell cell_bytes_override in
  let bytes_per_hash = Envconf.get_with_default config.hasher.hashfunc.length hash_bytes_override in
  let hash_func = Envconf.get_with_default config.hasher.hashfunc.algorithm hash_function_override in
  let with_count = Envconf.get_with_default config.with_count with_count_override in
  make_config ~bytes_per_cell ~bytes_per_hash ~hash_func ~with_count,
  Envconf.get_with_default keep_hash keep_hash_override

let mode t = Storage.mode t.storage

let get_storage c = c.storage

let equal t1 t2 =
  t1 == t2
  || (t1.storage == t2.storage && t1.config = t2.config)

let make_hasher (config : config) = Hash.Hasher.make config.hasher

let make_offsets (config : config) =
  Node_offsets.make_offsets
    ~bytes_per_cell:config.bytes_per_cell
    ~bytes_per_hash:config.hasher.hashfunc.length
    ~with_count:config.with_count

let make_storage_config (config : config) =
  (* 01234567890123456789
     PLEBEIA<-- 0 --->CHB
  *)
  Storage.{
    head_string = Head_string.make ~hash_func:config.hasher.hashfunc.algorithm ~bytes_per_hash:config.hasher.hashfunc.length ~with_count:config.with_count;
    version = Version.version;
    bytes_per_cell = config.bytes_per_cell;
    max_index = Index.(Unsafe.(max_index - Unsafe.of_int hard_limit_max_tag));
  }

let memory_only ?(keep_hash=false) config =
  let config, keep_hash = config_override config keep_hash in
  let hasher = make_hasher config in
  let offsets = make_offsets config in
  { config
  ; storage= Storage.null
  ; node_cache= None
  ; stat= Stat.create ()
  ; hasher
  ; keep_hash
  ; bytes_per_cell = config.bytes_per_cell
  ; with_count = config.with_count
  ; offsets
  }

let is_memory_only t = Storage.is_null t.storage

let create
    ?node_cache
    ?resize_step_bytes
    ?(keep_hash=false)
    config
    ~key
    fn =
  let config, keep_hash = config_override config keep_hash in
  let hasher = make_hasher config in
  let storage_config = make_storage_config config in
  let+ storage = Storage.create ~config:storage_config ~key ?resize_step_bytes fn in
  let offsets = make_offsets config in
  { config
  ; storage
  ; node_cache
  ; stat = Stat.create ()
  ; hasher
  ; keep_hash
  ; bytes_per_cell = config.bytes_per_cell
  ; with_count = config.with_count
  ; offsets
  }

let open_existing_for_read
    ?node_cache
    ?(keep_hash=false)
    ?key
    config
    fn =
  let open Result_lwt.Syntax in
  let config, keep_hash = config_override config keep_hash in
  let storage_config' = make_storage_config config in
  let* (storage_config, storage) = Storage.open_existing_for_read ?key fn in
  if storage_config <> storage_config' then
    Lwt.return_error @@ Storage.Config_mismatch { actual= storage_config; expected= storage_config' }
  else
    let hasher = make_hasher config in
    let offsets = make_offsets config in
    Lwt.return_ok  { config
                   ; storage
                   ; node_cache
                   ; stat = Stat.create ()
                   ; hasher
                   ; keep_hash
                   ; bytes_per_cell = config.bytes_per_cell
                   ; with_count = config.with_count
                   ; offsets
                   }

let open_for_write
    ?node_cache
    ?resize_step_bytes
    ?(keep_hash=false)
    config
    ~key
    fn =
  let config, keep_hash = config_override config keep_hash in
  let storage_config = make_storage_config config in
  let+ storage = Storage.open_for_write ?resize_step_bytes ~config:storage_config ~key fn in
  let hasher = make_hasher config in
  let offsets = make_offsets config in
  { config
  ; storage
  ; node_cache
  ; stat = Stat.create ()
  ; hasher
  ; keep_hash
  ; bytes_per_cell = config.bytes_per_cell
  ; with_count = config.with_count
  ; offsets
  }

let close { storage ; _ } = Storage.close storage

let [@inline] add_to_cache ctxt h i =
  match ctxt.node_cache with
  | None -> ()
  | Some node_cache -> Node_cache.add node_cache h i

let [@inline] find_in_cache ctxt h =
  match ctxt.node_cache with
  | None -> None
  | Some node_cache ->
      Node_cache.find_opt node_cache h
