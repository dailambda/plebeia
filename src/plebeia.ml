(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** { 1 Plebeia }

    Implementation of space-efficient binary Patricia trees in OCaml.
    The implementation is geared for used in Tezos, though it is rather
    generic. A stop-and-copy GC is provided. This implementation aims
    to maximize correctness and cares second about efficiency. *)

(** { 2 Internal implementation }

    For testing, debugging, and advance uses.
*)
module Internal = struct

  (** { 3 Base } *)
  module Limit            = Limit
  module Error            = Error
  module Utils            = Utils
  module Option           = Option
  module Result           = Result
  module Monad            = Monad
  module Log              = Log
  module Envconf          = Envconf

  (** { 3 Hash } *)
  module Hashfunc         = Hashfunc
  module Hash             = Hash
  module Commit_hash      = Commit_hash

  (** { 3 Storage } *)
  module Lock             = Lock
  module Xcstruct         = Xcstruct
  module Mmap             = Mmap
  module Storage          = Storage

  (** { 3 Patricia storage } *)
  module Patricia_storage = Patricia_storage

  (** { 3 Cache } *)
  module Hashcons         = Hashcons
  module Node_cache       = Node_cache

  (** { 3 Core } *)
  module Value            = Value
  module Index            = Index
  module Segment_int_array = Segment_int_array
  module Segment          = Segment
  module Path             = Path
  module Context          = Context
  module Node_type        = Node_type
  module Node_hash        = Node_hash
  module Node_offsets     = Node_offsets
  module Node_storage     = Node_storage
  module Node             = Node
  module Node_tools       = Node_tools
  module Traverse         = Traverse
  module Cursor           = Cursor

  (** { 3 High level } *)
  module Deep             = Deep

  (** { 3 Version control } *)
  module Commit           = Commit
  module Commit_tree      = Commit_tree
  module Commit_db        = Commit_db
  module Vc               = Vc

  (** { 3 File system } *)
  module Fs_types         = Fs_types
  module Fs_nameenc_impl  = Fs_nameenc_impl
  module Fs_nameenc       = Fs_nameenc
  module Fs_impl          = Fs_impl
  module Fs               = Fs
  module Fs_tree          = Fs_tree

  (** { 3 Helper } *)
  module Stat             = Stat
  module Debug            = Debug

  (** { 3 Tools } *)
  module Copy                = Copy
  module Diff                = Diff
  module Merkle_proof        = Merkle_proof
  module Cursor_tools        = Cursor_tools
  module Snapshot            = Snapshot
  module Data_encoding_tools = Data_encoding_tools
  module Key                 = Key
  module Result_lwt          = Result_lwt
  module Cursor_stat         = Cursor_stat

  (** { 3 Test } *)
  module Gen = Gen

end

include Internal
(** { 2 Standard APIs, module interfaces are currently equivalent with Internal } *)
