(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type t =
  { index_part : int
  ; hash_prefix : int
  ; flags : int
  ; link : int
  ; count : int
  ; flags_combined : bool
  ; segment_area_length : int
  }

let make_offsets ~bytes_per_cell ~bytes_per_hash ~with_count =
  let count_size = if with_count then 4 else 0 in
  if not (bytes_per_hash + 4 (* index *) + count_size <= bytes_per_cell) then
    invalid_arg "bytes_per_cell is too small for the hash size";
  let count = if with_count then 0 else -1 in
  let hash_prefix = if with_count then 4 else 0 in
  let flags_combined =
    bytes_per_hash + 4 (* index *) + count_size = bytes_per_cell in
  let flags_combined' =
    Hash.Hasher.flags_combined ~bytes_per_hash ~bytes_per_cell ~with_count
  in
  assert (flags_combined = flags_combined');
  let segment_area_length = bytes_per_cell - 5 in
  { index_part = bytes_per_cell - 4
  ; hash_prefix
  ; flags = bytes_per_cell - 5
  ; link = bytes_per_cell - 8
  ; count
  ; flags_combined
  ; segment_area_length
  }
