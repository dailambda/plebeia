(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Node_type

exception ReadFailure of Error.t

val write_node : ?clear:bool -> Context.t -> node -> (node * Index.t * Hash.t * int option, Error.t) result
(** Write a node to the storage, and returns the updated version of the node
    with its index and hash.

    If [clear=true], then it forgets the details of the given node after
    the commit and returns a [Disk _] node points to the commit.

    Note that this function does not update the header.  [Storage.commit]
    must be called to make the written node persistent.
*)

val read_node : Context.t -> Index.t -> view
(** Read the node at the given index of the context,
    parse it and create a view node with it.

    Note: [read_node] does not load the hash.  Use [read_hash] for hashes.
*)

val read_hash : Context.t -> t -> [`Hashed of Hash.Long.t * t | `Not_Hashed of view ]
(** Returns the hash of the node.  It does not compute the hash: it returns [`Not_Hashed]
    when the hash of the node is not avaialble either on memory or on storage.
*)

exception HashOnly of Hash.Long.t

val view : Context.t -> node -> view
(** Obtain the view of the node.  If the view is not available in the memory,
    it is loaded from the storage.

    Exception: if the node is [Hash h], it raises [HashOnly h].

    Note: [view] does not load the hash.
*)

val read_node_rec : reset_index: bool -> Context.t -> node -> node
(** Recusively visit and load all the subnodes in memory.

    (if [reset_index], all the indices are reset to [Not_Indexed].)

    Note: [read_node_rec] does not load the hash.
*)

val change_context : src:Context.t -> dst:Context.t -> node -> node
(** Copy the node from [src] context to another context [dst].

    This function assumes [src] and [dst] use the same hashing.  If their
    hashings are incompatible, it raises [Invalid_argument _].

    Note: the node is recursively loaded and stays in memory.
*)

val leaf : Context.t -> (Value.t * Index.t option * Hash.t option) -> node
val internal : Context.t -> (node * node * Index.t option * Hash.t option) -> node
val bud : Context.t -> (node option * Index.t option * Hash.t option) -> node
val extender : Context.t -> (Segment.t * node * Index.t option * Hash.t option) -> node

(** count the leaves and buds, loading nodes on demand.

    [count ctxt n], if [n] is a [Bud], returns the number of the leaves
    and buds under [n].

    Returns [None] when the pagination count is not in the context.
*)
val count : Context.t -> node -> (int * node) option

val read_leaf_value : Context.t -> view -> Value.t

module Internal : sig
  val parse_cell : keep_hash: bool -> Context.t -> Index.t -> view
  (** Parse the cell of the storage at the given index.  Exposed for test. *)

  val read_node_rec_for_test : Context.t -> node -> node
  (** Recusively visit and load all the subnodes in memory.
      Only for test purposes.  Not tail recursive.
  *)

  val equal_for_test : Context.t -> node -> node -> (unit, (node * node)) Result.t
  (** Used for test. *)
end
