(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** { 1 Merkle Patricia tree storage } *)

(** Maximum tag, 256 *)
val hard_limit_max_tag : int

(** { 2 Types } *)

type config =
  { hasher : Hash.Hasher.config
  ; bytes_per_cell : int
  ; with_count : bool
  }

val default_config : config

val pp_config : Format.formatter -> config -> unit

(** Short string to identify configs *)
val config_name : config -> string

val make_config :
  bytes_per_cell:int ->
  with_count:bool ->
  hash_func:[ `Blake2B | `Blake3 ] -> bytes_per_hash:int -> config

(** Configuration which affects test hashes *)
type old_config =
  { bytes_per_cell : int
  ; hash_func : [`Blake2B | `Blake3 ]
  ; bytes_per_hash : int
  ; with_count : bool
  }

val old_config : config -> old_config

type t =
  { config : config
  ; storage : Storage.t
  ; node_cache : Index.t Node_cache.t option
  ; stat : Stat.t         (* Statistics *)
  ; hasher : Hash.Hasher.t
  ; keep_hash : bool      (* Keep node hashes in memory when loaded.
                             if false, the hashes are loaded only when required. *)
  ; bytes_per_cell : int
  ; with_count : bool     (* Pagination count is enabled or not *)
  ; offsets : Node_offsets.t
  }

val get_config_name : t -> string

val get_storage : t -> Storage.t

val memory_only :
  ?keep_hash:bool
  -> config
  -> t
(** Create a memory_only context.

    Commiting nodes of a memory only context fails.
*)

val is_memory_only : t -> bool

val create :
  ?node_cache: Index.t Node_cache.t
  -> ?resize_step_bytes: int
  -> ?keep_hash:bool
  -> config
  -> key:Storage.Writer_key.t
  -> string (* path *)
  -> t Lwt.t
(** Create a new context storage.
    Note that if the file already exists, [create] fails.

    The context is created in Writer mode.

    length: initial size of the file in bytes
*)

val open_existing_for_read :
  ?node_cache: Index.t Node_cache.t
  -> ?keep_hash:bool
  -> ?key:Storage.Writer_key.t
  -> config
  -> string (* path *)
  -> (t, Error.t) result Lwt.t
(** Open an existing context storage. *)

val open_for_write :
  ?node_cache: Index.t Node_cache.t
  -> ?resize_step_bytes: int
  -> ?keep_hash:bool
  -> config
  -> key:Storage.Writer_key.t
  -> string (* path *)
  -> t Lwt.t
(** Open an existing context storage. *)

val close : t -> unit Lwt.t
(** Closes the context.

    If program exits or crashes without closing a context, some data
    may be lost, even if they are written on the disk.
*)

val mode : t -> Storage.mode
(** Returns writing mode *)

(** [t]s point to the same storage with the same storage configurations *)
val equal : t -> t -> bool

val add_to_cache : t -> Hash.Long.t -> Index.t -> unit
val find_in_cache : t -> Hash.Long.t -> Index.t option
