(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type config =
  { algorithm : [`Blake2B | `Blake3]
  ; length : int (** length in bytes *)
  }

let string_of_config { algorithm; length } =
  let algorithm =
    match algorithm with
    | `Blake2B -> "Blake2B"
    | `Blake3 -> "Blake3"
  in
  algorithm ^ "-" ^ string_of_int length

module type S = sig
  type t

  val config : config

  val algorithm : [`Blake2B | `Blake3]

  (** length in bytes *)
  val length : int

  val zero : t

  val is_zero : t -> bool

  val to_raw_string : t -> string

  val of_raw_string : string -> t

  val to_hex_string : t -> string

  val of_hex_string : string -> t

  val show : t -> string

  val pp : Format.formatter -> t -> unit

  val encoding : t Data_encoding.t

  val gen : t Gen.t

  val hash_bytes : bytes -> t

  val hash_string : string -> t

  val hash_strings : string list -> t

  (** Overwrite the last 2 bits by the integer *)
  val overwrite_last_2bits : t -> int -> unit

  val to_32byte_string : t -> string
end

module Base(A : sig
    val length : int
    val show : (string -> string) option
  end) = struct

  type t = string

  let length = A.length

  let to_raw_string x = x

  let of_raw_string x =
    if String.length x <> length then invalid_arg "Hashfunc: invalid length for hash";
    x

  let to_raw_bytes x = String.to_bytes x

  let of_raw_bytes x = of_raw_string @@ String.of_bytes x

  let to_hex_string s =
    let `Hex h = Hex.of_string s in
    h

  let of_hex_string h = of_raw_string @@ Hex.to_string (`Hex h)

  let zero = String.make length '\000'

  let is_zero x = x = zero

  let show =
    match A.show with
    | None -> to_hex_string
    | Some f -> f

  let pp ppf x = Format.pp_print_string ppf @@ show x

  let encoding =
    Data_encoding.(conv to_raw_bytes of_raw_bytes @@ Fixed.bytes length)

  let gen : t Gen.t = Gen.(string (return length) char)

  let overwrite_last_2bits h bb =
    let b = Bytes.unsafe_of_string h in
    let pos = String.length h - 1 in
    Bytes.unsafe_set b pos
    @@ Char.(chr @@
             (code (Bytes.unsafe_get b pos) land 0xfc)
             lor (bb land 0x03))

  let to_32byte_string t =
    if length = 32 then t
    else begin
      assert (length < 32);
      t ^ String.make (32 - length) '\000'
    end
end

module Blake2B(A : sig
    val length : int
    val show : (string -> string) option
  end) = struct

  let () =
    if A.length < 1 || A.length > 64 then
      invalid_arg "Blake2B.Make: bytes must be between 1 and 64"

  include Base(A)

  open Hacl_star

  let algorithm = `Blake2B

  let config = { algorithm; length }

  let key = Bytes.create 0

  let hash_bytes inbuf =
    Bytes.unsafe_to_string @@
    (* HACL* doesn't yet provide a multiplexing interface for Blake2b so we
     * perform this check here and use the faster version if possible *)
    if AutoConfig2.(has_feature VEC256) then
      Hacl.Blake2b_256.hash ~key inbuf A.length
    else Hacl.Blake2b_32.hash ~key inbuf A.length

  let hash_string inbuf = hash_bytes @@ Bytes.unsafe_of_string inbuf

  let hash_strings ss = hash_string (String.concat "" ss)
end

module Blake3(A : sig
    val length : int
    val show : (string -> string) option
  end) = struct

  let () =
    if A.length < 1 then
      invalid_arg "Blake3.Make: bytes must be between 1 and 2^64-1"

  include Base(A)

  let algorithm = `Blake3

  let config = { algorithm; length }

  let hash_bytes inbuf =
    let outbuf = Blake3.hash A.length inbuf in
    Bytes.unsafe_to_string outbuf

  let hash_string inbuf = hash_bytes @@ Bytes.unsafe_of_string inbuf

  let hash_strings ss = hash_string (String.concat "" ss)
end

let blake2b length =
  (module Blake2B(struct let length = length let show = None end) : S)

let blake3 length =
  (module Blake3(struct let length = length let show = None end) : S)

let make config =
  match config.algorithm with
  | `Blake2B -> blake2b config.length
  | `Blake3 -> blake3 config.length
