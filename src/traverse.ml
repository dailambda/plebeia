open Node_type

module Fold : sig
  val f :
    ('acc -> t -> [`Continue of 'acc * view | `Return of 'acc ]) -> 'acc -> t -> 'acc

  type 'acc state

  val interleaved :
    ('acc -> t -> [`Continue of 'acc * view | `Return of 'acc ]) ->
    'acc -> t ->
    ('acc state -> [`Left of 'acc state | `Right of 'acc ]) * 'acc state

end = struct
  type 'acc state = 'acc * Node_type.t list

  let step enter (acc, ns) =
    match ns with
    | [] -> `Right acc
    | n::ns ->
        (* traversal of n *)
        match enter acc n with
        | `Return acc -> `Left (acc, ns)
        | `Continue (acc, v) ->
            (* continue as v, not as n itself *)
            match v with
            | Leaf _ -> `Left (acc, ns)
            | Bud (None, _, _) -> `Left (acc, ns)
            | Bud (Some n, _, _) -> `Left (acc, n :: ns)
            | Extender (_, n, _, _) -> `Left (acc, n :: ns)
            | Internal (nl, nr, _, _) -> `Left (acc, nl :: nr :: ns)

  let f enter acc n =
    let rec loop acc ns = match step enter (acc, ns) with
      | `Right acc -> acc
      | `Left (acc, ns) -> loop acc ns
    in
    loop acc [n]

  let interleaved enter acc n =
    let ns = [n] in
    let stepper state = step enter state in
    stepper, (acc, ns)
end

module Iter : sig
  type state

  val f : (t -> [`Continue of view | `Return of unit ]) -> t -> unit

  val interleaved :
    (t -> [`Continue of view | `Return of unit ]) ->
    t ->
    (state -> [`Left of state | `Right of unit ]) * state

end = struct
  type state = unit Fold.state

  let f g n =
    let g () t =
      match g t with
      | `Continue v -> `Continue ((), v)
      | `Return acc -> `Return acc
    in
    Fold.f g () n

  let interleaved g n =
    let g () t =
      match g t with
      | `Continue v -> `Continue ((), v)
      | `Return acc -> `Return acc
    in
    Fold.interleaved g () n
end

module Map : sig
  type state

  val f :
    enter:(t -> [`Continue of view | `Return of t ]) ->
    leave:(org:view -> view -> t) ->
    t -> t

  val interleaved :
    enter:(t -> [`Continue of view | `Return of t ]) ->
    leave:(org:view -> view -> t) ->
    t ->
    (state -> [`Left of state | `Right of t ]) * state

end = struct
  type job =
    | Do of t
    | BudSome of view
    | Extender of view * Segment.t
    | Internal of view

  type state = job list * t list

  let step ~enter ~leave (js, ns) =
    match js, ns with
    | [], [n] -> `Right n
    | [], _ -> assert false
    | BudSome v::js, n::ns ->
        let v' = _Bud (Some n, None, None) in
        let n = leave ~org:v v' in
        `Left (js, (n::ns))
    | Extender (v, seg)::js, n::ns ->
        let v' = _Extender (seg, n, None, None) in
        let n = leave ~org:v v' in
        `Left (js, (n::ns))
    | Internal v::js, nr::nl::ns ->
        let v' = _Internal (nl, nr, None, None) in
        let n = leave ~org:v v' in
        `Left (js, (n::ns))
    | (BudSome _ | Extender _ | Internal _)::_, _ -> assert false
    | Do n::js, ns ->
        (* traversal of n *)
        match enter n with
        | `Return n -> `Left (js, (n::ns))
        | `Continue v ->
            (* continue as v, not as n itself *)
            match v with
            | Leaf _ ->
                let n = leave ~org:v v in
                `Left (js, n::ns)
            | Bud (None, _, _) ->
                let n = leave ~org:v v in
                `Left (js, n::ns)
            | Bud (Some n, _, _) ->
                `Left ((Do n :: BudSome v :: js), ns)
            | Extender (seg, n, _, _) ->
                `Left ((Do n :: Extender (v, seg) :: js), ns)
            | Internal (nl, nr, _, _) ->
                `Left ((Do nl :: Do nr :: Internal v :: js), ns)

  let interleaved ~enter ~leave n =
    let js_ss = [Do n], [] in
    let stepper x = step ~enter ~leave x in
    stepper, js_ss

  let f ~enter ~leave n =
    let rec loop js_ss = match step ~enter ~leave js_ss with
      | `Right res -> res
      | `Left js_ss -> loop js_ss
    in
    loop ([Do n], [])
end
