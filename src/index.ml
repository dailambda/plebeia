(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type t = int

let compare = Int.compare

let max_index = Stdint.Uint32.(to_int max_int)

let zero = 0

let one = 1

let min i j = if compare i j = -1 then i else j
let max i j = if compare i j = -1 then j else i

let zero_then_none x = if x = zero then None else Some x

module Set = Set.Make(struct
  type nonrec t = t
  let compare = compare
end)

module Map = Map.Make(struct
  type nonrec t = t
  let compare = compare
end)

let pp = Format.pp_print_int

let to_int x = x
let of_int x = x

let to_uint32 = Stdint.Uint32.of_int
let of_uint32 = Stdint.Uint32.to_int

let to_int64 = Int64.of_int
let of_int64 = Int64.to_int

module Unsafe = struct
  let of_int = of_int
  let to_int = to_int
  let to_int32 = Int32.of_int
  let of_int32 = Int32.to_int

  let (+) = (+)
  let (-) = (-)
  let ( * ) = ( * )
  let (/) = (/)
  let pred = pred
  let succ = succ
end

let to_bytes_big_endian i = Stdint.Int64.to_bytes_big_endian @@ Int64.of_int i
let of_bytes_big_endian b s = Int64.to_int @@ Stdint.Int64.of_bytes_big_endian b s

(* We assume 64bit arch *)
let () = if Sys.word_size < 64 then failwith "64bit arch required"
