(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(*
   The writer writes commits to the storage and the commit tree.

   If some commits are written to the storage but not to the commit tree,
   the writer recover them and write them to the commit tree.

   A reader only reads commits from the commit tree.
*)

open Commit
open Utils

module B = Mmap.Buffer

(** How to write the entry to context *)

(* commits

|0        15|16      19|20      23|24        27|28      31|
|<-  zero ->|<- zero ->|<- info ->|<-  prev  ->|<- idx  ->|
                         not used
cell-1:
|0                                                      31|
|<-------------------- hash ----------------------------->|

cell-2:
|0                                                      31|
|<-------------------- parent hash ---------------------->|

Hash value can be different from Plebeia's root Merkle hash
*)

let read storage (i : Index.t) =
  let buf = Storage.get_cell storage i in
  let index  = B.get_index buf 28 in
  let prev = Index.zero_then_none @@ B.get_index buf 24 in
  let j = Index.Unsafe.pred i in
  let buf = Storage.get_cell storage j in
  let hash = Commit_hash.of_raw_string @@ B.copy buf 0 32 in
  let k = Index.Unsafe.pred j in
  let buf = Storage.get_cell storage k in
  let parent = Commit_hash.of_raw_string @@ B.copy buf 0 32 in
  let parent = if parent = Commit_hash.zero then None else Some parent in
  Commit.{ index; parent; hash }, prev

let read_the_latest storage =
  match Storage.get_last_root_index storage with
  | None -> None
  | Some i -> Some (fst @@ read storage i)

(* at : the latter cell *)
let write_at storage ~at:i ~prev commit =

  let j = Index.Unsafe.pred i in
  assert (Option.value ~default:Index.zero prev < j);
  let buf = Storage.get_cell storage j in
  let s = Commit_hash.to_raw_string commit.hash in
  assert (String.length s = 32);
  B.write_string s buf 0;

  let k = Index.Unsafe.pred j in
  assert (Option.value ~default:Index.zero prev < k);
  let buf = Storage.get_cell storage k in
  let s = Commit_hash.to_raw_string (Option.value ~default:Commit_hash.zero commit.parent) in
  assert (String.length s = 32);
  B.write_string s buf 0;

  let buf = Storage.get_cell storage i in
  B.set_index buf 28 commit.index;
  B.set_index buf 24 (Option.value ~default:Index.zero prev);
  B.set_index buf 20 Index.zero; (* It was the space for info *)
  (* reserved *)
  B.write_string (String.make 20 '\000') buf 0;

  (* debug *)
  let read_commit, read_prev = read storage i in
  if commit <> read_commit || prev <> read_prev then begin
    Log.fatal "copy check error!";
    Log.fatal "  written: %a@ %a"
      pp commit (Format.option Index.pp) prev;
    Log.fatal "  read:    %a@ %a"
      pp read_commit (Format.option Index.pp) read_prev;
    assert false
  end

let index_2 = Index.Unsafe.of_int 2

let write storage commit =
  let open Lwt.Syntax in
  let prev = Storage.get_last_root_index storage in
  match Storage.new_indices storage 3 with
  | Error _ as e -> Lwt.return e
  | Ok i ->
      let i = Index.Unsafe.(+) i index_2 in (* no overflow assured *)
      write_at storage ~at:i ~prev commit;
      Storage.set_last_root_index storage (Some i);
      let+ () = Storage.commit storage in
      Ok i
