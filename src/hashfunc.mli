(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type config =
  { algorithm : [`Blake2B | `Blake3]
  ; length : int (** length in bytes *)
  }

val string_of_config : config -> string

module type S = sig
  type t

  val config : config

  val algorithm : [`Blake2B | `Blake3]

  (** length in bytes *)
  val length : int

  val zero : t

  val is_zero : t -> bool

  val to_raw_string : t -> string

  val of_raw_string : string -> t

  val to_hex_string : t -> string

  val of_hex_string : string -> t

  val show : t -> string

  val pp : Format.formatter -> t -> unit

  val encoding : t Data_encoding.t

  val gen : t Gen.t

  val hash_bytes : bytes -> t

  val hash_string : string -> t

  val hash_strings : string list -> t

  (** Overwrite the last 2 bits by the integer *)
  val overwrite_last_2bits : t -> int -> unit

  val to_32byte_string : t -> string
end

val make : config -> (module S)
