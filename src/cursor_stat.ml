(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type t =
  { internals : int
  ; buds : int
  ; leaves : int
  ; extenders : int
  ; extenders_point_previous : int
  ; leaves_by_size : (int, int) Hashtbl.t
  ; extender_length : (int, int) Hashtbl.t
  ; shared : int
  }

let pp ppf
  { internals
  ; buds
  ; leaves
  ; extenders
  ; extenders_point_previous
  ; leaves_by_size
  ; extender_length
  ; shared
  } =
  let f fmt = Format.fprintf ppf fmt in
  f "@[<v>";
  f "internals: %d@ " internals;
  f "buds: %d@ " buds;
  f "leaves: %d@ " leaves;
  f "extenders: %d@ " extenders;
  f "shared: %d@ " shared;
  f "extenders_point_previous: %d@ " extenders_point_previous;
  let keys = List.sort compare @@ List.of_seq @@ Hashtbl.to_seq_keys leaves_by_size in
  List.iter (fun k -> f "leaves cells %d : %d@ " k (Hashtbl.find leaves_by_size k)) keys;
  let keys = List.sort compare @@ List.of_seq @@ Hashtbl.to_seq_keys extender_length in
  List.iter (fun k -> f "extenders %d : %d@ " k (Hashtbl.find extender_length k)) keys;
  f "@]"

let count_nodes c =
  let open Cursor in
  let indices = Index.Set.empty in
  let internals = ref 0 in
  let buds = ref 0 in
  let leaves = ref 0 in
  let leaves_by_size = Hashtbl.create 101 in
  let extenders = ref 0 in
  let extenders_point_previous = ref 0 in
  let shared = ref 0 in
  let extender_length = Hashtbl.create 101 in
  let count i = function
    | Node.Internal _ -> incr internals
    | Bud _ -> incr buds
    | Leaf (v, _, _) ->
        incr leaves;
        let ncells = (Value.length v + 31) / 32 in
        let x = Option.value ~default:0 @@ Hashtbl.find_opt leaves_by_size ncells in
        Hashtbl.replace leaves_by_size ncells (x+1)
    | Extender (seg, n, _, _) ->
        incr extenders;
        let i' = Node.index n in
        let len = Segment.length seg in
        let x = Option.value ~default:0 @@ Hashtbl.find_opt extender_length len in
        Hashtbl.replace extender_length len (x+1);
        match i, i' with
        | Some i, Some i' when Index.Unsafe.succ i' = i ->
            incr extenders_point_previous
        | _ -> ()
  in
  ignore @@ fold ~init:indices c
    (fun indices c ->
       let _, v = view c in
       let i = index c in
       match i with
       | None ->
           count None v;
           `Continue, indices
       | Some i when Index.Set.mem i indices ->
           incr shared;
           `Up, indices
       | Some i ->
           let indices = Index.Set.add i indices in
           count (Some i) v;
           `Continue, indices);
  { internals = !internals
  ; buds = !buds
  ; leaves = !leaves
  ; extenders = !extenders
  ; extenders_point_previous = !extenders_point_previous
  ; leaves_by_size
  ; extender_length
  ; shared = !shared
  }

(* should be as same as [Node_storage.count] but much slower *)
let shallow_count c =
  let open Cursor in
  let count c =
    fold ~init:0 c
      (fun acc c ->
         let _, v = view c in
         match v with
         | Leaf _ | Bud _ -> `Up, acc + 1
         | _ -> `Continue, acc)
  in
  match go_below_bud c with
  | Error _ -> count c
  | Ok None -> 0
  | Ok (Some c) -> count c
