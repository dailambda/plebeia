(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Fs_types

(** File system over Plebeia tree, with a "tree" interface.

    Here, a `tree` is a subtree of Plebeia tree.  This is different from
    the `cursor` of `Fs` which is a zipper pointer to a tree.

    Accesses to the tree items are by the absolute path names from the root
    of the tree.  Fs_tree optimizes contiguous file accesses in the same
    directories using zipper internally.
*)

(** File system via Plebeia *)

(** Type of file name *)
type name = Name.t

module Name = Name

(** Path name, a list of names *)
module Path =  Path

module FsError = FsError

(** Type for the underlying cursor *)
type raw_cursor = Cursor.t

(** Type of Plebeia tree of a file or a directory *)
type tree

(** Type for the contents of a file or a directory *)
type view = Node_type.view

(** Hash of a file or a directory *)
type hash = Hash.t

(** Errors.  The first parameter is the name of the failed API function *)
type error = FsError.t

(** [make raw_tree path] wraps [raw_tree] which points to [path]
    and returns a tree *)
val make : raw_cursor -> Fs_nameenc.t -> Path.t -> tree

(** [empty context] returns a tree pointing the empty file system *)
val empty : Context.t -> Fs_nameenc.t -> tree

(** Returns the underlying context of the given tree *)
val context : tree -> Context.t

(** Get the underlying raw cursor *)
val get_raw_cursor : tree -> raw_cursor

val index : tree -> Index.t option

val top_tree : tree -> tree

val compute_hash : tree -> tree * Hash.Long.t

val write_top_tree : tree -> (tree * (Index.t * hash), Error.t) result

val may_forget : tree -> tree

module Op : sig
  (** Monad for synchronous file system operations *)
  type 'a t = tree -> (tree * 'a, Error.t) result

  include Monad.S1 with type 'a t := 'a t

  val lift_result : ('a, Error.t) result -> 'a t

  (** For debugging. Check the invariant of the current tree. *)
  val check_tree_invariant : unit t

  (** Fail with the given error *)
  val fail : error -> 'a t

  (** Get the current underlying raw cursor *)
  val raw_cursor : raw_cursor t

  (** [copy src dst] sets the tree at [src] to [dst].
      [dst] must not be empty. *)
  val copy : Path.t -> Path.t -> unit t

  (** Regular file read access. *)
  val read : Path.t -> Value.t t

  (** Create or update a regular file.  Directories are created if necessary.
      The path must not be empty. *)
  val write : Path.t -> Value.t -> unit t

  (** Remove a regular file or a directory.  The path must not be empty.

      recursive=false : fails when the target is a directory
      recursive=true : removes the target recursively if it is a directory
      ignore_error=false : fails when the target does not exist
      ignore_error=true : does not fail even if the target does not exist

      Returns [true] if the target is really removed.
      Returns [false] if the target does not exist.
  *)
  val rm : ?recursive:bool -> ?ignore_error:bool -> Path.t -> bool t

  (** Recursive removal of a directory.  The path must not be empty.

      ignore_error=false : fails when the target does not exist
      ignore_error=true : does not fail even if the target does not exist

      Returns [true] if the target is really removed.
      Returns [false] if the target does not exist.
  *)
  val rmdir : ?ignore_error:bool -> Path.t -> bool t

  (** Compute the Merkle hash of the tree specified by the path *)
  val compute_hash : Path.t -> hash t

  (** Clear the memory cache of the tree under the current tree,
      if it is already persisted on the disk. *)
  val may_forget: Path.t -> unit t

  (** Get the subtree of the specified path.
      It also returns the view of the subtree. *)
  val get_tree : Path.t -> (tree * view) t

  (** Set the tree at the specified path *)
  val set_tree : Path.t -> tree -> unit t

  (** Get the cursor for `Fs` module *)
  val tree : tree t

  (** Monad runner *)
  val run : tree -> 'a t -> (tree * 'a, Error.t) result

  (** For debugging.
      [do_then f op] executes [f] against the current tree, then performs [op]. *)
  val do_then : (tree -> unit) -> 'a t -> 'a t
end

module Op_lwt : sig
  (** Monad for asynchronous file system operations *)
  type 'a t = tree -> (tree * 'a, Error.t) result Lwt.t

  include Monad.S1 with type 'a t := 'a t

  (** Convert Op monad to Op_lwt *)
  val lift : 'a Op.t -> 'a t
  val lift_op : 'a Op.t -> 'a t

  (** Monad lifters *)
  val lift_lwt : 'a Lwt.t -> 'a t
  val lift_result : ('a, Error.t) result -> 'a t
  val lift_result_lwt : ('a, Error.t) result Lwt.t -> 'a t

  (** Lifted versions of Op functions *)
  val fail : error -> 'a t
  val raw_cursor : raw_cursor t
  val copy : Path.t -> Path.t -> unit t
  val read : Path.t -> Value.t t
  val write : Path.t -> Value.t -> unit t
  val rm : ?recursive:bool -> ?ignore_error:bool -> Path.t -> bool t
  val rmdir : ?ignore_error:bool -> Path.t -> bool t
  val compute_hash : Path.t -> hash t
  val may_forget: Path.t -> unit t
  val get_tree : Path.t -> (tree * view) t
  val set_tree : Path.t -> tree -> unit t
  val tree : tree t
  val do_then : (tree -> unit) -> 'a t -> 'a t

  (** Folding

      - `Continue: if the item is a directory, its items are recursively folded
      - `Up: if the item is a directory, its items are skipped
      - `Exit: terminate the folding immediately and returns the accumulator
               as the final result of [fold]
  *)
  val fold
    : 'a
    -> Path.t
    -> ('a
        -> Path.t
        -> tree
        -> ([`Continue | `Exit | `Up] * 'a, Error.t) result Lwt.t)
    -> 'a t

  (** Folding with a depth specification *)
  val fold'
    : ?depth:[`Eq of int | `Ge of int | `Gt of int | `Le of int | `Lt of int ]
    -> 'a
    -> Path.t
    -> ('a -> Path.t -> tree -> ('a, Error.t) result Lwt.t)
    -> 'a t

  (** List the directory specified by the path *)
  val ls : Path.t -> (name * tree) list t

  val ls2 : offset:int -> length:int -> Path.t -> (name * tree) list t

  (** count the leaves and buds, loading nodes on demand.

      [count path], if [path] points to a [Bud], it returns the number of
      the leaves and buds under [path].
  *)
  val count : Path.t -> int t

  (** Monad runner *)
  val run : tree -> 'a t -> (tree * 'a, Error.t) result Lwt.t

end

(** Version control *)
module Vc : sig
  (** Type of version controller *)
  type t =
    { vc : Vc.t
    ; enc : Fs_nameenc.t
    }

  (** Create an empty commit store *)
  val create :
    ?node_cache: Index.t Node_cache.t
    -> ?lock: bool
    -> ?resize_step_bytes:int
    -> ?auto_flush_seconds: int
    -> Context.config
    -> Fs_nameenc.t
    -> string
    -> (t, Error.t) result Lwt.t

  (** Opens a commit store of the given name if it exists.
      Otherwise, it creates a new store. *)
  val open_existing_for_read :
    ?node_cache: Index.t Node_cache.t
    -> ?key:Storage.Writer_key.t
    -> Context.config
    -> Fs_nameenc.t
    -> string
    -> (t, Error.t) result Lwt.t

  val open_for_write :
    ?node_cache: Index.t Node_cache.t
    -> ?resize_step_bytes:int
    -> ?auto_flush_seconds: int
    -> Context.config
    -> Fs_nameenc.t
    -> string
    -> (t, Error.t) result Lwt.t

  (** Close the version control.
      Once closed, further uses of [t] are unspecified. *)
  val close : t -> (unit, Error.t) result Lwt.t

  (** Returns an empty tree *)
  val empty : t -> tree

  (** Returns a tree of a leaf with the value specified
      by the given bytes *)
  val of_value : t -> Value.t -> tree

  (** Checkout the commit of the given commit hash *)
  val checkout : t -> Commit_hash.t -> (Commit.t * tree) option Lwt.t

  (** Check the given commit hash is known *)
  val mem : t -> Commit_hash.t -> bool Lwt.t

  (** Compute the commit hash for the root of the current tree.
      The tree is moved to the root.

      Note that the commit hash is NOT the top Merkle hash of the tree.
      It is computed from the top Merkle hash and the parent commit hash
  *)
  val compute_commit_hash :
    t -> parent: Commit_hash.t option -> tree -> tree * Commit_hash.t
  (* XXX check movement to the root *)

  (** Commit the contents of the tree at the root.
      It then returns the updated tree, the hash,
      and the commit information.

      If [override] is [false] (by default), hash collision fails
      the function.  If it is [true], it overwrites the hash.

      The commit will be persisted to the disk eventually, but may be
      lost if the program crashes.  To make it surely persisted,
      [flush] must be called explicitly.
  *)
  val commit :
    ?allow_missing_parent: bool
    -> t
    -> parent: Commit_hash.t option
    -> hash_override: Commit_hash.t option
    -> (Hash.t * Commit.t) Op_lwt.t

  (** Synchronize the commits to the disk.  Commits after the last call of
      this [flush] may be lost when the program crashes.

      Too frequent call of this function may slow down the system.
  *)
  val flush : t -> unit Lwt.t

  (** Underlying commit database *)
  val commit_db : t -> Commit_db.t

  (** Underlying context *)
  val context : t -> Context.t
end

module Merkle_proof : sig
  type t = Merkle_proof.t
  type detail = Path.t * Segment.segment list * Node_type.node option

  val encoding : Vc.t -> t Data_encoding.t

  val pp : Format.formatter -> t -> unit

  (** [make basedir paths] builds a Merkle proof of the [paths]
      from [basedir].  It also returns the objects at [paths].
  *)
  val make : Path.t -> Path.t list -> (t * detail list) Op.t

  (** Compute the top hash of the given Merkle proof.  It also returns
      the objects at the paths attached with the proof.
  *)
  val check : Vc.t -> t -> (Hash.t * detail list, Error.t) result
end
