val read : Storage.t -> Index.t -> Commit.t * Index.t option
val read_the_latest : Storage.t -> Commit.t option
val write : Storage.t -> Commit.t -> (Index.t, Error.t) Result_lwt.t
