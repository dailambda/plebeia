val fold :
  init:'a ->
  Segment.Segs.t ->
  Cursor.t ->
  ('a ->
   Segment.Segs.t ->
   Cursor.t ->
   ([< `Continue | `Exit | `Up ] * 'a, Error.t) result) ->
  ('a * Cursor.t, Error.t) result

(** Note: If [c] points to a bud, then it returns itself,
          rather than returning its contents.

    Cursors in the list do not carry the whole scan of ls.
 *)
val ls :
  Cursor.t ->
  ((Segment.segment * Node_type.view * Cursor.t) list * Cursor.t,
   Error.t)
  result

(** Note: If [c] points to a bud, then it returns itself,
          rather than returning its contents.

    Cursors in the list do not carry the whole scan of ls.
 *)
val ls_rec :
  Cursor.t ->
  ((Segment.segment list * Node_type.node * Cursor.t) list *
   Cursor.t, Error.t)
  result

module GenTraverse : sig

  type ('acc, 'data) visitor

  type dir = [ `Bud | `Extender | `Side of Segment.side ]

  val prepare :
    'acc ->
    'data ->
    Cursor.t ->
    ('acc ->
     'data ->
     Cursor.t ->
     (Cursor.t * ('acc * ('data * dir) list option), Error.t) result) ->
    ('acc, 'data) visitor

  val step :
    ('acc, 'data) visitor ->
    ([ `Continue of ('acc, 'data) visitor
     | `Finished of Cursor.t * 'acc ], Error.t)
    result

  val fold :
    'acc ->
    'data ->
    Cursor.t ->
    ('acc ->
     'data ->
     Cursor.t ->
     (Cursor.t * ('acc * ('data * dir) list option),
      Error.t)
     result) ->
    (Cursor.t * 'acc, Error.t) result

  (** Note: If [c] points to a bud, then it returns itself,
            rather than returning its contents.

      Cursors in the list do not carry the whole scan of ls.
   *)
  val ls :
    Cursor.t ->
    (Cursor.t * (Segment.t * Node_type.t * Cursor.t) list,
     Error.t)
    result

  (** Note: If [c] points to a bud, then it returns itself,
            rather than returning its contents.

      Cursors in the list do not carry the whole scan of ls.
   *)
  val ls_rec :
    Cursor.t ->
    (Cursor.t *
     (Segment.t list * Node_type.t * Cursor.t) list, Error.t)
    result
end
