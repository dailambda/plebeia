(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Segment encoding of file names *)

open Fs_types

type t = (module NAMEENC)

(** Path encoding. 8bits/1char, the simplest encoding but inefficient *)
val bits8 : t

(** Path encoding. 6bits/1char.  Only chars match with [0-9a-z_-] are
   allowed.
*)
val bits6 : t

(** Compact path encoding specialized for Tezos context.

   Hex [0-9a-f]+, 2n chars, 2n >= 16
   :  4bits/char + 1 bit L flag, no terminator.
      Assuming all the file names at the same directory
      have the same length.

   Other Hex [0-9a-f]+
   :  5bits/char + 2 bit RL flag, 1 bit terminator

   Others:
    :  32bit hash + 2 bit RR flag

   It takes the path name for the name record
*)
val compressed : string -> t

(** Other variants of compressed conversions for tests *)
val compressed' : t
val compressed'' : t

val to_segment : t -> Name.t -> Segment.t
val of_segment : t -> Segment.t -> Name.t option
val to_segments : t -> Path.t -> Segment.t list
val of_segments : t -> Segment.t list -> Path.t option
