(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Statistics *)

type t = {
  mutable loaded_nodes : int;
  mutable written_leaves : int;
  mutable written_empty_buds : int;
  mutable written_buds : int;
  mutable written_internals : int;
  mutable written_extenders : int;
  mutable written_big_extenders : int;
  mutable written_links : int;
  mutable written_leaf_sizes : (int, int) Hashtbl.t;
  mutable committed_leaf_sizes : (int, int) Hashtbl.t;
}

val create : unit -> t

val pp : Format.formatter -> t -> unit

val incr_loaded_nodes : t -> unit
val incr_written_leaves : t -> unit
val incr_written_empty_buds : t -> unit
val incr_written_buds : t -> unit
val incr_written_internals : t -> unit
val incr_written_extenders : t -> unit
val incr_written_big_extenders : t -> unit
val incr_written_links : t -> unit
val incr_written_leaf_sizes : t -> int -> unit
val incr_committed_leaf_sizes : t -> int -> unit
