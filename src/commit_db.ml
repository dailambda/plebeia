(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(*
   The writer writes commits to the storage and the commit tree.

   If some commits are written to the storage but not to the commit tree,
   the writer recover them and write them to the commit tree.

   A reader only reads commits from the commit tree.
*)

open Utils

type entry = Commit.t =
  { parent : Commit_hash.t option
  ; index  : Index.t
  ; hash   : Commit_hash.t
  }

type t =
  { storage_context : Storage.t
  ; mutable commit_tree : Commit_tree.t
  ; hashf : (module Hashfunc.S)
  }

type Error.t += Conflict
let () = Error.register_printer @@ function
  | Conflict -> Some "Conflict found in Plebeia commit tree"
  | _ -> None

let commit_tree t = t.commit_tree

let compute_hash t = Commit.compute_hash t.hashf

let make_commit t = Commit.make t.hashf

let add_to_commit_tree t entry index =
  t.commit_tree <- Commit_tree.add t.commit_tree entry.hash {parent= entry.parent; index}

let mode t =
  let smode = Storage.mode t.storage_context in
  let ctmode = Commit_tree.mode t.commit_tree in
  if smode = ctmode then smode
  else assert false

(* Get new commits from the context storage and add them to the commit_tree.
   It does not commit the updated commit_tree.
*)
let read_additional_commits t =
  match mode t with
  | Reader -> Ok 0
  | Writer ->
      let rec aux cnt = function
        | None -> Ok cnt
        | Some i ->
            let entry, prev = Commit_storage.read t.storage_context i in
            (* We stop when we are sure that the entry is also in commit_tree *)
            match Commit_tree.find t.commit_tree entry.hash with
            | Some {parent=_; index= i'} ->
                if i <> i' then begin
                  (* Oops, conflict found *)
                  Log.error "Conflict commit: storage: %a roots: %a"
                    Index.pp i
                    Index.pp i';
                  Error Conflict
                end else
                  Ok cnt
            | None ->
                add_to_commit_tree t entry i;
                aux (cnt + 1) prev
      in
      match aux 0 (Storage.get_last_root_index t.storage_context) with
      | Ok 0 -> Ok 0
      | Ok n ->
          Log.debug "read %d new commits from %s" n
            (Storage.filename t.storage_context);
          Result.get_ok @@ Commit_tree.write t.commit_tree;
          Ok n
      | Error e -> Error e

(* hash collisions are overridden *)
let add t commit_entry =
  let open Result_lwt.Syntax in
  (* Write to storage *)
  let+ i = Commit_storage.write t.storage_context commit_entry in
  add_to_commit_tree t commit_entry i;
  Result.get_ok @@ Commit_tree.write t.commit_tree;
  Log.debug "Added commit %a" Commit.pp commit_entry

let mem t = Commit_tree.mem t.commit_tree

let find t h =
  match Commit_tree.find t.commit_tree h with
  | Some {parent=_; index= idx} ->
      let ctxt = t.storage_context in
      let entry = Commit_storage.read ctxt idx |> fst in
      Some entry
  | None -> None

let commit t =
  let open Lwt.Syntax in
  let* () = Storage.commit t.storage_context in
  Commit_tree.commit t.commit_tree

let flush t =
  (* Flush the storage_context first, so that the commit_tree can be
     recovered from the storage_context by read_additional_commits. *)
  (* Question: do we need this recovery? *)
  let open Lwt.Syntax in
  let* () = Storage.flush t.storage_context in
  Commit_tree.flush t.commit_tree

let enable_process_sync t wk =
  Commit_tree.enable_process_sync t.commit_tree wk

let may_forget t =
  match Commit_tree.may_forget t.commit_tree with
  | Some ct -> t.commit_tree <- ct; true
  | None -> false

type Error.t += Conflicting_modes

let () = Error.register_printer (function
    | Conflicting_modes ->
        Some "Conflicting modes of the context and commit tree"
    | _ -> None)

(* Reader may call this *)
let create ~hash_func context commit_tree =
  let open Lwt.Syntax in
  let storage_context = context.Context.storage in
  let smode = Storage.mode storage_context in
  let ctmode = Commit_tree.mode commit_tree in
  if smode <> ctmode then
    Lwt.return_error Conflicting_modes
  else
    let hashf = Hashfunc.make { algorithm= hash_func; length= 32 } in
    let t = { storage_context; commit_tree; hashf } in
    match read_additional_commits t with
    | Error _ as e -> Lwt.return e
    | Ok 0 -> Lwt.return_ok t
    | Ok _ ->
        let+ _ = flush t in
        Ok t

let update_reader t =
  let open Lwt.Syntax in
  match mode t with
  | Reader ->
      let* tmp = Commit_tree.update_reader t.commit_tree in
      t.commit_tree <- tmp;
      Storage.update_reader t.storage_context
  | _ -> Lwt.return_unit

let parent t entry =
  match entry.parent with
  | None -> Ok None (* entry is genesis *)
  | Some parent ->
      match Commit_tree.find t.commit_tree parent with
      | None -> Error `Not_found (* old entry may not exist *)
      | Some {parent=_grand_parent; index= i} ->
          let ctxt = t.storage_context in
          let (entry, _prev_index) = Commit_storage.read ctxt i in
          Ok (Some entry)

let fold f t acc =
  let open Lwt.Syntax in
  let* () = update_reader t in
  Commit_tree.fold (fun h {index=i; parent=_} acc ->
      let ent, _ = Commit_storage.read t.storage_context i in
      assert (ent.hash = h);
      let parent = parent t ent in
      f ent ~parent acc) t.commit_tree acc

let to_list t = fold (fun ent ~parent:_ acc -> Lwt.return (ent::acc)) t []

let read_the_latest { storage_context; _} = Commit_storage.read_the_latest storage_context

let children t =
  let open Lwt.Syntax in
  let* () = update_reader t in
  let+ f = Commit_tree.children t.commit_tree in
  fun h ->
    let hents = f h in
    List.map (fun (_, {Commit_tree.index; _}) ->
        fst @@ Commit_storage.read t.storage_context index)
      hents

let geneses t =
  let open Lwt.Syntax in
  let* () = update_reader t in
  let+ hents = Commit_tree.geneses t.commit_tree in
  List.map (fun (_, {Commit_tree.index; _}) ->
      fst @@ Commit_storage.read t.storage_context index)
    hents

let ordered_fold f t acc =
  let open Lwt.Syntax in
  let* () = update_reader t in
  let commit_tree = t.commit_tree in
  Commit_tree.ordered_fold (fun _ch ent ~children acc ->
      let ent = fst @@ Commit_storage.read t.storage_context ent.index in
      let children =
        List.map (fun (_,ent) ->
            fst @@ Commit_storage.read t.storage_context ent.Commit_tree.index) children
      in
      f ent ~children acc)
    commit_tree acc
