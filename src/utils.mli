(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Exn : sig
  val catch : ('a -> 'b) -> 'a -> ('b, [> `Exn of exn ]) result
  val protect : (unit -> 'a) -> (unit -> unit) -> 'a
end

module Format : sig
  include module type of struct include Format end

  type 'a t = formatter -> 'a -> unit
  val string : string t
  val list : (unit, formatter, unit) format -> 'a t -> 'a list t
  val option : 'a t -> 'a option t
end

module String : sig
  include module type of struct include String end

  val find_char : (char -> bool) -> string -> int -> int option
  val split_by_char : (char -> bool) -> string -> string list
  val for_all : (char -> bool) -> string -> bool

  module Set : Set.S with type elt = string
  module Map : Map.S with type key = string
end

module List : sig
  include module type of struct include List end

  val ( @ ) : 'a list -> 'a list -> 'a list
  (** Tail recursive *)

  val rev_concat : 'a list list -> 'a list
  (** Reversed versio of [concat].  Tail recursive *)

  val concat : 'a list list -> 'a list
  (** Tail recursive *)

  val map : ('a -> 'b) -> 'a list -> 'b list
  (** Tail recursive *)

  val take_while_map : ('a -> 'b option) -> 'a list -> 'b list * 'a list

  val drop : int -> 'a list -> 'a list
  (** [drop n xs] drops the first [n] elements from [xs].
      If [xs] is shorter than [n], [drop n xs] returns [[]].
  *)

  val split_at : int -> 'a list -> 'a list * 'a list
  (** [split_at 3 [1;2;3;4;5] = ([1;2;3], [4;5])]
      [split_at 3 [1;2] = ([1;2], [])]
  *)

  val take : int -> 'a list -> 'a list
  (** [take 3 [1;2;3;4;5] = [1;2;3]]
      [take 3 [1;2] = [1;2]]
  *)

  val uniq_sorted : ('a -> 'a -> bool) -> 'a list -> 'a list

  val concat_map : ('a -> 'b list) -> 'a list -> 'b list

  val is_prefix : 'a list -> 'a list -> 'a list option
  (** [is_prefix p xs] checks [p] is a prefix of [xs].
      If it is, it returns the [Some postfix], where [postfix]
      is the rest of [xs] without the prefix.

      Otherwise, it returns [None].
  *)

  val partition_map : ('a -> [< `Left of 'b | `Right of 'c ]) -> 'a t -> 'b t * 'c t
end

module Array : sig
  include module type of struct include Array end
  module Syntax : sig
    val (.!()) : 'a t -> int -> 'a
    val (.!()<-) : 'a t -> int -> 'a -> unit
  end
end

module Open : sig
  val to_file : file: string -> string -> unit
  (** Create a file with the given string *)

  val ( ^/ ) : string -> string -> string
  (** [Filename.concat] *)

  val with_time : (unit -> 'a) -> 'a * Mtime.span
  (** Time the function in seconds *)

  val with_time_lwt : (unit -> 'a Lwt.t) -> ('a * Mtime.span) Lwt.t
  (** with_time for lwt *)

  val failwithf : ('a, Format.formatter, unit, 'b) format4 -> 'a
  (** failwith with printf interface *)

  val (@) : 'a list -> 'a list -> 'a list
  (** Tail recursive list concat *)

  (** Return the words occupied by the object.  Only for debugging.
      This costs a lot *)
  val reachable_words : 'a -> int
  val reachable_mbs : 'a -> float

  val min : 'a -> 'a -> 'a
    [@@ocaml.deprecated "Use monomorphic min, or use Stdlib.min"]

  val max : 'a -> 'a -> 'a
    [@@ocaml.deprecated "Use monomorphic max, or use Stdlib.max"]

  module Int : sig
    include module type of struct include Int end
    val min : int -> int -> int
    val max : int -> int -> int
  end
end

module MtimeSpan : sig
  (** Returns in seconds *)
  val to_float_s : Mtime.Span.t -> float
end

include module type of Open
