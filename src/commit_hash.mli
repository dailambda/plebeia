(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Commit hash identifies commits.  It is

    Hash of the top Plebeia node hash + the parent commit hash

    The size of commit hash is fixed to 32bytes
*)
type t

(** Get the 32 bytes binary string representation of [t]. *)
val to_raw_string : t -> string

(** 32 bytes binary string representation to [t].
    Fails if the string is inappropriate. *)
val of_raw_string : string -> t

(** = 32 *)
val length : int

(** [zero] returns 32 bytes of zeros *)
val zero : t

val is_zero : t -> bool

(** Convert to 64 chars of hexdigits to [t] *)
val to_hex_string : t -> string

(** Reverse of [to_hex_string].  Fails if the string is inappropriate. *)
val of_hex_string : string -> t

(** Behaviour can be changed via [Hash.show_ref] *)
val show : t -> string

val pp : Format.formatter -> t -> unit

val encoding : t Data_encoding.t

val gen : t Gen.t
