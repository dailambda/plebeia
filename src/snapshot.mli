(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The type for snapshot.  Using [Seq.t] inside *)
type snapshot
type t = snapshot

val encoding : int (* hash bytes *) -> t Data_encoding.encoding

(** Lazy dump of a node and its subnodes *)
val seq :
  node:(Node_type.t -> [< `Hash of Hash.Long.t | `View of Node_type.view ])
  -> Node_type.t
  -> t

(** Dump the snapshot of Plebeia tree to [file_descr].

    To reduce the size of the snapshot, it performs perfect hash-consing.
*)
val save : Lwt_unix.file_descr -> Context.t -> Node_type.t -> unit Lwt.t

(** Generalized version of [save], specifiable how to handle [Disk] nodes.

    [node] function is to preprocess each node.  It is to, for example,
      * load Disk
      * load the hash for hashconsing
      * replace the node by Hash to stop traversal
*)
val save_gen :
  Lwt_unix.file_descr
  -> int (* hash bytes *)
  -> node:(Node_type.t -> [`Hash of Hash.Long.t | `View of Node_type.view])
  -> Node_type.t
  -> unit Lwt.t

(** Load the snapshot file of Plebeia tree from [reader].

    The entire tree is node-hash computed.

    The entire tree is loaded on memory.

    [load] returns a boolean with the loaded node, which indicates
    whether the node contains [Hash _] or not.
*)
val load : Hash.Hasher.t -> Data_encoding_tools.reader -> (Node_type.t * bool) Lwt.t

(** Load the snapshot file of Plebeia tree from [t]. *)
val load' : Hash.Hasher.t -> t -> Node_type.t * bool

(** Statistics of the nodes in a snapshot *)
module Stat : sig
  type t = {
    nodes : int;
    segments : int * int;
    values : int * int;
    cached : int;
    hashes : int * int
  }

  val get : snapshot -> t
  val pp : Format.formatter -> t -> unit
end

module Internal : sig
  module Elem : sig
    type t =
      | End
      | BudNone
      | BudSome
      | Internal
      | Extender of Segment.segment
      | Value of Value.t
      | Cached of int32
      | HashOnly of Hash.Long.t

    val encoding : int -> t Data_encoding.t
    (**
       End:
          <1B>
         +----+
         | 00 |
         +----+

       BudNone:
          <1B>
         +----+
         | 01 |
         +----+

       BudSome:
          <1B>
         +----+
         | 02 |
         +----+

       Internal:
          <1B>
         +----+
         | 03 |
         +----+

       Extender:
          <1B> < variable >
         +----+------------+
         | 04 |  segment   |
         +----+------------+

         * See [Segment.encoding] for the encoding of segment

       Value:
          <1B> < variable >
         +----+------------+
         | 05 |    value   |
         +----+------------+

         * See [Value.encoding] for the encoding of value

       Cached:

          <1B> <--- 4 bytes ---->
         +----+------------------+
         | 06 |index (big endian)|
         +----+------------------+


       HashOnly:

          <1B> < variable >
         +----+------------+
         | 07 |    hash    |
         +----+------------+

         * See [Hash.encoding] for the encoding of hash
    *)

    val pp : Format.formatter -> t -> unit
  end
end
