(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Version Control *)

(** Type of the version control handle *)
type t

(** Create an empty commit store.

    node_cache: Node_cache configuration
    lock: Aquire a lock when [true]
    byte_per_cell: The width of the Plebeia cell
    hash_func: Hashing algorithm
    bytes_per_hash: Hash width
    resize_step_bytes: How much additional disk space is allocated
                       each time when the system finds the file is full.
    auto_flush_seconds: How often automatically flush the additions to
                        the disk at [commit] in seconds.
*)
val create :
  ?node_cache: Index.t Node_cache.t
  -> ?lock: bool
  -> ?resize_step_bytes:int
  -> ?auto_flush_seconds: int
  -> Context.config
  -> string
  -> (t, Error.t) Result_lwt.t

(** Opens a commit store of the given name if it exists.
    Otherwise, it creates a new store.

    mode: Opening mode.

    See [create] for the other parameters.
*)
val open_existing_for_read :
  ?node_cache: Index.t Node_cache.t
  -> ?auto_flush_seconds: int
  -> ?key:Storage.Writer_key.t
  -> Context.config
  -> string
  -> (t, Error.t) Result_lwt.t

val open_for_write :
  ?node_cache: Index.t Node_cache.t
  -> ?resize_step_bytes:int
  -> ?auto_flush_seconds: int
  -> Context.config
  -> string
  -> (t, Error.t) Result_lwt.t

(** Close the version control.
    Once closed, further uses of [t] are unspecified. *)
val close : t -> (unit, Error.t) result Lwt.t

(** For the writer, returns the writer key.  For readers, it returns [None]. *)
val writer_key : t -> Storage.Writer_key.t option

(** For readers, enable the process sync.  See [Storage.enable_process_sync] *)
val enable_process_sync : t -> Storage.Writer_key.t -> (unit, Error.t) result Lwt.t

val empty : t -> Cursor.t

val commit_db : t -> Commit_db.t
val context : t -> Context.t

(** Checkout the commit of the given commit hash. *)
val checkout : t -> Commit_hash.t -> (Commit.t * Cursor.t) option Lwt.t

(** Check the given commit hash is known *)
val mem : t -> Commit_hash.t -> bool Lwt.t

(** Compute the commit hash for the root of the current tree.
    The cursor is moved to the root.

    Note that the commit hash is NOT the top Merkle hash of the cursor.
    It is computed from the top Merkle hash and the parent commit hash
*)
val compute_commit_hash :
  t
  -> parent: Commit_hash.t option
  -> Cursor.t
  -> Cursor.t (* moved to top *) * Commit_hash.t

(** Commit the contents of the cursor, then returns the updated cursor
    at the top, the hash, and the commit information.

    If [override] is [false] (by default), hash collision fails
    the function.  If it is [true], it overwrites the hash.

    The commit may be lost if the program crashes.  To make it surely
    persisted, call [flush] explicitly or set [auto_flush_seconds].
*)
val commit :
  ?allow_missing_parent: bool
  -> t
  -> parent: Commit_hash.t option
  -> hash_override: Commit_hash.t option
  -> Cursor.t
  -> (Cursor.t * Hash.t * Commit.t, Error.t) result Lwt.t

(** Flush the commits already done to the disk.  The commits before
    the last [flush] call are persisted even after a system crash.

    Too frequent call of this function may slow down the system.
*)
val flush : t -> unit Lwt.t
