(** Merkle proof

    Hashed only nodes are represented by [Hash h].
*)

module Tree : sig
  (** Merkle proof tree.  Hash only nodes are expressed by [Hash] *)
  type t = Node_type.t

  val pp : Format.formatter -> t -> unit

  (** [snapshot ctxt tree] builds the snapshot of proof tree [tree].
      [Disk] nodes in [tree] are loaded from the context [ctxt]
      while snapshooting. *)
  val snapshot : Context.t -> t -> Snapshot.t

  (** The encoding of the proof tree.  The context is used to load
      [Disk] nodes and hash calculation.
  *)
  val encoding : Context.t -> t Data_encoding.t

  (** [make ctxt n paths] returns the Merkle proof tree of [paths]
      of node [n] under context [ctxt] and the nodes found (or not found)
      at [paths].

      The proof tree may contain [Disk] nodes which require [ctxt]
      to be loaded.  To export the proof out of the context, use [snapshot]
      or [encoding] to load all the nodes.

      - [n] must point to a [Bud], otherwise the function fails with
        [Invalid_argument].
  *)
  val make :
    Context.t ->
    Node_type.t ->
    Path.t list ->
    t * (Path.t * Node_type.t option) list

  (** Returns the hashed proof tree and its top hash.

      The proof tree must be [Disk] free, otherwise the function fails.
  *)
  val compute_hash : Hash.Hasher.t -> t -> Hash.Long.t * t

  (** [check paths tree] finds [paths] in the proof tree [tree]. *)
  val check :
    t ->
    Path.t list ->
    (Path.t * Node_type.t option) list

end

(** Proof tree packed with the paths proven in it *)
type t =
  { tree : Tree.t (** Proof tree *)
  ; paths : Path.t list (** The paths proven *)
  }

(** Encoding of [t].  The [Disk] nodes are loaded from [ctxt] on demand. *)
val encoding : Context.t -> t Data_encoding.t

val pp : Format.formatter -> t -> unit

(** [make ctxt n paths] returns the packed Merkle proof of [paths]
    of node [n] under context [ctxt] and the nodes found (or not found)
      at [paths].

    The proof may contain [Disk] nodes which require [ctxt] to be loaded.
    Use [encoding] to load all the nodes

    - [n] must point to a [Bud], otherwise the function fails.
*)
val make :
  Context.t ->
  Node_type.t ->
  Path.t list ->
  t * (Path.t * Node_type.t option) list

(** [check hasher t] returns the top Merkle hash of the proof tree of [t]
    and returns the nodes found at the [t.paths].

    It may raise [Invalid_argument] when [t] contains [Disk] nodes.
*)
val check :
  Hash.Hasher.t ->
  t ->
  Hash.Long.t * (Path.t * Node_type.t option) list
