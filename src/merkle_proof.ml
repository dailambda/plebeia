open Utils

open Node_type

module Tree = struct
  type t = Node_type.t

  let pp = Node_type.pp

  let snapshot ctxt raw =
    Snapshot.seq ~node:(function
        | Disk _ as n -> `View (Node_storage.view ctxt n)
        | Hash h -> `Hash h
        | View v -> `View v) raw

  let encoding ctxt =
    let open Data_encoding in
    conv
      (snapshot ctxt)
      (fun shot -> fst @@ Snapshot.load' ctxt.hasher shot)
    @@ Snapshot.encoding ctxt.hasher.config.hashfunc.length

  (* Since Extender itself does not carry a hash,
     its subnode must be loaded *)
  let load_hash ctxt n =
    match Node_storage.read_hash ctxt n with
    | `Hashed (_, t) -> t
    | `Not_Hashed v ->
        Format.eprintf "? %a@." Node_type.pp n;
        Format.eprintf "?? %a@." Node_type.pp (View v);
        assert false

  let make ctxt node (paths : Path.t list) =
    (* node must be a Bud *)
    let keys =
      match Node_storage.view ctxt node with
      | Bud _ -> List.map Key.of_path paths
      | _ -> invalid_arg "node must be a bud"
    in
    let make_hash v =
      let _, h = Node.compute_hash ctxt (View v) in
      Hash h
    in
    let rec f n ks =
      let v = Node_storage.view ctxt n in
      let n = View v in
      match ks with
      | [] -> make_hash v, []
      | keys ->
          match v with
          | Bud (Some n0, _, _) ->
              let found, keys =
                List.partition_map (fun (k,korg as kk) ->
                    if Key.is_empty k then `Left (korg, Some n) else `Right kk) keys
              in
              let rej, keys =
                List.partition_map (fun (k, korg) -> match Key.cut k with
                    | None -> assert false
                    | Some (Bud, k) -> `Right (k, korg)
                    | _ -> `Left (korg, None)) keys
              in
              let p, res = f n0 keys in
              let res = found @ rej @ res in
              let p = if found  = [] then new_bud (Some p) else n in
              p, res
          | Bud (None, _, _) ->
              let found, rejs =
                List.partition_map (fun (k,korg) ->
                    if Key.is_empty k then `Left (korg, Some n)
                    else `Right (korg, None)) keys
              in
              let p = if found = [] && rejs = [] then make_hash v else n in
              let res = found @ rejs in
              p, res
          | Leaf _ ->
              let found, rejs =
                List.partition_map (fun (k,korg) ->
                    if Key.is_empty k then `Left (korg, Some n)
                    else `Right (korg, None)) keys
              in
              let p = if found = [] && rejs = [] then make_hash v else n in
              let res = found @ rejs in
              p, res
          | Internal (n1, n2, _, _) ->
              let found, keys =
                List.partition_map (fun (k,korg as kk) ->
                    if Key.is_empty k then `Left (korg, Some n) else `Right kk
                  ) keys
              in
              let rejs, keys =
                List.partition_map (fun ((k,korg) as kkorg) ->
                    match Key.cut k with
                    | None -> assert false
                    | Some (Bud, _) -> `Left (korg, None)
                    | _ -> `Right kkorg) keys
              in
              let ls, rs =
                List.partition_map (fun (k,korg) ->
                    match Key.cut k with
                    | None -> assert false
                    | Some (Bud, _) -> assert false
                    | Some (Left, k) -> `Left (k, korg)
                    | Some (Right, k) -> `Right (k, korg)) keys
              in
              let lp, lres = f n1 ls in
              let rp, rres = f n2 rs in
              let res = found @ rejs @ lres @ rres in
              let p = if found <> [] then n else new_internal lp rp in
              p, res
          | Extender (seg, n0, _, _) ->
              let found, keys =
                List.partition_map (fun (k,korg as kk) ->
                    if Key.is_empty k then `Left (korg, Some n) else `Right kk) keys
              in
              (* if key ends in the middle of seg *)
              let found', keys =
                List.partition_map (fun (k,korg as kk) ->
                    match Key.is_prefix k seg with
                    | Some seg' ->
                        `Left (korg,
                               Some (new_extender seg' @@ load_hash ctxt n0 ))
                    | None -> `Right kk) keys
              in
              let keys, rejs =
                List.partition_map (fun (k, korg) ->
                    match Key.has_prefix seg k with
                    | Some k -> `Left (k, korg)
                    | None -> `Right (korg, None)) keys
              in
              let p, res = f n0 keys in
              let found = found @ found' in
              let res = found @ rejs @ res in
              let p =
                if found <> [] then n
                else new_extender seg @@ load_hash ctxt p
              in
              p, res
    in
    let t, res = f node (List.map (fun k -> (k,k)) keys) in
    let res = List.map (fun (k,nopt) ->
        match Key.to_path k with
        | Some p -> (p, nopt)
        | None -> assert false) res
    in
    t, res

  let compute_hash hasher node =
    match Node_hash.compute_without_context hasher node with
    | Some (n, h) -> h, n
    | None -> invalid_arg "Merkle_proof.Tree.hash"

  let check node paths =
    (* Bug: node must be a Bud *)
    (match node with
    | Hash _ | View (Bud _) -> ()
    | _ -> invalid_arg "node must be a bud");
    let keys = List.map Key.of_path paths in
    let loader = function
      | View v -> v
      | Hash _ | Disk _ -> assert false (* XXX failure. The key is not proven *)
    in
    let rec f n ks =
      match ks with
      | [] -> []
      | keys ->
          let v = loader n in
          let n = View v in
          match v with
          | Bud (Some n0, _, _) ->
              let found, keys =
                List.partition_map (fun (k,korg as kk) ->
                    if Key.is_empty k then `Left (korg, Some n) else `Right kk) keys
              in
              let rej, keys =
                List.partition_map (fun (k, korg) -> match Key.cut k with
                    | None -> assert false
                    | Some (Bud, k) -> `Right (k, korg)
                    | _ -> `Left (korg, None)) keys
              in
              let res = f n0 keys in
              let res = found @ rej @ res in
              res
          | Bud (None, _, _) ->
              let found, rejs =
                List.partition_map (fun (k,korg) ->
                    if Key.is_empty k then `Left (korg, Some n)
                    else `Right (korg, None)) keys
              in
              let res = found @ rejs in
              res
          | Leaf _ ->
              let found, rejs =
                List.partition_map (fun (k,korg) ->
                    if Key.is_empty k then `Left (korg, Some n)
                    else `Right (korg, None)) keys
              in
              let res = found @ rejs in
              res
          | Internal (n1, n2, _, _) ->
              let found, keys =
                List.partition_map (fun (k,korg as kk) ->
                    if Key.is_empty k then `Left (korg, Some n)
                    else `Right kk
                  ) keys
              in
              let rejs, keys =
                List.partition_map (fun ((k,korg) as kkorg) ->
                    match Key.cut k with
                    | None -> assert false
                    | Some (Bud, _) -> `Left (korg, None)
                    | _ -> `Right kkorg) keys
              in
              let ls, rs =
                List.partition_map (fun (k,korg) ->
                    match Key.cut k with
                    | None -> assert false
                    | Some (Bud, _) -> assert false
                    | Some (Left, k) -> `Left (k, korg)
                    | Some (Right, k) -> `Right (k, korg)) keys
              in
              let lres = f n1 ls in
              let rres = f n2 rs in
              let res = found @ rejs @ lres @ rres in
              res
          | Extender (seg, n0, _, _) ->
              let found, keys =
                List.partition_map (fun (k,korg as kk) ->
                    if Key.is_empty k then `Left (korg, Some n)
                    else `Right kk) keys
              in
              (* if key ends in the middle of seg *)
              let found', keys =
                List.partition_map (fun (k,korg as kk) ->
                    match Key.is_prefix k seg with
                    | Some seg' ->
                        let n' =
                          match new_extender seg' n0 with
                          | View _ -> n0
                          | Disk _ -> assert false
                          | _ -> assert false
                        in
                        `Left (korg, Some n')
                    | None -> `Right kk) keys
              in
              let keys, rejs =
                List.partition_map (fun (k, korg) ->
                    match Key.has_prefix seg k with
                    | Some k -> `Left (k, korg)
                    | None -> `Right (korg, None)) keys
              in
              let res = f n0 keys in
              let res = found @ found' @ rejs @ res in
              res
    in
    let knos = f node (List.map (fun k -> (k,k)) keys) in
    List.map (fun (k, no) ->
        match Key.to_path k with
        | Some p -> p, no
        | None -> assert false) knos
end

type t =
  { tree : Tree.t
  ; paths : Path.t list
  }

let encoding ctxt =
  let open Data_encoding in
  conv
    (fun t -> t.tree, t.paths)
    (fun (tree, paths) -> { tree; paths } )
  @@ obj2
    (req "tree" (Tree.encoding ctxt))
    (req "paths" (Data_encoding.list Path.encoding))

let pp ppf {tree=n; paths} =
  let open Format in
  fprintf ppf "@[tree= @[%a@];@ paths= @[%a@];@]"
    Node_type.pp n
    (list ";@ " Path.pp) paths

let make ctxt node paths =
  let tree, res = Tree.make ctxt node paths in
  { tree; paths = List.map fst res }, res

let check hash { tree; paths } =
  match Node_hash.compute_without_context hash tree with
  | Some (n, h) -> h, Tree.check n paths
  | None -> invalid_arg "Merkle_proof.check"
