(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Segment based diffs *)

open Segment
open Node_type

type t =
  | Add of node * Segs.t
    (* New node:
       [Add (new_node, segs_to_the_new_node)]
    *)

  | Del of Segs.t
    (* Node removal:
       [Del segs_to_the_removed_node]
    *)

  | CleanBud of Segs.t
    (* Node is emptied:
       [CleanBud segs_to_the_cleaned_bud]
    *)

  | ModLeaf of node * Value.t * Segs.t
    (* Leaf modification:
       [ModLeaf (new_leaf, new_value, segs_to_the_leaves)]
    *)

val pp : Format.formatter -> t -> unit
(** Pretty printing *)

val reset_for_another_context : Context.t -> t -> t
(** Load all the node in the diff and forget Index information
    so tht the diff can be directly applicable to a tree of the another context.
*)

val reset_for_another_context' : src:Context.t -> dst:Context.t -> t -> t

val diff : Context.t -> node -> node -> t list
(** [diff src dst] gets the segment based diff between 2 nodes *)

val apply : Cursor.t -> t -> (Cursor.t, Error.t) result
(** [Add (n,segs)] just insert [n] to the cursor.  If [Cursor.t] uses a different context
    from the one of diffs, indices in the [n] must be reset using [reset_for_another_context].
*)

module Internal : sig
  val apply_debug : Cursor.t -> t -> (Cursor.t, Error.t) result
  (** Same as [apply] but dump Graphviz dot files if fails *)

  val diff_with_check : Context.t -> node -> node -> t list
  (** Check the diff correctness *)
end
