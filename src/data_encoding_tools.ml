(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Lwt.Syntax
open Data_encoding

let pp_with_encoding encoding ppf v =
  Json.pp ppf (Data_encoding.Json.construct encoding v)

type reader = { reader : 'a . 'a Data_encoding.t -> (int * 'a) Lwt.t }

let make_reader fd =
  let bytes = Bytes.create 4096 in
  let stream = ref None in
  let rec f read_bytes = function
    | Data_encoding.Binary.Success { result; stream= str; _ } ->
        stream := Some str;
        Lwt.return (read_bytes, result)
    | Await fill ->
        let* read = Lwt_unix.read fd bytes 0 4096 in
        if read = 0 then assert false; (* unexpected eof *)
        f (read_bytes + read) @@ fill (Bytes.sub bytes 0 read)
    | Error e ->
        let* () = Lwt_fmt.eprintf "%a@." Data_encoding.Binary.pp_read_error e in
        assert false
  in
  { reader =
      fun encoding ->
        f 0 @@ Data_encoding.Binary.read_stream ?init:!stream encoding
  }
