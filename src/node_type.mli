(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Arthur Breitman                                        *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** { 1 Merkle Patricia tree } *)

(** { 2 Types } *)

type node =
  | Disk of Index.t
  (* Represents a node stored on the disk at a given index, the node hasn't
     been loaded yet. Although it's considered hashed, reading the hash
     requires a disk access and is expensive.
  *)

  | View of view
  (* A view node is the in-memory structure manipulated by programs in order
     to compute edits to the context. New view nodes can be committed to disk
     once the computations are done. *)

  | Hash of Hash.Long.t
  (* Hash only node.  The contents of the node is unkown but the hash.
     Used for Merkle proofs.
  *)


(** view constructors are private.  Use _Internal, _Bud, _Leaf, and _Extender
    functions with runtime invariant checks.
*)
and view = private
  | Internal of node (* L *) * node (* R *)
                * Index.t option
                * Hash.t option
  (* An internal node, left and right children and an internal path segment
     to represent part of the path followed by the key in the tree.
  *)

  | Bud of node option
           * Index.t option
           * Hash.t option
  (* Buds represent the end of a segment and the beginning of a new tree. They
     are used whenever there is a natural hierarchical separation in the key
     or, in general, when one wants to be able to grab sub-trees.
  *)

  | Leaf of Value.t
            * Index.t option
            * Hash.t option
  (* Leaf of a tree, the end of a path, contains or points to a value. *)

  | Extender of Segment.t
                * node
                * Index.t option
                * Hash.t option
  (* Extender node, contains a path to the next node. Represents implicitely
     a collection of internal nodes where one child is Null. *)

type t = node

(** { 2 Constructors with invariant checks } *)

val _Internal : node * node
               * Index.t option
               * Hash.t option
               -> view

val _Bud : node option
        * Index.t option
        * Hash.t option
        -> view

val _Leaf : Value.t
        * Index.t option
        * Hash.t option
        -> view

val _Extender : Segment.t
               * node
               * Index.t option
               * Hash.t option
               -> view

(** { 2 Accessors } *)

val indexed : node -> bool
val index   : node -> Index.t option
val hashed  : node -> bool

val index_of_view       : view -> Index.t option
val hash_prefix_of_view : view -> Hash.t option

(** { 2 Tools to create nodes without index nor hash } *)

val new_internal : node -> node -> node
val new_bud      : node option -> node
val new_leaf     : Value.t -> node
val new_extender : Segment.t -> node -> node

(** { 2 Loading of nodes } *)

val may_forget : node -> node option
(** If the node is indexed, forget the details *)

(** { 2 Debug } *)

val pp : Format.formatter -> node -> unit
(** Pretty printer *)

(** { 2 Mapper } *)

module Mapper : sig
  (** Mapper over a node *)

  type 'a mkview =
    { bud : (node * 'a) option -> Index.t option -> Hash.t option -> node * 'a
    ; extender : Segment.t -> node * 'a -> Index.t option -> Hash.t option -> node * 'a
    ; internal : node * 'a -> node * 'a -> Index.t option -> Hash.t option -> node * 'a
    ; leaf : Value.t -> Index.t option -> Hash.t option -> node * 'a
    }

  val default_mkview : unit mkview

  val map :
    node:(t -> [< `Return of t * 'a | `Continue of view ]) ->
    view:'a mkview ->
    t -> t * 'a
  (** Mapping over a node with folding

      When visiting a node:
        [node] is executed with the current node.  The function can stop
        the further traversal by returning [`Return _] or continue the traversal
        by returning [`Continue _].

      When leaving a node after traversing its sub nodes:
        When [node] returns [`Continue _] and its subnodes are traversed, then
        [view] is executed.  It is given a new [view] consists of the subnodes
        returned by the traversal and a list of result ['a].
  *)

  type 'a state (** Interleaved mapper state *)

  val interleaved :
    node:(t -> [< `Return of t * 'a | `Continue of view ]) ->
    view:'a mkview ->
    t ->
    ('a state -> [> `Left of 'a state | `Right of t * 'a]) * 'a state
  (** Interleaved version of [map].  Good to use with [Lwt] to achieve concurrency *)
end

module Fold : sig
  (** Fold over a node *)

  (* Called when the folding goes up to a parent *)
  type 'a leave =
    { bud : 'a option -> Index.t option -> Hash.t option -> 'a
    ; extender : Segment.t -> 'a -> Index.t option -> Hash.t option -> 'a
    ; internal : 'a -> 'a -> Index.t option -> Hash.t option -> 'a
    ; leaf : Value.t -> Index.t option -> Hash.t option -> 'a
    }

  val default_leave_rebuild : node leave

  val fold :
    enter:(t -> [< `Return of 'a | `Continue of view ]) ->
    leave:'a leave ->
    t -> 'a
  (** Folding over a node with folding

      When entering a node:
        [node] is executed with the current node.  The function can stop
        the further traversal by returning [`Return _] or continue the traversal
        by returning [`Continue _].

      When leaving a node after traversing its sub nodes:
        When [enter] returns [`Continue _] and its subnodes are traversed, then
        [leave] is executed.
  *)

  type 'a state (** Interleaved folder state *)

  val interleaved :
    enter:(t -> [< `Return of 'a | `Continue of view ]) ->
    leave:'a leave ->
    t ->
    ('a state -> [> `Left of 'a state | `Right of 'a]) * 'a state
  (** Interleaved version of [fold].  Good to use with [Lwt] to achieve concurrency *)
end

(** Returns the hash of the view already loaded.  It returns [None] if the view does
    not have the hash.

    Note that [hash_of_view (Extender (_, n, _, None)) = None] even when [n]
    has its hash loaded.
*)
val hash_of_view : view -> Hash.Long.t option

val hash_of_node : node -> view option * Hash.Long.t option

(** Random generators *)

val gen_leaf : t Gen.t
val gen_bud_none : t Gen.t

(** [gen_bud depth] is a generator of Bud node with about [depth] sides *)
val gen_bud : int -> t Gen.t

(** [gen_bud depth] is a generator of Internal node with about [depth] sides *)
val gen_internal : int -> t Gen.t

(** [gen_bud depth] is a generator of Extender node with about [depth] sides *)
val gen_extender : int -> t Gen.t
