(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Lwt.Syntax
open Data_encoding_tools
open Node_type

module Elem = struct
  (* Visit Plebeia tree in the fixed order
     to create a stream of node encodings *)
  type t =
    | End (* End of snapshot *)
    | BudNone
    | BudSome
    | Internal
    | Extender of Segment.t
    | Value of Value.t
    | Cached of int32 (* Same as i-th non-cached node, starting from 1 *)
    | HashOnly of Hash.Long.t (* Contents unknown.  Loaded as [Hash _].  Used for Merkle proof. *)

  let encoding hash_size =
    let open Data_encoding in
    union [ case ~title: "End" (Tag 0) null
              (function End -> Some () | _ -> None)
              (fun () -> End)
          ; case ~title: "BudNone" (Tag 1) (constant "budnone")
              (function BudNone -> Some () | _ -> None)
              (fun () -> BudNone)
          ; case ~title: "BudSome" (Tag 2) (constant "budsome")
              (function BudSome -> Some () | _ -> None)
              (fun () -> BudSome)
          ; case ~title:"Internal" (Tag 3) (constant "internal")
              (function Internal -> Some () | _ -> None)
              (fun () -> Internal)
          ; case ~title:"Extender" (Tag 4) (obj1 (req "extender" Segment.encoding))
              (function Extender seg -> Some seg | _ -> None)
              (fun seg -> Extender seg)
          ; case ~title:"Value" (Tag 5) (obj1 (req "value" Value.encoding))
              (function Value v -> Some v | _ -> None)
              (fun v -> Value v)
          ; case ~title:"Cached" (Tag 6) (obj1 (req "cached" int32))
              (function Cached i -> Some i | _ -> None)
              (fun i -> Cached i)
          ; case ~title:"HashOnly" (Tag 7) (obj1 (req "hash" (Hash.Long.encoding hash_size)))
              (function HashOnly nh -> Some nh | _ -> None)
              (fun nh -> HashOnly nh)
          ]

  let pp ppf =
    let open Format in
    function
    | End -> pp_print_string ppf "End"
    | BudNone -> pp_print_string ppf "BudNone"
    | BudSome -> pp_print_string ppf "BudSome"
    | Internal ->  pp_print_string ppf "Internal"
    | Extender seg -> fprintf ppf "@[Extender %a@]" Segment.pp seg
    | Value v -> fprintf ppf "@[Value %s@]" (Value.to_hex_string v)
    | Cached i -> fprintf ppf "@[Cached %ld@]" i
    | HashOnly h -> fprintf ppf "@[HashOnly %a@]" Hash.Long.pp h
end

open Elem

type snapshot_lwt = Elem.t Lwt_stream.t
type snapshot = Elem.t Seq.t
type t = snapshot

let encoding hash_size =
  let open Data_encoding in
  conv List.of_seq List.to_seq
    (list (Elem.encoding hash_size))

module Stat = struct
  type t = {
    nodes : int;
    segments : int * int;
    values : int * int;
    cached : int;
    hashes : int * int
  }

  let pp ppf {nodes; segments; values; cached; hashes} =
    Format.fprintf ppf
      "@[nodes= %d ; segments= %d , %d ; values: %d , %d ; cached : %d ; hashes : %d , %d@]"
      nodes
      (fst segments)
      (snd segments)
      (fst values)
      (snd values)
      cached
      (fst hashes)
      (snd hashes)

  let get t =
    let nodes = ref 0 in
    let segments = ref 0 in
    let segment_bytes = ref 0 in
    let values = ref 0 in
    let value_bytes = ref 0 in
    let cached = ref 0 in
    let hashes = ref 0 in
    let hash_bytes = ref 0 in
    Seq.iter (fun n ->
        incr nodes;
        match n with
        | End | BudNone | BudSome | Internal -> ()
        | Extender seg ->
            incr segments;
            segment_bytes :=
              !segment_bytes + Bytes.length (Result.get_ok @@ Data_encoding.Binary.to_bytes Segment.encoding seg)
        | Value v ->
            incr values;
            value_bytes := !value_bytes + Value.length v
        | Cached _ ->
             incr cached
        | HashOnly h ->
            incr hashes;
            hash_bytes := !hash_bytes + String.(length @@ concat "" @@ Hash.Long.to_strings h)
      ) t;
    { nodes = !nodes;
      segments = !segments, !segment_bytes;
      values = !values, !value_bytes;
      cached = !cached;
      hashes = !hashes, !hash_bytes }
end

(* Tree traversal commands for loading *)
type command =
  | End
  | Read
  | BudSome of int32
  | Extender of int32 * Segment.t
  | Internal of int32

(* Beware!  Loaded all in memory! *)
let load' h (seq : Elem.t Seq.t) =
  let has_hash = ref false in
  let tbl = Hashtbl.create 1000 in
  let incr cntr = Int32.add cntr 1l in
  let register cntr n =
    (* If n's child is [Disk] created by [HashOnly], we get into trouble *)
    let f n = match n with
      | Hash nh -> `Hashed (nh, n)
      | View (Leaf (_, _, Some hp))
      | View (Bud (_, _, Some hp))
      | View (Internal (_, _, _, Some hp)) -> `Hashed ((hp,""), n)
      | View (Extender (seg, _, _, Some hp)) -> `Hashed (Node_hash.of_extender seg hp, n)
      | View v -> `Not_Hashed v
      | Disk _ -> assert false
    in
    let n, _nh = Node_hash.compute h f n in
    Hashtbl.replace tbl cntr n;
    n
  in
  let register_hashonly cntr nh =
    let n = Hash nh in
    has_hash := true;
    Hashtbl.replace tbl cntr n;
    n
  in
  let rec loop seq cntr jobs ns = match jobs, ns with
    | [], _ -> assert false
    | [(End : command)], [n] -> n
    | End::_, _ -> assert false
    | BudSome cntr' :: jobs, n::ns ->
        let n = register cntr' @@ new_bud @@ Some n in
        loop seq cntr jobs (n::ns)
    | Extender (cntr', seg) :: jobs, n::ns ->
        let n = register cntr' @@ new_extender seg n in
        loop seq cntr jobs (n::ns)
    | Internal cntr' :: jobs, n2::n1::ns ->
        let n = register cntr' @@ new_internal n1 n2 in
        loop seq cntr jobs (n::ns)
    | (BudSome _ | Extender _) :: _, [] -> assert false
    | Internal _ :: _, ([] | [_]) -> assert false
    | Read :: jobs, _ ->
        match seq () with
        | Seq.Nil -> assert false
        | Cons (t, seq) ->
            match (t : Elem.t) with
            | End -> loop seq cntr (End :: jobs) ns
            | Cached i ->
                begin match Hashtbl.find tbl i with
                  | exception Not_found ->
                      Format.eprintf "not found cached %ld@." i;
                      assert false
                  | n -> loop seq cntr jobs (n :: ns)
                end
            | Value v ->
                let n = register cntr @@ new_leaf v in
                loop seq (incr cntr) jobs (n :: ns)
            | HashOnly nh ->
                let n = register_hashonly cntr nh in
                loop seq (incr cntr) jobs (n :: ns)
            | BudNone ->
                let n = register cntr @@ new_bud None in
                loop seq (incr cntr) jobs (n :: ns)
            | BudSome ->
                loop seq (incr cntr) (Read :: BudSome cntr :: jobs) ns
            | Extender seg ->
                loop seq (incr cntr) (Read :: Extender (cntr, seg) :: jobs) ns
            | Internal ->
                loop seq (incr cntr) (Read :: Read :: Internal cntr :: jobs) ns
  in
  let n = loop seq 1l [Read; End] [] in
  n, !has_hash

let load'_lwt h (seq : snapshot_lwt) =
  let has_hash = ref false in
  let tbl = Hashtbl.create 1000 in
  let incr cntr = Int32.add cntr 1l in
  let register cntr n =
    (* If n's child is [Disk] created by [HashOnly], we get into trouble *)
    let f n = match n with
      | Hash nh -> `Hashed (nh, n)
      | View (Leaf (_, _, Some hp))
      | View (Bud (_, _, Some hp))
      | View (Internal (_, _, _, Some hp)) -> `Hashed ((hp,""), n)
      | View (Extender (seg, _, _, Some hp)) -> `Hashed (Node_hash.of_extender seg hp, n)
      | View v -> `Not_Hashed v
      | Disk _ -> assert false
    in
    let n, _nh = Node_hash.compute h f n in
    Hashtbl.replace tbl cntr n;
    n
  in
  let register_hashonly cntr nh =
    let n = Hash nh in
    has_hash := true;
    Hashtbl.replace tbl cntr n;
    n
  in
  let rec loop seq cntr jobs ns = match jobs, ns with
    | [], _ -> assert false
    | [(End : command)], [n] -> Lwt.return n
    | End::_, _ -> assert false
    | BudSome cntr' :: jobs, n::ns ->
        let n = register cntr' @@ new_bud @@ Some n in
        loop seq cntr jobs (n::ns)
    | Extender (cntr', seg) :: jobs, n::ns ->
        let n = register cntr' @@ new_extender seg n in
        loop seq cntr jobs (n::ns)
    | Internal cntr' :: jobs, n2::n1::ns ->
        let n = register cntr' @@ new_internal n1 n2 in
        loop seq cntr jobs (n::ns)
    | (BudSome _ | Extender _) :: _, [] -> assert false
    | Internal _ :: _, ([] | [_]) -> assert false
    | Read :: jobs, _ ->
        let* res = Lwt_stream.get seq in
        match res with
        | None -> assert false
        | Some t ->
            match (t : Elem.t) with
            | End -> loop seq cntr (End :: jobs) ns
            | Cached i ->
                begin match Hashtbl.find tbl i with
                  | exception Not_found ->
                      Format.eprintf "not found cached %ld@." i;
                      assert false
                  | n -> loop seq cntr jobs (n :: ns)
                end
            | Value v ->
                let n = register cntr @@ new_leaf v in
                loop seq (incr cntr) jobs (n :: ns)
            | HashOnly nh ->
                let n = register_hashonly cntr nh in
                loop seq (incr cntr) jobs (n :: ns)
            | BudNone ->
                let n = register cntr @@ new_bud None in
                loop seq (incr cntr) jobs (n :: ns)
            | BudSome ->
                loop seq (incr cntr) (Read :: BudSome cntr :: jobs) ns
            | Extender seg ->
                loop seq (incr cntr) (Read :: Extender (cntr, seg) :: jobs) ns
            | Internal ->
                loop seq (incr cntr) (Read :: Read :: Internal cntr :: jobs) ns
  in
  let* n = loop seq 1l [Read; End] [] in
  Lwt.return (n, !has_hash)

let load (h : Hash.Hasher.t) { reader } =
  let read_nodes = ref 0 in
  let read_bytes = ref 0 in
  let report () =
    let kn = !read_nodes / 1_000 in
    let mb = !read_bytes / 1_000_000 in
    Lwt_fmt.eprintf "%s plebeia nodes read %sB@."
      (if kn = 0 then string_of_int !read_nodes else Printf.sprintf "%dK" kn)
      (if mb = 0 then string_of_int !read_bytes else Printf.sprintf "%dMi" mb)
  in
  let reader () =
    try
      let* bytes, t = reader (Elem.encoding h.config.hashfunc.length) in
      read_bytes := !read_bytes + bytes;
      incr read_nodes;
      let+ () =
        if !read_nodes mod 1_000_000 = 0 then report () else Lwt.return_unit
      in
      Some t
    with
    | _ -> Lwt.return_none
  in
  let seq : snapshot_lwt = Lwt_stream.from reader in
  let* res = load'_lwt h seq in
  let+ () = report () in
  res

let seq ~node n =
  (* XXX We will need check the memory usage.
     6GiB Memory required to dump level 2690000 of Tezos Mainnet *)
  let tbl = Hashtbl.create 1000 in
  let tbl_hash = Hashtbl.create 1000 in
  let cntr = ref 0l in
  let rev_written = ref [] in
  let write t = rev_written := t :: !rev_written in
  let f n =
    match node n with
    | `Hash nh ->
        (* Hash-cons'ing between Hash nodes.
           We do not hash-cons with non Hash nodes,
           since it would change the form of Merkle proof.
        *)
        begin match Hashtbl.find_opt tbl_hash nh with
          | Some i ->
              write (Cached i);
              `Return ()
          | None ->
              cntr := Int32.add !cntr 1l;
              Hashtbl.replace tbl_hash nh !cntr;
              write (HashOnly nh);
              `Return ()
        end
    | `View v ->
        let write_view v =
          match v with
          | Node_type.Bud (None, _, _) ->
              write BudNone
          | Bud (Some _, _, _) ->
              write BudSome
          | Internal (_, _, _, _) ->
              write Internal
          | Extender (seg, _, _, _) ->
              write (Extender seg)
          | Leaf (v, _, _) ->
              write (Value v)
        in
        (* if hash is available, try hashconsing *)
        match Node_type.hash_of_view v with
        | None ->
            write_view v;
            `Continue v
        | Some nh ->
            match Hashtbl.find_opt tbl nh with
            | Some i ->
                write (Cached i);
                `Return ()
            | None ->
                cntr := Int32.add !cntr 1l;
                Hashtbl.replace tbl nh !cntr;
                write_view v;
                `Continue v
  in
  let f, st = Traverse.Iter.interleaved f n in
  let st_ref = ref st in
  let written = ref [] in
  let rec seq () =
    match !written with
    | t::ts ->
        written := ts;
        Seq.Cons (t, seq)
    | [] ->
        match !rev_written with
        | _::_ ->
            written := List.rev !rev_written;
            rev_written := [];
            seq ()
        | [] ->
            match f !st_ref with
            | `Right _ -> Seq.Nil (* interleave finished *)
            | `Left st ->
                st_ref := st;
                seq ()
  in
  seq

let save_gen fd hash_size ~node n =
  let written_nodes = ref 0 in
  let written_bytes = ref 0 in
  let report () =
    let kn = !written_nodes / 1_000 in
    let mb = !written_bytes / 1_000_000 in
    Format.eprintf "%s plebeia nodes written %sB@."
      (if kn = 0 then string_of_int !written_nodes else Printf.sprintf "%dK" kn)
      (if mb = 0 then string_of_int !written_bytes else Printf.sprintf "%dMi" mb)
  in
  let seq = seq ~node n in
  let seq = Lwt_stream.of_seq seq in
  let+ () =
    Lwt_stream.iter_s (fun elem ->
        let bytes = Data_encoding.Binary.to_bytes_exn ~buffer_size:1024 (Elem.encoding hash_size) elem in
        let* written = Lwt_unix.write fd bytes 0 (Bytes.length bytes) in
        assert (Bytes.length bytes = written);
        written_bytes := !written_bytes + written;
        incr written_nodes;
        if !written_nodes mod 1_000_000 = 0 then report ();
        Lwt.return_unit) seq
  in
  report ()

let save fd ctxt n =
  let node = function
    | Node_type.Hash h ->
        `Hash h
    | n ->
        (* XXX We should have Node_storage.view_with_hash *)
        let v =
          match Node_storage.read_hash ctxt n with
          | `Hashed (_, n) -> Node_storage.view ctxt n
          | `Not_Hashed v -> v
        in
        `View v
  in
  save_gen fd ctxt.hasher.config.hashfunc.length ~node n

module Internal = struct
  module Elem = Elem
end
