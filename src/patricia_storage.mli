(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Append-only persistent storage based on binary Patricia tree

    This is a simple storage without versioning nor Merkle hashing.
*)

module Make
    (P : sig
       type key
       type value

       (** I/O. How Leaf and Internal nodes are written to the disk *)

       val parse : Storage.t -> Index.t -> [`Leaf of value
                                           | `Internal of Segment.t * Index.t * Segment.t * Index.t ]

       val write_leaf : Storage.t -> value -> (Index.t, Error.t) result
       val write_internal : Storage.t -> Segment.t * Index.t * Segment.t * Index.t -> (Index.t, Error.t) result

       val config : Storage.config

       val pp_value : Format.formatter -> value -> unit
       val equal_value : value -> value -> bool

       (** Encoding of key.

           For the correct behaviour, [segment_of_key k1] must not be
           a prefix of [segment_of_key k2] for any [k1] and [k2].
       *)
       val segment_of_key : key -> Segment.segment

       val key_of_segment : Segment.segment -> key option
     end) : sig
  type t

  val config : Storage.config

  type key = P.key
  type value = P.value

  (** Create a new empty storage *)
  val create : ?length:Index.t -> ?resize_step_bytes:int -> key:Storage.Writer_key.t -> string -> t Lwt.t

  (** If the file exists, load its latest. Otherwise, returns [None] *)
  val open_existing_for_read : ?key:Storage.Writer_key.t -> string -> (t, Error.t) result Lwt.t

  val open_for_write : ?resize_step_bytes:int -> config:Storage.config -> key:Storage.Writer_key.t -> string -> t Lwt.t

  val close : t -> (unit, Error.t) result Lwt.t

  val mode : t -> Storage.mode

  (** [gc fn] replaces the commit tree file [fn] by a new one only with
      the latest commit found in [fn].

      The original file is backed up as [fn ^ ".old"].  If the backup file
      already exists, it is overwritten.

      This GC is offline.  Never call this function when [fn] is being used.
  *)
  val offline_gc : ?resize_step_bytes:int -> string -> (unit, Error.t) result Lwt.t

  (** Write the modifications to [t] to the disk. *)
  val write : t -> (unit, Error.t) result

  val commit : t -> unit Lwt.t

  val flush : t -> unit Lwt.t

  val enable_process_sync : t -> Storage.Writer_key.t -> (unit, Error.t) result Lwt.t

  (** Update [t] to the latest commit tree on the disk.

      The reader process of the commit tree must call this function time to time
      to get the updates by the writer.
  *)
  val update_reader : t -> t Lwt.t

  (** Forget the on-memory cache of [t].  This fails and returns [None] if [t]
      has unsaved updates.
  *)
  val may_forget : t -> t option

  val mem : t -> key -> bool
  val find : t -> key -> value option

  (** If a binding already exists, it is overwritten. *)
  val add : t -> key -> value -> t

  val fold: (key -> value -> 'a -> 'a Lwt.t) -> t -> 'a -> 'a Lwt.t
  val iter: (key -> value -> unit Lwt.t) -> t -> unit Lwt.t

  (** test and debugging purpose *)

  module Internal : sig
    type storage = Storage.t

    type node = desc ref
    and desc =
      | View of view
      | Disk of Index.t
    and view =
      | Leaf of
          Index.t option ref (* where saved *)
          * value
      | Internal of
          Index.t option ref (* where saved *)
          * Segment.t * node (* left *)
          * Segment.t * node (* right *)

    val index : node -> Index.t option

    module View : sig
      type t = view
      val pp : Format.formatter -> t -> unit
      val equal : t -> t -> bool
      val parse : storage -> Index.t -> t
    end

    module Node : sig
      type t = node
      val view : storage -> t -> View.t
      val pp : Format.formatter -> t -> unit
      val equal : t -> t -> bool
      val write : storage -> t -> (unit, Error.t) result
      val may_forget : t -> t option
      val empty : t
      val is_empty : t -> bool
      val mk_leaf : value -> t
    end

    val get : storage -> Node.t -> Segment.segment -> Node.t option
    val set : storage -> Node.t -> Segment.segment -> Node.t -> Node.t

    val get_storage : t -> storage
    val get_node : t -> Node.t
  end
end
