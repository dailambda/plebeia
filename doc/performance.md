# Performance tuning parameters

`PLEBEIA_CELL_BYTES_OVERRIDE`
`PLEBEIA_HASH_BYTES_OVERRIDE`
`PLEBEIA_USE_REACHABLE_WORDS`

`PLEBEIA_SEGMENT_COUNT`
:  If set with a non empty string, `Segment` will keep stats of the memory usage of the segments.  This slows down the process a lot.

`PLEBEIA_KEEP_HASH_OVERRIDE`
:  If set with a non empty string, the hashes of the nodes are kept in memory.  This may cost memory (28 bytes per node by default) but may reduce the time for the reloading them.

## Compile time switches

(Not yet) Invariant check
:  Currently the node invariant check is disabled.  See `Node_type.Invariant_view.check`.  We must have a compile time switch to enable it.

