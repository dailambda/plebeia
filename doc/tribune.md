# Tribune

* B-Tree?
* B+?

## Current statistics of Plebeia tree

* The maximum segment : 72 bytes
* 261M (`261_459_466`) leaves (including shared) at level `1_047_836` of Tezos Mainnet

## Shallow Plebeia

Sparse but of a large order, such as 1024.

## 


* B-tree of order 1024.  If we assume the worst case, about 512 sub-nodes for each node, depth `d` of such a tree should have about `512^d` nodes.  At depth 4, it has already 68B nodes.
* B-tree of order 256.  4G nodes with depth 4.
* B-tree of order 128.  268M with depth 4.  34B with depth 5.

Let's fix the order: 256

B-tree node size

* Nodes for 261M keys: about `1_000_000` nodes at most
* pointers: 256 * pointer size
* names: 72 * 256 (the worst) bytes

* If we assume pointer size = 32bits (4bytes), then we can have 4294967296 possible pointers.
* The worst node size is 256 * 4 + 72 * 256 = 19456 bytes.  With 1M nodes, about 20GB.  This is too big.
* If we cleverly compress the names 256 * 4 + 72 + 128 = 10240.  Still 10GB.





