# Plebeia crash recovery

This document explains how to recover a crash of Plebeia process.

## Header value

A header carries the following information:

* `last_next_index`: the cell index for the next allocation
* `last_root_index`: the cell index of the last valid data

### Double write

2 copies of a header value are written with its Blake2B 24byte hash.

If the system crashes during writing the 2 coipes of a header value,
at least one value is valid: the values match with the hash.

### Lock

The pair of the header value on the disk is read+write protected by `flock()`.

## Header for disk and process syncs

Plebeia carries 2 kinds of headers:

Header for disk sync
:  This is for crasy recovery.  The data before its `last_root_index` are 
   assured valid even after a crash.

Header for process sync
:  This is for inter process synchronization.

### Header for disk sync

This is for the crasy recovery.  This header value must be written only 
after a call of `msync()` to make sure that the all the data pointed 
by the header value are persisted on the disk.

Too frequent calls of `msync()` slows down the system.  Therefore, the header
for the disk sync should NOT be written for each commit.  By default, 
the header for the disk sync is only written every 20 seconds.

Due to this interval of the disk sync header update, the reader processes will 
not get the latest commits from the header for the disk sync.  For faster 
synchrnoization, readers which require the updates from the writer immediately
must use the header for the process sync.

### Header for process sync

This is for the process sychronization.  The writer process writes this header 
for each commit, so that the reader processes can detect the latest commit 
by the writer process.

This process sync header is NOT written without calling `msync()`.  Therefore, 
if the OS crashes, the data pointed by the process sync header may be broken,
upto the data pointed by the disk sync header.

## The writer process

* The writer updates the process sync header for each commit, but without calling `msync`.
* At a commit, if the disk sycn header is not updated for long (like 30 seconds), if 
