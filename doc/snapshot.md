# Plebeia snapshot format

This document explains the format of Plebiea snapshot file.

The snapshot file is also used to stream Plebeia Merkle proofs.

## Value

A *value* is a binary data from 0 to 1,073,741,823 bytes.  It is used at Leaf node to carry its data.

Encoding:

```
      <-   4 bytes  -> <--- len bytes --->
     +----------------+-------------------+
     |len (big endian)|  value as string  |
     +----------------+-------------------+
```

`len`
:  The first 4 bytes is the length of the value part in bytes in big-endian `uint32`.

`value`
:  The contents of the value (len bytes) follows after `len`.

Example:

* ``"abc"`` : `00000003616263`
* `""` : `00000000`


## Segment

A *segment* is a list of side bits, `0` for `Left` and `1` for `Right`.

The maximum length of segments is `Segment.max_length = 1815`.

Encoding:

```
       <1B> <---- len bytes ------------------>
      +----+---------------------------+-------+
      |len |  segment bits (len bits)  |10{0,7}|
      +--------------------------------+-------+
```

`len`
:  The first byte of the encoding is the length of the rest of encoding in `uint8`.

Segment bits
:  The segment bits follows.

Trailer
:  The segment bits are followed by a trailer, 1 of `1` then 0 to 7 of `0`s to make the whole bits a multiply of 8.

Examples:

* Empty segment :  `0180`
* `LRLRLRLRL` :  `025540`
* `LRLRLRLR` :  `025580`
* `RRRRRRRRRRRRRRRRRRRR` :  `03fffff8`

## Hash prefix

A *hash prefix* is a fixed sized bytes.  Its size is determined by the configuration of Plebeia, such as 28 and 32.

Encoding:

```
   <-  fixed length   ->
  +---------------------+
  |     hash prefix     |
  +---------------------+
```

## Hash

A *hash* is a variable length hash of Plebeia, a pair of a hash prefix and a segment.

Encoding:

```
   <-  fixed length   -> < variable >
  +---------------------+------------+
  |     hash prefix     |  segment   |
  +---------------------+------------+
```

Example:

* `(0123456789abcdef0123456789abcdef0123456789abcdef01234567, LRLR)` : `0123456789abcdef0123456789abcdef0123456789abcdef01234567 0158`
* `(0123456789abcdef0123456789abcdef0123456789abcdef01234567, empty_segment)` : `0123456789abcdef0123456789abcdef0123456789abcdef01234567 0180`


## Snapshot elements

There are 8 snapshot elements:

* `End`
* `BudNone`
* `BudSome`
* `Internal`
* `Extender of segment`
* `Value of value`
* `Cached of int32`
* `HashOnly of hash`

### `End`

End of a snapshot.  `End` appears at the end of a snapshot element stream.

Encoding:

```
          <1B>
         +----+
         | 00 |
         +----+
```

### `BudNone`

Add a `Bud` without child node.

Encoding:

```
          <1B>
         +----+
         | 01 |
         +----+
```

### `BudSome`

Add a `Bud` with a child node.  The encoding of the child node follows after this element.

Encoding:

```
          <1B>
         +----+  +--- ...
         | 02 |  | child node encoding ...
         +----+  +--- ...
```

### `Internal`

Add an `Internal` node.  The encoding of the child nodes follows after this element in the order of the left child then the right child.

Encoding:

```
          <1B>
         +----+ +---...          +---...
         | 03 | | left child ... | right child
         +----+ +---...          +---...
```

### `Extender`

Add an `Extender` node with a segment.  The encoding of its child node follows:

Encoding:

```
          <1B> < variable >
         +----+------------+ +---...
         | 04 |  segment   | | child node ...
         +----+------------+ +---...
```

### `Value`

Add a `Leaf` node with a value.

Encoding:

```
          <1B> < variable >
         +----+------------+
         | 05 |    value   |
         +----+------------+
```

### `Cached`

Element for hash-consing.

`Cached i` adds `i`-th non `Cached` node (`uint32`, starts from `1`) in the snapshot element stream.

Encoding:

```
          <1B> <--- 4 bytes ---->
         +----+------------------+
         | 06 |index (big endian)|
         +----+------------------+
```

### `HashOnly`

Add a `Hash h` node with a hash `h`.  `Hash` nodes are only used for Merkle proofs.

Encoding:

```
          <1B> <-  variable  ->
         +----+----------------+
         | 07 |      hash      |
         +----+----------------+
```

## Snapshot

A snapshot is a stream of snapshot elements, ended by an `End`.

Encoding:

```
        +---------+---...       +-----+
        | elem #1 | elem #2 ... | End |
        +---------+---...       +-----+
```

The snapshot element stream is built by traversing Plebeia tree by depth-first left-to-right manner.  For example, the snapshot of the following Plebeia tree

```
             [/1]             /: Bud
              |               I: Internal
             [I2]             L: Leaf
             /  \             E: Extender
          [I3]  [E6]          H: Hash
          /  \    \ seg
        [L4] [H5] [L7]
```

has the snapshot stream as below:

```
BudSome;  (* for [/1] *)
Internal; (* for [I2] *)
Internal; (* for [I3] *)
Value;    (* for [L4] *)
HashOnly; (* for [H5] *)
Extender with seg; (* for [E6] *)
Value;    (* for [L7] *)
End
```

### Hash-consing

Non `Cached` snapshot elements in a snapshot stream are numbered from `1`, `2`...  These numbers can be used by `Cached i` for hash-consing.  `Cached i` inserts the node created by the snapshot element numbrered `i`.

For example, the following snapshot element stream:

```
BudSome;  (* for [/1] *)
Internal; (* for [I2] *)
Internal; (* for [I3] *)
Value;    (* for [L4] *)
HashOnly; (* for [H5] *)
Extender with seg; (* for [E6] *)
Cached 3; (* copy of the Internal node of [i3] *)
End
```

loads the following Plebeia tree:

```
             [/1]             /: Bud
              |               I: Internal
             [I2]             L: Leaf
             /  \             E: Extender
          [I3]  [E6]          H: Hash
          /  \    \ seg
        [L4] [H5] [I3]
                  /  \
                [L4] [H5]
```

Note that the node `E6` has a copy of `I3`.  Though drawn seprately in the figure, these 2 `I3` trees are loaded to the identical tree in memory.

### Example

A snapshot of the following Plebeia tree:

```
             [/1]             /: Bud
              |               I: Internal
             [I2]             L: Leaf
             /  \             E: Extender
          [I3]  [E6]          H: Hash
          /  \    \ LRLR
        [L4] [H5] [I3]
        "hi" hash /  \
                [L4] [H5]
                "hi" hash 
                
    hash : (0123456789abcdef0123456789abcdef0123456789abcdef01234567, empty_segment)
```

can be as follows:

```
BudSome;    (* 02 *)
Internal;   (* 03 *)
Internal;   (* 03 *)
Value "hi"; (* 05 00000002 6869 *)
HashOnly (0123456789abcdef0123456789abcdef0123456789abcdef01234567, empty_segment);
            (* 07 0123456789abcdef0123456789abcdef0123456789abcdef01234567 01 80 *)
Extender LRLR; (* 04 0158 *)
Cached 3;   (* 06 00000003 *)
End         (* 00 *)
```

The concatenation of the element encodings is:

```
02
03
03
05 00000002 6869
07 0123456789abcdef0123456789abcdef0123456789abcdef01234567 01 80
04 0158
06 00000003
00
```
