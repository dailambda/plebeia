# パラメータ探索

いずれも mainnet で 5cycles = 20480 level を reconstruct して plebeia.context の消費ノード数を計測

探索対象は
* Hashcons の max_leaf_size
* Hashcons縮小の threshold
* Node_cache縮小の threshold

## キャッシュなしvs推奨値

推奨値
* Hashcons 10MB
* Hashcons max_leaf_size = 64
* Node_cache 50MB

| キャッシュ有無 | #Node |
|-|-|
| 無 | 63161400 |
| 有 | 55433649 |

## 各パラメータの実験結果
### max_leaf_size

|Hashcons max_leaf_size|#Node|
|--------|--------|
|    32|60961936|
|    64|53780327|
|   128|53780305|
|   256|53557860|
|  1024|53552542|
|  4096|53543039|
|  8192|53535468|
![](./LWATjpq.png)

### Hashcons


| Hashcons threshold (MB) |#Node|
|------|--------|
|  0.01|56906617|
|  0.05|55928603|
|  0.1 |55745941|
|  0.5 |55121415|
|  1.0 |54779532|
|  5.0 |53968467|
| 10.0 |53743831|
| 50.0 |53495751|
|100.0 |53429359|
|500.0 |53391097|


![](./kwo1osj.png)

### Node_cache

|Node_cache threshold (MB)|   #Node|
|  -----|--------|
|   10.0|61527182|
|   50.0|54918777|
|  100.0|54038383|
|  500.0|53641244|
| 1000.0|53552542|
| 5000.0|53376374|
|10000.0|53329602|
![](./56kpR83.png)
