open Plebeia
module F = Fs
open F.Op_lwt.Syntax

type Error.t += Not_found_commit of string | Unknown_command

let _ =
  (* Error.String の printer が register されていない *)
  Error.register_printer (function
    | Error.String str -> Some str
    | Not_found_commit str ->
        Some (Format.asprintf "Commit %s is not found" str)
    | Unknown_command -> Some "Unknown Command"
    | _ -> None)

let name_of_string s =
  let ss = String.split_on_char '/' s in
  List.filter (function "" -> false | _ -> true) ss

(* Write data read from the file pointed by [filename] to plebeia tree *)
let add filename : unit F.Op_lwt.t =
  let path = name_of_string filename in
  let stream = Lwt_io.chars_of_file filename in
  let* data = F.Op_lwt.lift_lwt @@ Lwt_stream.to_string stream in
  F.Op_lwt.write path (Value.of_string data)

(* Delete data or directory pointed by [filename] in plebeia tree*)
let rm filename =
  let path = name_of_string filename in
  let* _b = F.Op_lwt.rm ~recursive:true path in
  assert _b;
  F.Op_lwt.return ()

(* Get and print data pointed by [filename] in plebeia tree*)
let cat filename : unit F.Op_lwt.t =
  let path = name_of_string filename in
  let* v = F.Op_lwt.read path in
  let* () = F.Op_lwt.lift_lwt @@ Log.lwt_notice "%a" Value.pp v in
  F.Op_lwt.return ()

(* Print file and directory names in [path] in plebeia tree*)
let ls path =
  let path = name_of_string path in
  let* lst = F.Op_lwt.ls path in
  let plst = List.map fst lst in
  let* () =
    F.Op_lwt.lift_lwt
    @@ Log.lwt_notice "%a"
         (Format.pp_print_list ~pp_sep:Format.pp_print_newline Fs_types.Name.pp)
         plst
  in
  F.Op_lwt.return ()

(* Make a commit (Create a checkpoint to current tree) *)
let commit vc parent =
  let* ((_, _) as r) = F.Vc.commit vc ~parent ~hash_override:None in
  F.Op_lwt.return r

(* Checkout a commit *)
let checkout vc hash =
  let open Result_lwt.Syntax in
  let*= cop = F.Vc.checkout vc hash in
  let* r =
    match cop with
    | Some cur -> Lwt.return_ok cur
    | None -> Lwt.return_error (Not_found_commit (Commit_hash.to_hex_string hash))
  in
  Lwt.return_ok r

(* Get parent hashes of [hash] *)
let log ?(n = 10) vc hash =
  let open Result_lwt.Syntax in
  let db = F.Vc.commit_db vc in
  let lst =
    let*? c =
      Commit_db.find db hash |> function
      | Some a -> Ok a
      | None -> Error (Not_found_commit (Commit_hash.to_hex_string hash))
    in
    let rec loop c n l =
      if n <= 0 then Result.return l
      else
        let*? par =
          Result.map_error
            (function
              | `Not_found -> Not_found_commit (Commit_hash.to_hex_string hash))
            (Commit_db.parent db c)
        in
        match par with
        | Some c -> loop c (n - 1) (c :: l)
        | None -> Result.return l
    in
    let*? lst = loop c n [ c ] in
    let lst = List.map (fun e -> e.Commit.hash) lst in
    Result.return lst
  in
  let* lst = Lwt.return lst in
  let lst = List.rev lst in
  let*= () =
    Log.lwt_notice "%a"
      (Format.pp_print_list
         ~pp_sep:(fun ppf () -> Format.fprintf ppf "\n")
         Commit_hash.pp)
      lst
  in
  Result_lwt.return ()

type t = { cur : F.cursor; hash : Commit_hash.t option }

let ctxt_fn = ".plebeia-git"

(* A Plebeia-fs example in Interpreter-style *)
let () =
  let open Result_lwt.Syntax in
  let init =
    let conv = Fs_nameenc.bits8 in
    let* vc = F.Vc.open_for_write Context.default_config conv ctxt_fn in

    (* Move to the latest commmit *)
    let db = F.Vc.commit_db vc in
    let c = Commit_db.read_the_latest db in
    let ch = Option.map (fun c -> c.Commit.hash) c in
    let*= cur =
      match ch with
      | Some ch -> Lwt.map (fun x -> snd @@ Option.get x) @@ F.Vc.checkout vc ch
      | None -> Lwt.return @@ F.empty (F.Vc.context vc) conv
    in
    Result_lwt.return (vc, cur, ch)
  in
  let vc, cur, hash = Result.get_ok @@ Lwt_main.run init in

  let rec loop t =
    let cmd ({ cur; hash } as state) : (t * bool, Error.t) Result_lwt.t =
      let*= () = Lwt_io.eprint "> " in
      let*= op = Lwt_io.read_line Lwt_io.stdin in
      let op = Str.split (Str.regexp " ") op in
      match op with
      | [ "exit" ] -> Lwt.return_ok (state, false)
      | [ "add"; fn ] ->
          let+ cur, () = F.Op_lwt.run cur @@ add fn in
          ({ state with cur }, true)
      | [ "rm"; fn ] ->
          let+ cur, () = F.Op_lwt.run cur @@ rm fn in
          ({ state with cur }, true)
      | [ "cat"; fn ] ->
          let+ cur, () = F.Op_lwt.run cur @@ cat fn in
          ({ state with cur }, true)
      | [ "ls" ] ->
          let+ cur, () = F.Op_lwt.run cur @@ ls "/" in
          ({ state with cur }, true)
      | [ "ls"; path ] ->
          let+ cur, () = F.Op_lwt.run cur @@ ls path in
          ({ state with cur }, true)
      | [ "commit" ] ->
          let+ cur, (_hash, cmt) = F.Op_lwt.run cur @@ commit vc hash in
          let parent = Some cmt.hash in
          ({ cur; hash = parent }, true)
      | [ "checkout"; hash ] ->
          let hash = Commit_hash.of_hex_string hash in
          let+ _, cur = checkout vc hash in
          ({ cur; hash = Some hash }, true)
      | [ "log" ] ->
          let hash =
            match hash with None -> Commit_hash.zero | Some hash -> hash
          in
          let+ () = log vc hash in
          (state, true)
      | [ "log"; hash ] ->
          let hash = Commit_hash.of_hex_string hash in
          let+ () = log vc hash in
          (state, true)
      | _ -> Lwt.return_error Unknown_command
    in
    let*= t, b =
      Lwt.bind (cmd t) (function
        | Ok e -> Lwt.return e
        | Error err ->
            let*= () = Log.lwt_notice "%a" Error.pp err in
            Lwt.return (t, true))
    in
    if b then loop t else Lwt.return_unit
  in
  Lwt_main.run @@ loop { cur; hash }
