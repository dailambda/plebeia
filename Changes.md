(**) : Big incompatibile change
(*) : Incompatible change
(-) : Updates

# 2.3.0

* Use of ephemeron to cache the map from node hashes to indices
- Bug fix of Fs_tree.Op.set when path is empty
- Storage: sets the default_resize_step_bytes by 512MB
- CI is now manual
- Relaxed context compatibility at Vc.commit

# 2.2.0

* File opening APIs now takes a config record, instead of optional arguments of the parameters.
- Pagination count support.
  - If pagination count is enabled, each cell is extended by 4 bytes to carry a counter, indicates
    how many [Leaf]s and [Bud]s are found under each node.
  - [Fs.Op_lwt.ls2] lists directory with pagination parameters using the counter.
- Introduced simpler data storage for leaf format 33 <= size < 4GB
- Fixed msync() call at flushing: now 1 call of msync() synchronizes the entire file, which is
  faster than 2 calls of it for the header and the updated tail.
- Large refactoring of Fs* modules
  - Segment encoding of file names are now in Fs_nameenc.  Fs_nameenc.Compressed now saves its known names
    to a text file.

# 2.1.0

** [commit] is now split into 2: [commit] and [flush].
  - New [commit] no longer assures crash recovery of the past commits.
    [flush] must be called explicitly to make the past commits persistent.
- [enable_process_sync] for faster writer-reader communcation using shared memory.
- Introduced Fs_impl.Base to share the error constructors properly between Fs and Fs_tree
* Renamed Segment.unsafe_of_encoding by Segment.unsafe_of_bits
* Renamed Segment.to_encoding by Segment.to_bits

# 2.0.2

- Fixed synchronization bug introduced in 2.0.0

# 2.0.1

- Relaxed OPAM version constraints

# 2.0.0

- Major restructuring and speed-ups

# 1.1.0

## Module renaming

`Hash_prefix`
:    `Hash` is renamed as `Hash_prefix`, since `Hash.t` was not the hash of the nodes but the 28 byte prefix.

## New modules

`Snapshot`
:    Making a snapshot

`Data_encoding_tools`
:    Pretty printer using encoding.  Reader.

## Deprecation

Old modules `Search`, `Ediff` and `Traverse` are removed.
